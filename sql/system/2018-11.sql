
--------------------------------------
--用户表 xinxin.deng 2018/12/23 16:05
--------------------------------------
CREATE TABLE `order_manager` (
  `cms_id` int(11) NOT NULL AUTO_INCREMENT,
  `cms_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cms_password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cms_state` int(4) NOT NULL DEFAULT '0' COMMENT '启用禁用状态,0启用，1禁用',
  `cms_role_id` tinyint(4) DEFAULT NULL COMMENT '角色ID',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `cms_login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '登录时间',
  `cms_login_count` int(4) DEFAULT '0' COMMENT '登陆次数',
  `cms_type` int(4) DEFAULT NULL,
  `cms_login_fail_numbers` tinyint(3) NOT NULL DEFAULT '0' COMMENT '登录账号失败次数',
  `cms_user_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '登录此账号的ip',
  `cms_password_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '密码修改时间',
  `cms_telephone` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '手机号',
  `cms_email` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `cms_token` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '用户登录时生成的token',
  `cms_token_expire` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '用户登录的token有效时间',
  `cms_desc` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述信息',
  `cms_sex` tinyint(1) DEFAULT '1' COMMENT '性别,1未知,2男,3女',
  `cms_country` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '国家',
  `cms_address` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '地址',
  `cms_establish_date` date DEFAULT '0000-00-00' COMMENT '成立时间',
  `cms_main_product` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主营产品',
  `cms_sale_channels` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '销售渠道',
  `cms_bank_info` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '对公银行账号',
  `cms_courier_info` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '快递发货地址、电话、收件人',
  `cms_courier_big_info` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '大件发货地址、电话、收件人',
  `cms_company_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '企业名称(允许个人)',
  `cms_head_img` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '头像',
  `cms_user_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '用户余额',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1246 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='管理员表';

----------------------------
-- 通用项 （必填）
----------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_wx_num` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '微信号';
----------------------------
-- 通用项 （选填）
----------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_qq` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'QQ号';
----------------------------
-- 样板师、样衣师特殊项(必填)
----------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_use_software` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '使用软件,0其他,1ET,2至尊宝,3BKR';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_good_at_style` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '擅长风格,0厚型,1薄型';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_printer_equipment` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '打印设备,0无,1有';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_plateing_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '制板费';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_pushing_plate_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '推版费';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_is_full_time` tinyint(1) DEFAULT '0' COMMENT '是否专职,0否,1是';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_have_workroom` tinyint(1) DEFAULT '0' COMMENT '有无工作室,0否,1是';

----------------------------
-- 样衣师特殊项(必填)
----------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_good_at_material` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '擅长布料,0针织,1梭织';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_own_equipment` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '自有设备,0平缝机,1锁边机,2平头锁眼机,3圆头锁眼机,4钉扣机,5套结机';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_good_at_model` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '擅长款式,0童装,1外套,2女时装,3裤子,4棉衣';

----------------------------
-- 供应商特殊项(必填)
----------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_nature` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '供应商性质,0贸易商,1生产厂';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_taxation` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '税务情况,0无发票,1普通发票,2增值税发票';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_retriever` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
 COMMENT '品种,0纽扣,1拉链,2缝纫线,3罗口,4粘合衬,5皮标,6金属牌,7花边,8绳带,9喷咬棉,10羽绒/填充物,11唛头,12针织/梭织,13素色/提花/印花,14棉/麻/毛/丝/皮革/混纺类/化纤类';

----------------------------
-- 生产商特殊项(必填)
----------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_product_nature` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '生产性质,0针织,1梭织,2印花,3绣花,4水洗';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_people_num` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '车间人数,0 ~10,1 11~30,2 31~50,3 51~100,4 101~200,5 201~';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_capacity` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '产能,0 ~10~20~烘箱数,1 ~1000~2000~4000~件每天,2 ~2000~5000~件每天';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_production_chain` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '生产链,0裁床,1车缝,2后整,3技术科';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_goods_img` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '产品图';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_factor_img` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '工厂图';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_iso_certificate` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT 'ISO证书';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_human_certificate` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '人权证书';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_business_license_img` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '营业执照';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_taxation_certificate` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '税务登记证';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_good_at_goods` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '擅长产品,内衣/外套,童装/大人,薄料/厚料';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_equipment_state` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '设备状况，0平车（型号）,1拷边（型号）,2平头锁眼机,3圆头锁眼机,4套结机,5钉扣机,6四合扣机,7橡筋机,8烫台,9裁床,10其他专业设备（衬衫全套设备/西服全套设备/封胶设备/充绒机）';