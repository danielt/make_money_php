
/* Alter table in target */
ALTER TABLE `system_menu` 
	ADD COLUMN `cms_type` tinyint(3) unsigned   NULL DEFAULT 0 COMMENT '0 目录 |  1 页面' after `cms_state` , 
	CHANGE `cms_create_time` `cms_create_time` datetime   NULL COMMENT '创建时间' after `cms_type` , 
	CHANGE `cms_modify_time` `cms_modify_time` datetime   NULL COMMENT '修改时间' after `cms_create_time` ;

/* Create table in target */
CREATE TABLE `system_menu_button`(
	`cms_id` int(10) unsigned NOT NULL  auto_increment COMMENT 'UUID' , 
	`cms_menu_id` int(10) unsigned NULL  COMMENT '目录ID' , 
	`cms_mark` varchar(32) COLLATE utf8_unicode_ci NULL  DEFAULT '' COMMENT '标记' , 
	`cms_name` varchar(64) COLLATE utf8_unicode_ci NULL  , 
	`cms_function` varchar(32) COLLATE utf8_unicode_ci NULL  DEFAULT '' COMMENT '方法' , 
	`cms_order` float(6,3) unsigned NULL  COMMENT '权重' , 
	`cms_url` varchar(512) COLLATE utf8_unicode_ci NULL  DEFAULT '' COMMENT '地址' , 
	`cms_state` tinyint(4) unsigned NULL  COMMENT '状态 0 启用 | 1 禁用' , 
	`cms_is_default` tinyint(3) unsigned NULL  DEFAULT 1 COMMENT '0 默认入口 | 1 非默认入口' , 
	`cms_create_time` datetime NULL  COMMENT '创建时间' , 
	`cms_modify_time` datetime NULL  COMMENT '修改时间' , 
	PRIMARY KEY (`cms_id`) , 
	UNIQUE KEY `idx_unique`(`cms_menu_id`,`cms_mark`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_unicode_ci';


/* Create table in target */
CREATE TABLE `system_menu_function`(
	`cms_id` int(10) unsigned NOT NULL  COMMENT 'UUID' , 
	`cms_function` varchar(32) COLLATE utf8_unicode_ci NULL  DEFAULT '' COMMENT '按钮方法' , 
	`cms_name` varchar(128) COLLATE utf8_unicode_ci NULL  DEFAULT '' COMMENT '按钮名称' , 
	`cms_icon` varchar(128) COLLATE utf8_unicode_ci NULL  DEFAULT '' COMMENT '按钮图标' , 
	`cms_state` tinyint(3) unsigned NULL  DEFAULT 0 COMMENT '状态 0 启用 | 1 禁用' , 
	`cms_order` float(6,3) unsigned NULL  COMMENT '权重' , 
	`cms_create_time` datetime NULL  COMMENT '创建时间' , 
	`cms_modify_time` datetime NULL  COMMENT '修改时间' , 
	PRIMARY KEY (`cms_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_unicode_ci';




/* Alter table in target */
ALTER TABLE `system_role` 
	ADD COLUMN `cms_name` varchar(255)  COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名称' after `cms_id` , 
	ADD COLUMN `cms_state` tinyint(3) unsigned   NULL DEFAULT 0 COMMENT '状态 0 启用 | 1 禁用 ' after `cms_name` , 
	CHANGE `cms_desc` `cms_desc` varchar(512)  COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '角色描述' after `cms_state` , 
	CHANGE `cms_create_time` `cms_create_time` datetime   NOT NULL COMMENT '创建时间' after `cms_desc` , 
	CHANGE `cms_modify_time` `cms_modify_time` datetime   NOT NULL COMMENT '修改时间' after `cms_create_time` , 
	DROP COLUMN `cms_project_id` , 
	DROP COLUMN `cms_role_name` , 
	DROP COLUMN `cms_roles` ;

/* Create table in target */
CREATE TABLE `system_role_bind`(
	`cms_id` int(10) unsigned NOT NULL  auto_increment COMMENT 'GUID' , 
	`cms_role_id` int(10) unsigned NOT NULL  COMMENT '角色ID' , 
	`cms_button_id` int(10) unsigned NOT NULL  COMMENT '按钮ID' , 
	`cms_state` tinyint(3) unsigned NULL  DEFAULT 0 COMMENT '状态  0 启用  |  1 禁用' , 
	`cms_create_time` datetime NULL  COMMENT '创建时间' , 
	`cms_modify_time` datetime NULL  COMMENT '修改时间' , 
	PRIMARY KEY (`cms_id`) , 
	UNIQUE KEY `id_unique`(`cms_role_id`,`cms_button_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_unicode_ci';
