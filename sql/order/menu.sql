-- ------------------------------------------------------------------------------------
-- Table insert 角色数据 add by zhiyong.luo 2018-12-27
-- ------------------------------------------------------------------------------------
INSERT INTO `system_project` (`cms_id`, `cms_name`, `cms_mark`, `cms_mobilephone_number`, `cms_telphone_number`, `cms_remark`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(1,	'订货商系统',	'order',	'',	'',	'',	0.000,	1,	NULL,	'2018-12-27 10:50:21'),
(2,	'平台管理系统',	'cms',	'',	'',	'',	0.000,	1,	NULL,	'2018-12-27 10:50:21'),
(3,	'生产商系统',	'product',	'',	'',	'',	0.000,	1,	NULL,	'2018-12-27 10:50:21'),
(4,	'供应商系统',	'supplier',	'',	'',	'',	0.000,	1,	NULL,	'2018-12-27 10:50:21'),
(5,	'样板师系统',	'pattern',	'',	'',	'',	0.000,	1,	NULL,	'2018-12-27 10:50:21'),
(6,	'样衣师系统',	'sample',	'',	'',	'',	0.000,	1,	NULL,	'2018-12-27 10:50:21');
-- ------------------------------------------------------------------------------------
-- Table insert 角色数据 add by zhiyong.luo 2018-12-27
-- ------------------------------------------------------------------------------------
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(1,	1,	'用户中心',	'order/con_manager/c_manager/index',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(2,	1,	'订购',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(3,	1,	'我的订单',	'order/con_client/c_client_order/index',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(4,	1,	'财务信息',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(5,	1,	'物流查询',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(6,	2,	'系统管理',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(7,	2,	'用户管理',	'order/con_manager/c_manager/user_list',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(8,	2,	'生产商管理',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(9,	2,	'订单管理',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(10,	2,	'财务管理',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(11,	2,	'系统设置',	'',	2,	6,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(12,	2,	'管理员管理',	'order/con_manager/c_manager/user_list',	2,	6,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(13,	2,	'消息管理',	'',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(14,	3,	'用户中心',	'order/con_manager/c_manager/index',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(15,	3,	'信息发布',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(16,	3,	'订单管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(17,	3,	'财务管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(18,	4,	'用户中心',	'order/con_manager/c_manager/index',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(19,	4,	'产品管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(20,	4,	'订单管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(21,	4,	'财务管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(22,	5,	'用户中心',	'order/con_manager/c_manager/index',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(23,	5,	'订单管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(24,	6,	'用户中心',	'order/con_manager/c_manager/index',	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(25,	6,	'订单管理',	NULL,	1,	0,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(26,	4,	'面辅料管理',	'order/fabirc/c_fabirc_manager/fabirc_manager_list',	2,	19,	0.000,	1,	NULL,	'2018-12-27 10:54:39'),
(27,	4,	'面辅料属性管理',	'order/fabirc/c_fabirc_manager/fabirc_attribute_list',	2,	19,	0.000,	1,	NULL,	'2018-12-27 10:54:39');

-- ------------------------------------------------------------------------------------
-- Table insert 支付、积分管理 add by kan.yang 2019-01-04
-- ------------------------------------------------------------------------------------
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(28,1,'支付管理','',1,0,0.000,1,NULL,'2019-01-04 10:54:39'),
(29,1,'商户管理','pay/pay_order/c_channel/list_partner',2,28,0.000,1,NULL,'2019-01-04 10:54:39'),
(30,1,'订单管理','pay/pay_order/c_pay_order/list_order',2,28,0.000,1,NULL,'2019-01-04 10:54:39'),
(31,1,'充值订单管理','pay/pay_order/c_recharge/list_recharge',2,28,0.000,1,NULL,'2019-01-04 10:54:39'),
(32,1,'积分管理','',1,0,0.000,1,NULL,'2019-01-04 10:54:39'),
(33,1,'积分列表','pay/integral/c_integral/list_integral_detail',2,32,0.000,1,NULL,'2019-01-04 10:54:39');
-- ------------------------------------------------------------------------------------
-- Table insert 面辅料产品管理 add by zhiyong.luo 2019-01-04
-- ------------------------------------------------------------------------------------
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(34,	4,	'面辅料类型管理',	'order/fabirc/c_fabirc_manager/fabirc_type_list',	2,	19,	4.000,	1,	NULL,	'2018-12-27 10:54:39'),
(35,	4,	'面辅料产品管理',	'order/fabirc/c_fabirc_product/fabirc_product_list',	2,	19,	0.000,	1,	'2019-01-03 12:00:00',	'2019-01-03 12:00:00');

-- ------------------------------------------------------------------------------------
-- Table insert system_menu
-- ------------------------------------------------------------------------------------
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES 
(36, 2, '产品管理', NULL, '1', '0', '0.000', '1', '2019-01-05 17:21:07', '2019-01-05 17:21:11');

UPDATE `system_menu` SET cms_project_id=2,cms_parent_id=36 WHERE cms_id=26;
UPDATE `system_menu` SET cms_project_id=2,cms_parent_id=36 WHERE cms_id=27;
UPDATE `system_menu` SET cms_project_id=2,cms_parent_id=36 WHERE cms_id=34;


INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('37', '1', '生产订单', 'order/con_client/c_client_order/index', '2', '3', '0.000', '1', '2019-01-02 16:00:59', '2019-01-02 16:01:02');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('38', '1', '小样订单', NULL, '2', '3', '0.000', '1', '2019-01-02 16:01:46', '2019-01-02 16:01:49');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('39', '1', '样衣订单', NULL, '2', '3', '0.000', '1', '2019-01-02 16:02:22', '2019-01-02 16:02:25');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('40', '1', '样板订单', NULL, '2', '3', '0.000', '1', '2019-01-02 16:02:48', '2019-01-02 16:02:51');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('41', '2', '样版', 'order/model_dress/c_model_dress/index', '1', '0', '0.000', '1', '2019-01-02 16:34:23', '2019-01-02 16:34:34');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('42', '2', '样衣', 'order/simple_dress/c_simple_dress/index', '1', '0', '0.000', '1', '2019-01-02 16:35:54', '2019-01-02 16:35:56');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('43', '2', '客户订单', NULL, '2', '9', '0.000', '1', '2019-01-02 16:36:39', '2019-01-02 16:36:42');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('44', '2', '供应商订单', NULL, '2', '9', '0.000', '1', '2019-01-02 16:36:57', '2019-01-02 16:37:00');
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES ('45', '2', '生产订单', NULL, '2', '9', '0.000', '1', '2019-01-02 16:37:14', '2019-01-02 16:37:16');

INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(46,	2,	'服装部件分类',	'order/clothing_parts/c_clothing_parts/parts_type_list',	2,	36,	6.000,	1,	'2019-01-02 16:37:14',	'2019-01-02 16:37:16'),
(47,	2,	'服装部件',	'order/clothing_parts/c_clothing_parts/parts_list',	2,	36,	5.000,	1,	'2019-01-02 16:37:14',	'2019-01-06 21:54:06');


UPDATE `system_menu` SET cms_state=0 WHERE cms_id in ('1','14','18','22','24');

update `system_menu` SET cms_url='order/con_manager/c_manager/user_list?role_id=1,3,4,5,6&' WHERE cms_id in ('7');
update `system_menu` SET cms_url='order/con_manager/c_manager/user_list?role_id=2&' WHERE cms_id in ('12');

-- ------------------------------------------------------------------------------------
-- Table insert 支付、积分、财务 add by kan.yang 2019-01-12
-- ------------------------------------------------------------------------------------
-- 删除无效菜单
UPDATE system_menu SET cms_state = 0 WHERE cms_id IN (28,28,30,31,32,33);
-- 积分管理：订货商系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(48,1,'积分管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(49,1,'积分列表','pay/integral/c_integral/list_integral_detail',2,48,0.000,1,NULL,'2019-01-12 10:54:39');
-- 积分管理：平台管理系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(50,2,'积分管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(51,2,'用户积分','pay/integral/c_integral/list_integral',2,50,0.000,1,NULL,'2019-01-12 10:54:39');
-- 积分管理：生产商系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(52,3,'积分管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(53,3,'积分列表','pay/integral/c_integral/list_integral_detail',2,52,0.000,1,NULL,'2019-01-12 10:54:39');
-- 积分管理：供应商系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(54,4,'积分管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(55,4,'积分列表','pay/integral/c_integral/list_integral_detail',2,54,0.000,1,NULL,'2019-01-12 10:54:39');
-- 积分管理：样板师系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(56,5,'积分管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(57,5,'积分列表','pay/integral/c_integral/list_integral_detail',2,56,0.000,1,NULL,'2019-01-12 10:54:39');
-- 积分管理：样衣师系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(58,6,'积分管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(59,6,'积分列表','pay/integral/c_integral/list_integral_detail',2,58,0.000,1,NULL,'2019-01-12 10:54:39');
-- 商户管理
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(60,2,'商户管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(61,2,'商户列表','pay/pay_order/c_channel/list_partner',2,60,0.000,1,NULL,'2019-01-12 10:54:39');
-- 财务管理：订货商系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(62,1,'支付订单','pay/pay_order/c_pay_order/list_order',2,4,0.000,1,NULL,'2019-01-12 10:54:39'),
(63,1,'充值订单','pay/pay_order/c_recharge/list_recharge',2,4,0.000,1,NULL,'2019-01-12 10:54:39');
-- 财务管理：平台管理系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(64,2,'用户充值列表','pay/pay_order/c_recharge/all_list_recharge',2,10,0.000,1,NULL,'2019-01-12 10:54:39');
-- 财务管理：生产商系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(65,3,'支付订单','pay/pay_order/c_pay_order/list_order',2,17,0.000,1,NULL,'2019-01-12 10:54:39'),
(66,3,'充值订单','pay/pay_order/c_recharge/list_recharge',2,17,0.000,1,NULL,'2019-01-12 10:54:39');
-- 财务管理：供应商系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(67,4,'支付订单','pay/pay_order/c_pay_order/list_order',2,21,0.000,1,NULL,'2019-01-12 10:54:39'),
(68,4,'充值订单','pay/pay_order/c_recharge/list_recharge',2,21,0.000,1,NULL,'2019-01-12 10:54:39');
-- 财务管理：样板师系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(69,5,'财务管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(70,5,'支付订单','pay/pay_order/c_pay_order/list_order',2,69,0.000,1,NULL,'2019-01-12 10:54:39'),
(71,5,'充值订单','pay/pay_order/c_recharge/list_recharge',2,69,0.000,1,NULL,'2019-01-12 10:54:39');
-- 财务管理：样衣师系统
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_state`, `cms_create_time`, `cms_modify_time`) VALUES
(72,6,'财务管理','',1,0,0.000,1,NULL,'2019-01-12 10:54:39'),
(73,6,'支付订单','pay/pay_order/c_pay_order/list_order',2,72,0.000,1,NULL,'2019-01-12 10:54:39'),
(74,6,'充值订单','pay/pay_order/c_recharge/list_recharge',2,72,0.000,1,NULL,'2019-01-12 10:54:39');

-- 物流管理界面
insert into `system_menu` (`cms_id`,`cms_project_id`, `cms_mark`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_is_top`, `cms_state`, `cms_create_time`, `cms_modify_time`) values
(75,'2','','物流管理','pay/pay_order/c_logistics/logistics_list','1','0','0.000','0','1',NULL,'2018-12-27 10:54:39');

update system_menu set cms_url='order/manufacture/c_leisure/manager_leisure' where cms_id= 15;
update system_menu set cms_state=0 where cms_id=38;
update system_menu set cms_url='order/simple_dress/c_simple_dress/manager_index' where cms_id= 39;
update system_menu set cms_url='order/model_dress/c_model_dress/manager_index' where cms_id= 40;
update system_menu set cms_url='order/simple_dress/c_simple_dress/admin_index' where cms_id= 42;
update system_menu set cms_url='order/model_dress/c_model_dress/admin_index' where cms_id= 41;

update system_menu set cms_url='order/manufacture/c_manufacture/index' where cms_id= 8;

-- 款式管理
INSERT INTO `system_menu` (`cms_id`, `cms_project_id`, `cms_mark`, `cms_name`, `cms_url`, `cms_level`, `cms_parent_id`, `cms_order`, `cms_is_top`, `cms_state`, `cms_type`, `cms_create_time`, `cms_modify_time`) VALUES ('76', '2', NULL, '款式管理', 'order/con_client/c_client_order/basic_style_manager', '1', '0', '0.000', '0', '1', '0', NULL, '2019-01-15 16:35:38');
