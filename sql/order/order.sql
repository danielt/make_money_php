-- ------------------------------------------------------------------------------------
-- Table update cms_channel_mode 支付方式 change by kan.yang 2018-12-27
-- ------------------------------------------------------------------------------------
ALTER TABLE `system_channel_mode` MODIFY cms_status TINYINT(1) NOT NULL DEFAULT 1 COMMENT '支付方式状态，默认1：1开启；0禁止';
ALTER TABLE `system_channel_mode` CHANGE `nns_transport` `cms_transport` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '传输协议';
ALTER TABLE `system_channel_mode` CHANGE `nns_channel_mode_flag` `cms_channel_mode_flag` TINYINT(1) NOT NULL COMMENT '支付渠道类型：0二维码；1APP；2WAP';
ALTER TABLE `system_channel` CHANGE `nns_partner_id` `cms_partner_id` INT(11) NOT NULL  COMMENT '商户ID';
ALTER TABLE `system_buy_order` CHANGE `nns_order_parent` `cms_order_parent` INT(11) NOT NULL DEFAULT 0 COMMENT '父级订单ID';

-- ------------------------------------------------------------------------------------
-- Table add system_user_integral 用户积分 add by kan.yang 2018-12-28 01:05:00
-- ------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `system_user_integral` (
  `cms_id` INT(11) AUTO_INCREMENT NOT NULL COMMENT '主键ID，积分类型ID',
  `cms_user_id` INT(11) NOT NULL COMMENT '用户ID',
  `cms_integral` INT(8) NOT NULL DEFAULT 0 COMMENT '当前积分',
  `cms_intrgral_history` INT(11) NOT NULL DEFAULT 0 COMMENT '历史积分（只增不减）',
  `cms_create_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `cms_uuid` CHAR(32) NOT NULL DEFAULT '' COMMENT 'GUUID，外部标识',
  PRIMARY KEY (`cms_id`),
  UNIQUE KEY (`cms_user_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户积分表';

-- ------------------------------------------------------------------------------------
-- Table add system_user_integral_detail 积分明细 add by kan.yang 2018-12-28 01:05:00
-- ------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `system_user_integral_detail` (
  `cms_id` INT(11) AUTO_INCREMENT NOT NULL COMMENT '主键ID，积分类型ID',
  `cms_integral_type` TINYINT(1) NOT NULL COMMENT '积分类型',
  `cms_user_id` INT(11) NOT NULL COMMENT '用户ID',
  `cms_integral` INT(4) NOT NULL DEFAULT 0 COMMENT '积分值',
  `cms_incr_decr` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '增减积分，默认0。0正；1负',
  `cms_extend_id` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '支出积分关联数据',
  `cms_create_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  `cms_uuid` CHAR(32) NOT NULL DEFAULT '' COMMENT 'GUUID，外部标识',
  `cms_desc` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '描述信息',
  PRIMARY KEY (`cms_id`),
  KEY (`cms_user_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='积分明细表';

-- ------------------------------------------------------------------------------------
-- Alter table order_client_order add column cms_name
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_client_order`
ADD COLUMN `cms_name`  varchar(255) NULL DEFAULT '' COMMENT '订单名称';

-- ------------------------------------------------------------------------------------
-- Add table order_simple_dress_order
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_simple_dress_order` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cms_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '订单名称',
  `cms_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '订单内容',
  `cms_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态',
  `cms_create_time` datetime DEFAULT NULL,
  `cms_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- Add table order_model_dress_order
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_model_dress_order` (
  `cms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cms_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '订单名称',
  `cms_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '订单内容',
  `cms_state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '订单状态',
  `cms_create_time` datetime DEFAULT NULL,
  `cms_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- Add table order_producer_time
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_producer_time` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `cms_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '空期信息名称',
  `cms_manager_id` int(11) unsigned NOT NULL COMMENT '用户账号id',
  `cms_person_amount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '工厂人数',
  `cms_free_time` int(11) unsigned NOT NULL COMMENT '用户填写的空期时间',
  `cms_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '空期信息的状态；0有效1过期',
  `cms_create_time` datetime DEFAULT NULL COMMENT '空期信息的创建时间',
  `cms_modify_time` datetime DEFAULT NULL COMMENT '空期信息的修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 面辅料类型表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc_type` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cms_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '面辅料类型名称',
  `cms_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 面辅料类型与面辅料绑定表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc_map` (
  `cms_fabirc_type_id` int(11) unsigned NOT NULL COMMENT '面辅料类型ID',
  `cms_fabirc_id` int(11) unsigned NOT NULL COMMENT '面辅料ID',
  `cms_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  UNIQUE KEY `fabirc_unique` (`cms_fabirc_type_id`,`cms_fabirc_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 面辅料表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `cms_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '面辅料名称',
  `cms_fabirc_attribute` char(32) COLLATE utf8_unicode_ci DEFAULT '' COMMENT '面辅料属性，多个以,逗号分隔',
  `cms_create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 面辅料属性表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc_attribute` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '面辅料属性自增ID',
  `cms_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '属性名称',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='面辅料属性';

-- ------------------------------------------------------------------------------------
-- 面辅料属性值表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc_attribute_value` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cms_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '属性值名称',
  `cms_value` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '属性值',
  `cms_fabirc_attribute_id` int(11) unsigned NOT NULL COMMENT '属性ID',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ------------------------------------------------------------------------------------
-- Add Column cms_order_price_payed By system_buy_order kan.yang 2019-01-05
-- ------------------------------------------------------------------------------------
ALTER TABLE `system_buy_order` ADD COLUMN `cms_order_price_payed` DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT '已支付订单金额';
-- ------------------------------------------------------------------------------------
-- 面辅料产品表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc_material` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键自增ID',
  `cms_fabirc_id` int(11) unsigned NOT NULL COMMENT '分类ID',
  `cms_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT '选料类型',
  `cms_summary` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '产品描述',
  `cms_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '选料海报',
  `cms_is_scarce` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否属于奇缺面辅料0普通1奇缺',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='面辅选料';


-- ------------------------------------------------------------------------------------
-- 面辅料与属性绑定表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_fabirc_material_bind_attribute` (
  `cms_attribute_value_id` int(11) unsigned NOT NULL COMMENT '属性值ID',
  `cms_material_id` int(11) unsigned NOT NULL COMMENT '选料ID',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  UNIQUE KEY `material_vlaue` (`cms_attribute_value_id`,`cms_material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='选料与属性值绑定';

-- ------------------------------------------------------------------------------------
-- Alter table order_order_type
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_order_type`
CHANGE COLUMN `cms_category_id` `cms_category_first`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '第一级分类',
ADD COLUMN `cms_category_second`  int(11) NOT NULL DEFAULT 0 COMMENT '第二级分类',
ADD COLUMN `cms_category_third`  int(11) ZEROFILL NOT NULL DEFAULT 0 COMMENT '第三级分类';


-- ------------------------------------------------------------------------------------
-- 服装部件分类表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_clothing_parts_type` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cms_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '服装部件类型名',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 服装部件与大类分类绑定表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_parts_type_bind_clothing` (
  `cms_parts_type_id` int(11) NOT NULL COMMENT '部件类型ID',
  `cms_clothing_id` int(11) NOT NULL COMMENT '服装分类ID',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  UNIQUE KEY `parts_clothing` (`cms_parts_type_id`,`cms_clothing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 服装部件信息表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_clothing_parts_info` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cms_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '部件名称',
  `cms_parts_type_id` int(11) NOT NULL COMMENT '部件类型ID',
  `cms_image` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '部件图片',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ------------------------------------------------------------------------------------
-- 为面辅料产品表增加用户ID
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_fabirc_material`
ADD COLUMN `cms_user_id`  int(11) NOT NULL COMMENT '用户ID' AFTER `cms_id`;


-- ------------------------------------------------------------------------------------
-- Alter table order_order_type add cms_src
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_order_type`
ADD COLUMN `cms_src`  varchar(255) NOT NULL DEFAULT '' COMMENT '图片地址';

-- ------------------------------------------------------------------------------------
-- 修改表字段 xinxin.deng 2019/1/7 17:25
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_plateing_price` tinyint(4) DEFAULT NULL COMMENT '制板费,0为100，1为120，2为150';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_pushing_plate_price` tinyint(4) DEFAULT NULL COMMENT '推版费，0为15，1为20，2为25';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_is_full_time` tinyint(1) DEFAULT NULL COMMENT '是否专职,0否,1是';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_have_workroom` tinyint(1) DEFAULT NULL COMMENT '有无工作室,0否,1是';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_use_software` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '使用软件：其他,ET,至尊宝,BKR';

-- ------------------------------------------------------------------------------------
-- Alter table order_client_order add cms_order_id and cms_order_parent_type_id
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_client_order`
ADD COLUMN `cms_order_id`  char(32) NOT NULL COMMENT '订单id',
ADD COLUMN `cms_order_parent_type_id`  int(11) NOT NULL COMMENT '订单的大类id';

-- ------------------------------------------------------------------------------------
-- 修改积分历史积分字段 xinxin.deng 2019/1/8 14:26
-- ------------------------------------------------------------------------------------
ALTER TABLE `system_user_integral`
 CHANGE `cms_intrgral_history` `cms_integral_history` INT(11) NOT NULL DEFAULT 0 COMMENT '历史积分（只增不减）';

-- ------------------------------------------------------------------------------------
-- Alter table order_fabirc_material add cms_price
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_fabirc_material`
ADD COLUMN `cms_price`  float(7,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '价格' AFTER `cms_is_scarce`;

-- ------------------------------------------------------------------------------------
-- Alter table order_fabirc_material add cms_unit
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_fabirc_material`
ADD COLUMN `cms_unit` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '单位' AFTER `cms_price`;

-- ------------------------------------------------------------------------------------
-- Alter table order_model_dress_order  order_simple_dress_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_simple_dress_order`
ADD COLUMN `cms_order_id`  char(32) NOT NULL COMMENT '订单id';
ALTER TABLE `order_model_dress_order`
ADD COLUMN `cms_order_id`  char(32) NULL COMMENT '订单id';

ALTER TABLE `order_simple_dress_order`
ADD COLUMN `cms_manager_id`  int NULL COMMENT '样衣所属用户id';
ALTER TABLE `order_model_dress_order`
ADD COLUMN `cms_manager_id`  int NULL COMMENT '样板所属用户id';

-- ------------------------------------------------------------------------------------
-- Alter table order_client_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_client_order`
ADD COLUMN `cms_manager_id`  int NULL COMMENT '订单所属用户';

-- ------------------------------------------------------------------------------------
-- Alter table order_logistics 添加物流录入表，后期应该要进行分表
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_logistics` (
  `cms_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cms_buy_order_id` INT(10) NOT NULL COMMENT '订单ID',
  `cms_desc` varchar(255) NOT NULL DEFAULT '' COMMENT '物流描述',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- ------------------------------------------------------------------------------------
-- Alter table order_client_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_model_dress_order`
ADD COLUMN `cms_acceptor_id`  int NULL COMMENT '接单的样板师id';

ALTER TABLE `order_simple_dress_order`
ADD COLUMN `cms_acceptor_id`  int NULL COMMENT '接单的样衣师id';

-- ------------------------------------------------------------------------------------
-- Add table order_manufacture_order
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_manufacture_order` (
`cms_order_id`  char(32) NOT NULL ,
`cms_content`  varchar(255) NULL COMMENT '订单内容' ,
`cms_manager_id`  int NULL COMMENT '订单所属的用户id' ,
`cms_acceptor_id`  int NULL COMMENT '接单的用户id' ,
`cms_state`  tinyint NULL COMMENT '状态' ,
`cms_create_date`  datetime NULL COMMENT '建立时间' ,
`cms_modify-date`  datetime NULL COMMENT '更新时间' ,
PRIMARY KEY (`cms_order_id`)
);

-- ------------------------------------------------------------------------------------
-- Add table order_supplier_order
-- ------------------------------------------------------------------------------------
CREATE TABLE `order_supplier_order` (
`cms_order_id`  char(32) NOT NULL ,
`cms_content`  varchar(255) NULL COMMENT '订单内容' ,
`cms_manager_id`  int NULL COMMENT '订单所属的用户id' ,
`cms_acceptor_id`  int NULL COMMENT '接单的用户id' ,
`cms_state`  tinyint NULL COMMENT '状态' ,
`cms_create_date`  datetime NULL COMMENT '建立时间' ,
`cms_modify-date`  datetime NULL COMMENT '更新时间' ,
PRIMARY KEY (`cms_order_id`)
);
-- ------------------------------------------------------------------------------------
-- Add table order_model_dress_order、order_simple_dress_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_model_dress_order`
ADD COLUMN `cms_parent_order_id`  int(11) NULL COMMENT '父级订单id';
ALTER TABLE `order_simple_dress_order`
ADD COLUMN `cms_parent_oder_id`  int(11) NULL COMMENT '父级订单id';

-- ------------------------------------------------------------------------------------
-- Add table order_model_dress_order、order_simple_dress_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_client_order`
MODIFY COLUMN `cms_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT FIRST ;
ALTER TABLE `order_client_order`
ADD COLUMN `cms_amount`  int(11) NULL DEFAULT 0 COMMENT '数量';
ALTER TABLE `order_supplier_order`
CHANGE COLUMN `cms_modify-date` `cms_modify_date`  datetime NULL DEFAULT NULL COMMENT '更新时间';
ALTER TABLE `order_supplier_order`
ADD COLUMN `cms_parent_order_id`  char(32) NULL COMMENT '父级订单id';
ALTER TABLE `order_client_order`
ADD COLUMN `cms_price` decimal(10,2) NULL COMMENT '订单单价价钱';
ALTER TABLE `order_simple_dress_order`
ADD COLUMN `cms_value_order_id`  int(11) NULL COMMENT '支付主订单ID';
ALTER TABLE `order_model_dress_order`
ADD COLUMN `cms_value_order_id`  int(11) NULL COMMENT '支付主订单ID';

-- ------------------------------------------------------------------------------------
-- Alter table order_client_order modify column
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_client_order`
CHANGE COLUMN `cms_status` `cms_state`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单状态：0未支付1已支付';

-- ------------------------------------------------------------------------------------
-- Alter table order_order_type
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_order_type`
ADD COLUMN `cms_price`  decimal(10,2) NOT NULL DEFAULT 0 COMMENT '价钱';
ALTER TABLE `order_model_dress_order`
ADD COLUMN `cms_price`  decimal(10,2) NULL DEFAULT 0 COMMENT '样板定价';
ALTER TABLE `order_simple_dress_order`
ADD COLUMN `cms_price`  decimal(10,2) NULL DEFAULT 0 COMMENT '样衣定价';

-- ------------------------------------------------------------------------------------
-- Alter table order_simple_dress_order order_model_dress_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_simple_dress_order`
MODIFY COLUMN `cms_parent_oder_id`  char(32) NULL DEFAULT '' COMMENT '父级订单id';
ALTER TABLE `order_model_dress_order`
MODIFY COLUMN `cms_parent_order_id`  char(32) NULL DEFAULT '' COMMENT '父级订单id';

-- ------------------------------------------------------------------------------------
-- Alter table order_simple_dress_order
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_simple_dress_order`
CHANGE COLUMN `cms_parent_oder_id` `cms_parent_order_id`  char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '' COMMENT '父级订单id';

-- ------------------------------------------------------------------------------------
-- 修改用户表字段销售渠道、主营产品，添加企业类型
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_manager`
CHANGE COLUMN `cms_sale_channels` `cms_sale_channels` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '销售渠道：批发,零售,实体店,网销,其他';
ALTER TABLE `order_manager`
CHANGE COLUMN `cms_main_product` `cms_main_product` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主营产品:梭织服装,针织服装,毛衫,裘皮/防裘皮,皮革/仿皮,家纺,服饰,其他';
ALTER TABLE `order_manager`
ADD COLUMN `cms_company_type` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '企业类型:服装品牌商,服装贸易商,批发档口,个人设计师,淘宝店主,微商,网红及团队,其他';
ALTER TABLE `order_manager`
ADD COLUMN `cms_courier_contact` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '快递联系人';
ALTER TABLE `order_manager`
ADD COLUMN `cms_courier_telephone` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '快递联系人电话';
ALTER TABLE `order_manager`
ADD COLUMN `cms_courier_big_contact` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '大件快递联系人';
ALTER TABLE `order_manager`
ADD COLUMN `cms_courier_big_telephone` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '大件快递联系人电话';
ALTER TABLE `order_manager`
ADD COLUMN `cms_taxpayer_type` tinyint(4) NULL COMMENT '纳税人类型，1一般纳税人,2小规模纳税人' ;
ALTER TABLE `order_manager`
ADD COLUMN `cms_need_invoice` tinyint(4) NULL COMMENT '是否需要发票，1需要,2不需要,3待定' ;
ALTER TABLE `order_manager`
ADD COLUMN `cms_duty_num` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '税号';
ALTER TABLE `order_manager`
ADD COLUMN `cms_base_bank_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '开户银行';
ALTER TABLE `order_manager`
ADD COLUMN `cms_base_bank_num` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '基本账号';
ALTER TABLE `order_manager`
ADD COLUMN `cms_contact_bank_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '往来账户开户银行';
ALTER TABLE `order_manager`
ADD COLUMN `cms_contact_bank_num` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '往来账号';

-- ------------------------------------------------------------------------------------
-- 添加用户表字段厂名,修改是否专职，有无工作室
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_manager`
ADD COLUMN `cms_factor_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '厂名';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_is_full_time` VARCHAR(1) DEFAULT '' COMMENT '是否专职,0否,1是';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_have_workroom` VARCHAR(1) DEFAULT '' COMMENT '有无工作室,0否,1是';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_plateing_price` VARCHAR(4) DEFAULT '' COMMENT '制板费,0为100,1为120,2为150,3为175,4为200';
ALTER TABLE `order_manager`
 MODIFY COLUMN `cms_pushing_plate_price` VARCHAR(4) DEFAULT '' COMMENT '推版费,0为15,1为20,2为25';

-- ------------------------------------------------------------------------------------
-- 添加用户表索引 xinxin.deng 2019/2/19 15:01
-- ------------------------------------------------------------------------------------
CREATE UNIQUE INDEX telephone_key ON order_manager (`cms_telephone`);
CREATE UNIQUE INDEX primary_key ON order_manager (`cms_id`, `cms_telephone`, `cms_role_id`);

-- ------------------------------------------------------------------------------------
-- 供应商特有字段增加 xinxin.deng 2019/3/1 9:41
-- ------------------------------------------------------------------------------------
ALTER TABLE `order_manager`
 ADD COLUMN `cms_fabric_weaving` VARCHAR(64) DEFAULT '' COMMENT '面料织法:针织,梭织;多个逗号隔开';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_fabric_pattern` VARCHAR(64) DEFAULT '' COMMENT '面料花型:素色,提花,印花;多个逗号隔开';
ALTER TABLE `order_manager`
 ADD COLUMN `cms_fabric_composition` VARCHAR(64) DEFAULT '' COMMENT '面料成分:棉,麻,毛,丝,皮革,混纺类,化纤类;多个逗号隔开';