-- ------------------------------------------------------------------------------------
-- 创建表 2019/5/5 16:45 dengxinxin
-- ------------------------------------------------------------------------------------
CREATE TABLE `wintv_category` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID',
  `cms_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '栏目名称',
  `cms_category_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '栏目id',
  `cms_parent_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '父级栏目id，没有父级默认0',
  `cms_column_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源库ID',
  `cms_platform_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '平台、运营商ID',
  `cms_type` tinyint(1) DEFAULT '0' COMMENT '栏目类型，0点播，1直播',
  `cms_order` int(4) NOT NULL DEFAULT '0' COMMENT '栏目排序',
  `cms_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '管理状态  0 启用 | 1 禁用  默认禁用',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '栏目表';

CREATE TABLE `wintv_channel` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID',
  `cms_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '频道名称',
  `cms_channel_num` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '频道号',
  `cms_import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '频道注入id',
  `cms_import_src` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '频道注入来源',
  `cms_platform_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '平台、运营商id',
  `cms_category_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '所属资源库栏目',
  `cms_column_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '所属资源库跟栏目id',
  `cms_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '频道类型，电影、电视剧等',
  `cms_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '管理状态  0 启用 | 1 禁用  默认禁用',
  `cms_realtime` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否实时状态  0 是 | 1 否  默认否',
  `cms_desc` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '频道表';

CREATE TABLE `wintv_column` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID',
  `cms_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '资源库名称',
  `cms_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '资源库类型，0点播，1直播，默认0',
  `cms_platform_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '资源库对应平台ID',
  `cms_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '管理状态  0 启用 | 1 禁用  默认禁用',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '资源库跟栏目';

CREATE TABLE `wintv_platform` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID',
  `cms_name` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '平台、运营商名称',
  `cms_desc` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `cms_state` tinyint(1) DEFAULT '1' COMMENT '管理状态  0 启用 | 1 禁用  默认禁用',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '平台、运营商';

CREATE TABLE `wintv_programme` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID',
  `cms_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节目单名称',
  `cms_channel_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '节目单所属频道id',
  `cms_import_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节目单注入id',
  `cms_import_src` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '节目单注入来源',
  `cms_started_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '节目单开始时间',
  `cms_ended_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '节目单结束时间',
  `cms_duration` int(128) NOT NULL DEFAULT '0' COMMENT '节目单时长',
  `cms_state` tinyint(1) DEFAULT '1' COMMENT '管理状态  0 启用 | 1 禁用  默认禁用',
  `cms_desc` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '节目单表';


CREATE TABLE `wintv_terminal_params` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `cms_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '键',
  `cms_value` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '值',
  `cms_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '描述',
  `cms_group` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '分组',
  `cms_type` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT '下行参数类型',
  `cms_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0启用 1禁用',
  `cms_realtime` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否实时，0是 1否',
  `cms_create_time` datetime DEFAULT NULL,
  `cms_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '终端下行参数表';

CREATE TABLE `wintv_terminal_params_group` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `cms_key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `cms_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `cms_create_time` datetime DEFAULT NULL,
  `cms_modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '终端下行参数分组表';

CREATE TABLE `wintv_terminal_params_type` (
  `cms_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL COMMENT 'UUID',
  `cms_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '终端下行参数类型id',
  `cms_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '终端下行参数类型名',
  `cms_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '管理状态  0 启用 | 1 禁用  默认禁用',
  `cms_create_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `cms_modify_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`cms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT '终端下行参数类型表';
