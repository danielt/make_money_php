<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/10 10:00
 */
include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/order/buy_order/c_logistics/logic_logistics.class.php';
class c_logistics extends CI_Controller
{
    //项目基路径
    private $str_base_path   = '';

    private $obj_logistics_logic   = '';//logic对象

    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->database();
        //加载自定义配置文件
        $this->config->load('config_app', true);
        $this->obj_logistics_logic = new logistics_logic($this);
        $this->str_base_path = dirname(dirname(dirname(dirname(__DIR__))));
    }

    /**
     * 物流列表
     */
    public function logistics_list()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '非法'
            ),
        ),$this->arr_params);
        $system_file_list = array(
            array(
                'url' => 'pay/pay_order/c_logistics/logistics_add.php',//右侧按钮弹框
                'class' => 'pay_pay_order_c_logistics_logistics_add',//form表单ID
                'ajax' => 'pay/pay_order/c_logistics/logistics_add',//form表单提交控制器
                'function' => 'add',//行为动作
                'button_data' => array(
                    array(
                        'name' => '添加',
                        'icon' => 'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url' => 'pay/pay_order/c_logistics/logistics_del.php',
                'class' => 'pay/pay_order/c_logistics/logistics_del',
                'ajax' => 'pay/pay_order/c_logistics/logistics_del',
                'function' => 'delete',
                'button_data' => array(
                    array(
                        'name' => '删除',
                        'icon' => 'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            )
        );
        $this->_init_page();

        //查询数据
        $arr_partner_list = $this->obj_logistics_logic->get_list($this->arr_params,'*');
        //操作菜单
        $arr_partner_list['system_file_list'] = $system_file_list;
        //输出数据
        $this->load_view_file($arr_partner_list,__LINE__);
    }

    /**
     * 录入物流
     */
    public function logistics_add()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_buy_order_id'=> array(
                'rule'   => 'notnull',
                'reason' => '订单号非法'
            ),
            'cms_desc'=> array(
                'rule'   => 'notnull',
                'reason' => '物流信息非法'
            ),
        ),$this->arr_params);

        $re = $this->obj_logistics_logic->add($this->arr_params);
        $this->load_view_file($re,__LINE__);
    }

    /**
     * 删除物流
     */
    public function logistics_del()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'array',
                'reason' => '物流id非法'
            ),
        ),$this->arr_params);

        $re = $this->obj_logistics_logic->del($this->arr_params['cms_id']);

        $this->load_view_file($re,__LINE__);
    }

    /**
     * 嵌套订单页
     */
    public function buy_order_frame()
    {
        // 初始化页面
        $this->_init_page();

        //先查询是否已经存在了
        $arr_order = $this->auto_load_table('order', 'con_client', 'c_client_order', 'client_order', 'query');
        //输出数据
        $this->load_view_file($arr_order,__LINE__);
    }


}