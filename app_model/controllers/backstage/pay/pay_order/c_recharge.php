<?php
/**
 * Created by PhpStorm.
 * Use : 充值业务逻辑
 * User: kan.yang@starcor.com
 * Date: 18-12-22
 * Time: 下午1:06
 */

class c_recharge extends CI_Controller
{

    //项目基路径
    private $str_base_path   = '';

    /**
     * 默认构造函数
     */
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->database();
        $this->str_base_path = dirname(dirname(dirname(dirname(__DIR__))));
        //初始化在线用户ID
        $this->arr_params['cms_user_id'] = empty($this->arr_params['cms_user_id']) ? $_SESSION['user_id'] : $this->arr_params['cms_user_id'];
    }

    /**
     * 查询充值订单列表
     */
    public function all_list_recharge()
    {
        //分页处理
        $this->_init_page();

        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_buy_order/logic_order_buy.class.php';
        $obj_order_buy_logic = new order_buy_logic($this);

        unset($this->arr_params['cms_user_id']);
        $arr_recharge_list = $obj_order_buy_logic->list_user_recharge($this->arr_params);
        //输出数据
        $this->load_view_file($arr_recharge_list,__LINE__);
    }

    /**
     * 查询充值订单列表
     */
    public function list_recharge()
    {
        //列表菜单
        $system_file_list = array(

            array(
                'url'=>'pay/pay_order/c_recharge/item_recharge_edit.php',
                'class'=>'pay_pay_order_c_recharge_item_recharge_edit',
                'ajax'=>'',
                'function'=>'item_recharge_edit',
                'button_data'=>array(
                    array(
                        'name'=>'详情',
                        'icon'=>'fa-pencil-square-o',
                        'params'=>'',
                        'where' =>'',
                        'bottom'=>true,
                    ),
                ),
            ),
            array(
                'url'=>'pay/pay_order/c_pay_order/c_pay.php',
                'class'=>'pay_pay_order_c_pay_order_c_pay',
                'ajax'=>'',
                'function'=>'c_pay',
                'button_data'=>array(
                    array(
                        'name'=>'充值',
                        'icon'=>'fa-pencil-square-o',
                        'params'=>'',
                        'where' =>'',
                        'self_action'=>'obj_pay_order_logic.recharge_order()',
                    ),
                ),
            ),
        );
        //分页处理
        $this->_init_page();
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_buy_order/logic_order_buy.class.php';
        $obj_order_buy_logic = new order_buy_logic($this);

        $this->arr_params['cms_order_type']= 5;
        $arr_recharge_list   = $obj_order_buy_logic->get_list($this->arr_params,'*',$this->arr_page_params);
        //操作菜单
        $arr_recharge_list['system_file_list'] = $system_file_list;
        //输出数据
        $this->load_view_file($arr_recharge_list,__LINE__);
    }

    /**
     * 充值订单详情
     */
    public function item_recharge()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '充值订单非法'
            ),
        ),$this->arr_params);

        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_buy_order/logic_order_buy.class.php';
        $obj_order_buy_logic = new order_buy_logic($this);

        $this->arr_params['cms_order_type']= 5;
        $arr_recharge_info = $obj_order_buy_logic->get_one($this->arr_params,'*');
        $this->load_view_file($arr_recharge_info,__LINE__);
    }

    /**
     * 充值页面
     */
    public function index_recharge()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付方用户非法'
            ),
        ), $this->arr_params);
        //处理商户ID
        if($this->arr_params['cms_user_id'] != $this->config->config['system_pay_manager'])
        {
            $this->arr_params['cms_partner_user_id'] = $this->config->config['system_pay_manager'];
        }
        else
        {
            $this->arr_params['cms_partner_user_id'] = $this->arr_params['cms_user_id'];
        }
        $this->load_view_file(array('data_info' => $this->arr_params),__LINE__);
    }

    /**
     * 充值
     */
    public function recharge()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付方用户非法'
            ),
            'cms_order_price' => array(
                'rule'   => 'number',
                'reason' => '订单金额非法'
            ),
            'cms_order_type' => array(
                'rule'   => 'in',
                'in'         => '5',
                'reason' => '订单类型非法'
            ),
            'cms_pay_partner_id' => array(
                'rule'   => 'notnull',
                'reason' => '商户非法'
            ),
            'cms_pay_channel_id' => array(
                'rule'   => 'notnull',
                'reason' => '支付渠道非法'
            ),
            'cms_pay_channel_mode_id' => array(
                'rule'   => 'notnull',
                'reason' => '支付渠道-支付模式非法'
            ),
            'cms_order_price_payed' => array(
                'rule'   => 'notnull',
                'reason' => '本次支付金额非法'
            ),
        ),$this->arr_params);
        $this->flag_ajax_reurn = true;
        //校验商户是否有效
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_partner/logic_partner.class.php';
        $obj_partner_logic = new partner_logic($this);
        $arr_partner_info  = $obj_partner_logic->get_one(array(
                'cms_id'   => $this->arr_params['cms_pay_partner_id'],
                'cms_status'  => 1),
                'cms_id'
        );
        if($arr_partner_info['ret'] != 0 || empty($arr_partner_info['data_info']))
        {
            $this->load_view_file($arr_partner_info,__LINE__);
            return false;
        }
        unset($obj_partner_logic, $arr_partner_info);
        //校验支付渠道是否有效
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_channel/logic_channel.class.php';
        $obj_channel_logic = new channel_logic($this);
        $arr_channel_info  = $obj_channel_logic->get_one(array(
                'cms_id'   => $this->arr_params['cms_pay_channel_id'],
                'cms_status'    => 1,
                'nns_partner_id'=> $this->arr_params['cms_pay_partner_id'],
                ),
            'cms_id,cms_platform_id'
        );
        if($arr_channel_info['ret'] != 0 || empty($arr_channel_info['data_info']))
        {
            $this->load_view_file($arr_channel_info,__LINE__);
            return false;
        }
        else
        {
            $arr_channel_info = $arr_channel_info['data_info'];
        }
        unset($obj_channel_logic);
        //校验支付模式是否有效
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_channel_mode/logic_channel_mode.class.php';
        $obj_channel_mode_logic = new channel_mode_logic($this);
        $arr_channel_mode_info  = $obj_channel_mode_logic->get_one(array(
                'cms_id'   => $this->arr_params['cms_pay_channel_mode_id'],
                'cms_status'    => 1,
                'cms_channel_id'=> $this->arr_params['cms_pay_channel_id'],
        ));
        if($arr_channel_mode_info['ret'] != 0 || empty($arr_channel_mode_info['data_info']))
        {
            $this->load_view_file($arr_channel_mode_info,__LINE__);
            return false;
        }
        else
        {
            $arr_channel_mode_info = $arr_channel_mode_info['data_info'];
        }
        unset($obj_channel_mode_logic);
        //支付模式下单
        include_once $this->str_base_path . '/logic/backstage/order/pay/c_pay/pay.php';
        $obj_pay = new pay($this,'');
        //订单处理
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_buy_order/logic_order_buy.class.php';
        $obj_order_buy_logic = new order_buy_logic($this);
        if(!empty($this->arr_params['cms_order_id']))
        {
            $arr_order_info = $obj_order_buy_logic->get_one(array('cms_uuid' => $this->arr_params['cms_order_id']));
            if($arr_order_info['ret'] != 0 || empty($arr_order_info['data_info']))
            {
                $arr_order_info['reason'] = '错误：校验订单【' . $this->arr_params['cms_order_id'] . '】不存在';
                $this->load_view_file($arr_order_info,__LINE__);
                return false;
            }
            else
            {
                $arr_order_info = $arr_order_info['data_info'];
            }
        }
        //组装订单信息
        if(empty($arr_order_info))
        {
            $arr_order_info  = array(
                'cms_user_id'         => $this->arr_params['cms_user_id'],
                'cms_order_name'      => empty($this->arr_params['cms_order_name']) ? '充值订单' : $this->arr_params['cms_order_name'],
                'cms_order_price'     => $this->arr_params['cms_order_price'],
                'cms_order_type'      => $this->arr_params['cms_order_type'],
                'cms_order_state'     => '0',
                'cms_business_state'  => '0',
                'nns_order_parent'    => '',
                'cms_pay_order_code'  => '',
                'cms_pay_partner_id'  => $this->arr_params['cms_pay_partner_id'],
                'cms_pay_channel_id'  => $this->arr_params['cms_pay_channel_id'],
                'cms_pay_channel_mode'=> $this->arr_params['cms_pay_channel_mode_id'],
                'cms_pay_mode_type'   => $arr_channel_info['cms_platform_id'] . $arr_channel_mode_info['nns_channel_mode_flag'],
                'cms_order_desc'      => '充值订单：' . $this->arr_params['cms_user_id'] . ' 于 ' . date('Y-m-d h:i:s') . ' 充值 ' . $this->arr_params['cms_order_price'] . ' 元',
                'cms_uuid'            => system_guid_rand(),
                'cms_order_price_payed'=> $this->arr_params['cms_order_price_payed'],
            );
        }
        $arr_third_response = array('ret' => 1,'reason' => '未知支付方式');
        switch($arr_channel_info['cms_platform_id'])
        {
            //微信
            case 1:
                WxPayConfig::$APPID = $arr_channel_mode_info['cms_pay_appid'];
                WxPayConfig::$MCHID = $arr_channel_mode_info['cms_pay_partner_id'];
                WxPayConfig::$KEY = $arr_channel_mode_info['cms_pay_partner_key'];
                if($arr_channel_mode_info['cms_channel_mode_flag'] == 1)
                {
                    $arr_third_response = $obj_pay->wechat_scan(array(
                        'cms_expire_time'=> $arr_channel_mode_info['cms_qr_expire_time'],
                        'cms_notify_url' => $arr_channel_mode_info['cms_notify_url']
                    ),array(
                        'cms_order_id'   => $arr_order_info['cms_uuid'],
                        'cms_order_name' => $arr_order_info['cms_order_name'],
                        'cms_order_price'=> $arr_order_info['cms_order_price_payed'],
                        'cms_product_id' => $arr_order_info['cms_uuid'],
                        'cms_uuid'       => $arr_order_info['cms_uuid'],
                    ));
                }
            break;
            //支付宝
            case 2:
                if($arr_channel_mode_info['cms_channel_mode_flag'] == 1)
                {
                    $arr_third_response = $obj_pay->alipay_scan(array(
                        'cms_pay_appid'  => $arr_channel_mode_info['cms_pay_appid'],
                        'cms_expire_time'=> $arr_channel_mode_info['cms_qr_expire_time'],
                        'cms_notify_url' => $arr_channel_mode_info['cms_notify_url'],
                        'cms_public_key' => dirname($this->str_base_path) . '/data_model/upload/' . ltrim($arr_channel_mode_info['cms_public_key'],'/'),
                        'cms_private_key'=> dirname($this->str_base_path) . '/data_model/upload/' . ltrim($arr_channel_mode_info['cms_private_key'],'/'),
                        'cms_sign_type'  => $this->config->config['system_pay_alipay_sign_type'][$arr_channel_mode_info['cms_sign_type']],
                    ),array(
                        'cms_order_id'   => $arr_order_info['cms_uuid'],
                        'cms_order_name' => $arr_order_info['cms_order_name'],
                        'cms_order_price'=> $arr_order_info['cms_order_price_payed'],
                        'cms_product_id' => $arr_order_info['cms_uuid'],
                        'cms_order_num'  => '1',
                        'cms_uuid'       => $arr_order_info['cms_uuid'],
                    ));
                }
            break;
            //未知
            default:
            break;
        }
        //创建订单
        if(($arr_third_response['ret'] == 0 && empty($this->arr_params['cms_order_id'])))
        {
            //创建充值订单
            $arr_add_order = $obj_order_buy_logic->add($arr_order_info);
            if($arr_add_order['ret'] != 0)
            {
                $this->load_view_file($arr_add_order,__LINE__);
                return false;
            }
        }
        $arr_third_response['cms_order_id']    = $arr_order_info['cms_uuid'];
        $arr_third_response['cms_expire_time'] = $arr_channel_mode_info['cms_qr_expire_time'];
        $this->load_view_file($arr_third_response,__DIR__);
        return true;
    }

    /**
     * 轮询订单状态
     */
    public function poll()
    {
        $this->control_params_check(array(
            'cms_order_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付订单非法'
            ),
        ),$this->arr_params);
        //异步返回
        $this->flag_ajax_reurn = true;
        //查询订单状态
        $this->load->driver('cache');
        $int_order_status = $this->cache->redis->get($this->arr_params['cms_order_id']);
        //$int_order_status = TRADE_SUCCESS;
        if($int_order_status == TRADE_SUCCESS)
        {//成功

            $this->load_view_file(array('ret' => TRADE_SUCCESS),__LINE__);
        }
        elseif($int_order_status == TRADE_FAIL)
        {//失败

            $this->load_view_file(array('ret' => TRADE_FAIL),__LINE__);
        }
        else
        {//默认处理中

            $this->load_view_file(array('ret' => TRADE_PROCESS),__LINE__);
        }
    }
} 