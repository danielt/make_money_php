<?php
/**
 * Created by PhpStorm.
 * Use : 商户、支付渠道、支付方式业务逻辑
 * User: kan.yang@starcor.com
 * Date: 18-12-22
 * Time: 下午1:07
 */

class c_channel extends CI_Controller
{

    //项目基路径
    private $str_base_path    = '';
    //商户
    private $obj_partner_logic= null;
    //支付渠道
    private $obj_channel_logic= null;
    //支付方式
    private $obj_channel_mode_logic = null;

    /**
     * 默认构造函数
     */
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->database();
        $this->str_base_path = dirname(dirname(dirname(dirname(__DIR__))));
        $this->_init();
        //初始化在线用户ID
        $this->arr_params['cms_user_id'] = empty($this->arr_params['cms_user_id']) ? $_SESSION['user_id'] : $this->arr_params['cms_user_id'];
    }

/*==================================================== 商户管理 开始 ===================================================*/

    /**
     * 查询商户列表
     */
    public function list_partner()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
        ),$this->arr_params);
        //列表菜单
        $system_file_list = array(
            array(
                'url'=>'pay/pay_order/c_channel/add_partner.php',
                'class'=>'pay-pay_order-c_channel-add_partner',
                'ajax'=>'pay/pay_order/c_channel/add_partner',
                'function'=>'add_partner',
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',
                        'params'=>base64_encode(json_encode(array('cms_user_id' => $this->arr_params['cms_user_id']))),
                        'where'=>'',
                    ),
                ),
            ),
            array(
                'url'=>'pay/pay_order/c_channel/edit_partner.php',
                'class'=>'pay-pay_order-c_channel-edit_partner',
                'ajax'=>'pay/pay_order/c_channel/edit_partner',
                'function'=>'edit_partner',
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'params'=>'',
                        'where' =>'',
                        'bottom'=>true,
                    ),
                ),
            ),
            array(
                'url'=>'pay/pay_order/c_channel/delete.php',
                'class'=>'pay-pay_order-c_channel-delete_partner',
                'ajax'=>'pay/pay_order/c_channel/delete_partner',
                'function'=>'delete_partner',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params'=>'',
                        'where'=>'',
                    ),
                ),
            ),
            array(
                'url'  =>'pay/pay_order/c_channel/state.php',
                'class'=>'pay-pay_order-c_channel-modify_partner_state',
                'ajax' =>'pay/pay_order/c_channel/modify_partner_state',
                'function'=>'modify_partner_state',
                'button_data'=>array(
                    array(
                        'name'=>'启用',
                        'icon'=>'fa-unlock',
                        'params'=>'&cms_state=0',
                        'where'=>array(
                            'cms_state'=>0,
                        ),
                    ),
                    array(
                        'name'=>'禁用',
                        'icon'=>'fa-lock',
                        'params'=>'&cms_state=1',
                        'where'=>array(
                            'cms_state'=>1,
                        ),
                    ),
                ),
            ),
        );
        //分页处理
        $this->_init_page();
        //查询数据
        $arr_partner_list = $this->obj_partner_logic->get_list($this->arr_params,'*');
        //操作菜单
        $arr_partner_list['system_file_list'] = $system_file_list;
        //输出数据
        $this->load_view_file($arr_partner_list,__LINE__);
    }

    /**
     * 查询商户详情
     */
    public function item_partner()
    {
        //处理商户ID
        if(isset($this->arr_params['cms_system_partner']) && $this->arr_params['cms_system_partner'] == 1)
        {
            $this->arr_params['cms_user_id'] = $this->config->config['system_pay_manager'];
        }
        else
        {
            //参数验证
            $this->control_params_check(array(
                'cms_id'=> array(
                    'rule'   => 'notnull',
                    'reason' => '商户非法'
                ),
            ),$this->arr_params);
        }
        $arr_partner_list = $this->obj_partner_logic->get_one($this->arr_params,'*');
        $this->load_view_file($arr_partner_list,__LINE__);
    }

    /**
     * 添加商户
     */
    public function add_partner()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_name'=> array(
                'rule'   => 'notnull',
                'reason' => '商户名称非法'
            ),
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
        ),$this->arr_params);

        $arr_partner_add = $this->obj_partner_logic->add($this->arr_params);
        $this->load_view_file($arr_partner_add,__LINE__);
    }

    /**
     * 更新商户
     */
    public function edit_partner()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '商户非法'
            ),
            'cms_name'=> array(
                'rule'   => 'notnull',
                'reason' => '商户名称非法'
            ),
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
        ),$this->arr_params);

        $arr_partner_edit = $this->obj_partner_logic->edit($this->arr_params['cms_id'],$this->arr_params);
        $this->load_view_file($arr_partner_edit,__LINE__);
    }

    /**
     * 删除商户
     */
    public function delete_partner()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'array',
                'reason' => '商户非法'
            ),
        ),$this->arr_params);
        //校验商户是否绑定了支付渠道
        $arr_channel_count = $this->obj_channel_logic->get_one(array('nns_partner_id' => $this->arr_params['cms_id']),'count(1) total');
        if($arr_channel_count['ret'] != 0 || !empty($arr_channel_count['data_info']['total']))
        {
            $arr_partner_del = array('ret' => 1,'reason' => '删除商户，请优先删除绑定的支付渠道');
        }
        else
        {
            $arr_partner_del = $this->obj_partner_logic->del($this->arr_params['cms_id']);
        }
        $this->load_view_file($arr_partner_del,__LINE__);
    }

    /**
     * 更新商户状态
     */
    public function modify_partner_state()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'array',
                'reason' => '商户非法'
            ),
            'cms_state'=> array(
                'rule'   => 'in',
                'in'     => '0,1',
                'reason' => '商户状态非法'
            ),
        ),$this->arr_params);
        //特殊处理
        $this->arr_params['cms_status'] = $this->arr_params['cms_state'] == 1 ? 0 : 1;
        unset($this->arr_params['cms_state']);
        $arr_partner_edit = $this->obj_partner_logic->edit($this->arr_params['cms_id'],$this->arr_params);
        $this->load_view_file($arr_partner_edit,__LINE__);
    }

/*==================================================== 商户管理 结束 ===================================================*/


/*==================================================== 支付渠道管理 开始 ================================================*/

    /**
     * 查询支付渠道列表
     */
    public function list_channel()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_partner_id'=> array(
                'rule'   => 'notnull',
                'reason' => '商户非法'
            ),
        ),$this->arr_params);
        if(isset($this->arr_params['cms_system_partner']) && $this->arr_params['cms_system_partner'] == 1)
        {
            $this->arr_params['cms_user_id'] = $this->config->config['system_pay_manager'];
        }
        //分页处理
        $this->_init_page();
        $arr_channel_list = $this->obj_channel_logic->get_list($this->arr_params,'*');
        //操作菜单
        if(!isset($this->arr_params['flag_ajax_reurn']) || $this->arr_params['flag_ajax_reurn'] == 0)
        {
            //列表菜单
            $system_file_list = array(
                array(
                    'url'=>'pay/pay_order/c_channel/add_channel.php',
                    'class'=>'pay_pay_order_c_channel_add_channel',
                    'ajax'=>'pay/pay_order/c_channel/add_channel',
                    'function'=>'add_channel',
                    'button_data'=>array(
                        array(
                            'name'=>'添加',
                            'icon'=>'fa-plus',
                            'params'=>base64_encode(json_encode(array(
                                'cms_user_id'    => $this->arr_params['cms_user_id'],
                                'cms_partner_id' => $this->arr_params['cms_partner_id'],
                                'cms_platform_id'=> 1,
                            ))),
                            'where'=>'',
                        ),
                    ),
                ),
                array(
                    'url'=>'pay/pay_order/c_channel/edit_channel.php',
                    'class'=>'pay_pay_order_c_channel_edit_channel',
                    'ajax'=>'pay/pay_order/c_channel/edit_channel',
                    'function'=>'edit_channel',
                    'button_data'=>array(
                        array(
                            'name'=>'修改',
                            'icon'=>'fa-pencil-square-o',
                            'params'=>'',
                            'where' =>'',
                            'bottom'=>true,
                        ),
                    ),
                ),
                array(
                    'url'=>'pay/pay_order/c_channel/delete.php',
                    'class'=>'pay_pay_order_c_channel_delete_channel',
                    'ajax'=>'pay/pay_order/c_channel/delete_channel',
                    'function'=>'delete_channel',
                    'button_data'=>array(
                        array(
                            'name'=>'删除',
                            'icon'=>'fa-trash-o',
                            'params'=>'',
                            'where'=>'',
                        ),
                    ),
                ),
                array(
                    'url'  =>'pay/pay_order/c_channel/state.php',
                    'class'=>'pay_pay_order_c_channel_modify_channel_state',
                    'ajax' =>'pay/pay_order/c_channel/modify_channel_state',
                    'function'=>'modify_channel_state',
                    'button_data'=>array(
                        array(
                            'name'=>'启用',
                            'icon'=>'fa-unlock',
                            'params'=>'&cms_state=0',
                            'where'=>array(
                                'cms_state'=>0,
                            ),
                        ),
                        array(
                            'name'=>'禁用',
                            'icon'=>'fa-lock',
                            'params'=>'&cms_state=1',
                            'where'=>array(
                                'cms_state'=>1,
                            ),
                        ),
                    ),
                ),
            );
            $arr_channel_list['system_file_list'] = $system_file_list;
        }
        //输出数据
        $this->load_view_file($arr_channel_list,__LINE__);
    }

    /**
     * 查询支付渠道详情
     */
    public function item_channel()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道非法'
            ),
        ),$this->arr_params);
        if(isset($this->arr_params['cms_system_partner']) && $this->arr_params['cms_system_partner'] == 1)
        {
            $this->arr_params['cms_user_id'] = $this->config->config['system_pay_manager'];
        }
        $arr_channel_info = $this->obj_channel_logic->get_one($this->arr_params,'*');
        $this->load_view_file($arr_channel_info,__LINE__);
    }

    /**
     * 添加支付渠道
     */
    public function add_channel()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
            'cms_name'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道名称非法'
            ),
            'cms_platform_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台非法'
            ),
            'cms_partner_id'=> array(
                'rule'   => 'notnull',
                'reason' => '商户非法'
            ),
        ),$this->arr_params);

        $arr_channel_add = $this->obj_channel_logic->add($this->arr_params);
        $this->load_view_file($arr_channel_add,__LINE__);
    }

    /**
     * 更新支付渠道
     */
    public function edit_channel()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道非法'
            ),
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
            'cms_name'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道名称非法'
            ),
            'cms_platform_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台非法'
            ),
            'cms_partner_id'=> array(
                'rule'   => 'notnull',
                'reason' => '商户非法'
            ),
        ),$this->arr_params);

        $arr_channel_edit = $this->obj_channel_logic->edit($this->arr_params['cms_id'],$this->arr_params);
        $this->load_view_file($arr_channel_edit,__LINE__);
    }

    /**
     * 删除支付渠道
     */
    public function delete_channel()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'array',
                'reason' => '支付渠道非法'
            ),
        ),$this->arr_params);
        //校验商户是否绑定了支付渠道
        $arr_channel_mode_count = $this->obj_channel_mode_logic->get_one(array('cms_channel_id' => $this->arr_params['cms_id']),'count(1) total');
        if($arr_channel_mode_count['ret'] != 0 || !empty($arr_channel_mode_count['data_info']['total']))
        {
            $arr_mode_del = array('ret' => 1,'reason' => '删除支付渠道，请优先删除绑定的支付方式');
        }
        else
        {
            $arr_mode_del = $this->obj_channel_logic->del($this->arr_params['cms_id']);
        }

        $this->load_view_file($arr_mode_del,__LINE__);
    }

    /**
     * 更新支付渠道状态
     */
    public function modify_channel_state()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'array',
                'reason' => '支付渠道非法'
            ),
            'cms_state'=> array(
                'rule'   => 'in',
                'in'     => '0,1',
                'reason' => '支付渠道状态非法'
            ),
        ),$this->arr_params);
        //特殊处理
        $this->arr_params['cms_status'] = $this->arr_params['cms_state'] == 1 ? 0 : 1;
        unset($this->arr_params['cms_state']);
        $arr_partner_edit = $this->obj_channel_logic->edit($this->arr_params['cms_id'],$this->arr_params);
        $this->load_view_file($arr_partner_edit,__LINE__);
    }

/*==================================================== 支付渠道管理 结束 ================================================*/




/*==================================================== 支付方式管理 开始 ================================================*/

    /**
     * 查询支付方式列表
     */
    public function list_channel_mode()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
            'cms_channel_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道非法'
            ),
        ),$this->arr_params);
        if(isset($this->arr_params['cms_system_partner']) && $this->arr_params['cms_system_partner'] == 1)
        {
            $this->arr_params['cms_user_id'] = $this->config->config['system_pay_manager'];
        }
        //分页处理
        $this->_init_page();
        $arr_mode_list = $this->obj_channel_mode_logic->get_list($this->arr_params,'*');
        //操作菜单
        if(!isset($this->arr_params['flag_ajax_reurn']) || $this->arr_params['flag_ajax_reurn'] == 0)
        {
            //列表菜单
            $system_file_list = array(
                array(
                    'url'=>'pay/pay_order/c_channel/add_channel_mode.php',
                    'class'=>'pay_pay_order_c_channel_add_channel_mode',
                    'ajax'=>'pay/pay_order/c_channel/add_channel_mode',
                    'function'=>'add_channel_mode',
                    'button_data'=>array(
                        array(
                            'name'=>'添加',
                            'icon'=>'fa-plus',
                            'params'=>base64_encode(json_encode(array(
                                'cms_user_id'    => $this->arr_params['cms_user_id'],
                                'cms_channel_id' => $this->arr_params['cms_channel_id'],
                                'cms_channel_mode_flag' => 1,
                            ))),
                            'where'=>'',
                        ),
                    ),
                ),
                array(
                    'url'=>'pay/pay_order/c_channel/edit_channel_mode.php',
                    'class'=>'pay_pay_order_c_channel_edit_channel_mode',
                    'ajax'=>'pay/pay_order/c_channel/edit_channel_mode',
                    'function'=>'edit_channel_mode',
                    'button_data'=>array(
                        array(
                            'name'=>'修改',
                            'icon'=>'fa-pencil-square-o',
                            'params'=>'',
                            'where' =>'',
                            'bottom'=>true,
                        ),
                    ),
                ),
                array(
                    'url'=>'pay/pay_order/c_channel/delete.php',
                    'class'=>'pay-pay_order-c_channel-delete_channel_mode',
                    'ajax'=>'pay/pay_order/c_channel/delete_channel_mode',
                    'function'=>'delete_channel_mode',
                    'button_data'=>array(
                        array(
                            'name'=>'删除',
                            'icon'=>'fa-trash-o',
                            'params'=>'',
                            'where'=>'',
                        ),
                    ),
                ),
                array(
                    'url'  =>'pay/pay_order/c_channel/state.php',
                    'class'=>'pay-pay_order-c_channel-modify_channel_mode_state',
                    'ajax' =>'pay/pay_order/c_channel/modify_channel_mode_state',
                    'function'=>'modify_channel_mode_state',
                    'button_data'=>array(
                        array(
                            'name'=>'启用',
                            'icon'=>'fa-unlock',
                            'params'=>'&cms_state=0',
                            'where'=>array(
                                'cms_state'=>0,
                            ),
                        ),
                        array(
                            'name'=>'禁用',
                            'icon'=>'fa-lock',
                            'params'=>'&cms_state=1',
                            'where'=>array(
                                'cms_state'=>1,
                            ),
                        ),
                    ),
                ),
            );
            $arr_mode_list['system_file_list'] = $system_file_list;
        }
        //输出数据
        $this->load_view_file($arr_mode_list,__LINE__);
    }

    /**
     * 查询支付方式详情
     */
    public function item_channel_mode()
    {
        //参数验证
        //$this->control_params_check(array(
        //    'cms_id'=> array(
        //        'rule'   => 'notnull',
        //       'reason' => '支付方式非法'
        //    ),
        //),$this->arr_params);
        if(isset($this->arr_params['cms_system_partner']) && $this->arr_params['cms_system_partner'] == 1)
        {
            $this->arr_params['cms_user_id'] = $this->config->config['system_pay_manager'];
        }
        $arr_mode_info = $this->obj_channel_mode_logic->get_one($this->arr_params,'*');
        $this->load_view_file($arr_mode_info,__LINE__);
    }

    /**
     * 添加支付方式
     */
    public function add_channel_mode()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
            'cms_channel_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道非法'
            ),
            'cms_pay_appid'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台：APP ID'
            ),
            'cms_pay_partner_key'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台：商户KEY'
            ),
            'cms_pay_partner_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台：商户ID'
            ),
            'cms_notify_url'=> array(
                'rule'   => 'notnull',
                'reason' => '支付异步通知地址非法'
            ),
        ),$this->arr_params);

        $arr_mode_add = $this->obj_channel_mode_logic->add($this->arr_params);
        $this->load_view_file($arr_mode_add,__LINE__);
    }

    /**
     * 更新支付方式
     */
    public function edit_channel_mode()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付方式非法'
            ),
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
            'cms_channel_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付渠道非法'
            ),
            'cms_pay_appid'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台：APP ID'
            ),
            'cms_pay_partner_key'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台：商户KEY'
            ),
            'cms_pay_partner_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付平台：商户ID'
            ),
            'cms_notify_url'=> array(
                'rule'   => 'notnull',
                'reason' => '支付异步通知地址非法'
            ),
        ),$this->arr_params);

        $arr_mode_edit = $this->obj_channel_mode_logic->edit($this->arr_params['cms_id'],$this->arr_params);
        $this->load_view_file($arr_mode_edit,__LINE__);
    }

    /**
     * 删除支付方式
     */
    public function del_channel_mode()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'notnull',
                'reason' => '支付方式非法'
            ),
        ),$this->arr_params);

        $arr_mode_del = $this->obj_channel_mode_logic->del($this->arr_params['cms_id']);
        $this->load_view_file($arr_mode_del,__LINE__);
    }

    /**
     * 更新支付方式状态
     */
    public function modify_channel_mode_state()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_id'=> array(
                'rule'   => 'array',
                'reason' => '支付方式非法'
            ),
            'cms_state'=> array(
                'rule'   => 'in',
                'in'     => '0,1',
                'reason' => '支付方式状态非法'
            ),
        ),$this->arr_params);
        //特殊处理
        $this->arr_params['cms_status'] = $this->arr_params['cms_state'] == 1 ? 0 : 1;
        unset($this->arr_params['cms_state']);
        $arr_partner_edit = $this->obj_channel_mode_logic->edit($this->arr_params['cms_id'],$this->arr_params);
        $this->load_view_file($arr_partner_edit,__LINE__);
    }

    /**
     * 处理支付方式中的证书文件
     */
    private function _handle_channel_credential()
    {



    }

/*==================================================== 支付方式管理 结束 ================================================*/

    /**
     * 初始化函数
     */
    private function _init()
    {
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_partner/logic_partner.class.php';
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_channel/logic_channel.class.php';
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/c_channel_mode/logic_channel_mode.class.php';

        $this->obj_partner_logic = new partner_logic($this);
        $this->obj_channel_logic = new channel_logic($this);
        $this->obj_channel_mode_logic = new channel_mode_logic($this);
    }

} 