<?php
/**
 * Created by PhpStorm.
 * Use : 积分业务逻辑
 * User: kan.yang@starcor.com
 * Date: 18-12-28
 * Time: 上午1:10
 */

class c_integral extends CI_Controller
{

    //项目基路径
    private $str_base_path    = '';
    //用户积分
    private $obj_user_integral_logic= null;
    //积分明细
    private $obj_integral_detail_logic= null;
    //sign key
    private $str_sign_key = '04997110aa2db7e27991ece0749064f4';

    /**
     * 默认构造函数
     */
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->database();
        $this->str_base_path = dirname(dirname(dirname(dirname(__DIR__))));
        $this->_init();
        //初始化在线用户ID
        $this->arr_params['cms_user_id'] = empty($this->arr_params['cms_user_id']) ? $_SESSION['user_id'] : $this->arr_params['cms_user_id'];
    }

/*================================================== 用户积分管理 开始 ==================================================*/

    /**
     * 用户积分列表
     */
    public function list_integral()
    {
        unset($this->arr_params['cms_user_id']);
        $str_field = 'i.*,o.cms_name cms_user_name';
        $str_join = 'system_user_integral i left join order_manager o on i.cms_user_id = o.cms_id';
        //查询数据
        $arr_integral = $this->obj_user_integral_logic->get_list($this->arr_params,$str_field,null,$str_join,'','i.cms_modify_time desc');
        //输出数据
        $this->load_view_file($arr_integral,__LINE__);
        return true;
    }


    /**
     * 查询用户积分
     */
    public function item_user_integral()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
        ),$this->arr_params);
        //异步验签
        if(isset($this->arr_params['flag_ajax_reurn']) && $this->arr_params['flag_ajax_reurn'] == 1)
        {
            $bool_sign = $this->_check_sign(array(
                'cms_user_id'   => $this->arr_params['cms_user_id'],
            ));
            if(!$bool_sign)
            {
                $this->load_view_file(array(
                    'ret' => 1,
                    'reason' => '验签失败',
                    'data_info' => array()
                ),__LINE__);
                return false;
            }
        }
        //查询数据
        $arr_user_integral = $this->obj_user_integral_logic->get_one($this->arr_params,'*');
        //整理用户积分
        if($arr_user_integral['ret'] != 0 || empty($arr_user_integral['data_info']))
        {
            $arr_user_integral = array(
                'ret' => 0,
                'reason' => 'ok',
                'data_info' => array(
                    'cms_user_id'     => empty($arr_query_params['cms_user_id']) ? '' : $arr_query_params['cms_user_id'],
                    'cms_integral'    => 0,
                    'cms_integral_history' => 0,
                )
            );
        }
        //输出数据
        $this->load_view_file($arr_user_integral,__LINE__);
        return true;
    }


/*================================================== 用户积分管理 结束 ==================================================*/


/*================================================== 积分明细管理 开始 ==================================================*/

    /**
     * 查询积分明细列表
     */
    public function list_integral_detail()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
        ),$this->arr_params);
        $this->need_login = false;
        //异步验签
        if(isset($this->arr_params['flag_ajax_reurn']) && $this->arr_params['flag_ajax_reurn'] == 1)
        {
            $bool_sign = $this->_check_sign(array(
                'cms_user_id'      => $this->arr_params['cms_user_id'],
                'cms_integral_type'=> $this->arr_params['cms_integral_type'],
                'cms_incr_decr'    => $this->arr_params['cms_incr_decr'],
                'cms_page_num'     => $this->arr_page_params['cms_page_num'],
                'cms_page_size'    => $this->arr_page_params['cms_page_size'],
            ));
            if(!$bool_sign)
            {
                $this->load_view_file(array(
                    'ret' => 1,
                    'reason'    => '验签失败',
                    'data_info' => array(),
                    'page_info' => array(
                        'cms_page_num'  => $this->arr_params['cms_page_num'],
                        'cms_page_size' =>$this->arr_params['cms_page_size'],
                        'cms_data_count'=>0,
                    ),
                ),__LINE__);
                return false;
            }
        }
        //分页处理
        $this->_init_page();
        $arr_channel_list = $this->obj_integral_detail_logic->get_list($this->arr_params,'*');
        //输出数据
        $this->load_view_file($arr_channel_list,__LINE__);
        return true;
    }

    /**
     * 添加积分明细
     */
    public function add_integral_detail()
    {
        //参数验证
        $this->control_params_check(array(
            'cms_integral_type'=> array(
                'rule'   => 'in',
                'in'     => '0,1,2',
                'reason' => '积分类型非法'
            ),
            'cms_integral'=> array(
                'rule'   => 'number',
                'reason' => '积分值非法'
            ),
            'cms_incr_decr'=> array(
                'rule'   => 'in',
                'in'     => '0,1',
                'reason' => '增减积分类型非法'
            ),
            'cms_user_id'=> array(
                'rule'   => 'notnull',
                'reason' => '用户非法'
            ),
        ),$this->arr_params);

        $arr_partner_add = $this->obj_integral_detail_logic->add($this->arr_params);
        $this->load_view_file($arr_partner_add,__LINE__);
    }

/*================================================== 积分明细管理 结束 ==================================================*/


    /**
     * 初始化函数
     */
    private function _init()
    {
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/user_integral/logic_user_integral.class.php';
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/integral_detail/logic_integral_detail.class.php';

        $this->obj_user_integral_logic = new user_integral_logic($this);
        $this->obj_integral_detail_logic = new integral_detail_logic($this);
    }

    /**
     * 验签
     */
    private function _check_sign($arr_sign_params)
    {
        $str_sign = $this->str_sign_key;
        foreach($arr_sign_params as $val)
        {
            $str_sign .= strlen($val) > 0 ? $val : '';
        }
        return $this->arr_params['sign'] == md5($str_sign);
    }

} 