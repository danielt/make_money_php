<?php
/**
 * Created by PhpStorm.
 * Use : 支付回调
 * User: kan.yang@starcor.com
 * Date: 18-12-16
 * Time: 上午11:29
 */
header("content-type:text/html;charset=utf-8");
ini_set('date.timezone','Asia/Shanghai');
ini_set('display_errors','1');
defined('BASEPATH') or exit('No direct script access allowed');
class c_notify extends CI_Controller
{

    /**
     * 默认构造函数
     */
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->flag_ajax_reurn = true;
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->database();
    }

    /**
     * 微信支付回调
     */
    public function wx_notify()
    {
        //提取回调数据
        $obj_notify_data = !empty($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : file_get_contents('php://input');
        //引入文件
        include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/order/pay/pay_notify/c_wx_notify.php';
        $obj_logic_wx_notify = new logic_wx_notify($this);

        return $obj_logic_wx_notify->notify($obj_notify_data);
    }

    /**
     * 支付宝支付回调
     */
    public function alipay_notify()
    {
        //引入文件
        include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/order/pay/pay_notify/c_alipay_notify.php';
        $obj_logic_alipay_notify = new logic_alipay_notify($this);

        //回调数据
        $obj_file_input = file_get_contents('php://input');
        $obj_notify_data = !empty($_POST) ? $_POST : (!empty($obj_file_input) ? $obj_file_input : $_GET);

        return $obj_logic_alipay_notify->notify($obj_notify_data);
    }


} 