<?php
/**
 * @Description：点播或者直播资源库栏目
 * @Author：xinxin.deng
 * @CreateTime：2019/5/5 9:50
 */
defined('BASEPATH') or exit('No direct script access allowed');

class c_category extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;//暂且设置为不需要登录验证
        parent::__construct($str_class, $str_file, $str_method, $str_directory);

        $this->flag_ajax_reurn = true;//以json的格式返回数据
    }

    public function index()
    {
        $where_params = array('where' => ($this->arr_params));
        $category_list = $this->auto_load_table('wintv', 'category', 'c_category', 'category', 'query', $where_params);

        if ($category_list['ret'] != 0 || !is_array($category_list['data_info']) || empty($category_list['data_info']))
        {
            $re = em_return::return_data(1, '数据不存在');
            $this->load_view_file($re, __LINE__);
        }

        $list = array();

        //组装栏目数据
        foreach ($category_list['date_info'] as $category)
        {
            if ($category['cms_parent_id'] == '0')
            {
                $children = $this->_get_category_children($category['cms_category_id'], $category_list['date_info']);
                $temp = array(
                    'name'=>'category',
                    'attributes' => array(
                        'id' => $category['cms_category_id'],
                        'name' => $category['cms_name'],
                        'parent' => $category['cms_parent_id'],
                        'order'  => $category['cms_order']
                    ),
                );

                if(!empty($children))
                {
                    $temp['children'] = $children;
                }

                $list[] = $temp;
            }
        }

        $column_params = array(
            'where' => array(
                'type' => 0,
            )
        );

        //查询跟栏目
        $column = $this->auto_load_table('wintv', 'column', 'c_column', 'column', 'query_only', $column_params);
        if ($column['ret'] != 0 || !is_array($column['data_info']) || empty($column['data_info'])) {
            $re = em_return::return_data(1, '数据不存在');
            $this->load_view_file($re, __LINE__);
        }

        //媒资库信息
        $arr_list['depot_info'] = array(
            "id"    => $column['data_info']['cms_id'],
            "platform_id"=> $column['data_info']['cms_platform_id'],
            "platform_type"=> $column['data_info']['cms_platform_type'],
            "type"  => $column['data_info']['cms_type']
        );

        //定义顶级根栏目
        $arr_list['attributes'] = array(
            "id"    => "10000",
            "name"  => "资源库",
            "parent"=> "0",
            "cp_id" => "",
        );
        $arr_list['list'] = $list;

        $this->load_view_file($arr_list, __LINE__);
    }

    public function add()
    {
        $params = array(
            'where' => array(
                'parent_id' => $this->arr_params['parent_id'],
                'category_id' => $this->arr_params['category_id'],
                'column_id' => $this->arr_params['column_id'],
                'type' => $this->arr_params['type'],
            )
        );

        //先查询是否已经存在了
        $exist = $this->auto_load_table('wintv', 'category', 'c_category', 'category', 'query_only', $params);
        if ($exist['ret'] == 0 && is_array($exist['data_info']) && count($exist['data_info']) > 0) {
            $re = em_return::return_data(1, '栏目ID已经存在');
            $this->load_view_file($re, __LINE__);
        }

        //检查同级目录是否存在相同名称的栏目
        $params = array(
            'where' => array(
                'parent_id' => $this->arr_params['parent_id'],
                'name' => $this->arr_params['name'],
                'column_id' => $this->arr_params['column_id'],
                'type' => $this->arr_params['type'],
            )
        );

        $exist = $this->auto_load_table('wintv', 'category', 'c_category', 'category', 'query_only', $params);
        if ($exist['ret'] == 0 && is_array($exist['data_info']) && !empty($exist['data_info'])) {
            $re = em_return::return_data(1, '栏目名称已经存在');
            $this->load_view_file($re, __LINE__);
        }

        $add_params = array(
            'insert' => $this->arr_params
        );

        //执行添加
        $add_re = $this->auto_load_table('wintv', 'category', 'c_category', 'category', 'add', $add_params);

        $this->load_view_file($add_re, __LINE__);
    }

    public function edit()
    {
        if (empty($this->arr_params['id']) || !isset($this->arr_params['id']))
        {
            $re = em_return::return_data(1, 'ID无效');
            $this->load_view_file($re, __LINE__);
        }

        $cms_id = $this->arr_params['id'];
        unset($this->arr_params['cms_id']);

        $edit_params = array(
            'set' => $this->arr_params,
            'where' => array(
                'id' => $cms_id,
            ),
        );
        $edit_params['set']['modify_time'] = date('Y-m-d H:i:s');

        $edit_re = $this->auto_load_table('wintv', 'category', 'c_category', 'category', 'edit', $edit_params);

        $this->load_view_file($edit_re, __LINE__);
    }

    public function delete()
    {
        $del_params = array(
            'where' => array(
                'id' => $this->arr_params['id']
            )
        );

        $edit_re = $this->auto_load_table('wintv', 'category', 'c_category', 'category', 'rel_del', $del_params);

        $this->load_view_file($edit_re, __LINE__);
    }

    /**
     * @Description：递归整理栏目及其子栏目
     * @param $parent_id
     * @param $data_list
     * @return array
     * @Author：xinxin.deng
     * @CreateTime：2019/5/5 17:51
     */
    private function _get_category_children($parent_id, &$data_list)
    {
        $children_list = array();
        foreach ($data_list as $key=> $item)
        {
            if($parent_id === $item['cms_parent_id'])
            {
                $children = $this->_get_category_children($item['cms_category_id'], $data_list);
                $temp = array(
                    'attributes' => array(
                        'id' => $item['cms_category_id'],
                        'name' => $item['cms_name'],
                        'parent' => $item['cms_parent_id'],
                        'order'  => $item['cms_order'],
                    ),
                    'children' => $children,
                );

                $children_list[] = $temp;
            }
        }
        return $children_list;
    }
}