<?php
/**
 * @Description：平台、运营商
 * @Author：xinxin.deng
 * @CreateTime：2019/5/5 10:04
 */
defined('BASEPATH') or exit('No direct script access allowed');

class c_platform extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;//暂且设置为不需要登录验证
        parent::__construct($str_class, $str_file, $str_method, $str_directory);

        $this->flag_ajax_reurn = true;//以json的格式返回数据
    }

    public function index()
    {
        $where_params = array('where' => ($this->arr_params));
        $list = $this->auto_load_table('wintv', 'platform', 'c_platform', 'platform', 'query', $where_params);
        $this->load_view_file($list, __LINE__);
    }

    public function add()
    {
        $params = array(
            'where' => array(
                'name' => $this->arr_params['name'],
            )
        );

        //先查询是否已经存在了
        $exist = $this->auto_load_table('wintv', 'platform', 'c_platform', 'platform', 'query_only', $params);
        if ($exist['ret'] == 0 && is_array($exist['data_info']) && count($exist['data_info']) > 0) {
            $re = em_return::return_data(1, '该数据已经存在');
            $this->load_view_file($re, __LINE__);
        }

        $add_params = array(
            'insert' => $this->arr_params
        );

        //执行添加
        $add_re = $this->auto_load_table('wintv', 'platform', 'c_platform', 'platform', 'add', $add_params);

        $this->load_view_file($add_re, __LINE__);
    }

    public function edit()
    {
        if (empty($this->arr_params['id']) || !isset($this->arr_params['id']))
        {
            $re = em_return::return_data(1, 'ID无效');
            $this->load_view_file($re, __LINE__);
        }

        $cms_id = $this->arr_params['id'];
        unset($this->arr_params['id']);

        $edit_params = array(
            'set' => $this->arr_params,
            'where' => array(
                'id' => $cms_id,
            ),
        );
        $edit_params['set']['modify_time'] = date('Y-m-d H:i:s');

        $edit_re = $this->auto_load_table('wintv', 'platform', 'c_platform', 'platform', 'edit', $edit_params);

        $this->load_view_file($edit_re, __LINE__);
    }

    public function delete()
    {
        $del_params = array(
            'where' => array(
                'id' => $this->arr_params['id']
            )
        );

        $edit_re = $this->auto_load_table('wintv', 'platform', 'c_platform', 'platform', 'rel_del', $del_params);

        $this->load_view_file($edit_re, __LINE__);
    }
}