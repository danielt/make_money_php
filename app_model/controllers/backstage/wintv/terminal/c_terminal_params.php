<?php
/**
 * @Description：终端下行参数控制器
 * @Author：xinxin.deng
 * @CreateTime：2019/4/28 10:40
 */

defined('BASEPATH') or exit('No direct script access allowed');

class c_terminal_params extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;//暂且设置为不需要登录验证
        parent::__construct($str_class, $str_file, $str_method, $str_directory);

        $this->flag_ajax_reurn = true;//以json的格式返回数据
    }

    public function index()
    {
        $where_params = array('where' => ($this->arr_params));
        $list = $this->auto_load_table('wintv', 'terminal', 'c_terminal', 'terminal_params', 'query', $where_params);
        $this->load_view_file($list, __LINE__);
    }

    public function add()
    {
        $params = array(
            'where' => array(
                'key' => $this->arr_params['key'],
            )
        );

        //先查询是否已经存在了
        $exist = $this->auto_load_table('wintv', 'terminal', 'c_terminal', 'terminal_params', 'query_only', $params);
        if ($exist['ret'] == 0 && is_array($exist['data_info']) && count($exist['data_info']) > 0) {
            $re = em_return::return_data(1, '该数据已经存在');
            $this->load_view_file($re, __LINE__);
        }

        $time = date('Y-m-d H:i:s');
        $add_params = array(
            'insert' => array(
                'key' => $this->arr_params['key'],
                'value' => $this->arr_params['value'],
                'group' => $time,
            )
        );

        //执行添加
        $add_re = $this->auto_load_table('wintv', 'terminal', 'c_terminal', 'terminal_params', 'add', $add_params);

        $this->load_view_file($add_re, __LINE__);
    }

    public function edit()
    {
        if (empty($this->arr_params['id']) || !isset($this->arr_params['id']))
        {
            $re = em_return::return_data(1, 'ID无效');
            $this->load_view_file($re, __LINE__);
        }

        $cms_id = $this->arr_params['id'];
        unset($this->arr_params['cms_id']);

        $edit_params = array(
            'set' => $this->arr_params,
            'where' => array(
                'id' => $cms_id,
            ),
        );
        $edit_params['set']['modify_time'] = date('Y-m-d H:i:s');

        $edit_re = $this->auto_load_table('wintv', 'terminal', 'c_terminal', 'terminal_params', 'edit', $edit_params);

        $this->load_view_file($edit_re, __LINE__);
    }

    public function delete()
    {
        $del_params = array(
            'where' => array(
                'id' => $this->arr_params['id']
            )
        );

        $edit_re = $this->auto_load_table('wintv', 'terminal', 'c_terminal', 'terminal_params', 'rel_del', $del_params);

        $this->load_view_file($edit_re, __LINE__);
    }
}