<?php
/**
 * Created by <stacrcor.com>.
 * Author: xinxin.deng
 * Date: 2018/11/29 10:58
 */
defined('BASEPATH') or exit('No direct script access allowed');

class c_manager extends CI_Controller
{

    private $str_base_path = '';
    //用户积分
    private $obj_user_integral_logic= null;
    //积分明细
    private $obj_integral_detail_logic= null;

    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->library('session');
        //加载自定义配置文件
        $this->config->load('config_app', true);

        $this->str_base_path = dirname(dirname(dirname(dirname(__DIR__))));

        include_once $this->str_base_path . '/logic/backstage/order/buy_order/user_integral/logic_user_integral.class.php';
        include_once $this->str_base_path . '/logic/backstage/order/buy_order/integral_detail/logic_integral_detail.class.php';

        $this->obj_user_integral_logic = new user_integral_logic($this);
        $this->obj_integral_detail_logic = new integral_detail_logic($this);
    }

    /**
     * 判断用户上下线，0表示在线，1表示下线
     * @return array
     */
    public function check_online()
    {
        $this->flag_ajax_reurn = true;
        $max_online_time = $this->config->item('no_operation_auto_unline_according_to_minute');
        $max_online_time = isset($max_online_time) && $max_online_time != "" ? $max_online_time : 30;
        //无操作自动下线
        if ($this->session->userdata() && $this->session->userdata('user_online_time'))
        {
            if ((time() - $this->session->userdata('user_online_time')) > $max_online_time*60)
            {
                $this->session->sess_destroy();
                $data = array(
                    'ret' => 1,
                    'reason' => 'offline'
                );
            }
            else
            {
                $this->session->set_userdata(array('user_online_time' => time()));
                $data = array(
                    'ret' => 0,
                    'reason' => 'online'
                );
            }
        }
        else
        {
            $data = array(
                'ret' => 1,
                'reason' => 'offline'
            );
        }
        $this->load_view_file($data, __LINE__);
    }

    public function test()
    {
        $this->load_view();
    }

    /**
     * 个人中心
     */
    public function index()
    {
        if (empty($this->session->userdata('telephone')) || empty($this->session->userdata('role_id'))) {
            echo "<script>window.location.href='../../../order/con_manager/c_manager/login';</script>";
            die;
        }

        $this->_init_page();

        if (empty($this->session->userdata['telephone']) || !isset($this->session->userdata['telephone'])) {
            $re = em_return::return_data(1, '手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        if (empty($this->session->userdata['role_id']) || !isset($this->session->userdata['role_id'])) {
            $re = em_return::return_data(1, '角色不能为空');
            $this->load_view_file($re, __LINE__);
        }
        if (empty($this->session->userdata['user_id']) || !isset($this->session->userdata['user_id'])) {
            $re = em_return::return_data(1, '用户id不能为空');
            $this->load_view_file($re, __LINE__);
        }

        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->session->userdata['telephone'],
                'role_id' => $this->session->userdata['role_id'],
                'id' => $this->session->userdata['user_id'],
            ),
        );
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) && count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在');
            $this->load_view_file($re, __LINE__);
        }
        switch ($user['data_info']['cms_role_id']) {
            case 1:
                $user['data_info']['cms_role_name'] = '订货客户';
                break;
            case 2:
                $user['data_info']['cms_role_name'] = '平台管理员';
                break;
            case 3:
                $user['data_info']['cms_role_name'] = '生产厂商';
                break;
            case 4:
                $user['data_info']['cms_role_name'] = '供应商';
                break;
            case 5:
                $user['data_info']['cms_role_name'] = '样板师';
                break;
            case 6:
                $user['data_info']['cms_role_name'] = '样衣师';
                break;
        }
        $params = array(
            'user' => $user['data_info'],
        );

        //用户积分
        $integral_params = array(
            'cms_user_id' => $this->session->userdata('user_id'),
        );
        //查询数据
        $arr_user_integral = $this->obj_user_integral_logic->get_one($integral_params,'*');

        //整理用户积分
        if($arr_user_integral['ret'] != 0 || empty($arr_user_integral['data_info']))
        {
            $arr_user_integral = array(
                'ret' => 0,
                'reason' => 'ok',
                'data_info' => array(
                    'cms_user_id'     => empty($arr_query_params['cms_user_id']) ? '' : $arr_query_params['cms_user_id'],
                    'cms_integral'    => 0,
                    'cms_integral_history' => 0,
                )
            );
        }
        $params['user']['cms_integral'] = $arr_user_integral['data_info']['cms_integral'];
        $params['user']['cms_integral_history'] = $arr_user_integral['data_info']['cms_integral_history'];
        $this->load_view_file($params, __LINE__);
    }

    /**
     * 登陆页面
     */
    public function login()
    {
        $this->load_view();
    }

    /**
     * 登陆动作
     */
    public function sigin()
    {
        if (empty($this->arr_params['telephone']) || !isset($this->arr_params['telephone'])) {
            $re = em_return::return_data(1, '手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        if (empty($this->arr_params['password']) || !isset($this->arr_params['password'])) {
            $re = em_return::return_data(1, '密码不能为空');
            $this->load_view_file($re, __LINE__);
        }

        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
            ),
        );
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) && count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在');
            $this->load_view_file($re, __LINE__);
        }
        //用户密码加密后进行验证
        if ($user['data_info']['cms_password'] != $this->arr_params['password']) {
            $re = em_return::return_data(1, '密码错误');
            $this->load_view_file($re, __LINE__);
        }
        //添加到session
        $data = array(
            'set' => array(
                'login_time' => date('Y-m-d H:i:s'),
                'user_ip' => $_SERVER['REMOTE_ADDR'],
                'login_count' => $user['data_info']['cms_login_count'] + 1,
            ),
            'where' => array(
                'id' => $user['data_info']['cms_id'],
            ),
        );
        $modify_info = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $data);
        if ($modify_info['ret'] != 0) {
            $re = em_return::return_data(1, '登陆异常，请联系超级管理员');
            $this->load_view_file($re, __LINE__);
        }
        if ($user['data_info']['cms_state'] == 1) {
            $re = em_return::return_data(1, '您已被禁止登录,请联系管理员');
            $this->load_view_file($re, __LINE__);
        }
        $data['set']['telephone'] = $this->arr_params['telephone'];
        $data['set']['user_id'] = isset($user['data_info']['cms_id']) ? $user['data_info']['cms_id'] : '';
        $data['set']['role_id'] = isset($user['data_info']['cms_role_id']) ? $user['data_info']['cms_role_id'] : '';
        $data['set']['user_online_time'] = time();//用户登录时间点
        $data['set']['cms_user_money'] = $user['data_info']['cms_user_money'];//用户余额
        $this->session->set_userdata($data['set']);
        switch ($user['data_info']['cms_role_id']) {
            case 1:
                $user['data_info']['cms_role_name'] = '订货商';
                break;
            case 2:
                $user['data_info']['cms_role_name'] = '平台管理员';
                break;
            case 3:
                $user['data_info']['cms_role_name'] = '生产商';
                break;
            case 4:
                $user['data_info']['cms_role_name'] = '供应商';
                break;
            case 5:
                $user['data_info']['cms_role_name'] = '样板师';
                break;
            case 6:
                $user['data_info']['cms_role_name'] = '样衣师';
                break;
        }
        $this->get_menu_button_cache($user['data_info']['cms_role_id']);
        $re = em_return::return_data(0, '登陆成功', $user['data_info']);
        $this->load_view_file($re, __LINE__);
    }

    /**
     * 退出登陆
     */
    public function logout()
    {
        //$this->session->unset_userdata('telephone');
        //$this->session->unset_userdata('user_id');
        //$this->session->unset_userdata('role_id');
        //$this->session->unset_userdata('login_time');
        //$this->session->unset_userdata('user_online_time');
        $this->session->sess_destroy();
        echo "<script>window.location.href='home';</script>";
        exit;
    }

    /**
     * 退出登陆
     */
    public function outline()
    {
        $this->flag_ajax_reurn = true;
        $this->session->sess_destroy();
        $data = array(
            'ret' => 0,
            'reason' => '退出成功'
        );
        $this->load_view_file($data, __LINE__);
    }
    /**
     * 注册页面
     */
    public function register()
    {
        $this->load_view_file(array('1', '2'), __LINE__);

    }

    /**
     * 注册动作
     */
    public function registry()
    {
        //手机号
        if (empty($this->arr_params['telephone']) || !isset($this->arr_params['telephone'])) {
            $re = em_return::return_data(1, '手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //密码
        if (empty($this->arr_params['password']) || !isset($this->arr_params['password']) || empty($this->arr_params['confirmPassword']) || !isset($this->arr_params['confirmPassword'])) {
            $re = em_return::return_data(1, '密码不能为空');
            $this->load_view_file($re, __LINE__);
        }
        if ($this->arr_params['password'] != $this->arr_params['confirmPassword']) {
            $re = em_return::return_data(1, '两次密码不一致');
            $this->load_view_file($re, __LINE__);
        }
        //用户角色
        if (empty($this->arr_params['role_id']) || !isset($this->arr_params['role_id'])) {
            $re = em_return::return_data(1, '请选择用户角色');
            $this->load_view_file($re, __LINE__);
        }
        //验证码
        if (empty($this->arr_params['num']) || !isset($this->arr_params['num'])) {
            $re = em_return::return_data(1, '验证码不正确');
            $this->load_view_file($re, __LINE__);
        }


        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
            ),
        );

        //先查询是否已经存在了
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] == 0 && is_array($user['data_info']) && count($user['data_info']) > 0) {
            $re = em_return::return_data(1, '用户已经存在,请直接登陆');
            $this->load_view_file($re, __LINE__);
        }

        //调取短信验证码，并验证短信
        include_once $this->str_base_path . '/logic/backstage/system/auto/c_smsg/system_smsg.class.php';
        $obj_system_smsg = new system_smsg($this, '');
        $arr_send_ret = $obj_system_smsg->check_verify_code($this->arr_params['telephone'], $this->arr_params['num']);
        if ($arr_send_ret['ret'] != 0) {
            $re = em_return::return_data(1, $arr_send_ret['reason']);
            $this->load_view_file($re, __LINE__);
        }
        $addr = isset($this->arr_params['city-picker3']) ? str_replace('/', '', $this->arr_params['city-picker3']) : '';
        $time = date('Y-m-d H:i:s');
        $add_params = array(
            'insert' => array(
                'telephone' => $this->arr_params['telephone'],
                'password' => $this->arr_params['password'],
                'create_time' => $time,
                'modify_time' => $time,
                'user_ip' => $_SERVER['REMOTE_ADDR'],
                'role_id' => $this->arr_params['role_id'],
                'sex' => $this->arr_params['sex'],
                'name' => $this->arr_params['username'],
                'company_name' => $this->arr_params['company_name'],
                'country' => $this->arr_params['country'],
                'address' => $addr
            )
        );


        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'add', $add_params);
        if ($add_user['ret'] != 0) {
            $re = em_return::return_data(1, '注册失败');
            $this->load_view_file($re, __LINE__);
        }

        //查询用户信息
        $params_q = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
            ),
        );
        $user_data = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params_q);

        if ($user_data['ret'] == 0)
        {
            $arr_incr_decr =  $this->config->item('pay_order_to_integral');
            //埋点，注册送用户积分
            $params_add_integral = array(
                'cms_uuid' => $this->get_guid(),
                'cms_integral_type' => '2',
                'cms_integral' => $arr_incr_decr[2],
                'cms_incr_decr' => '0',
                'cms_user_id' => $user_data['data_info']['cms_id'],
            );
            $this->obj_integral_detail_logic->add($params_add_integral);
        }

        $this->load_view_file($add_user, __LINE__);
    }

    /**
     * 完善修改资料
     */
    public function edit_profile()
    {
        $this->_init_page();

        //手机号
        if (empty($this->arr_params['telephone']) || !isset($this->arr_params['telephone'])) {
            $re = em_return::return_data(1, '手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }

        if (empty($this->arr_params['user_id']) || !isset($this->arr_params['user_id'])) {
            $re = em_return::return_data(1, '用户id不能为空');
            $this->load_view_file($re, __LINE__);
        }

        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
                'id' => $this->arr_params['user_id'],
            ),
        );
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) && count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在');
            $this->load_view_file($re, __LINE__);
        }


        ////微信号
        //if (!empty($this->arr_params['wx_num']) && isset($this->arr_params['wx_num'])) {
        //    $params['set']['wx_num'] = $this->arr_params['wx_num'];
        //}
        ////qq
        //if (!empty($this->arr_params['qq']) && isset($this->arr_params['qq'])) {
        //    $params['set']['qq'] = $this->arr_params['qq'];
        //}
        ////email
        //if (!empty($this->arr_params['email']) && isset($this->arr_params['email'])) {
        //    $params['set']['email'] = $this->arr_params['email'];
        //}
        ////头像
        //if (!empty($this->arr_params['cms_head_img']) && isset($this->arr_params['cms_head_img'])) {
        //    $params['set']['cms_head_img'] = $this->arr_params['cms_head_img'];
        //}
        ////国家
        //if (!empty($this->arr_params['country']) && isset($this->arr_params['country'])) {
        //    $params['set']['country'] = $this->arr_params['country'];
        //}
        ////地址
        //if (!empty($this->arr_params['address']) && isset($this->arr_params['address'])) {
        //    $params['set']['address'] = $this->arr_params['address'];
        //}
        ////成立时间
        //if (!empty($this->arr_params['establish_date']) && isset($this->arr_params['establish_date'])) {
        //    $params['set']['establish_date'] = date('Y-m-d', strtotime($this->arr_params['establish_date']));
        //}
        ////主营产品
        //if (!empty($this->arr_params['main_product']) && isset($this->arr_params['main_product'])) {
        //    $params['set']['cms_main_product'] = $this->arr_params['main_product'];
        //}
        ////销售渠道
        //if (!empty($this->arr_params['sale_channels']) && isset($this->arr_params['sale_channels'])) {
        //    $params['set']['sale_channels'] = $this->arr_params['sale_channels'];
        //}
        ////企业类型
        //if (!empty($this->arr_params['cms_company_type']) && isset($this->arr_params['cms_company_type'])) {
        //    $params['set']['cms_company_type'] = $this->arr_params['cms_company_type'];
        //}
        //
        ////对公银行账户数据信息
        //if (!empty($this->arr_params['bank_info']) && isset($this->arr_params['bank_info'])) {
        //    $params['set']['bank_info'] = $this->arr_params['bank_info'];
        //}
        ////递发货地址
        //if (!empty($this->arr_params['courier_info']) && isset($this->arr_params['courier_info'])) {
        //    $params['set']['courier_info'] = $this->arr_params['courier_info'];
        //}
        ////递发货地址收件人
        //if (!empty($this->arr_params['cms_courier_contact']) && isset($this->arr_params['cms_courier_contact'])) {
        //    $params['set']['cms_courier_contact'] = $this->arr_params['cms_courier_contact'];
        //}
        ////递发货收件人电话
        //if (!empty($this->arr_params['cms_courier_telephone']) && isset($this->arr_params['cms_courier_telephone'])) {
        //    $params['set']['cms_courier_telephone'] = $this->arr_params['cms_courier_telephone'];
        //}
        ////大件发货地址
        //if (!empty($this->arr_params['courier_big_info']) && isset($this->arr_params['courier_big_info'])) {
        //    $params['set']['courier_big_info'] = $this->arr_params['courier_big_info'];
        //}
        ////大件发货地址收件人
        //if (!empty($this->arr_params['cms_courier_big_contact']) && isset($this->arr_params['cms_courier_big_contact'])) {
        //    $params['set']['cms_courier_big_contact'] = $this->arr_params['cms_courier_big_contact'];
        //}
        ////大件发货收件人电话
        //if (!empty($this->arr_params['cms_courier_big_telephone']) && isset($this->arr_params['cms_courier_big_telephone'])) {
        //    $params['set']['cms_courier_big_telephone'] = $this->arr_params['cms_courier_big_telephone'];
        //}
        ////描述
        //if (!empty($this->arr_params['desc']) && isset($this->arr_params['desc'])) {
        //    $params['set']['desc'] = $this->arr_params['desc'];
        //}
        ///********订购商特殊必填 start***********/
        ////纳税人类型
        //if (!empty($this->arr_params['cms_taxpayer_type']) && isset($this->arr_params['cms_taxpayer_type'])) {
        //    $params['set']['cms_taxpayer_type'] = $this->arr_params['cms_taxpayer_type'];
        //}
        ////是否需要发票
        //if (!empty($this->arr_params['cms_need_invoice']) && isset($this->arr_params['cms_need_invoice'])) {
        //    $params['set']['cms_need_invoice'] = $this->arr_params['cms_need_invoice'];
        //}
        ////税号
        //if (!empty($this->arr_params['cms_duty_num']) && isset($this->arr_params['cms_duty_num'])) {
        //    $params['set']['cms_duty_num'] = $this->arr_params['cms_duty_num'];
        //}
        ////开户银行
        //if (!empty($this->arr_params['cms_base_bank_name']) && isset($this->arr_params['cms_base_bank_name'])) {
        //    $params['set']['cms_base_bank_name'] = $this->arr_params['cms_base_bank_name'];
        //}
        ////基本账号
        //if (!empty($this->arr_params['cms_base_bank_num']) && isset($this->arr_params['cms_base_bank_num'])) {
        //    $params['set']['cms_base_bank_num'] = $this->arr_params['cms_base_bank_num'];
        //}
        ////往来账户开户银行
        //if (!empty($this->arr_params['cms_contact_bank_name']) && isset($this->arr_params['cms_contact_bank_name'])) {
        //    $params['set']['cms_contact_bank_name'] = $this->arr_params['cms_contact_bank_name'];
        //}
        ////往来账号
        //if (!empty($this->arr_params['cms_contact_bank_num']) && isset($this->arr_params['cms_contact_bank_num'])) {
        //    $params['set']['cms_contact_bank_num'] = $this->arr_params['cms_contact_bank_num'];
        //}
        ///********订购商特殊必填 start***********/
        ///********生产商特殊必填 start***********/
        ////厂名
        //if (!empty($this->arr_params['cms_factor_name']) && isset($this->arr_params['cms_factor_name'])) {
        //    $params['set']['cms_factor_name'] = $this->arr_params['cms_factor_name'];
        //}
        ////生产性质
        //if (!empty($this->arr_params['cms_product_nature']) && isset($this->arr_params['cms_product_nature'])) {
        //    $params['set']['cms_product_nature'] = $this->arr_params['cms_product_nature'];
        //}
        ////生产链
        //if (!empty($this->arr_params['cms_production_chain']) && isset($this->arr_params['cms_production_chain'])) {
        //    $params['set']['cms_production_chain'] = $this->arr_params['cms_production_chain'];
        //}
        ////擅长产品
        //if (!empty($this->arr_params['cms_good_at_goods']) && isset($this->arr_params['cms_good_at_goods'])) {
        //    $params['set']['cms_good_at_goods'] = $this->arr_params['cms_good_at_goods'];
        //}
        ////设备状况
        //if (!empty($this->arr_params['cms_equipment_state']) && isset($this->arr_params['cms_equipment_state'])) {
        //    $params['set']['cms_equipment_state'] = $this->arr_params['cms_equipment_state'];
        //}
        ////车工人数
        //if (!empty($this->arr_params['cms_people_num']) && isset($this->arr_params['cms_people_num'])) {
        //    $params['set']['cms_people_num'] = $this->arr_params['cms_people_num'];
        //}
        ////产能
        //if (!empty($this->arr_params['cms_capacity']) && isset($this->arr_params['cms_capacity'])) {
        //    $params['set']['cms_capacity'] = $this->arr_params['cms_capacity'];
        //}
        ////工厂图片
        //if (!empty($this->arr_params['cms_factor_img']) && isset($this->arr_params['cms_factor_img'])) {
        //    $params['set']['cms_factor_img'] = $this->arr_params['cms_factor_img'];
        //}
        ////ISO证书
        //if (!empty($this->arr_params['cms_iso_certificate']) && isset($this->arr_params['cms_iso_certificate'])) {
        //    $params['set']['cms_iso_certificate'] = $this->arr_params['cms_iso_certificate'];
        //}
        ////人权证书
        //if (!empty($this->arr_params['cms_human_certificate']) && isset($this->arr_params['cms_human_certificate'])) {
        //    $params['set']['cms_human_certificate'] = $this->arr_params['cms_human_certificate'];
        //}
        ///*********生产商特殊必填 end************/
        ///*********供应商特殊必填 start**********/
        ////品种
        //if (!empty($this->arr_params['cms_retriever']) && isset($this->arr_params['cms_retriever'])) {
        //    $params['set']['cms_retriever'] = $this->arr_params['cms_retriever'];
        //}
        ////面料织法
        //if (!empty($this->arr_params['cms_fabric_weaving']) && isset($this->arr_params['cms_fabric_weaving'])) {
        //    $params['set']['cms_fabric_weaving'] = $this->arr_params['cms_fabric_weaving'];
        //}
        ////面料花型
        //if (!empty($this->arr_params['cms_fabric_pattern']) && isset($this->arr_params['cms_fabric_pattern'])) {
        //    $params['set']['cms_fabric_pattern'] = $this->arr_params['cms_fabric_pattern'];
        //}
        ////面料成分
        //if (!empty($this->arr_params['cms_fabric_composition']) && isset($this->arr_params['cms_fabric_composition'])) {
        //    $params['set']['cms_fabric_composition'] = $this->arr_params['cms_fabric_composition'];
        //}
        ////税务情况
        //if (!empty($this->arr_params['cms_taxation']) && isset($this->arr_params['cms_taxation'])) {
        //    $params['set']['cms_taxation'] = $this->arr_params['cms_taxation'];
        //}
        ////产品图片
        //if (!empty($this->arr_params['cms_goods_img']) && isset($this->arr_params['cms_goods_img'])) {
        //    $params['set']['cms_goods_img'] = $this->arr_params['cms_goods_img'];
        //}
        ////供应商性质
        //if (!empty($this->arr_params['cms_nature']) && isset($this->arr_params['cms_nature']))
        //{
        //    $params['set']['cms_nature'] =  $this->arr_params['cms_nature'];
        //}
        //
        ///*********供应商特殊必填 end************/
        ///*********生产商、供应商特殊选填 start**********/
        ////营业执照
        //if (!empty($this->arr_params['cms_business_license_img']) && isset($this->arr_params['cms_business_license_img'])) {
        //    $params['set']['cms_business_license_img'] = $this->arr_params['cms_business_license_img'];
        //}
        ////税务登记证
        //if (!empty($this->arr_params['cms_taxation_certificate']) && isset($this->arr_params['cms_taxation_certificate'])) {
        //    $params['set']['cms_taxation_certificate'] = $this->arr_params['cms_taxation_certificate'];
        //}
        ///*********生产商、供应商特殊选填 end************/
        ///*********订货商、供应商特殊选填 end************/
        ////企业名称
        //if (!empty($this->arr_params['cms_company_name']) && isset($this->arr_params['cms_company_name'])) {
        //    $params['set']['cms_company_name'] = $this->arr_params['cms_company_name'];
        //}
        ///*********生产商、供应商特殊选填 end************/
        ///*********样板师特殊必填 start**********/
        ////使用软件
        //if (!empty($this->arr_params['cms_use_software']) && isset($this->arr_params['cms_use_software'])) {
        //    $params['set']['cms_use_software'] = $this->arr_params['cms_use_software'];
        //}
        ////打印设备
        //if (!empty($this->arr_params['cms_printer_equipment']) && isset($this->arr_params['cms_printer_equipment'])) {
        //    $params['set']['cms_printer_equipment'] = $this->arr_params['cms_printer_equipment'];
        //}
        ////制板费
        //if (strlen($this->arr_params['cms_plateing_price']) > 0 && isset($this->arr_params['cms_plateing_price'])) {
        //    $params['set']['cms_plateing_price'] = $this->arr_params['cms_plateing_price'];
        //}
        ////推板放板费
        //if (strlen($this->arr_params['cms_pushing_plate_price']) > 0 && isset($this->arr_params['cms_pushing_plate_price'])) {
        //    $params['set']['cms_pushing_plate_price'] = $this->arr_params['cms_pushing_plate_price'];
        //}
        ///*********样板师特殊必填 end************/
        ///*********样衣师特殊必填 start**********/
        ////擅长布料
        //if (!empty($this->arr_params['cms_good_at_material']) && isset($this->arr_params['cms_good_at_material'])) {
        //    $params['set']['cms_good_at_material'] = $this->arr_params['cms_good_at_material'];
        //}
        ////自有设备
        //if (!empty($this->arr_params['cms_own_equipment']) && isset($this->arr_params['cms_own_equipment'])) {
        //    $params['set']['cms_own_equipment'] = $this->arr_params['cms_own_equipment'];
        //}
        ////擅长款式
        //if (!empty($this->arr_params['cms_good_at_model']) && isset($this->arr_params['cms_good_at_model'])) {
        //    $params['set']['cms_good_at_model'] = $this->arr_params['cms_good_at_model'];
        //}
        ///*********样衣师特殊必填 end************/
        ///*********样衣师、样板师特殊 start**********/
        ////是否专职
        //if (strlen($this->arr_params['cms_is_full_time']) > 0 && isset($this->arr_params['cms_is_full_time'])) {
        //    $params['set']['cms_is_full_time'] = $this->arr_params['cms_is_full_time'];
        //}
        ////有无工作室
        //if (strlen($this->arr_params['cms_have_workroom']) > 0 && isset($this->arr_params['cms_have_workroom'])) {
        //    $params['set']['cms_have_workroom'] = $this->arr_params['cms_have_workroom'];
        //}
        ////擅长风格
        //if (!empty($this->arr_params['cms_good_at_style']) && isset($this->arr_params['cms_good_at_style'])) {
        //    $params['set']['cms_good_at_style'] = $this->arr_params['cms_good_at_style'];
        //}
        //*********样衣师、样板师 end************/

        $params['set'] = $this->arr_params;

        //用户名
        if (!empty($this->arr_params['username']) && isset($this->arr_params['username'])) {
            $params['set']['name'] = $this->arr_params['username'];
        }
        $params['set']['modify_time'] = date('Y-m-d H:i:s');

        $modify_info = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $params);
        if ($modify_info['ret'] != 0) {
            $re = em_return::return_data(1, '修改失败,原因' . $modify_info['reason']);
            $this->load_view_file($re, __LINE__);
        }
        if ($this->arr_params['flag_ajax_reurn'] == 1) {
            $params = array(
                'where' => array(
                    'telephone' => $this->arr_params["telephone"],
                    'id' => $this->arr_params["user_id"],
                ),
            );
            $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
            $modify_info["data_info"] = $user['data_info'];
            $this->load_view_file($modify_info, __LINE__);
        }


        $this->load_view_file($modify_info, __LINE__);
    }

    /**
     * 通过手机短信找回密码，重置密码
     */
    public function re_password()
    {
        if (empty($this->arr_params) || !isset($this->arr_params) || is_null($this->arr_params)) {
            $this->load_view_file(array('1', '2'), __LINE__);
        } else {
            //手机号
            if (empty($this->arr_params['telephone']) || !isset($this->arr_params['telephone'])) {
                $re = em_return::return_data(1, '手机号不能为空');
                $this->load_view_file($re, __LINE__);
            }
            //密码
            if (empty($this->arr_params['password']) || !isset($this->arr_params['password']) || empty($this->arr_params['confirmPassword']) || !isset($this->arr_params['confirmPassword'])) {
                $re = em_return::return_data(1, '密码不能为空');
                $this->load_view_file($re, __LINE__);
            }
            if ($this->arr_params['password'] != $this->arr_params['confirmPassword']) {
                $re = em_return::return_data(1, '两次密码不一致');
                $this->load_view_file($re, __LINE__);
            }
            //验证码
            if (empty($this->arr_params['num']) || !isset($this->arr_params['num'])) {
                $re = em_return::return_data(1, '验证码不正确');
                $this->load_view_file($re, __LINE__);
            }

            //验证用户是否存在
            $params = array(
                'where' => array(
                    'telephone' => $this->arr_params['telephone'],
                ),
            );

            //先查询是否已经存在了
            $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
            if ($user['ret'] != 0 || !is_array($user['data_info']) || count($user['data_info']) < 1) {
                $re = em_return::return_data(1, '用户不存在或找回密码异常');
                $this->load_view_file($re, __LINE__);
            }

            //调取短信验证码，并验证短信
            include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/system/auto/c_smsg/system_smsg.class.php';
            $obj_system_smsg = new system_smsg($this, '');
            $arr_send_ret = $obj_system_smsg->check_verify_code($this->arr_params['telephone'], $this->arr_params['num']);
            if ($arr_send_ret['ret'] != 0) {
                $re = em_return::return_data(1, $arr_send_ret['reason']);
                $this->load_view_file($re, __LINE__);
            }

            $edit_params = array(
                'set' => array(
                    'login_time' => date('Y-m-d H:i:s'),
                    'modify_time' => date('Y-m-d H:i:s'),
                    'pass_word_modify_time' => date('Y-m-d H:i:s'),
                    'password' => $this->arr_params['password']
                ),
                'where' => array(
                    'id' => $user['data_info']['cms_id'],
                    'telephone' => $user['data_info']['cms_telephone']
                ),
            );
            $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params);
            if ($add_user['ret'] != 0) {
                $re = em_return::return_data(1, '重置密码失败');
                $this->load_view_file($re, __LINE__);
            }
            $this->load_view_file($add_user, __LINE__);
        }

    }

    /**
     * 后台修改密码
     */
    public function edit_password()
    {
        $this->flag_ajax_reurn = true;
        //手机号
        if (empty($this->arr_params['telephone']) || !isset($this->arr_params['telephone'])) {
            $re = em_return::return_data(1, '手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //用户id
        if (empty($this->arr_params['user_id']) || !isset($this->arr_params['user_id'])) {
            $re = em_return::return_data(1, '用户id不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //原密码
        if (empty($this->arr_params['old_password']) || !isset($this->arr_params['old_password']) ||
            empty($this->arr_params['new_password']) || !isset($this->arr_params['new_password'])
            || empty($this->arr_params['confirmPassword']) || !isset($this->arr_params['confirmPassword'])) {
            $re = em_return::return_data(1, '密码不能为空');
            $this->load_view_file($re, __LINE__);
        }
        if ($this->arr_params['new_password'] != $this->arr_params['confirmPassword']) {
            $re = em_return::return_data(1, '两次新密码不一致');
            $this->load_view_file($re, __LINE__);
        }

        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
                'id' => $this->arr_params['user_id'],
            ),
        );

        //先查询是否已经存在了
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) || count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在或找回密码异常');
            $this->load_view_file($re, __LINE__);
        }
        //用户密码加密后进行验证
        if ($user['data_info']['cms_password'] != $this->arr_params['old_password']) {
            $re = em_return::return_data(1, '旧密码输入错误');
            $this->load_view_file($re, __LINE__);
        }

        $edit_params = array(
            'set' => array(
                'modify_time' => date('Y-m-d H:i:s'),
                'pass_word_modify_time' => date('Y-m-d H:i:s'),
                'password' => $this->arr_params['new_password']
            ),
            'where' => array(
                'id' => $user['data_info']['cms_id'],
                'telephone' => $user['data_info']['cms_telephone']
            ),
        );
        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params);
        if ($add_user['ret'] != 0) {
            $re = em_return::return_data(1, '重置密码失败');
            $this->load_view_file($re, __LINE__);
        }
        //修改后将session重置，然后进行重新登录
        //$this->session->unset_userdata('telephone');
        //$this->session->unset_userdata('user_id');
        //$this->session->unset_userdata('role_id');

        $this->load_view_file($add_user, __LINE__);
    }

    /**
     * 后台修改手机号
     */
    public function edit_telephone()
    {
        $this->flag_ajax_reurn = true;
        //手机号
        if (empty($this->arr_params['old_telephone']) || !isset($this->arr_params['old_telephone'])) {
            $re = em_return::return_data(1, '旧手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //用户id
        if (empty($this->arr_params['user_id']) || !isset($this->arr_params['user_id'])) {
            $re = em_return::return_data(1, '用户id不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //旧手机验证码
        if (empty($this->arr_params['num']) || !isset($this->arr_params['num'])) {
            $re = em_return::return_data(1, '旧手机验证码不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //新手机号
        if (empty($this->arr_params['new_telephone']) || !isset($this->arr_params['new_telephone'])) {
            $re = em_return::return_data(1, '新手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //新手机验证码
        if (empty($this->arr_params['re_num']) || !isset($this->arr_params['re_num'])) {
            $re = em_return::return_data(1, '新手机验证码不能为空');
            $this->load_view_file($re, __LINE__);
        }


        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['old_telephone'],
                'id' => $this->arr_params['user_id'],
            ),
        );

        //先查询是否已经存在了
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) || count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在或找回密码异常');
            $this->load_view_file($re, __LINE__);
        }

        //验证新是否存在
        $new_params = array(
            'where' => array(
                'telephone' => $this->arr_params['new_telephone']
            ),
        );
        //先查询是否已经存在了
        $new_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $new_params);
        if ($new_user['ret'] != 0) {
            $re = em_return::return_data(1, '用户不存在');
            $this->load_view_file($re, __LINE__);
        }
        if ($new_user['ret'] == 0 && is_array($new_user['data_info']) && count($new_user['data_info']) > 0) {
            $re = em_return::return_data(1, '新手机号已经存在,请直接登陆');
            $this->load_view_file($re, __LINE__);
        }
        //调取短信验证码，并验证短信
        include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/system/auto/c_smsg/system_smsg.class.php';
        $obj_system_smsg = new system_smsg($this, '');
        $arr_send_ret = $obj_system_smsg->check_verify_code($this->arr_params['old_telephone'], $this->arr_params['num']);
        if ($arr_send_ret['ret'] != 0) {
            $re = em_return::return_data(1, '旧手机' . $arr_send_ret['reason']);
            $this->load_view_file($re, __LINE__);
        }
        $arr_send_ret = $obj_system_smsg->check_verify_code($this->arr_params['new_telephone'], $this->arr_params['re_num']);
        if ($arr_send_ret['ret'] != 0) {
            $re = em_return::return_data(1, '新手机' . $arr_send_ret['reason']);
            $this->load_view_file($re, __LINE__);
        }

        $edit_params = array(
            'set' => array(
                'modify_time' => date('Y-m-d H:i:s'),
                'telephone' => $this->arr_params['new_telephone'],
            ),
            'where' => array(
                'id' => $user['data_info']['cms_id'],
            ),
        );
        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params);
        if ($add_user['ret'] != 0) {
            $re = em_return::return_data(1, '修改手机号失败');
            $this->load_view_file($re, __LINE__);
        }
        //修改后将session的手机号重置
        $this->session->set_userdata(array('telephone' => $this->arr_params['new_telephone']));

        $this->load_view_file($add_user, __LINE__);
    }

    /**
     * 用户管理
     */
    public function user_list()
    {
        $system_file_list = array(
            array(
                'url' => 'order/con_manager/c_manager/user_add.php',//右侧按钮弹框
                'class' => 'order_con_manager_c_manager_user_add',//form表单ID
                'ajax' => 'order/con_manager/c_manager/user_add',//form表单提交控制器
                'function' => 'add',//行为动作
                'button_data' => array(
                    array(
                        'name' => '添加',
                        'icon' => 'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url' => 'order/con_manager/c_manager/user_edit.php',//右侧按钮弹框
                'class' => 'order_con_manager_c_manager_user_edit',//form表单ID
                'ajax' => 'order/con_manager/c_manager/user_edit',//form表单提交控制器
                'function' => 'edit',//行为动作
                'button_data' => array(
                    array(
                        'name' => '修改信息',
                        'icon' => 'fa-pencil-square-o',//样式
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url' => 'order/con_manager/c_manager/user_edit_password.php',//右侧按钮弹框
                'class' => 'order_con_manager_c_manager_user_edit_password',//form表单ID
                'ajax' => 'order/con_manager/c_manager/user_edit_password',//form表单提交控制器
                'function' => 'edit',//行为动作
                'button_data' => array(
                    array(
                        'name' => '修改密码',
                        'icon' => 'fa-pencil-square-o',//样式
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url' => 'order/con_manager/c_manager/user_state.php',
                'class' => 'order_con_manager_c_manager_user_state',
                'ajax' => 'order/con_manager/c_manager/user_state',
                'function' => 'state',
                'button_data' => array(
                    array(
                        'name' => '启用',
                        'icon' => 'fa-unlock',
                        'params' => '&cms_state=0',
                        'where' => array(
                            'cms_state' => 0,
                        ),
                    ),
                    array(
                        'name' => '禁用',
                        'icon' => 'fa-lock',
                        'params' => '&cms_state=1',
                        'where' => array(
                            'cms_state' => 1,
                        ),
                    ),
                ),
            ),
            array(
                'url' => 'order/con_manager/c_manager/user_del.php',
                'class' => 'order_con_manager_c_manager_user_del',
                'ajax' => 'order/con_manager/c_manager/user_del',
                'function' => 'delete',
                'button_data' => array(
                    array(
                        'name' => '删除',
                        'icon' => 'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            )
        );
        $this->_init_page();
        $this->arr_params['role_id'] = isset($this->arr_params['role_id']) ? explode(',', $this->arr_params['role_id']) : array();
        if (!in_array('2', $this->arr_params['role_id']))
        {
            unset($system_file_list[0]);
            unset($system_file_list[4]);
        }
        $where_params = array('where' => ($this->arr_params));
        $user_list = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query', $where_params);
        $user_list['system_file_list'] = $system_file_list;
        $this->load_view_file($user_list, __LINE__);
    }

    /**
     * 添加平台管理员
     */
    public function user_add()
    {
        //手机号
        if (empty($this->arr_params['telephone']) || !isset($this->arr_params['telephone'])) {
            $re = em_return::return_data(1, '手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //密码
        if (empty($this->arr_params['password']) || !isset($this->arr_params['password']) || empty($this->arr_params['confirmPassword']) || !isset($this->arr_params['confirmPassword'])) {
            $re = em_return::return_data(1, '密码不能为空');
            $this->load_view_file($re, __LINE__);
        }
        if ($this->arr_params['password'] != $this->arr_params['confirmPassword']) {
            $re = em_return::return_data(1, '两次密码不一致');
            $this->load_view_file($re, __LINE__);
        }
        //用户名称
        if (empty($this->arr_params['username']) || !isset($this->arr_params['username'])) {
            $re = em_return::return_data(1, '请填写用户名称');
            $this->load_view_file($re, __LINE__);
        }

        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
            ),
        );

        //先查询是否已经存在了
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] == 0 && is_array($user['data_info']) && count($user['data_info']) > 0) {
            $re = em_return::return_data(1, '用户已经存在,请直接登陆');
            $this->load_view_file($re, __LINE__);
        }
        $time = date('Y-m-d H:i:s');
        $add_params = array(
            'insert' => array(
                'telephone' => $this->arr_params['telephone'],
                'password' => $this->arr_params['password'],
                'create_time' => $time,
                'modify_time' => $time,
                'role_id' => 2,
                'sex' => $this->arr_params['sex'],
                'name' => $this->arr_params['username'],
                'desc' => isset($this->arr_params['desc']) && strlen($this->arr_params['desc']) ? $this->arr_params['desc'] : '',
            )
        );

        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'add', $add_params);
        if ($add_user['ret'] != 0) {
            $re = em_return::return_data(1, '添加失败');
            $this->load_view_file($re, __LINE__);
        }
        $this->load_view_file($add_user, __LINE__);
    }

    /**
     * 修改平台管理员
     */
    public function user_edit()
    {
        if (empty($this->arr_params['cms_id']) || !isset($this->arr_params['cms_id'])) {
            $re = em_return::return_data(1, '用户id无效');
            $this->load_view_file($re, __LINE__);
        }
        $cms_id = $this->arr_params['cms_id'];

        //验证用户是否存在
        $params = array(
            'where' => array(
                'cms_telephone' => $this->arr_params['cms_telephone'],
            ),
        );

        //先查询是否已经存在了
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0) {
            $re = em_return::return_data(1, '查询用户失败');
            $this->load_view_file($re, __LINE__);
        }
        if (isset($user['data_info']) && is_array($user['data_info']) && !empty($user['data_info'])) {
            if ($user['data_info']['cms_id'] != $cms_id) {
                $re = em_return::return_data(1, '修改手机号失败，平台已经存在有相同手机号的管理员了');
                $this->load_view_file($re, __LINE__);
            }
        }
        unset($this->arr_params['cms_id']);
        unset($this->arr_params['cms_telephone']);
        if (!empty($this->arr_params['re_password']) && isset($this->arr_params['re_password'])) {
            $this->arr_params['password'] = $this->arr_params['re_password'];
            unset($this->arr_params['re_password']);
        }

        $edit_params = array(
            'set' => $this->arr_params,
            'where' => array(
                'id' => $cms_id,
            ),
        );
        $edit_params['set']['modify_time'] = date('Y-m-d H:i:s');
        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params);
        if ($add_user['ret'] != 0) {
            $re = em_return::return_data(1, '修改信息失败');
            $this->load_view_file($re, __LINE__);
        }
        $this->load_view_file($add_user, __LINE__);
    }

    /**
     * 管理员启用/禁用
     */
    public function user_state()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if (empty($cms_id) && !is_array($cms_id)) {
            $this->load_view_file(em_return::return_data(1, '用户id为空'), __LINE__);
        }
        $cms_state = isset($this->arr_params['cms_state']) ? $this->arr_params['cms_state'] : null;
        if (strlen($cms_state) < 1) {
            $this->load_view_file(em_return::return_data(1, '状态参数为空'), __LINE__);
        }
        $edit_params = array(
            'set' => array(
                'cms_state' => $cms_state,
                'modify_time' => date('Y-m-d H:i:s'),
            ),
            'where' => array(
                'cms_id' => $cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params), __LINE__);
    }

    /**
     * 管理员删除用户
     */
    public function user_del()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if (empty($cms_id) && !is_array($cms_id)) {
            $this->load_view_file(em_return::return_data(1, '删除参数条件为空'), __LINE__);
        }
        $delete_params = array(
            'where' => array(
                'cms_id' => $cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'rel_del', $delete_params), __LINE__);
    }


    public function user_edit_password()
    {
        if (empty($this->arr_params['cms_id']) || !isset($this->arr_params['cms_id'])) {
            $re = em_return::return_data(1, '用户id无效');
            $this->load_view_file($re, __LINE__);
        }
        $cms_id = $this->arr_params['cms_id'];

        if (empty($this->arr_params['re_password']) || !isset($this->arr_params['re_password'])
            || empty($this->arr_params['re_confirmPassword']) || !isset($this->arr_params['re_confirmPassword'])) {
            $re = em_return::return_data(1, '密码无效');
            $this->load_view_file($re, __LINE__);
        }
        if ($this->arr_params['re_password'] != $this->arr_params['re_confirmPassword']) {
            $re = em_return::return_data(1, '两次密码不一致');
            $this->load_view_file($re, __LINE__);
        }
        $this->arr_params['password'] = $this->arr_params['re_password'];
        unset($this->arr_params['re_password']);
        $edit_params = array(
            'set' => $this->arr_params,
            'where' => array(
                'id' => $cms_id,
            ),
        );
        $edit_params['set']['modify_time'] = date('Y-m-d H:i:s');
        $edit_params['set']['password_modify_time'] = date('Y-m-d H:i:s');
        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params);
        $this->load_view_file($add_user, __LINE__);
    }


    /**
     * 微信登录接口
     */
    public function user_info()
    {
        $this->flag_ajax_reurn = true;

        $this->_init_page();

        $str_tel = trim($_REQUEST["telephone"]);
        $str_role = trim($_REQUEST["role_id"]);
        $str_user = trim($_REQUEST["user_id"]);

        if (strlen($str_user) == 0 || strlen($str_role) == 0 || strlen($str_tel) == 0) {
            $re = em_return::return_data(1, '参数为空');
            $this->load_view_file($re, __LINE__);
        }


        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $str_tel,
                'role_id' => $str_role,
                'id' => $str_user,
            ),
        );
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) && count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在');
            $this->load_view_file($re, __LINE__);
        }
        switch ($user['data_info']['cms_role_id']) {
            case 1:
                $user['data_info']['cms_role_name'] = '订货客户';
                break;
            case 2:
                $user['data_info']['cms_role_name'] = '平台管理员';
                break;
            case 3:
                $user['data_info']['cms_role_name'] = '生产厂商';
                break;
            case 4:
                $user['data_info']['cms_role_name'] = '供应商';
                break;
            case 5:
                $user['data_info']['cms_role_name'] = '样板师';
                break;
            case 6:
                $user['data_info']['cms_role_name'] = '样衣师';
                break;
        }
        $params = array(
            "ret" => 0,
            "reason" => "查询成功",
            'data_info' => $user['data_info'],
        );
        $this->load_view_file($params, __LINE__);
    }


    /**
     * 检查用户是否存在
     */
    public function register_check()
    {
        $this->flag_ajax_reurn = true;
        //验证用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['telephone'],
            ),
        );
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] == 0 && is_array($user['data_info']) && count($user['data_info']) > 0) {
            $re = em_return::return_data(1, '该手机号已注册');
            $this->load_view_file($re, __LINE__);
        }

        $re = em_return::return_data(0, '用户不存在');
        $this->load_view_file($re, __LINE__);
    }


    public function wx_logout()
    {
        $this->flag_ajax_reurn = ture;
        $re = em_return::return_data(0, '退出登录');
        $this->load_view_file($re, __LINE__);
    }

    /**
     * 检查是否需要完善资料
     */
    public function need_perfect_profile()
    {
        $this->flag_ajax_reurn = true;
        $data = array(
            'need_perfect_profile' => 1,
        );

        if (empty($this->arr_params['user_id']) || !isset($this->arr_params['user_id']))
        {
            $re = em_return::return_data(1,'用户id不能为空',$data);
            $this->load_view_file($re,__LINE__);
        }
        //验证用户是否存在
        $params = array(
            'where' => array(
                'id' => $this->arr_params['user_id'],
            ),
        );
        $user = $this->auto_load_table('order','manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) && count($user['data_info']) < 1)
        {
            $re = em_return::return_data(1,'用户不存在',$data);
            $this->load_view_file($re,__LINE__);
        }

        $user_info = $user['data_info'];

        if (empty($user_info['cms_name']) || empty($user_info['cms_telephone'])
            || empty($user_info['cms_wx_num']))
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        if ((empty($user_info['cms_use_software']) || (empty($user_info['cms_plateing_price']) && !isset($user_info['cms_plateing_price']))
            || (empty($user_info['cms_pushing_plate_price']) && !isset($user_info['cms_pushing_plate_price']))
                || (empty($user_info['cms_printer_equipment']) && !isset($user_info['cms_printer_equipment']))) && $user_info['cms_role_id'] == 5)
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        //订货商
        if ($user_info['cms_role_id'] == 1 && (empty($user_info['cms_company_type']) || empty($user_info['cms_sale_channels'])
                || empty($user_info['cms_main_product']) || empty($user_info['cms_main_product'])))
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        if (($user_info['cms_role_id'] == 5 || $user_info['cms_telephone'] == 6)
            && (empty($user_info['cms_good_at_style']) || !isset($user_info['cms_is_full_time']) || !isset($user_info['cms_have_workroom'])) )
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        if ($user_info['cms_role_id'] == 6 && (empty($user_info['cms_good_at_material'])
            || empty($user_info['cms_good_at_model']) || empty($user_info['cms_own_equipment'])))
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        if ($user_info['cms_role_id'] == 3 && (empty($user_info['cms_product_nature'])
                || empty($user_info['cms_production_chain']) || empty($user_info['cms_people_num'])
                || empty($user_info['cms_capacity']) || empty($user_info['cms_factor_name'])))
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        if ($user_info['cms_role_id'] == 4 && empty($user_info['cms_nature']))
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }

        if (($user_info['cms_role_id'] == 3 || $user_info['cms_telephone'] == 4)
            && empty($user_info['cms_taxation']))
        {
            $re = em_return::return_data(0,'资料还未完善需要完善资料',$data);
            $this->load_view_file($re,__LINE__);
        }
        $data['need_perfect_profile'] = 0;
        $re = em_return::return_data(0,'资料已经完善',$data);
        $this->load_view_file($re,__LINE__);
    }


    /**
     * 小程序修改手机号
     */
    public function wx_edit_telephone()
    {
        $this->flag_ajax_reurn = true;
        //手机号
        if (empty($this->arr_params['old_telephone']) || !isset($this->arr_params['old_telephone'])) {
            $re = em_return::return_data(1, '旧手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //用户id
        if (empty($this->arr_params['user_id']) || !isset($this->arr_params['user_id'])) {
            $re = em_return::return_data(1, '用户id不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //旧手机验证码
        if (empty($this->arr_params['num']) || !isset($this->arr_params['num'])) {
            $re = em_return::return_data(1, '旧手机验证码不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //新手机号
        if (empty($this->arr_params['new_telephone']) || !isset($this->arr_params['new_telephone'])) {
            $re = em_return::return_data(1, '新手机号不能为空');
            $this->load_view_file($re, __LINE__);
        }
        //新手机验证码
        if (empty($this->arr_params['re_num']) || !isset($this->arr_params['re_num'])) {
            $re = em_return::return_data(1, '新手机验证码不能为空');
            $this->load_view_file($re, __LINE__);
        }

        if ($this->arr_params['old_telephone'] == $this->arr_params['new_telephone'])
        {
            $re = em_return::return_data(1, '两次手机号不能一致');
            $this->load_view_file($re, __LINE__);
        }

        //验证旧手机号用户是否存在
        $params = array(
            'where' => array(
                'telephone' => $this->arr_params['old_telephone'],
                'id' => $this->arr_params['user_id'],
            ),
        );

        //先查询是否已经存在了
        $user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $params);
        if ($user['ret'] != 0 || !is_array($user['data_info']) || count($user['data_info']) < 1) {
            $re = em_return::return_data(1, '用户不存在');
            $this->load_view_file($re, __LINE__);
        }

        //验证新是否存在
        $new_params = array(
            'where' => array(
                'telephone' => $this->arr_params['new_telephone']
            ),
        );
        //先查询是否已经存在了
        $new_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'query_only', $new_params);
        if ($new_user['ret'] != 0) {
            $re = em_return::return_data(1, '根据新手机号查询失败');
            $this->load_view_file($re, __LINE__);
        }
        if ($new_user['ret'] == 0 && is_array($new_user['data_info']) && count($new_user['data_info']) > 0) {
            $re = em_return::return_data(1, '新手机号已经注册,请直接登录');
            $this->load_view_file($re, __LINE__);
        }

        //调取短信验证码，并验证短信
        include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/system/auto/c_smsg/system_smsg.class.php';
        $obj_system_smsg = new system_smsg($this, '');
        $arr_send_ret = $obj_system_smsg->check_verify_code($this->arr_params['new_telephone'], $this->arr_params['re_num']);
        if ($arr_send_ret['ret'] != 0) {
            $re = em_return::return_data(1, '新手机' . $arr_send_ret['reason']);
            $this->load_view_file($re, __LINE__);
        }

        $edit_params = array(
            'set' => array(
                'modify_time' => date('Y-m-d H:i:s'),
                'telephone' => $this->arr_params['new_telephone'],
            ),
            'where' => array(
                'id' => $user['data_info']['cms_id'],
            ),
        );
        $add_user = $this->auto_load_table('order', 'manager', 'c_manager', 'manager', 'edit', $edit_params);
        if ($add_user['ret'] != 0) {
            $re = em_return::return_data(1, '修改手机号失败');
            $this->load_view_file($re, __LINE__);
        }
        //修改后将session的手机号重置
        $this->session->set_userdata(array('telephone' => $this->arr_params['new_telephone']));

        $this->load_view_file($add_user, __LINE__);
    }

    public function home(){
        $this->load_view_file(array(), __LINE__);
    }

    public function find_back_password(){
        $this->load_view_file(array(), __LINE__);
    }
}