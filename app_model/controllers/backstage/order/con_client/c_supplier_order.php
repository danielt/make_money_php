<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 超级管理管理模块
 * @author pan.liang
 */
class c_supplier_order extends CI_Controller
{
    /**
     * 列表页
     */
    public function index()
    {
        //初始化按钮
        $this->init_system_file_button_list();
        //初始化分页
        $this->_init_page();
        $this->arr_params['cms_acceptor_id'] = $this->session->userdata['user_id'];
        $where_params = array('where'=>($this->arr_params));
        $data_info = $this->auto_load_table('order','con_client','c_supplier_order','order_supplier_order', 'query',$where_params);
        $this->load_view_file($data_info,__LINE__);
    }
    
}
