<?php
/**
 * Created by PhpStorm.
 * User: fingal
 * Date: 2018/12/16
 * Time: 13:17
 */

class c_client_order extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->library('session');
        $this->load->database();
    }

    /**
     * 点击新增订单
     * 第一步-选择订单类型
     */
    public function index()
    {
        // 初始化页面
        $this->_init_page();

        //先查询是否已经存在了
        $arr_order = $this->auto_load_table('order', 'con_client', 'c_client_order', 'client_order', 'query');

        // 分页信息
        $page_info = $arr_order["page_info"];
        $arr_order = $arr_order["data_info"];

        //选择框
        $system_file_list = array(
            array(
                'url' => 'order/con_client/c_client_order/check_client_order_edit.php',
                'class' => 'order_con_client_c_client_order_check_client_order_edit',
                'ajax' => '',
                'function' => 'check_client_order_edit',
                'button_data' => array(
                    array(
                        'name' => '订单详情',
                        'icon' => 'fa-pencil-square-o',
                        'params' => 'cms_id',
                        'where' => '',
                        'bottom' => true,
                    ),
                ),
            ),
            array(
                'url' => 'order/con_client/c_client_order/logistics_desc_edit.php',
                'class' => 'order_con_client_c_client_order_logistics_desc_edit',
                'ajax' => '',
                'function' => 'logistics_desc_edit',
                'button_data' => array(
                    array(
                        'name' => '物流详情',
                        'icon' => 'fa-pencil-square-o',
                        'params' => 'cms_id',
                        'where' => '',
                        'bottom' => true,
                    ),
                ),
            ),
        );
        $return_arr = array(
            'data_info' => $arr_order,//订单列表
            'system_file_list' => $system_file_list,
            'url_params' => $this->arr_params,
            'page_info' => $page_info,
            'list_url' => 'order/index/c_index/index',
        );

        if ($arr_order['ret'] == 0 && is_array($arr_order['data_info']) && count($arr_order['data_info']) > 0) {
            $order_id_arr = array();
            foreach ($arr_order['data_info'] as $val) {
                $order_id_arr[] = $val['cms_order_id'];
            }
            $logistics_params = array(
                'cms_buy_order_id' => $order_id_arr,
            );
            //获取物流信息
            include_once dirname(dirname(dirname(dirname(__DIR__)))) . '/logic/backstage/order/buy_order/c_logistics/logic_logistics.class.php';
            $obj_logistics_logic = new logistics_logic($this);
            //查询数据
            $arr_logistics_info = $obj_logistics_logic->get_list($logistics_params, '*');
            if ($arr_logistics_info['ret'] == 0 && is_array($arr_logistics_info['data_info']) && count($arr_logistics_info['data_info']) > 0) {
                foreach ($arr_order['data_info'] as &$val) {
                    foreach ($arr_logistics_info['data_info'] as $items) {
                        if ($items['cms_buy_order_id'] == $val['cms_order_id']) {
                            $val['logistics_list'][] = $items;
                        }
                    }

                }
            }
            $return_arr['data_info'] = $arr_order;
        }
        $this->load_view_file($return_arr, __LINE__);
    }

    /**
     * 订单类型
     */
    public function add_order()
    {
        // 初始化页面
        $this->_init_page();
        if ($this->arr_params['flag_ajax_reurn'] == 1) {
            $this->flag_ajax_reurn = true;
            $first_type = $this->arr_params['first_type'];
            $second_type = $this->arr_params['second_type'];
            $third_type = $this->arr_params['third_type'];
            $params = array(
                'where' => array(
                    'category_second' => $this->arr_params['second_type'],
                    'category_third' => $this->arr_params['third_type'],
                ),
            );
            if ($second_type == 0) {
                unset($params['where']['category_second']);
            }
            if ($third_type == 0) {
                unset($params['where']['category_third']);
            }
            $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'query', $params);
            $arr_order_parent_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type_parent', 'query_all');
            $return_arr = array(
                'parent_type' => $arr_order_parent_type['data_info'],//订单列表
                'data_info' => $arr_order_type['data_info'],//订单列表
                'page_info' => $arr_order_type['page_info'],
            );
            $this->load_view_file($return_arr, __LINE__);
        }


        $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'query');

        $arr_order_parent_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type_parent', 'query_all');
        $return_arr = array(
            'parent_type' => $arr_order_parent_type['data_info'],//订单列表
            'type' => $arr_order_type['data_info'],//订单列表
            'page_info' => $arr_order_type['page_info'],
        );
        $this->load_view_file($return_arr, __LINE__);
    }


    /**
     * 用户首页栏目数据
     */
    public function client_basic_category()
    {
        $this->flag_ajax_reurn = true;
        $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'query_all');

        $arr_order_parent_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type_parent', 'query_all');

        $arr_re = array();
        $arr_order_parent_type = $arr_order_parent_type['data_info'];

        foreach ($arr_order_parent_type as $key => $arr_value) {
            if ($arr_value['cms_category_parent_id'] == 1) {
                $arr_re['first'][$key] = array(
                    'category_id' => $arr_value['cms_category_id'],
                    'category_name' => $arr_value['cms_category_name'],
                );
            }
            if ($arr_value['cms_category_parent_id'] == 2) {
                $arr_re['second'][$key] = array(
                    'category_id' => $arr_value['cms_category_id'],
                    'category_name' => $arr_value['cms_category_name'],
                );
            }
            if ($arr_value['cms_category_parent_id'] == 3) {
                $arr_re['third'][$key] = array(
                    'category_id' => $arr_value['cms_category_id'],
                    'category_name' => $arr_value['cms_category_name'],
                );
            }
        }
        $arr_return = array(
            'ret' => 0,
            'reason' => 'ok',
            'data_info' => $arr_re
        );

        $this->load_view_file($arr_return, __FILE__);
    }

    /**
     * 用户基本款式
     */
    public function client_basic_type()
    {

        $this->flag_ajax_reurn = true;
        $second_type = $this->arr_params['second_type'];
        $third_type = $this->arr_params['third_type'];
        $params = array(
            'where' => array(
                'category_second' => $this->arr_params['second_type'],
                'category_third' => $this->arr_params['third_type'],
            ),
        );
        if ($second_type == 0) {
            unset($params['where']['category_second']);
        }
        if ($third_type == 0) {
            unset($params['where']['category_third']);
        }

        $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'home_type');

        $this->load_view_file($arr_order_type, __FILE__);
    }


    /**
     * 基本款式管理
     */
    public function basic_style_manager()
    {

        $this->_init_page();
        $second_type = $this->arr_params['second_type'];
        $third_type = $this->arr_params['third_type'];
        $params = array(
            'where' => array(
                'category_second' => $this->arr_params['second_type'],
                'category_third' => $this->arr_params['third_type'],
            ),
        );
        if ($second_type == 0) {
            unset($params['where']['category_second']);
        }
        if ($third_type == 0) {
            unset($params['where']['category_third']);
        }

        $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'home_type');

        $this->load_view_file($arr_order_type, __FILE__);

    }

    /**
     * 生成订单
     */
    public function generate_order()
    {
        $this->flag_ajax_reurn = true;

        $str_order_id = $this->get_guid('rand');
        $str_manager_id = $this->session->userdata('user_id');
        $str_first_type = $this->arr_params["cms_first_type_id"];
        $str_basic_type = $this->arr_params["cms_basic_type_id"];
        $str_process_type = $this->arr_params['cms_process'];
        $json_process_type = json_encode($str_process_type);
        $arr_material_list = $this->arr_params['cms_material_list'];
        $json_material_list = json_encode($arr_material_list);
        $str_style = $this->arr_params['cms_style'];
        $str_status = 1;

        if (strlen($json_material_list) < 1 || strlen($json_process_type) < 1 || $str_style < 1) {
            $str_status = 0;
        }

        // 生成订单
        $add_param = array(
            'cms_order_id' => $str_order_id,
            'cms_manager_id' => $str_manager_id,
            'cms_order_parent_type_id' => $str_first_type,
            'cms_order_type_id' => $str_basic_type,
            'cms_process_type' => $json_process_type,
            'cms_material_list' => $json_material_list,
            'cms_price' => 0.1,
            'cms_style' => $str_style,
            'cms_state' => $str_status,
        );

        $add_result = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_client_order', 'add', $add_param);

        if ($add_result['ret'] != 0) {
            $this->load_view_file($add_result, __FILE__);
        }

        $add_result['data_info']['cms_order_id'] = $str_order_id;
        // 如果是包工包料、半包工 生成面辅料订单
        // 如果是清加工，不生成面辅料订单
        if ($str_process_type != 3) {

            $re = $this->generate_material_order($str_order_id, $json_material_list);
            if ($re['ret'] != 0) {
                $this->load_view_file($re, __FILE__);
            }
        }

        // 生成样板订单
        $re_model = $this->generate_model_order($str_order_id, $str_style);
        if ($re_model['ret'] != 0) {
            $this->load_view_file($re_model, __FILE__);
        }

        $this->load_view_file($add_result, __FILE__);
    }

    /**
     * 生成样板订单
     */
    public function generate_model_order($order_id, $content)
    {

        $str_order_id = $this->get_guid('rand');
        $int_manager_id = $this->session->userdata('user_id');
        $str_parent_order_id = $order_id;
        $str_content = $content;

        // 生成订单
        $add_param = array(
            'cms_order_id' => $str_order_id,
            'cms_manager_id' => $int_manager_id,
            'cms_parent_order_id' => $str_parent_order_id,
            'cms_content' => $str_content,
        );

        $add_result = $this->auto_load_table('order', 'model_dress', 'c_model_dress', 'order_model_dress_order', 'add', $add_param);

        return $add_result;
    }

    /**
     * 生成面辅料订单
     */
    public function generate_material_order($order_id, $json_material)
    {
        $str_order_id = $this->get_guid('rand');
        $int_manager_id = $this->session->userdata('user_id');
        $str_parent_order_id = $order_id;
        $str_content = $json_material;

        // 生成订单
        $add_param = array(
            'cms_order_id' => $str_order_id,
            'cms_manager_id' => $int_manager_id,
            'cms_parent_order_id' => $str_parent_order_id,
            'cms_content' => $str_content,
        );

        $add_result = $this->auto_load_table('order', 'con_client', 'c_supplier_order', 'order_supplier_order', 'add', $add_param);

        return $add_result;
    }

    /**
     * order_type 0批量订单；1面料小样；2样板订单；3样衣订单；4稀缺面料定金订单；5充值订单
     * order_id   物理订单id
     */
    public function query_order_info()
    {
        $this->flag_ajax_reurn = true;
        $str_order_id = $this->arr_params['order_id'];
        $str_order_type = $this->arr_params['order_type'];

        $param = array(
            'cms_order_id' => $str_order_id,
        );

//        $arr_result = array(
//            'ret' => 0,
//            'data_info' => array(array(
//                'cms_id' => $str_order_id,
//                'cms_name' => '',
//                'cms_price' => '120.16',
//            ))
//        );
//        $this->load_view_file($arr_result, __FILE__);

        switch ($str_order_type) {
            case '0':
                //批量订单
                $arr_result = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_client_order', 'query', $param);
                $this->load_view_file($arr_result, __FILE__);
                break;
            case '1':
                //面料小样
                break;
            case '2':
                //样板订单
                $arr_result = $this->auto_load_table('order', 'model_dress', 'c_model_dress', 'order_model_dress_order', 'query', $param);
                $this->load_view_file($arr_result, __FILE__);
                break;
            case '3':
                //3样衣订单
                $arr_result = $this->auto_load_table('order', 'simple_dress', 'c_simple_dress', 'order_simple_dress_order', 'query', $param);
                $this->load_view_file($arr_result, __FILE__);
                break;
            case '4':
                //4稀缺面料定金订单
                break;
            case '5':
                //5充值订单
                break;

        }
    }

    /**
     * 返回订单总价
     * @return float|int
     */
    public function generate_total_price()
    {
        $basic_price = $this->arr_params['cms_basic_type_price'];
        $amount = $this->arr_params['cms_amount'];
        $material_price = $this->arr_params['cms_material_price'];
        $process_type = $this->arr_params['cms_process'];
        $total_price = 0;
        if ($amount < 200) {
            switch ($process_type) {
                case '0':
                    $total_price = ($basic_price + $material_price) * 1.5;
                    break;
                case '1':
                    $total_price = ($basic_price + $material_price) * 3;
                    break;
                case '2':
                    $total_price = ($basic_price) * 3;
                    break;
            }
        }
        if ($amount > 200 && $amount < 500) {
            switch ($process_type) {
                case '0':
                    $total_price = ($basic_price + $material_price) * 1.4;
                    break;
                case '1':
                    $total_price = ($basic_price + $material_price) * 2.5;
                    break;
                case '2':
                    $total_price = ($basic_price) * 2.5;
                    break;
            }
        }
        if ($amount > 500 && $amount < 1000) {
            switch ($process_type) {
                case '0':
                    $total_price = ($basic_price + $material_price) * 1.3;
                    break;
                case '1':
                    $total_price = ($basic_price + $material_price) * 2;
                    break;
                case '2':
                    $total_price = ($basic_price) * 2;
                    break;
            }
        }

        if ($amount > 1000) {
            switch ($process_type) {
                case '0':
                    $total_price = ($basic_price + $material_price) * 1.2;
                    break;
                case '1':
                    $total_price = ($basic_price + $material_price) * 1.5;
                    break;
                case '2':
                    $total_price = ($basic_price) * 1.5;
                    break;
            }
        }

        return $total_price;
    }

    /**
     * 支付成功之后，更新订单信息
     * 【必填】
     * cms_order_id     订单id
     * cms_order_type   订单类型
     * 【选填】
     * cms_state 0未支付1已支付
     * cms_price 价钱
     * cms_amount 数量
     *
     */
    public function update_order()
    {
        $this->flag_ajax_reurn = true;
        $str_order_id = $this->arr_params['cms_order_id'];
        $str_order_type = $this->arr_params['cms_order_type'];

        switch ($str_order_type) {
            case '0':
                //批量订单
                $set_params = array();
                if (isset($this->arr_params['cms_price'])) {
                    $set_params['cms_price'] = $this->arr_params['cms_price'];
                }
                if (isset($this->arr_params['cms_amount'])) {
                    $set_params['cms_amount'] = $this->arr_params['cms_amount'];
                }
                if (isset($this->arr_params['cms_state'])) {
                    $set_params['cms_state'] = $this->arr_params['cms_state'];
                }
                $update_param = array(
                    'set' => $set_params,
                    'where' => array(
                        'cms_order_id' => $str_order_id,
                    ),
                );
                $arr_result = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_client_order', 'edit', $update_param);
                $this->load_view_file($arr_result, __FILE__);
                break;
            case '1':
                //面料小样
                break;
            case '2':
                //样板订单
                $set_params = array();
                if (isset($this->arr_params['cms_state'])) {
                    $set_params['cms_state'] = $this->arr_params['cms_state'];
                }
                $update_param = array(
                    'set' => $set_params,
                    'where' => array(
                        'cms_order_id' => $str_order_id,
                    ),
                );
                $arr_result = $this->auto_load_table('order', 'model_dress', 'c_model_dress', 'order_model_dress_order', 'edit', $update_param);
                $this->load_view_file($arr_result, __FILE__);
                break;
            case '3':
                //3样衣订单
                $set_params = array();
                if (isset($this->arr_params['cms_state'])) {
                    $set_params['cms_state'] = $this->arr_params['cms_state'];
                }
                $update_param = array(
                    'set' => $set_params,
                    'where' => array(
                        'cms_order_id' => $str_order_id,
                    ),
                );
                $arr_result = $this->auto_load_table('order', 'simple_dress', 'c_simple_dress', 'order_simple_dress_order', 'edit', $update_param);
                $this->load_view_file($arr_result, __FILE__);
                break;
            case '4':
                //4稀缺面料定金订单
                break;
            case '5':
                //5充值订单
                break;

        }

    }

    /**
     * 获取主订单的价格
     *  ****request****
     * order_id       订单id
     * *****return data*****
     * $process_type   加工类型  1包工包料2半包料3清加工
     * $basic_price    基本加工费 报价算法中的X
     * $material_price 辅料加工费  报价算法中的Z
     */
    public function get_order_price(){
        //查主订单
        $where_params = array('cms_order_id'=>$this->arr_params['order_id']);
        $arr_order = $this->auto_load_table('order', 'con_client', 'c_client_order', 'client_order', 'query_only',$where_params);
        $arr_order = $arr_order['data_info'];
        $process_type = array_shift($arr_order);
        //查基本款式价格
        $int_basic_type_id = $arr_order['cms_order_type_id'];
        $order_type_where = array('cms_type_id'=>$int_basic_type_id);
        $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'query_only',$order_type_where);
        $basic_price = $arr_order_type['data_info']['cms_price'];
        //查面辅料价格
        $json_material_id = $arr_order['cms_material_list'];
        $arr_material_id = json_decode($json_material_id,true);
        $fabirc_material_id = array('cms_id'=>$arr_material_id);
        $arr_material = $this->auto_load_table('order', 'fabirc', 'c_fabirc_product', 'fabirc_material', 'query_all',$fabirc_material_id);
        $arr_material = $arr_material['data_info'];
        $material_price = '0.00';
        foreach ($arr_material as $arr_material_item){
            $material_price += $arr_material_item['cms_price'];
        }
        $arr_re = array(
            'ret' => 0,
            'reason' => 'ok',
            'data_info' => array(
                'str_process_type' => $process_type,
                'str_basic_price' => $basic_price,
                'float_material_price' => $material_price,
            )
        );
        $this->flag_ajax_reurn=true;
        $this->load_view_file($arr_re,__FILE__);
    }

    public function check_client_order_edit(){
        $this->arr_params['cms_id'];
        $where_params = array('cms_order_id'=>$this->arr_params['order_id']);
        $arr_order = $this->auto_load_table('order', 'con_client', 'c_client_order', 'client_order', 'query_only',$where_params);
    }


    public function test(){
        $id = '5c3f1138a4af1cbc0f3e2632ccd8c9d3';
        $arr_where = array(
            'cms_order_id'=>$id,
            'cms_state'=>1,
        );
        $arr_order = $this->auto_load_table('order', 'simple_dress', 'c_simple_dress', 'simple_dress_order', 'update_simple_order',$arr_where);

        var_dump($arr_order);exit;
    }
}