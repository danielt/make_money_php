<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2019/1/4
 * Time: 0:15
 */
defined('BASEPATH') or exit('No direct script access allowed');
class c_fabirc_product extends CI_Controller
{
//    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
//    {
//        $this->need_login = false;
//        parent::__construct($str_class, $str_file, $str_method, $str_directory);
//    }
    public function fabirc_product_list()
    {
        $this->arr_params['cms_user_id'] = $this->session->userdata['user_id'];
        $this->_init_page();
        $fabirc_product = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'query_by_condition',$this->arr_params);
        $page_info = $fabirc_product['page_info'];
        $product = array();
        if($fabirc_product['ret'] == 0 && !empty($fabirc_product['data_info']))
        {
            $product = $fabirc_product['data_info'];
        }
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/fabirc/c_fabirc_product/fabirc_product_add.php',//右侧按钮弹框
                'class'=>'order_fabirc_c_fabirc_product_fabirc_product_add',//form表单ID
                'ajax'=>'order/fabirc/c_fabirc_product/fabirc_product_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_product/fabirc_product_edit.php',
                'class'=>'order_fabirc_c_fabirc_product_fabirc_product_edit',
                'ajax'=>'order/fabirc/c_fabirc_product/fabirc_product_edit',
                'function'=>'edit',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_product/fabirc_product_delete.php',
                'class'=>'order_fabirc_c_fabirc_product_fabirc_product_delete',
                'ajax'=>'order/fabirc/c_fabirc_product/fabirc_product_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );
        //获取所有属性分类及属性
        $fabirc_attribute_list = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'query_attribute_and_value');
        if($fabirc_attribute_list['ret'] == 0 && !empty($fabirc_attribute_list['data_info']))
        {
            $fabirc_attribute_list = $fabirc_attribute_list['data_info'];
        }
        else
        {
            $fabirc_attribute_list = array();
        }
        $attribute_list = array();
        if(!empty($fabirc_attribute_list))
        {
            foreach ($fabirc_attribute_list as $list)
            {
                $attribute_list[$list['cms_id']]['cms_name'] = $list['cms_name'];
                if(!empty($list['value_id']) && !empty($list['value_name']))
                {
                    $attribute_list[$list['cms_id']]['cms_value'][] = array('cms_id' => $list['value_id'],'cms_name' => $list['value_name']);
                }
            }
        }
        //查询绑定的属性
        $fabirc_bind_attribute = array();
        if(!empty($product))
        {
            foreach ($product as $value)
            {
                $fabirc_bind_attribute[$value['cms_id']] = $value;
            }
            $bind_result = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material_bind_attribute', 'query_all', array('cms_material_id' => array_keys($fabirc_bind_attribute)));

            if($bind_result['ret'] == 0 && is_array($bind_result['data_info']) && !empty($bind_result['data_info']))
            {
                foreach ($bind_result['data_info'] as $val)
                {
                    $fabirc_bind_attribute[$val['cms_material_id']]['cms_attribute_value_id'][] = $val['cms_attribute_value_id'];
                    $fabirc_bind_attribute[$val['cms_material_id']]['cms_attribute_value_name'][] = $val['value_name'];
                }
            }
        }

        if(is_array($fabirc_bind_attribute) && !empty($fabirc_bind_attribute))
        {
            foreach ($fabirc_bind_attribute as &$reset)
            {
                if(isset($reset['cms_attribute_value_id']))
                {
                    $reset['cms_attribute_value_id'] = implode(",", $reset['cms_attribute_value_id']);
                }
                if(isset($reset['cms_attribute_value_name']))
                {
                    $reset['cms_attribute_value_name'] = implode(",", $reset['cms_attribute_value_name']);
                }
            }
        }

        //获取全部面辅料
        $fabirc = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'query_all');
        if($fabirc['ret'] != 0)
        {
            $fabirc = array();
        }
        else
        {
            $fabirc = $fabirc['data_info'];
        }
        $return_arr = array(
            'data_info' => $fabirc_bind_attribute,
            'system_file_list' => $system_file_list,
            'page_info'   => $page_info,
            'attribute_list' => $attribute_list,
            'fabirc' => $fabirc,
        );
        $this->load_view_file($return_arr,__LINE__);
    }

    public function fabirc_product_add()
    {
        $this->arr_params['cms_user_id'] = $this->session->userdata['user_id'];
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'add', $this->arr_params);
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = $fabirc_attribute;
        }
        else
        {
            $map_arr = array(
                'cms_material_id' => $fabirc_attribute['data_info']['cms_id'],
                'cms_attribute_value_id' => explode(",", $this->arr_params['cms_attribute_value_id']),
            );
            //添加type绑定关系
            $return_arr = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material_bind_attribute', 'add', $map_arr);
            if($return_arr['ret'] != 0)
            {
                //删除选料
                $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'del', array('cms_id' => $fabirc_attribute['data_info']['cms_id']));
            }
        }
        $this->load_view_file($return_arr);
    }

    public function fabirc_product_edit()
    {
        //先删除绑定关系
        $fabirc_type_map_del = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material_bind_attribute', 'del', array('cms_material_id' => $this->arr_params['cms_id']));

        if($fabirc_type_map_del['ret'] != 0)
        {
            $return_arr = $fabirc_type_map_del;
        }
        else
        {
            $map_arr = array(
                'cms_material_id' => $this->arr_params['cms_id'],
                'cms_attribute_value_id' => explode(",", $this->arr_params['cms_attribute_value_id']),
            );
            //添加type绑定关系
            $fabirc_type_map = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material_bind_attribute', 'add', $map_arr);
            if($fabirc_type_map['ret'] != 0)
            {
                $return_arr = $fabirc_type_map;
            }
            else
            {
                $return_arr = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'edit', $this->arr_params);
            }
        }
        $this->load_view_file($return_arr);
    }

    public function fabirc_product_delete()
    {
        //先删除绑定关系
        $fabirc_type_map_del = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material_bind_attribute', 'del', array('cms_material_id' => $this->arr_params['cms_id']));
        if($fabirc_type_map_del['ret'] != 0)
        {
            $return_arr = $fabirc_type_map_del;
        }
        else
        {
            //再删除数据
            $return_arr = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'del', $this->arr_params);
        }
        $this->load_view_file($return_arr);
    }
}