<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/17
 * Time: 19:23
 */
defined('BASEPATH') or exit('No direct script access allowed');
class c_fabirc_manager extends CI_Controller
{
    /********************************************面辅料类型管理*********************************************/
    public function fabirc_type_list()
    {
        $this->_init_page();
        $fabirc_type = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_type', 'query',$this->arr_params);
        $page_info = $fabirc_type['page_info'];
        $attribute_list = array();
        if($fabirc_type['ret'] == 0 && !empty($fabirc_type['data_info']))
        {
            $attribute_list = $fabirc_type['data_info'];
        }
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/fabirc/c_fabirc_manager/fabirc_type_add.php',//右侧按钮弹框
                'class'=>'order-fabirc-c_fabirc_manager-fabirc_typee_add',//form表单ID
                'ajax'=>'order/fabirc/c_fabirc_manager/fabirc_type_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/fabirc_type_edit.php',
                'class'=>'order-fabirc-c_fabirc_manager-fabirc_type_edit',
                'ajax'=>'order/fabirc/c_fabirc_manager/fabirc_type_edit',
                'function'=>'edit',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/fabirc_type_delete.php',
                'class'=>'order-fabirc-c_fabirc_manager-fabirc_type_delete',
                'ajax'=>'order/fabirc/c_fabirc_manager/fabirc_type_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );
        $return_arr = array(
            'data_info' => $attribute_list,
            'system_file_list' => $system_file_list,
            'page_info'   => $page_info,
        );
        $this->load_view_file($return_arr,__LINE__);
    }
    public function fabirc_type_add()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_type', 'add', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }
        $this->load_view_file($return_arr);
    }

    public function fabirc_type_delete()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_type', 'del', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }

        $this->load_view_file($return_arr);
    }
    public function fabirc_type_edit()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_type', 'edit', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }

        $this->load_view_file($return_arr);
    }
    /********************************************面辅料类型管理*********************************************/

    /********************************************面辅料管理*********************************************/
    /**
     * 面辅料管理界面
     */
    public function fabirc_manager_list()
    {
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/fabirc/c_fabirc_manager/add.php',
                'class'=>'order-fabirc-c_fabirc_manager-add',
                'ajax'=>'order/fabirc/c_fabirc_manager/add',
                'function'=>'add',
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',
                        'params'=>'',
                        'where'=>'',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/edit.php',
                'class'=>'order-fabirc-c_fabirc_manager-edit',
                'ajax'=>'order/fabirc/c_fabirc_manager/edit',
                'function'=>'edit',
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'params'=>'',
                        'where'=>'',
                        'button_display' => 'true',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/delete.php',
                'class'=>'order-fabirc-c_fabirc_manager-delete',
                'ajax'=>'order/fabirc/c_fabirc_manager/delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params'=>'',
                        'where'=>'',
                    ),
                ),
            ),
        );
        //获取面辅料属性
        $fabirc_attribute_list = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'query_all');
        if($fabirc_attribute_list['ret'] == 0 && !empty($fabirc_attribute_list['data_info']))
        {
            $fabirc_attribute_list = $fabirc_attribute_list['data_info'];
        }
        else
        {
            $fabirc_attribute_list = array();
        }
        //获取面辅料类型
        $fabirc_type = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_type', 'query_all');
        if($fabirc_type['ret'] == 0 && !empty($fabirc_type['data_info']))
        {
            $fabirc_type = $fabirc_type['data_info'];
        }
        else
        {
            $fabirc_type = array();
        }

        //分页查询所有面辅料
        $this->_init_page();
        $fabirc = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'query',$this->arr_params);
        $page_info = $fabirc['page_info'];
        if($fabirc['ret'] == 0 && !empty($fabirc['data_info']))
        {
            $fabirc = $fabirc['data_info'];
        }
        else
        {
            $fabirc = array();
        }
        $fabirc_reset = array();
        if(!empty($fabirc)) //查询绑定的TYPEid
        {
            foreach ($fabirc as $value)
            {
                $fabirc_reset[$value['cms_id']] = $value;
            }
            $fabirc_type_map = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_map', 'query_all', array('cms_fabirc_id' => array_keys($fabirc_reset)));

            if($fabirc_type_map['ret'] == 0 && is_array($fabirc_type_map['data_info']) && !empty($fabirc_type_map['data_info']))
            {
                foreach ($fabirc_type_map['data_info'] as $val)
                {
                    $fabirc_reset[$val['cms_fabirc_id']]['cms_fabirc_type_id'][] = $val['cms_fabirc_type_id'];
                }
            }
        }

        if(is_array($fabirc_reset) && !empty($fabirc_reset))
        {
            foreach ($fabirc_reset as &$reset)
            {
                if(isset($reset['cms_fabirc_type_id']))
                {
                    $reset['cms_fabirc_type_id'] = implode(",", $reset['cms_fabirc_type_id']);
                }
            }
        }

        $return_arr = array(
            'data_info' => $fabirc_reset,//面辅料
            'system_file_list' => $system_file_list,
            'page_info'   => $page_info,
            'fabirc_attribute_list' => $fabirc_attribute_list,
            'fabirc_type' => $fabirc_type,
        );
        //取出面辅料
        $this->load_view_file($return_arr,__LINE__);
    }

    /**
     * 面辅料添加
     */
    public function add()
    {
        //先添加面辅料
        $fabirc = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'add', $this->arr_params);
        if($fabirc['ret'] != 0)
        {
            $return_arr = $fabirc;
        }
        else
        {
            $map_arr = array(
                'cms_fabirc_id' => $fabirc['data_info']['cms_id'],
                'cms_fabirc_type_id' => $this->arr_params['cms_fabirc_type_id'],
            );
            //添加type绑定关系
            $return_arr = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_map', 'add', $map_arr);
            if($return_arr['ret'] != 0)
            {
                //添加失败删除面辅料
                $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'del', array('cms_id' => $fabirc['data_info']['cms_id']));
            }
        }
        $this->load_view_file($return_arr);
    }
    /**
     * 面辅料修改
     */
    public function edit()
    {
        //根据面辅料，先删除绑定关系
        $fabirc_type_map_del = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_map', 'del', array('cms_fabirc_id' => $this->arr_params['cms_id']));

        if($fabirc_type_map_del['ret'] != 0)
        {
            $return_arr = $fabirc_type_map_del;
        }
        else
        {
            //添加MAP绑定关系
            $map_arr = array(
                'cms_fabirc_id' => $this->arr_params['cms_id'],
                'cms_fabirc_type_id' => $this->arr_params['cms_fabirc_type_id'],
            );
            //添加type绑定关系
            $fabirc_type_map = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_map', 'add', $map_arr);
            if($fabirc_type_map['ret'] != 0)
            {
                $return_arr = $fabirc_type_map;
            }
            else
            {
                //修改面辅料
                $return_arr = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'edit', $this->arr_params);
            }
        }
        $this->load_view_file($return_arr);
    }

    /**
     * 删除
     */
    public function delete()
    {
        //先删除绑定关系
        $fabirc_type_map_del = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_map', 'del', array('cms_fabirc_id' => $this->arr_params['cms_id']));
        if($fabirc_type_map_del['ret'] != 0)
        {
            $return_arr = $fabirc_type_map_del;
        }
        else
        {
            //再删除数据
            $return_arr = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'del', $this->arr_params);
        }
        $this->load_view_file($return_arr);
    }
    /********************************************面辅料管理*********************************************/

    /********************************************面辅料属性*********************************************/
    /**
     * 面辅料属性列表
     */
    public function fabirc_attribute_list()
    {
        $this->_init_page();
        $fabirc_attribute_value = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'query',$this->arr_params);
        $page_info = $fabirc_attribute_value['page_info'];
        $attribute_list = array();
        if($fabirc_attribute_value['ret'] == 0 && !empty($fabirc_attribute_value['data_info']))
        {
            $attribute_list = $fabirc_attribute_value['data_info'];
        }
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/fabirc/c_fabirc_manager/attribute_add.php',//右侧按钮弹框
                'class'=>'order-fabirc-c_fabirc_manager-attribute_add',//form表单ID
                'ajax'=>'order/fabirc/c_fabirc_manager/attribute_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/attribute_edit.php',
                'class'=>'order-fabirc-c_fabirc_manager-attribute_edit',
                'ajax'=>'order/fabirc/c_fabirc_manager/attribute_edit',
                'function'=>'edit',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/attribute_delete.php',
                'class'=>'order-fabirc-c_fabirc_manager-attribute_delete',
                'ajax'=>'order/fabirc/c_fabirc_manager/attribute_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );
        $return_arr = array(
            'data_info' => $attribute_list,//面辅料属性列表
            'system_file_list' => $system_file_list,
            'page_info'   => $page_info,
        );
        $this->load_view_file($return_arr,__LINE__);
    }

    /**
     * 面辅料属性添加，json返回
     */
    public function attribute_add()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'add', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }
        $this->load_view_file($return_arr);
    }

    /**
     * 面辅料属性删除，json返回
     */
    public function attribute_delete()
    {
        //属性删除前判断属性下是否有属性值
        $attribute_list = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute_value', 'query',array('cms_fabirc_attribute_id' => $this->arr_params['cms_id']));
        if($attribute_list['ret'] == 0 && is_array($attribute_list['data_info']) && count($attribute_list['data_info']) > 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败，当前属性下包含属性值，请先删除属性值！');
        }
        else
        {
            $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'del', $this->arr_params);
            $return_arr = array('ret' => 0, 'reason' => '操作成功');
            if($fabirc_attribute['ret'] != 0)
            {
                $return_arr = array('ret' => 1, 'reason' => '操作失败');
            }
        }
        $this->load_view_file($return_arr);
    }
    /**
     * 面辅料属性修改,json返回
     */
    public function attribute_edit()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'edit', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }

        $this->load_view_file($return_arr);
    }
    /********************************************面辅料属性*********************************************/

    /********************************************面辅料属性值*********************************************/
    /**
     * 面辅料属性值列表
     */
    public function fabirc_attribute_value_list()
    {
        $this->_init_page();
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute_value', 'query',$this->arr_params);
        $page_info = $fabirc_attribute['page_info'];
        $attribute_list = array();
        if($fabirc_attribute['ret'] == 0 && !empty($fabirc_attribute['data_info']))
        {
            $attribute_list = $fabirc_attribute['data_info'];
        }
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/fabirc/c_fabirc_manager/attribute_value_add.php',//右侧按钮弹框
                'class'=>'order-fabirc-c_fabirc_manager-attribute_value_add',//form表单ID
                'ajax'=>'order/fabirc/c_fabirc_manager/attribute_value_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/attribute_value_edit.php',
                'class'=>'order-fabirc-c_fabirc_manager-attribute_value_edit',
                'ajax'=>'order/fabirc/c_fabirc_manager/attribute_value_edit',
                'function'=>'edit',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/fabirc/c_fabirc_manager/attribute_value_delete.php',
                'class'=>'order-fabirc-c_fabirc_manager-attribute_value_delete',
                'ajax'=>'order/fabirc/c_fabirc_manager/attribute_value_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );
        $return_arr = array(
            'data_info' => $attribute_list,//面辅料属性列表
            'system_file_list' => $system_file_list,
            'page_info'   => $page_info,
            'attribute_id' => $this->arr_params['cms_fabirc_attribute_id'],
        );
        $this->load_view_file($return_arr,__LINE__);
    }
    /**
     * 面辅料属性添加，json返回
     */
    public function attribute_value_add()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute_value', 'add', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }
        $this->load_view_file($return_arr);
    }

    /**
     * 面辅料属性删除，json返回
     */
    public function attribute_value_delete()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute_value', 'del', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }

        $this->load_view_file($return_arr);
    }
    /**
     * 面辅料属性修改,json返回
     */
    public function attribute_value_edit()
    {
        $fabirc_attribute = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute_value', 'edit', $this->arr_params);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($fabirc_attribute['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }

        $this->load_view_file($return_arr);
    }
    /********************************************面辅料属性值*********************************************/
}