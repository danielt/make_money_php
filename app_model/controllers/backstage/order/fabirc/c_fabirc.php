<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/12
 */
defined('BASEPATH') or exit('No direct script access allowed');
class c_fabirc extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
    }
    public function fabirc_view()
    {
        $this->_init_page();
        $fabirc_type = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc_type', 'query_all');
        if($fabirc_type['ret'] != 0)
        {
            $fabirc_type = array();
        }
        else
        {
            $fabirc_type = $fabirc_type['data_info'];
        }
        //获取全部产品
        $fabirc_prodcut = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'query');
        if($fabirc_prodcut['ret'] != 0)
        {
            $fabirc_prodcut = array();
        }
        else
        {
            $fabirc_prodcut = $fabirc_prodcut['data_info'];
        }
        $return_arr = array(
            'fabirc_type' => $fabirc_type,//面辅料订单类型
            'fabirc_product' => $fabirc_prodcut,
        );
        $this->load_view_file($return_arr);
    }

    /**
     * 前端AJAX请求属性
     */
    public function fabirc_attribute()
    {
        //根据面辅料订单类型获取右侧面辅料清单
        if(isset($this->arr_params['cms_fabirc_type_id']) && !empty($this->arr_params['cms_fabirc_type_id']))
        {
            $fabirc = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'query_by_type', array('cms_fabirc_type_id' => $this->arr_params['cms_fabirc_type_id']));
        }
        else
        {
            $fabirc = $this->auto_load_table('order','fabirc', 'c_fabirc', 'order_fabirc', 'query');
        }
        if($fabirc['ret'] == 0 && !empty($fabirc['data_info']))
        {
            $fabirc = $fabirc['data_info'];
        }
        else
        {
            $fabirc = array();
        }
        $return_arr = array(
            'data_info' => $fabirc,//面辅料
        );
        //取出面辅料
        $this->load_view_file($return_arr,__LINE__);
    }

    /**
     * 前端根据属性ID请求属性清单和对应的值
     */
    public function fabirc_attribute_value()
    {
        $fabirc_attribute_value = array();
        if(isset($this->arr_params['cms_fabirc_attribute']) && !empty($this->arr_params['cms_fabirc_attribute']))
        {
            $arr_attribute = explode(",", $this->arr_params['cms_fabirc_attribute']);
            $fabirc_attribute_value = $this->auto_load_table('order','fabirc', 'c_fabirc_manager', 'order_fabirc_attribute', 'query_attribute_and_value', array('cms_id' => $arr_attribute));
        }
        $attribute_value = array();
        if(!empty($fabirc_attribute_value['data_info']) && is_array($fabirc_attribute_value['data_info']))
        {
            foreach ($fabirc_attribute_value['data_info'] as $value)
            {
                $attribute_value[$value['cms_id']]['id'] = $value['cms_id'];
                $attribute_value[$value['cms_id']]['value'] = $value['cms_name'];
                $attribute_value[$value['cms_id']]['childrend'][] = array('id' => $value['value_id'], 'value' => $value['value_name']);
            }
            $return_arr = array(
                'ret' => 0,
                'data_info' => $attribute_value,
            );
        }
        else
        {
            $return_arr = array(
                'ret' => 1,
                'data_info' => '数据请求失败'
            );
        }

        $this->load_view_file($return_arr,__LINE__);
    }
    /**
     * 前端根据属性ID（多个以逗号分隔）与面辅料分类ID（多个以逗号分隔），合起来使用交集
     */
    public function fabirc_product_list()
    {
        $this->_init_page();
        $query = array(
            'cms_fabirc_id' => !empty($this->arr_params['cms_fabirc_id']) ? explode(",", $this->arr_params['cms_fabirc_id']) : '',
            'cms_attribute_value_id' => !empty($this->arr_params['cms_attribute_value_id']) ? explode(",", $this->arr_params['cms_attribute_value_id']) : '',
        );
        $fabirc_prodcut = $this->auto_load_table('order','fabirc', 'c_fabirc_product', 'order_fabirc_material', 'query_product',$query);

        if($fabirc_prodcut['ret'] != 0 || !is_array($fabirc_prodcut['data_info']) || empty($fabirc_prodcut['data_info']))
        {
            $return_arr = array(
                'ret' => 1,
                'reason' => '数据请求失败',
            );
        }
        else
        {
            $return_arr = array(
                'ret' => 0,
                'reason' => '成功',
                'page_info' => $fabirc_prodcut['page_info'],
                'data_info' => $fabirc_prodcut['data_info'],
            );
        }
        $this->load_view_file($return_arr,__LINE__);
    }
    /**
     * 预存用户选择面辅料订单
     */
    public function user_fabirc_order()
    {
        $return_arr = array(
            'ret' => 1,
            'reason' => '参数错误'
        );
        if(strlen($this->arr_params['cms_fabirc_type_id']) > 0 && strlen($this->arr_params['cms_first_type_id']) > 0 && strlen($this->arr_params['cms_basic_id']) > 0
            && strlen($this->arr_params['cms_user_id']) > 0 && strlen($this->arr_params['cms_product_id']) > 0)
        {
            $product = explode(",", $this->arr_params['cms_product_id']);
            $this->load->driver('cache');
            $key = md5($this->arr_params['cms_fabirc_type_id'] . "|#" . $this->arr_params['cms_first_type_id'] . "|#" . $this->arr_params['cms_basic_id'] . "|#" . $this->arr_params['cms_user_id']);
            //获取数据
            $menu_data = $this->cache->redis->get($key);
            if($menu_data) //存在数据，更新
            {
                foreach ($product as $val)
                {
                    if(!in_array($val,$menu_data))
                    {
                        $menu_data[] = $val;
                    }
                }
            }
            else
            {
                $menu_data = $product;
            }
            $re = $this->cache->redis->save($key,$menu_data,86400);
            if($re)
            {
                $return_arr = array('ret' => 0);
            }
        }
        $this->load_view_file($return_arr,__LINE__);
    }

    /**
     * 获取用户面辅料订单数据
     */
    public function get_user_fabirc_order()
    {
        $return_arr = array(
            'ret' => 1,
            'reason' => '参数错误'
        );
        if(strlen($this->arr_params['cms_fabirc_type_id']) > 0 && strlen($this->arr_params['cms_first_type_id']) > 0 && strlen($this->arr_params['cms_basic_id']) > 0
            && strlen($this->arr_params['cms_user_id']) > 0)
        {
            $this->load->driver('cache');
            $key = md5($this->arr_params['cms_fabirc_type_id'] . "|#" . $this->arr_params['cms_first_type_id'] . "|#" . $this->arr_params['cms_basic_id'] . "|#" . $this->arr_params['cms_user_id']);
            //获取数据
            $menu_data = $this->cache->redis->get($key);
            if($menu_data)
            {
                $return_arr = array(
                    'ret' => 0,
                    'data_info' => $menu_data
                );
            }
            else
            {
                $return_arr = array(
                    'ret' => 1,
                    'reason' => '无数据'
                );
            }
        }
        $this->load_view_file($return_arr,__LINE__);
    }
}