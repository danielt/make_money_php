<?php
class c_simple_dress extends CI_Controller
{

    public $order_not_public = 0; //未支付
    public $order_public = 1;  //已支付
    public $order_accepting = 2; //发布中
    public $order_accepted = 3; //发布
    public $order_acceptor_done = 4; //样衣师傅完成订单
    public $order_completed = 5;//订单完成



    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->library('session');
    }


    /**
     * 管理员订单
     */
    public function admin_index()
    {
        // 初始化页面
        $this->_init_page();

        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'query');

        // 分页信息
        $page_info = $arr_order["page_info"];
        $arr_order = $arr_order["data_info"];

        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/simple_dress/c_simple_dress/republic_edit.php',
                'class'=>'order-simple_dress-c_simple_dress-republic_edit',
                'ajax'=>'order/simple_dress/c_simple_dress/republic_edit',
                'function'=>'republic_edit',
                'button_data'=>array(
                    array(
                        'name'=>'发布',
                        'icon'=>'fa-pencil-square-o',
                        'params'=>'',
                        'where'=>'',
                        'button' => 'false',
                    ),
                ),
            ),

        );
        $return_arr = array(
            'data_info' => $arr_order,//订单列表
            'system_file_list' => $system_file_list,
            'url_params'  => $this->arr_params,
            'page_info'   => $page_info,
            'list_url' =>'order/simple_dress/c_simple_dress/admin_index',
        );
        $this->load_view_file($return_arr,__LINE__);
    }

    /**
     * 发布样衣订单
     * @return array
     */
    public function republic_edit(){
        $cms_id = $this->arr_params['cms_id'];
        $cms_price = $this->arr_params['cms_price'];

        $param_where = array(
            'where' => array(
                'cms_id' => $cms_id,
            ),
        );
        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'query_only',$param_where);
        if($arr_order['data_info']['cms_state'] !== '1'){

            $this->load_view_file(array('ret'=>1,'reason'=>'无法发布的订单'),__LINE__);
        }
        $arr_param = array(
            'set'=>array(
                'cms_state'=>2,
                'cms_price'=>$cms_price,
            ),
            'where'=>array(
                'cms_id'=>$this->arr_params['cms_id'],
            ),
        );

        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'edit',$arr_param);

        if ($arr_param['ret'] != 0){
            return em_return::_return_error_data($arr_order);
        }
        $this->load_view_file($arr_order,__LINE__);
    }

    /**
     * 用户订单
     */
    public function manager_index(){
        // 初始化页面
        $this->_init_page();

        //查询
        $where_param = array(
            'where'=>array(
                'cms_manager_id' => $this->session->userdata('user_id'),
            ),
        );
        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'query');

        // 分页信息
        $page_info = $arr_order["page_info"];
        $arr_order = $arr_order["data_info"];

        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/simple_dress/c_simple_dress/check.php',
                'class'=>'order-simple_dress-c_simple_dress-check',
                'ajax'=>'order/simple_dress/c_simple_dress/check',
                'function'=>'check',
                'button_data'=>array(
                    array(
                        'name'=>'查看',
                        'icon'=>'fa-pencil-square-o',
                        'params'=>'',
                        'where'=>'',
                        'button_display' => 'true',
                    ),
                ),
            ),

        );
        $return_arr = array(
            'data_info' => $arr_order,//订单列表
            'system_file_list' => $system_file_list,
            'url_params'  => $this->arr_params,
            'page_info'   => $page_info,
            'list_url' =>'order/simple_dress/c_simple_dress/manager_index',
        );
        $this->load_view_file($return_arr,__LINE__);
    }

    /**
     * 获取发布状态中的样衣订单
     */
    public function get_republic_order(){

        $this->arr_page_params['cms_page_num'] = isset($this->arr_params["cms_page_num"]) ? $this->arr_params["cms_page_num"] : 1;
        $this->arr_page_params['cms_page_size'] = isset($this->arr_params["cms_page_size"]) ? $this->arr_params["cms_page_size"] : 5;

        $arr_where = array(
            'where'=>array(
                'cms_state' => 2,
            ),
        );

        //根据样板师样衣师id查询对应自己的订单
        if(isset($this->arr_params['cms_simple_manager_id']) && strlen($this->arr_params['cms_simple_manager_id']) > 1){
            $arr_where['where']['cms_acceptor_id'] = $this->arr_params['cms_simple_manager_id'];
        }
        if(isset($this->arr_params['cms_state']) && strlen($this->arr_params['cms_state']) > 0){
            $arr_where['where']['cms_state'] = $this->arr_params['cms_state'];
        }


        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'query',$arr_where);

        foreach ($arr_order['data_info'] as &$arr_order_item){
            if(strlen($arr_order_item['cms_content']) > 1){
                $arr_order_item['cms_content'] = 'www.unioncloth.com/data_model/upload/' . $arr_order_item['cms_content'];
            }
        }

        if ($arr_order['ret'] != 0){
            $this->load_view_file($arr_order,__FILE__);
        }
        $this->flag_ajax_reurn = true;
        $return_arr = array(
            'ret' => 0,
            'reason' => '获取成功',
            'data_info' => $arr_order['data_info'],//订单列表
            'page_info'   => $arr_order['page_info'],
        );
        $this->load_view_file($return_arr,__FILE__);
    }

    /**
     * 样衣师接单接口
     * ***request***
     * cms_acceptor_id   接单样衣师id
     * cms_order_id      订单的order_id
     * ***return***
     * array('ret'=>0成功1失败,'reason'=>'信息','data_info'=>array())
     */
    public function acceptor_simple_dress_order(){

        $cms_acceptor_id = $this->arr_params['cms_acceptor_id'];
        $cms_order_id = $this->arr_params['cms_order_id'];

        $arr_where = array(
            'set' => array(
                'cms_acceptor_id' => $cms_acceptor_id,
                'cms_state' => 3,
            ),
            'where' => array(
                'cms_order_id' => $cms_order_id,
            ),
        );

        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'edit',$arr_where);

        $this->flag_ajax_reurn = true;

        if($arr_order['ret'] != 0){
            $arr_order['reason'] = '接单失败';
            $this->load_view_file($arr_order,__FILE__);
        }
        $this->load_view_file($arr_order,__FILE__);
    }

    /**
     * 样衣师完成订单
     * ***request***
     * cms_acceptor_id   接单样衣师id
     * cms_order_id      订单的order_id
     * ***return***
     * array('ret'=>0成功1失败,'reason'=>'信息','data_info'=>array())
     */
    public function simple_complete_order(){

        $cms_acceptor_id = $this->arr_params['cms_acceptor_id'];
        $cms_order_id = $this->arr_params['cms_order_id'];

        $arr_where = array(
            'set' => array(
                'cms_state' => 4,
            ),
            'where' => array(
                'cms_acceptor_id' => $cms_acceptor_id,
                'cms_order_id' => $cms_order_id,
            ),
        );

        $arr_order = $this->auto_load_table('order','simple_dress', 'c_simple_dress', 'simple_dress_order', 'edit',$arr_where);
        $this->flag_ajax_reurn = true;

        if($arr_order['ret'] != 0){
            $arr_order['reason'] = '完成订单失败';
            $this->load_view_file($arr_order,__FILE__);
        }
        $this->load_view_file($arr_order,__FILE__);
    }

}