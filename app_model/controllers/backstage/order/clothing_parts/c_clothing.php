<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2019/1/9
 * Time: 23:45
 */
/**
 * 服装部件拼装前端
 */
defined('BASEPATH') or exit('No direct script access allowed');
class c_clothing extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        $this->need_login = false;
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
    }
    public function clothing_parts()
    {
      $return_arr = array(
            'cms_clothing_id' => $this->arr_params['cms_type_id'],
        );
        $this->load_view_file($return_arr);
    }

    /**
     * 前端AJAX请求部件分类
     */
    public function parts_type()
    {
        //根据大类ID获取服装部件类型
        if(strlen($this->arr_params['cms_clothing_id']) > 0)
        {
            $parts = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_type', 'query_all_by_clothing_type', array('cms_clothing_id' => $this->arr_params['cms_clothing_id']));
            if($parts['ret'] != 0 || empty($parts['data_info']) || !is_array($parts['data_info']))
            {
                $return_arr = array(
                    'ret' => 1,
                    'reason' => '部件类型查询错误'
                );
            }
            else
            {
                $return_arr = array(
                    'ret' => 0,
                    'data_info' => $parts['data_info'],
                );
            }
        }
        else
        {
            $return_arr = array(
                'ret' => 1,
                'reason' => '大类分类ID不存在'
            );
        }
        $this->load_view_file($return_arr);
    }

    public function parts_list()
    {
        //根据部件类型ID获取部件
        if(strlen($this->arr_params['cms_parts_type_id']) > 0)
        {
            $parts = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts','order_clothing_parts_info', 'query_all', array('cms_parts_type_id' => $this->arr_params['cms_parts_type_id']));
            if($parts['ret'] != 0 || empty($parts['data_info']) || !is_array($parts['data_info']))
            {
                $return_arr = array(
                    'ret' => 1,
                    'reason' => '部件查询错误'
                );
            }
            else
            {
                $return_arr = array(
                    'ret' => 0,
                    'data_info' => $parts['data_info'],
                );
            }
        }
        else
        {
            $return_arr = array(
                'ret' => 1,
                'reason' => '部件分类ID不存在'
            );
        }
        $this->load_view_file($return_arr);
    }
}