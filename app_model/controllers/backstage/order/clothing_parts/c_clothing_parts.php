<?php
/**
 * 服装部件拼装后端
 */
defined('BASEPATH') or exit('No direct script access allowed');
class c_clothing_parts extends CI_Controller
{
//    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
//    {
//        $this->need_login = false;
//        parent::__construct($str_class, $str_file, $str_method, $str_directory);
//    }
    /**************************************************部件类型-begin**************************************************/
    /**
     * 部件类型列表
     */
    public function parts_type_list()
    {
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/clothing_parts/c_clothing_parts/parts_type_add.php',//右侧按钮弹框
                'class'=>'order_clothing_parts_c_clothing_parts_parts_type_add',//form表单ID
                'ajax'=>'order/clothing_parts/c_clothing_parts/parts_type_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/clothing_parts/c_clothing_parts/parts_type_edit.php',
                'class'=>'order_clothing_parts_c_clothing_parts_parts_type_edit',
                'ajax'=>'order/clothing_parts/c_clothing_parts/parts_type_edit',
                'function'=>'edit',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/clothing_parts/c_clothing_parts/parts_type_delete.php',
                'class'=>'order_clothing_parts_c_clothing_parts_parts_type_delete',
                'ajax'=>'order/clothing_parts/c_clothing_parts/parts_type_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );

        $this->_init_page();
        //联表获取服装分类信息及名称
        $clothing_parts = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_type', 'query',$this->arr_params);
        $page_info = $clothing_parts['page_info'];
        if($clothing_parts['ret'] != 0)
        {
            $clothing_parts = array();
        }
        else
        {
            $clothing_parts = $clothing_parts['data_info'];
        }
        //查询出绑定关系
        $reset = array();
        if(!empty($clothing_parts)) //查询绑定的服装分类ID
        {
            foreach ($clothing_parts as $value)
            {
                $reset[$value['cms_id']] = $value;
            }
            $type_map = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_parts_type_bind_clothing', 'query_all', array('cms_parts_type_id' => array_keys($reset)));

            if($type_map['ret'] == 0 && is_array($type_map['data_info']) && !empty($type_map['data_info']))
            {
                foreach ($type_map['data_info'] as $val)
                {
                    $reset[$val['cms_parts_type_id']]['cms_clothing_id'][] = $val['cms_clothing_id'];
                }
            }
        }

        if(is_array($reset) && !empty($reset))
        {
            foreach ($reset as &$v)
            {
                if(isset($v['cms_clothing_id']))
                {
                    $v['cms_clothing_id'] = implode(",", $v['cms_clothing_id']);
                }
            }
        }

        //获取全部服装分类
        $arr_order_type = $this->auto_load_table('order', 'con_client', 'c_client_order', 'order_order_type', 'query_all');
        $type_id = array_column($arr_order_type['data_info'],'cms_type_id');
        $type_name = array_column($arr_order_type['data_info'],'cms_type_name');
        $clothing_list = array();

        foreach ($arr_order_type['data_info'] as $key=>$arr_value){
            $clothing_list[$key]['cms_id'] =$arr_value['cms_type_id'];
            $clothing_list[$key]['cms_name'] =$arr_value['cms_type_name'];
         }

        $return_arr = array(
            'page_info'   => $page_info,
            'data_info' => $reset,
            'system_file_list' => $system_file_list,
            'clothing_list' => $clothing_list,//服装分类
        );
        $this->load_view_file($return_arr);
    }

    public function parts_type_add()
    {
        //先添加部件类型
        $parts = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_type', 'add', $this->arr_params);
        if($parts['ret'] != 0)
        {
            $return_arr = $parts;
        }
        else
        {
            $map_arr = array(
                'cms_parts_type_id' => $parts['data_info']['cms_id'],
                'cms_clothing_id' => explode(",", $this->arr_params['cms_clothing_id']),
            );
            //添加type绑定关系
            $return_arr = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_parts_type_bind_clothing', 'add', $map_arr);
            if($return_arr['ret'] != 0)
            {
                //添加失败删除面辅料
                $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_parts_type_bind_clothing', 'del', array('cms_id' => $parts['data_info']['cms_id']));
            }
        }
        $this->load_view_file($return_arr);
    }

    public function parts_type_edit()
    {
        //根据部件类型ID先删除绑定关系
        $type_map_del = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_parts_type_bind_clothing', 'del', array('cms_parts_type_id' => $this->arr_params['cms_id']));

        if($type_map_del['ret'] != 0)
        {
            $return_arr = $type_map_del;
        }
        else
        {
            //删除成功再添加MAP绑定关系
            $map_arr = array(
                'cms_parts_type_id' => $this->arr_params['cms_id'],
                'cms_clothing_id' => explode(",", $this->arr_params['cms_clothing_id']),
            );
            //添加type绑定关系
            $type_map = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_parts_type_bind_clothing', 'add', $map_arr);
            if($type_map['ret'] != 0)
            {
                $return_arr = $type_map;
            }
            else
            {
                //修改面部件类型
                $return_arr = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_type', 'edit', $this->arr_params);
            }
        }
        $this->load_view_file($return_arr);
    }

    public function parts_type_delete()
    {
        //先删除绑定关系
        $type_map_del = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_parts_type_bind_clothing', 'del', array('cms_parts_type_id' => $this->arr_params['cms_id']));
        if($type_map_del['ret'] != 0)
        {
            $return_arr = $type_map_del;
        }
        else
        {
            //再删除数据
            $return_arr = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_type', 'del', $this->arr_params);
        }
        $this->load_view_file($return_arr);
    }
    /**************************************************部件类型-end**************************************************/

    /**************************************************部件-begin**************************************************/
    public function parts_list()
    {
        $this->_init_page();
        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/clothing_parts/c_clothing_parts/parts_add.php',//右侧按钮弹框
                'class'=>'order_clothing_parts_c_clothing_parts_parts_add',//form表单ID
                'ajax'=>'order/clothing_parts/c_clothing_parts/parts_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/clothing_parts/c_clothing_parts/parts_edit.php',
                'class'=>'order_clothing_parts_c_clothing_parts_parts_edit',
                'ajax'=>'order/clothing_parts/c_clothing_parts/parts_edit',
                'function'=>'edit',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'修改',
                        'icon'=>'fa-pencil-square-o',
                        'button_display' => true,//按钮是否隐藏，默认打开
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/clothing_parts/c_clothing_parts/parts_delete.php',
                'class'=>'order_clothing_parts_c_clothing_parts_parts_delete',
                'ajax'=>'order/clothing_parts/c_clothing_parts/parts_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );
        $parts = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_info', 'query_by_condition',$this->arr_params);
        $page_info = $parts['page_info'];
        $product = array();
        if($parts['ret'] == 0 && !empty($parts['data_info']))
        {
            $product = $parts['data_info'];
        }

        //获取所有部件分类
        $clothing_type = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_type', 'query_all');
        if($clothing_type['ret'] == 0 && !empty($clothing_type['data_info']))
        {
            $clothing_type = $clothing_type['data_info'];
        }
        else
        {
            $clothing_type = array();
        }

        $return_arr = array(
            'data_info' => $product,
            'system_file_list' => $system_file_list,
            'page_info'   => $page_info,
            'clothing_type' => $clothing_type,
        );
        $this->load_view_file($return_arr,__LINE__);
    }

    public function parts_add()
    {
        $return_arr = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_info', 'add', $this->arr_params);

        $this->load_view_file($return_arr);
    }

    public function parts_edit()
    {
        $return_arr = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_info', 'edit', $this->arr_params);
        $this->load_view_file($return_arr);
    }

    public function parts_delete()
    {
        $return_arr = $this->auto_load_table('order','clothing_parts', 'c_clothing_parts', 'order_clothing_parts_info', 'del', $this->arr_params);

        $this->load_view_file($return_arr);
    }
    /**************************************************部件-end**************************************************/
}