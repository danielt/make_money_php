<?php
/**
 * Created by PhpStorm.
 * User: fingal
 * Date: 2018/12/25
 * Time: 13:56
 */

class c_leisure extends CI_Controller
{
    public function __construct($str_class = null, $str_file = null, $str_method = null, $str_directory = null)
    {
        parent::__construct($str_class, $str_file, $str_method, $str_directory);
        $this->load->library('session');
    }

    public function index()
    {

        // 初始化页面
        $this->_init_page();

        $free_time = strtotime($this->arr_params['cms_free_time']);

        // 搜索关键字
        $arr_param = array(
            'where' => array(
                'cms_name' => $this->arr_params['cms_name'],
                'cms_free_time' => $free_time,
                'cms_person_amount' => $this->arr_params['cms_person_amount'],
            ),
        );

        if (strlen($this->arr_params['cms_name']) < 1) {
            unset($arr_param['where']['cms_name']);
        }
        if (strlen($this->arr_params['cms_free_time']) < 1) {
            unset($arr_param['where']['cms_free_time']);
        }
        if (strlen($this->arr_params['cms_person_amount']) < 1) {
            unset($arr_param['where']['cms_person_amount']);
        }

        $arr_order = $this->auto_load_table('order', 'manufacture', 'c_leisure', 'producer_time', 'query', $arr_param);


        // 分页信息
        $page_info = $arr_order["page_info"];
        $arr_order = $arr_order["data_info"];

        //底部选择框
        $system_file_list = array(
            array(
                'url' => 'order/con_client/c_client_order/check.php',
                'class' => 'order-con_client-c_client_order-check',
                'ajax' => 'order/con_client/c_client_order/edit',
                'function' => 'edit',
                'button_data' => array(
                    array(
                        'name' => '查看',
                        'icon' => 'fa-pencil-square-o',
                        'params' => '',
                        'where' => '',
                        'button_display' => '',
                    ),
                ),
            ),

        );
        $return_arr = array(
            'data_info' => $arr_order,//订单列表
            'system_file_list' => $system_file_list,
            'url_params' => $this->arr_params,
            'page_info' => $page_info,
            'list_url' => 'order/index/c_index/index',
        );


        $this->load_view_file($return_arr, __LINE__);
    }

    public function manager_leisure(){

        // 初始化页面
        $this->_init_page();

        $user_id = $this->session->userdata('user_id');
        $free_time = strtotime($this->arr_params['cms_free_time']);

        // 搜索关键字
        $arr_param = array(
            'where' => array(
                'cms_name' => $this->arr_params['cms_name'],
                'cms_free_time' => $free_time,
                'cms_manager_id' => $user_id,
                'cms_person_amount' => $this->arr_params['cms_person_amount']
            ),
        );

        if (strlen($this->arr_params['cms_name']) < 1) {
            unset($arr_param['where']['cms_name']);
        }
        if (strlen($this->arr_params['cms_free_time']) < 1) {
            unset($arr_param['where']['cms_free_time']);
        }
        if (strlen($this->arr_params['cms_person_amount']) < 1) {
            unset($arr_param['where']['cms_person_amount']);
        }

        $arr_order = $this->auto_load_table('order', 'manufacture', 'c_leisure', 'producer_time', 'query', $arr_param);


        // 分页信息
        $page_info = $arr_order["page_info"];
        $arr_order = $arr_order["data_info"];

        //底部选择框
        $system_file_list = array(
            array(
                'url'=>'order/manufacture/c_leisure/free_time_add.php',//右侧按钮弹框
                'class'=>'order-manufacture-c_leisure-free_time_add',//form表单ID
                'ajax'=>'order/manufacture/c_leisure/free_time_add',//form表单提交控制器
                'function'=>'add',//行为动作
                'button_data'=>array(
                    array(
                        'name'=>'添加',
                        'icon'=>'fa-plus',//样式
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
            array(
                'url'=>'order/manufacture/c_leisure/free_time_delete.php',
                'class'=>'order-manufacture-c_leisure-free_time_delete',
                'ajax'=>'order/manufacture/c_leisure/free_time_delete',
                'function'=>'delete',
                'button_data'=>array(
                    array(
                        'name'=>'删除',
                        'icon'=>'fa-trash-o',
                        'params' => '',
                        'where' => '',
                    ),
                ),
            ),
        );
        $return_arr = array(
            'data_info' => $arr_order,//订单列表
            'system_file_list' => $system_file_list,
            'url_params' => $this->arr_params,
            'page_info' => $page_info,
            'list_url' => 'order/index/c_index/manager_leisure',
        );
        $this->load_view_file($return_arr, __LINE__);
    }

    /**
     * 工厂删除空期信息
     */
    public function free_time_delete(){
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if (empty($cms_id) && !is_array($cms_id)) {
            $this->load_view_file(em_return::return_data(1, '删除参数条件为空'), __LINE__);
        }
        $delete_params = array(
                'cms_id' => $cms_id,
        );
        $re = $this->auto_load_table('order', 'manufacture', 'c_leisure', 'order_producer_time', 'del', $delete_params);
        $this->load_view_file($re, __LINE__);
    }

    /**
     * 添加空期信息
     */
    public function free_time_add()
    {
        $free_time = strtotime($this->arr_params['cms_free_time']);
        $person_amount = $this->arr_params['cms_person_amount'];
        $user_id = $this->session->userdata('user_id');
        $str_leisure_name = $this->arr_params['cms_name'];
        $add_param = array(
            'cms_person_amount' => $person_amount,
            'cms_free_time' => $free_time,
            'cms_manager_id' => $user_id,
            'cms_name' => $str_leisure_name,
        );


        $producer_time = $this->auto_load_table('order','manufacture', 'c_leisure', 'order_producer_time', 'add', $add_param);
        $return_arr = array('ret' => 0, 'reason' => '操作成功');
        if($producer_time['ret'] != 0)
        {
            $return_arr = array('ret' => 1, 'reason' => '操作失败');
        }
        $this->load_view_file($return_arr);
    }

}