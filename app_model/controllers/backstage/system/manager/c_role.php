<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 角色模块
 * @author pan.liang
 */
class c_role extends CI_Controller
{
    
    /**
     * 列表页
     */
    public function index()
    {
        //初始化按钮
        $this->init_system_file_button_list();
        $menu_data_list = $this->CI_make_menu();
        $this->_init_page();
        $where_params = array('where'=>($this->arr_params));
        $data_info = $this->auto_load_table('system','manager','c_role','system_role', 'query',$where_params);
        $data_info['menu_data_list'] = $menu_data_list;
        $this->load_view_file($data_info,__LINE__);
    }
    
    /**
     * 添加
     */
    public function add()
    {
        $arr_button_id = null;
        if(isset($this->arr_params['cms_button_id']))
        {
            $arr_button_id = $this->arr_params['cms_button_id'];
            unset($this->arr_params['cms_button_id']);
        }
        $where_params = array('where'=>array('cms_name'=>$this->arr_params['cms_name']));
        $result_query = $this->auto_load_table('system','manager','c_role','system_role', 'query',$where_params);
        if($result_query['ret'] !=0)
        {
            $this->load_view_file($result_query,__LINE__);
        }
        if(isset($result_query['data_info']) && !empty($result_query['data_info']) && is_array($result_query['data_info']))
        {
            $this->load_view_file(em_return::return_data(1,'数据已经存在，不做任何操作'),__LINE__);
        }
        $insert_params = array('insert'=>$this->arr_params);
        $result_add = $this->auto_load_table('system','manager','c_role','system_role', 'add',$insert_params);
        if($result_add['ret'] !=0 || !is_array($arr_button_id) || empty($arr_button_id))
        {
            $this->load_view_file($result_add,__LINE__);
        }
        $result_role_id = $result_add['data_info']['cms_id'];
        $del_params = array('where'=>array('cms_role_id'=>$result_role_id));
        $result_button_del = $this->auto_load_table('system','manager','c_role','system_role_bind', 'delete',$del_params);
        if($result_button_del['ret'] !=0)
        {
            $this->load_view_file($result_button_del,__LINE__);
        }
        foreach ($arr_button_id as $value)
        {
            $insert_params = array('insert'=>array('cms_role_id'=>$result_role_id,'cms_button_id'=>$value,'cms_state'=>0));
            $result_add = $this->auto_load_table('system','manager','c_role','system_role_bind', 'add',$insert_params);
            if($result_add['ret'] !=0)
            {
                $this->load_view_file($result_add,__LINE__);
            }
        }
        $this->load_view_file($result_add,__LINE__);
    }
    
    public function bind ()
    {
        $this->load_view_file(null,__LINE__);
    }
    
    
    public function CI_make_child($level = 1,$parent=0)
    {
        $last_data=$temp_data=null;
        $num = rand(1, 6);
        if($level >3)
        {
            return null;
        }
        for ($i=0;$i<$num;$i++)
        {
            $id = em_guid::em_guid_rand();
            $last_data[] = array(
                'text'=>$level."级分类[{$i}]",
                'text1111'=>$level."级分类[{$i}]",
                'levels'=>1,
                'tags'=>array(        ),
                'cms_name'=>$level."级分类[{$i}]",
                'cms_parent_id'=>$parent,
                'cms_id'=>$id,
                'cms_order'=>0.00,
                'nodes'=>$this->CI_make_child($level+1,$id),
            );
        }
        return $last_data;
    }
    
    /**
     * 修改
     */
    public function edit()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(strlen($cms_id) <1)
        {
            $this->load_view_file(em_return::return_data(1,'修改参数条件为空'),__LINE__);
        }
        unset($this->arr_params['cms_id']);
        $edit_params = array(
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
            'set'=>$this->arr_params,
        );
        $this->load_view_file($this->auto_load_table('system','manager','c_role','system_role', 'edit',$edit_params),__LINE__);
    }
    
    
    
    /**
     * 删除
     */
    public function delete()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(empty($cms_id) && !is_array($cms_id))
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $delete_params = array(
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('system','manager','c_role','system_role', 'delete',$delete_params),__LINE__);
    }
    
    
    
    public function state()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(empty($cms_id) && !is_array($cms_id))
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $cms_state = isset($this->arr_params['cms_state']) ? $this->arr_params['cms_state'] : null;
        if(strlen($cms_state) <1)
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $edit_params = array(
            'set'=>array(
                'cms_state'=>$cms_state,
            ),
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('system','manager','c_role','system_role', 'edit',$edit_params),__LINE__);
    }
}