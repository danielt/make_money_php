<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 超级管理管理模块
 * @author pan.liang
 */
class c_manager extends CI_Controller
{
    /**
     * 列表页
     */
    public function index()
    {
        //初始化按钮
        $this->init_system_file_button_list();
        //初始化分页
        $this->_init_page();
        $where_params = array('where'=>($this->arr_params));
        $data_info = $this->auto_load_table('system','manager','c_manager','system_manager', 'query',$where_params);
        $this->load_view_file($data_info,__LINE__);
    }
    
    
    /**
     * 添加
     */
    public function add()
    {
        $insert_params = array('insert'=>($this->arr_params));
        $this->load_view_file($this->auto_load_table('system','manager','c_manager','system_manager', 'add',$insert_params),__LINE__);
    }
    
    public function bind ()
    {
        $this->load_view_file(null,__LINE__);
    }
    
    
    /**
     * 修改
     */
    public function edit()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(strlen($cms_id) <1)
        {
            $this->load_view_file(em_return::return_data(1,'修改参数条件为空'),__LINE__);
        }
        unset($this->arr_params['cms_id']);
        $edit_params = array(
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
            'set'=>$this->arr_params,
        );
        $this->load_view_file($this->auto_load_table('system','manager','c_manager','system_manager', 'edit',$edit_params),__LINE__);
    }
    
    
    
    /**
     * 删除
     */
    public function delete()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(empty($cms_id) && !is_array($cms_id))
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $delete_params = array(
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('system','manager','c_manager','system_manager', 'delete',$delete_params),__LINE__);
    }
    
    
    
    public function state()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(empty($cms_id) && !is_array($cms_id))
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $cms_state = isset($this->arr_params['cms_state']) ? $this->arr_params['cms_state'] : null;
        if(strlen($cms_state) <1)
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $edit_params = array(
            'set'=>array(
                'cms_state'=>$cms_state,
            ),
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('system','manager','c_manager','system_manager', 'edit',$edit_params),__LINE__);
    }
}