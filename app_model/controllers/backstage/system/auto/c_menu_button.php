<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 超级管理管理模块
 * @author pan.liang
 */
class c_menu_button extends CI_Controller
{
    /**
     * 列表页
     */
    public function index()
    {
        //初始化按钮
        $this->init_system_file_button_list();
        $result_menu = $this->auto_load_table('system','auto','c_project','system_menu', 'query',array('where'=>array('cms_level'=>3)));
        if($result_menu['ret'] !=0)
        {
            return $result_menu;
        }
        $menu_list = null;
        if(isset($result_menu['data_info']) && is_array($result_menu['data_info']) && !empty($result_menu['data_info']))
        {
            foreach ($result_menu['data_info'] as $val)
            {
                $menu_list[$val['cms_id']] = $val;
            }
        }
        //初始化分页
        $this->_init_page();
        $where_params = array('where'=>($this->arr_params));
        $data_info = $this->auto_load_table('system','auto','c_project','system_menu_button', 'query',$where_params);
        $data_info['menu_list'] = $menu_list;
        $this->load_view_file($data_info,__LINE__);
    }
    

    
    /**
     * 添加
     */
    public function add()
    {
        $insert_params = array('insert'=>($this->arr_params));
        $this->load_view_file($this->auto_load_table('system','auto','c_project','system_menu_button', 'add',$insert_params),__LINE__);
    }
    
    
    /**
     * 修改
     */
    public function edit()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(strlen($cms_id) <1)
        {
            $this->load_view_file(em_return::return_data(1,'修改参数条件为空'),__LINE__);
        }
        unset($this->arr_params['cms_id']);
        $edit_params = array(
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
            'set'=>$this->arr_params,
        );
        $this->load_view_file($this->auto_load_table('system','auto','c_project','system_menu_button', 'edit',$edit_params),__LINE__);
    }
    
    
    
    /**
     * 删除
     */
    public function delete()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(empty($cms_id) && !is_array($cms_id))
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $delete_params = array(
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('system','auto','c_project','system_menu_button', 'delete',$delete_params),__LINE__);
    }
    
    
    
    public function state()
    {
        $cms_id = isset($this->arr_params['cms_id']) ? $this->arr_params['cms_id'] : null;
        if(empty($cms_id) && !is_array($cms_id))
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $cms_state = isset($this->arr_params['cms_state']) ? $this->arr_params['cms_state'] : null;
        if(strlen($cms_state) <1)
        {
            $this->load_view_file(em_return::return_data(1,'删除参数条件为空'),__LINE__);
        }
        $edit_params = array(
            'set'=>array(
                'cms_state'=>$cms_state,
            ),
            'where'=>array(
                'cms_id'=>$cms_id,
            ),
        );
        $this->load_view_file($this->auto_load_table('system','auto','c_project','system_menu_button', 'edit',$edit_params),__LINE__);
    }
    
    
    public function auto_make_menu_button()
    {
        $base_dir = dirname(dirname(dirname(dirname(__FILE__))));
        $result = $this->auto_load_class('app_model/libraries/em_file.class.php');
        if($result['ret'] !=0)
        {
            $this->load_view_file($result,__LINE__);
        }
        $em_file = new em_file($this,'php');
        $result_files = $em_file->get_files_list($base_dir);
        if(empty($result_files) ||!is_array($result_files))
        {
            $this->load_view_file(em_return::return_data(1,'NO file find'),__LINE__);
        }
        $last_data = null;
        $result = $this->auto_load_table('system','auto','c_project','system_menu_button', 'edit',array('set'=>array('cms_state'=>1,'cms_modify_time'=>date("Y-m-d H:i:s"))));
        if($result['ret'] !=0)
        {
            $this->load_view_file($result,__LINE__);
        }
        $temp_arr = null;
        foreach ($result_files as $file_list)
        {
            if(!file_exists($file_list['path']))
            {
                continue;
            }
            $content = file_get_contents($file_list['path']);
            $content = preg_replace('/([\x80-\xff]*)/i','',$content);
            $content = str_replace(array("\r\n","\r","\n"),' ',$content);
            $content = str_replace(array("  "),' ',$content);
            preg_match_all("/\s+function\s+[a-z0-9_]+\s*\(/i",$content,$matches);
            if(!isset($matches[0]) || empty($matches[0]) || !is_array($matches))
            {
                continue;
            }
            $matches = $matches[0];
            $last_function = null;
            $default = '';
            foreach ($matches as $match_key=>$match_value)
            {
                $match_value = str_replace(array("\s*"),' ',$match_value);
                $match_value = trim(rtrim(trim($match_value),'('));
                $arr_match_value = explode(" ", $match_value);
                if(!is_array($arr_match_value) || count($arr_match_value) != 2 || !isset($arr_match_value[1]) || strlen($arr_match_value[1])<1)
                {
                    continue;
                }
                $arr_match_value[1] = trim($arr_match_value[1]);
                $value_length = strlen($arr_match_value[1]);
                if($value_length <1)
                {
                    continue;
                }
                if($value_length>=3)
                {
                    if(substr($arr_match_value[1],0,3) == 'CI_')
                    {
                        continue;
                    }
                    if(substr($arr_match_value[1],0,2) == '__')
                    {
                        continue;
                    }
                    if(substr($arr_match_value[1],0,1) == '_')
                    {
                        continue;
                    }
                }
                if($arr_match_value[1] == 'index')
                {
                    $default = 'index';
                }
                $last_function[] = $arr_match_value[1];
            }
            if(empty($last_function) || !is_array($last_function))
            {
                continue;
            }
            if(strlen($default) <1)
            {
                $default = $last_function[0];
            }
            $temp_path = trim(trim(trim(str_replace($this->config->_config_paths[0].'controllers', '', $file_list['path']),'\\'),'/'));
            $arr_temp = explode('/', $temp_path);
            if(!is_array($arr_temp) || empty($arr_temp) || count($arr_temp) != 4)
            {
                continue;
            }
            array_shift($arr_temp);
            $temp_project = array_shift($arr_temp);
            $temp_model = array_shift($arr_temp);
            $temp_child_model = array_shift($arr_temp);
            $temp_child_model = pathinfo($temp_child_model);
            $temp_child_model = isset($temp_child_model['filename']) ? $temp_child_model['filename'] : '';
            if(strlen($temp_child_model) <1 || strlen($temp_model) <1 ||strlen($temp_project) <1 || count(explode('.', $temp_child_model)) >2)
            {
                continue;
            }
            $temp_arr[] = array(
                'url'=>$temp_project.'/'.$temp_model.'/'.$temp_child_model,
                'button'=>$last_function,
                'default'=>$default,
            );
        }
        if(!is_array($temp_arr) || empty($temp_arr))
        {
            $this->load_view_file(em_return::return_data(1,'没有需要生成的按钮'),__LINE__);
        }
        $data = null; 
        foreach ($temp_arr as $value)
        {
            $data_info =$this->auto_load_table('system','auto','c_project','system_menu', 'query',array('where' => array('cms_url' => $value['url'],'cms_level'=>3)));
            
            if($data_info['ret']!=0)
            {
                $this->load_view_file($data_info,__LINE__);
            }
            if(!isset($data_info['data_info'][0]) || empty($data_info['data_info'][0]) || !is_array($data_info['data_info'][0]))
            {
                continue;
            }
            $data_info = $data_info['data_info'][0];
            foreach ($value['button'] as $_value)
            {
                $cms_is_default = ($_value == $value['default']) ? 0 : 1;
                $result = $this->auto_load_table('system', 'auto', 'c_project', 'system_menu_button', 'add_edit_one', array(
                    'insert' => array(
                        'cms_menu_id' => $data_info['cms_id'],
                        'cms_name'=>$_value,
                        'cms_mark'=> $_value,
                        'cms_function'=> $_value,
                        'cms_url' => $value['url'].'/'.$_value,
                        'cms_state'=>0,
                        'cms_is_default'=>$cms_is_default,
                    ),
                    'set' => array(
                        'cms_url' => $value['url'].'/'.$_value,
                        'cms_state'=>0,
                        'cms_is_default'=>$cms_is_default,
                    ),
                    'where' => array(
                        'cms_menu_id' => $data_info['cms_id'],
                        'cms_mark'=> $_value,
                    )
                ));
                if($result['ret'] !=0)
                {
                    return $result;
                }
            }
        }
        $result = $this->auto_load_table('system', 'auto', 'c_project', 'system_menu_button', 'delete', array('where' => array('cms_state' => 1)));
        $this->load_view_file($result,__LINE__);
    }
}
