<?php
/**
 * @Description：栏目
 * @Author：xinxin.deng
 * @CreateTime：2019/4/28 10:57
 */

class wintv_category_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'base_info' => array(
            'cms_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => 'UUID',
            ),
            'cms_name' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-128',
                'desc' => '栏目名称',
            ),
            'cms_category_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '栏目id',
            ),
            'cms_parent_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '父级栏目id，没有父级默认0',
            ),
            'cms_column_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '资源库ID',
            ),
            'cms_platform_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '平台、运营商ID',
            ),
            'cms_type' => array(
                'type' => 'int',
                'default'   => '0',
                'isempty' => '',
                'length' => '0-4',
                'desc' => '栏目类型，0点播，1直播',
            ),
            'cms_order' => array(
                'type' => 'int',
                'default'   => '0',
                'isempty' => '',
                'length' => '0-4',
                'desc' => '栏目排序',
            ),
            'cms_state' => array(
                'type' => 'tinyint',
                'default'   => 0,
                'isempty' => '',
                'length' => '0-4',
                'desc' => '管理状态  0 启用 | 1 禁用  默认禁用',
            ),
            'cms_create_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '创建时间',
            ),
            'cms_modify_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '修改时间',
            ),
        ),
    );
}