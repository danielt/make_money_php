<?php
/**
 * @Description：资源库根栏目
 * @Author：xinxin.deng
 * @CreateTime：2019/4/28 10:57
 */

class wintv_column_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'base_info' => array(
            'cms_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => 'UUID',
            ),
            'cms_name' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '资源库名称',
            ),
            'cms_type' => array(
                'type' => 'tinyint',
                'default'   => '',
                'isempty' => '',
                'length' => '1',
                'desc' => '资源库类型，0点播，1直播，默认0',
            ),
            'cms_platform_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '资源库对应平台ID',
            ),
            'cms_state' => array(
                'type' => 'tinyint',
                'default'   => 0,
                'isempty' => '',
                'length' => '0-4',
                'desc' => '管理状态  0 启用 | 1 禁用  默认禁用',
            ),
            'cms_create_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '创建时间',
            ),
            'cms_modify_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '修改时间',
            ),
        ),
    );
}