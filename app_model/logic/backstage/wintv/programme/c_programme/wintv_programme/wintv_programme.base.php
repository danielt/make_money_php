<?php
/**
 * @Description：节目单
 * @Author：xinxin.deng
 * @CreateTime：2019/4/28 10:57
 */

class wintv_programme_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'base_info' => array(
            'cms_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => 'UUID',
            ),
            'cms_name' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-128',
                'desc' => '节目单名称',
            ),
            'cms_channel_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '节目单所属频道id',
            ),
            'cms_import_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '节目单注入id',
            ),
            'cms_import_src' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '节目单注入来源',
            ),
            'cms_started_at' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '节目单开始时间',
            ),
            'cms_ended_at' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '节目单结束时间',
            ),
            'cms_duration' => array(
                'type' => 'int',
                'default'   => 0,
                'isempty' => '',
                'length' => '128',
                'desc' => '节目单时长',
            ),
            'cms_state' => array(
                'type' => 'tinyint',
                'default'   => 0,
                'isempty' => '',
                'length' => '0-4',
                'desc' => '管理状态  0 启用 | 1 禁用  默认禁用',
            ),
            'cms_desc' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-128',
                'desc' => '描述',
            ),
            'cms_create_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '创建时间',
            ),
            'cms_modify_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '修改时间',
            ),
        ),
    );
}