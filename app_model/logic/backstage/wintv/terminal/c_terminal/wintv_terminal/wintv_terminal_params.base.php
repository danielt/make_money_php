<?php
/**
 * @Description：
 * @Author：xinxin.deng
 * @CreateTime：2019/4/28 10:57
 */

class wintv_terminal_params_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'base_info' => array(
            'cms_id' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => 'UUID',
            ),
            'cms_key' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-128',
                'desc' => '终端下行参数键',
            ),
            'cms_value' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '终端下行参数值',
            ),
            'cms_group' => array(
                'type' => 'int',
                'default'   => '1',
                'isempty' => '',
                'length' => '0-4',
                'desc' => '终端下行参数分组',
            ),
            'cms_state' => array(
                'type' => 'tinyint',
                'default'   => 0,
                'isempty' => '',
                'length' => '0-4',
                'desc' => '管理状态  0 启用 | 1 禁用  默认禁用',
            ),
            'cms_realtime' => array(
                'type' => 'tinyint',
                'default'   => 0,
                'isempty' => '',
                'length' => '1',
                'desc' => '是否实时状态  0 是 | 1 否  默认否',
            ),
            'cms_desc' => array(
                'type' => 'varchar',
                'default'   => '',
                'isempty' => '',
                'length' => '0-128',
                'desc' => '终端下行参数描述',
            ),
            'cms_create_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '创建时间',
            ),
            'cms_modify_time' => array(
                'type' => 'datetime',
                'default'   => '0000-00-00 00:00',
                'isempty' => '',
                'length' => '',
                'desc' => '修改时间',
            ),
        ),
    );
}