<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/13
 * Time: 10:14
 */
class order_fabirc_material_bind_attribute extends order_fabirc_material_bind_attribute_base
{
    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function add($arr_params)
    {
        foreach ($arr_params['cms_attribute_value_id'] as $type_id)
        {
            $insert_params = array(
                'cms_attribute_value_id' => $type_id,
                'cms_material_id' => $arr_params['cms_material_id'],
                'cms_create_time' => date("Y-m-d H:i:s",time()),
                'cms_modify_time' => date("Y-m-d H:i:s",time()),
            );
            $insert_re = $this->make_insert_sql($this->except_useless_params($insert_params,$this->table_define));
        }
        return $insert_re;
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function del()
    {
        return $this->make_delete_sql($this->except_useless_params($this->arr_params,$this->table_define));
    }

    /**
     * LOGIC 查询
     * @param $arr_params
     * @return array|multitype|NULL
     */
    public function query()
    {
        return $this->make_query_sql($this->arr_params);
    }

    /**
     * LOGIC 查询唯一 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function query_only()
    {
        return $this->make_query_only_sql($this->except_useless_params($this->arr_params, $this->table_define,true),$this->str_base_table);
    }

    public function query_all($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select bm.*,v.cms_name as value_name from {$this->str_base_table} as bm LEFT JOIN order_fabirc_attribute_value as v on bm.cms_attribute_value_id=v.cms_id $where";

        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }
}