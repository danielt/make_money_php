<?php
class order_fabirc_material_bind_attribute_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'cms_attribute_value_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => '属性值ID',
        ),
        'cms_material_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => '选料ID',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
    );
}