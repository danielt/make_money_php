<?php
class order_fabirc_material_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'cms_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => 'UUID',
        ),
        'cms_user_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => 'UUID',
        ),
        'cms_fabirc_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => 'UUID',
        ),
        'cms_name' => array(
            'type' => 'varchar',
            'isempty' => '',
            'length' => '0-64',
            'desc' => '名称',
        ),
        'cms_summary' => array(
            'type' => 'varchar',
            'isempty' => '',
            'length' => '0-512',
            'desc' => '产品描述',
        ),
        'cms_image' => array(
            'type' => 'varchar',
            'isempty' => '',
            'length' => '0-255',
            'desc' => '海报',
        ),
        'cms_is_scarce' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '1',
            'desc' => '珍稀程度',
        ),
        'cms_price' => array(
            'type' => 'float',
            'isempty' => '',
            'length' => '1',
            'desc' => '价格',
        ),
        'cms_unit' => array(
            'type' => 'char',
            'isempty' => '',
            'length' => '3',
            'desc' => '单位',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
        'cms_user_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => 'UUID',
        ),
    );
}