<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/13
 * Time: 10:14
 */
class order_fabirc_material extends order_fabirc_material_base
{
    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function add()
    {
        $insert_params = array(
            'cms_name' => $this->arr_params['cms_name'],
            'cms_fabirc_id' => $this->arr_params['cms_fabirc_id'],
            'cms_summary' => $this->arr_params['cms_summary'],
            'cms_image' => !empty($this->arr_params['cms_image']) ? $this->arr_params['cms_image'] : '',
            'cms_user_id' => $this->arr_params['cms_user_id'],
            'cms_price' => $this->arr_params['cms_price'],
            'cms_unit' => $this->arr_params['cms_unit'],
            'cms_is_scarce' => $this->arr_params['cms_is_scarce'],
            'cms_create_time' => date("Y-m-d H:i:s",time()),
            'cms_modify_time' => date("Y-m-d H:i:s",time()),
        );
        return $this->make_insert_sql($this->except_useless_params($insert_params,$this->table_define));
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function del()
    {
        return $this->make_delete_sql($this->except_useless_params($this->arr_params,$this->table_define));
    }

    /**
     * 修改
     * @param $arr_params
     * @return array|multitype|NULL|string
     */
    public function edit()
    {
        $arr_params_set = array(
            'cms_fabirc_id' => $this->arr_params['cms_fabirc_id'],
            'cms_summary' => $this->arr_params['cms_summary'],
            'cms_image' => $this->arr_params['cms_image'],
            'cms_is_scarce' => $this->arr_params['cms_is_scarce'],
            'cms_name' => $this->arr_params['cms_name'],
            'cms_price' => $this->arr_params['cms_price'],
            'cms_unit' => $this->arr_params['cms_unit'],
            'cms_modify_time' => date('Y-m-d H:i:s',time()),
        );
        $arr_params_where = array(
            'cms_id' => $this->arr_params['cms_id'],
        );
        return $this->make_update_sql($arr_params_set,$arr_params_where);
    }

    /**
     * LOGIC 查询
     * @param $arr_params
     * @return array|multitype|NULL
     */
    public function query()
    {
        $arr_params_where = array();
        if(isset($this->arr_params['cms_name']) && !empty($this->arr_params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $this->arr_params['cms_name']);
        }
        if(isset($this->arr_params['cms_fabirc_id']) && !empty($this->arr_params['cms_fabirc_id']))
        {
            $arr_params_where['cms_fabirc_id'] = $this->arr_params['cms_fabirc_id'];
        }
        if(isset($this->arr_params['cms_is_scarce']) && $this->arr_params['cms_is_scarce'] !== '')
        {
            $arr_params_where['cms_is_scarce'] = $this->arr_params['cms_is_scarce'];
        }
        if(isset($params['cms_user_id']) && !empty($params['cms_user_id']))
        {
            $arr_params_where['cms_user_id'] = $params['cms_user_id'];
        }

        return $this->make_query_sql($arr_params_where);
    }

    /**
     * LOGIC 查询唯一 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function query_only()
    {
        return $this->make_query_only_sql($this->except_useless_params($this->arr_params, $this->table_define,true),$this->str_base_table);
    }

    public function query_by_condition($params)
    {
        $arr_params_where = array();
        if(isset($params['cms_name']) && !empty($params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $params['cms_name']);
        }
        if(isset($params['cms_fabirc_id']) && !empty($params['cms_fabirc_id']))
        {
            $arr_params_where['cms_fabirc_id'] = $params['cms_fabirc_id'];
        }
        if(isset($params['cms_is_scarce']) && $params['cms_is_scarce'] !== '')
        {
            $arr_params_where['cms_is_scarce'] = $params['cms_is_scarce'];
        }
        if(isset($params['cms_user_id']) && !empty($params['cms_user_id']))
        {
            $arr_params_where['cms_user_id'] = $params['cms_user_id'];
        }

        $wh = array();
        if(!empty($arr_params_where))
        {
            foreach ($arr_params_where as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "m.$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    if(!empty($value))
                    {
                        $wh[] = "m.$key='$value'";
                    }
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        else
        {
            $where = '';
        }
        $mix_limit = $this->make_page_limit();
        if(strlen($mix_limit['sql'])>0 && strpos($mix_limit['sql'], ',') !== FALSE)
        {
            $sql_count = "select count(1) as count from {$this->str_base_table} as m $where ";
            $data_count = $this->_make_query_sql($sql_count);
            if($data_count['ret'] != 0)
            {
                return $data_count;
            }
            $mix_limit['cms_data_count'] = isset($data_count['data_info'][0]['count']) ? $data_count['data_info'][0]['count'] : 0;
        }
        $sql = "select m.*,f.cms_name as cms_fabirc_name from {$this->str_base_table} as m LEFT JOIN order_fabirc as f on m.cms_fabirc_id=f.cms_id {$where} {$this->str_order} {$mix_limit['sql']}";
        unset($mix_limit['sql']);
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null,$mix_limit);
    }

    public function query_product($params)
    {
        $arr_params_where = array();
        if(isset($params['cms_fabirc_id']) && !empty($params['cms_fabirc_id']))
        {
            $arr_params_where['fm.cms_fabirc_id'] = $params['cms_fabirc_id'];
        }
        if(isset($params['cms_attribute_value_id']) && !empty($params['cms_attribute_value_id']))
        {
            $arr_params_where['fmba.cms_attribute_value_id'] = $params['cms_attribute_value_id'];
        }
        $this->make_group("fm.cms_id");
        $this->make_order("fm.cms_create_time desc");
        $wh = array();
        if(!empty($arr_params_where))
        {
            foreach ($arr_params_where as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key='$value'";
                    }
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        else
        {
            $where = '';
        }
        $mix_limit = $this->make_page_limit();
        if(strlen($mix_limit['sql'])>0 && strpos($mix_limit['sql'], ',') !== FALSE)
        {
            $sql_count = "select count(DISTINCT cms_id) as count from {$this->str_base_table} as fm LEFT JOIN order_fabirc_material_bind_attribute as fmba on fm.cms_id=fmba.cms_material_id {$where}";
            $data_count = $this->_make_query_sql($sql_count);
            if($data_count['ret'] != 0)
            {
                return $data_count;
            }
            $mix_limit['cms_data_count'] = isset($data_count['data_info'][0]['count']) ? $data_count['data_info'][0]['count'] : 0;
        }
        $sql = "select fm.* from {$this->str_base_table} as fm LEFT JOIN order_fabirc_material_bind_attribute as fmba on fm.cms_id=fmba.cms_material_id {$where} {$this->str_group} {$this->str_order} {$mix_limit['sql']}";

        unset($mix_limit['sql']);
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null,$mix_limit);
    }

    public function query_all($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select * from {$this->str_base_table} $where";
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }
}