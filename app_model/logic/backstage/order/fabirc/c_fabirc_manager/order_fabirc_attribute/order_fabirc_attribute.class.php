<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/13
 * Time: 10:14
 */
class order_fabirc_attribute extends order_fabirc_attribute_base
{
    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function add()
    {
        $insert_params = array(
            'cms_name' => $this->arr_params['cms_name'],
            'cms_create_time' => date("Y-m-d H:i:s",time()),
            'cms_modify_time' => date("Y-m-d H:i:s",time()),
        );
        return $this->make_insert_sql($this->except_useless_params($insert_params,$this->table_define));
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function del()
    {
        return $this->make_delete_sql($this->except_useless_params($this->arr_params,$this->table_define));
    }
    /**
     * 修改
     * @param $arr_params
     * @return array|multitype|NULL|string
     */
    public function edit()
    {
        $arr_params_set = array(
            'cms_name' => $this->arr_params['cms_name'],
            'cms_modify_time' => date('Y-m-d H:i:s',time()),
        );
        $arr_params_where = array(
            'cms_id' => $this->arr_params['cms_id'],
        );
        return $this->make_update_sql($arr_params_set,$arr_params_where);
    }

    /**
     * LOGIC 查询
     * @return array|multitype|NULL
     */
    public function query()
    {
        $arr_params_where = array();
        if(isset($this->arr_params['cms_fabirc_attribute_id']) && !empty($this->arr_params['cms_fabirc_attribute_id']))
        {
            $arr_params_where['cms_fabirc_attribute_id'] = $this->arr_params['cms_fabirc_attribute_id'];
        }
        if(isset($this->arr_params['cms_name']) && !empty($this->arr_params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $this->arr_params['cms_name']);
        }

        return $this->make_query_sql($arr_params_where);
    }

    /**
     * LOGIC 查询唯一 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function query_only()
    {
        return $this->make_query_only_sql($this->except_useless_params($this->arr_params, $this->table_define,true),$this->str_base_table);
    }

    public function query_all($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select * from {$this->str_base_table} $where";
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }

    public function query_attribute_and_value($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "a.$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "a.$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select a.cms_id,a.cms_name,v.cms_id as value_id,v.cms_name as value_name from {$this->str_base_table} as a LEFT JOIN order_fabirc_attribute_value as v on a.cms_id=v.cms_fabirc_attribute_id $where";
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }
}