<?php

/**
 * 基本表定义参数用于排除非法字段，验证字段
 * @var array
 */
class order_producer_time_base extends em_logic
{
    public $table_define = array(
        'base_info' => array(
            'cms_id' => array(
                'desc' => '主键id',
            ),
            'cms_manager_id' => array(
                'desc' => '用户账号id',
            ),
            'cms_name' => array(
                'desc' => '名称',
                ),
            'cms_person_amount' => array(
                'desc' => '工人数量',
            ),
            'cms_free_time' => array(
                'desc' => '工期时间',
            ),
            'cms_status' => array(
                'desc' => '空期信息状态；0有效1过期',
            ),

            'cms_create_time' => array(
                'desc' => '创建时间',
            ),
            'cms_modify_time' => array(
                'desc' => '修改时间',
            ),
        )
    );
}

