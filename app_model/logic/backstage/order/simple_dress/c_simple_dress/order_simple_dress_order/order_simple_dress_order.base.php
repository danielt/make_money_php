<?php

class order_simple_dress_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'base_info' => array(
            'cms_id' => array(
                'type' => 'int',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '主键id',
            ),
            'cms_manager_id' => array(
                'type' => 'int',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '下单用户id',
            ),
            'cms_acceptor_id' => array(
                'type' => 'int',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '接单用户id',
            ),
            'cms_order_id' => array(
                'type' => 'char',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '订单ID',
            ),
            'cms_parent_oder_id' => array(
                'type' => 'int',
                'isempty' => '',
                'length' => '0-32',
                'desc' => '父级订单id',
            ),
            'cms_name' => array(
                'type' => 'varchar',
                'isempty' => '',
                'length' => '0-256',
                'desc' => '订单名称',
            ),
            'cms_content' => array(
                'type' => 'varchar',
                'isempty' => '',
                'length' => '0-256',
                'desc' => '订单内容',
            ), 'cms_state' => array(
                'type' => 'tinyint',
                'isempty' => '',
                'length' => '0-256',
                'desc' => '订单状态',
            ),

            'cms_create_time' => array(
                'type' => 'datetime',
                'isempty' => '',
                'length' => '',
                'desc' => '创建时间',
            ),
            'cms_modify_time' => array(
                'type' => 'datetime',
                'isempty' => '',
                'length' => '',
                'desc' => '修改时间',
            ),
        )
    );
}