<?php
class order_simple_dress_order extends order_simple_dress_base {

    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function add($arr_params)
    {
        $insert_params = array(
            'cms_order_id' => $this->arr_params['cms_order_id'],
            'cms_manager_id' => $this->arr_params['cms_manager_id'],
            'cms_parent_order_id' => $this->arr_params['cms_parent_order_id'],
            'cms_content' => $this->arr_params['cms_content'],
            'cms_state' => 0,
            'cms_create_time' => date("Y-m-d H:i:s",time()),
            'cms_modify_time' => date("Y-m-d H:i:s",time()),
        );
        return $this->make_insert_sql($insert_params);
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function del()
    {
        return $this->make_del_sql($this->except_useless_params($this->arr_params, $this->table_define,true),__LINE__);
    }

    /**
     * LOGIC 真实删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function rel_del()
    {
        return $this->make_rel_del_sql($this->except_useless_params($this->arr_params, $this->table_define,true),__LINE__);
    }

    /**
     * 修改
     * @param $arr_params
     * @return array|multitype|NULL|string
     */
    public function edit($arr_params)
    {
        $arr_params_set = $arr_params['set'];
        $arr_params_where = $arr_params['where'];

        return $this->make_update_sql($arr_params_set,$arr_params_where);
    }

    /**
     * 更改订单
     * ****request****
     * cms_order_id  订单id
     * cms_state      订单状态 0未支付1已支付
     * @return array
     */
    public function update_simple_order(){
        if (strlen($this->arr_params['cms_order_id']) < 1) {
            return em_return::_return_error_data($this->arr_params['cms_order_id']);
        }
        if (strlen($this->arr_params['cms_state']) < 1) {
            return em_return::_return_error_data($this->arr_params['cms_state']);
        }
        $arr_where = array('cms_order_id'=>$this->arr_params['cms_order_id']);
        $arr_set = array('cms_state'=>$this->arr_params['cms_state']);

        return $this->make_update_sql($arr_set,$arr_where);
    }

    /**
     * LOGIC 查询
     * @param $arr_params
     * @return array|multitype|NULL
     */
    public function query($arr_params, $str_field = '*')
    {
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_where = $this->_except_empty_data($arr_params_where);
        $arr_params_where = $this->_check_query_params($this->table_define, $arr_params_where);
        if($arr_params_where['ret'] !=0)
        {
            return $arr_params_where;
        }
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        return $this->make_query_sql($arr_params_where,$this->str_base_table,$str_field);
    }

    /**
     * LOGIC 查询唯一 操作
     * @param $arr_params
     * @return array|null array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function query_only($arr_params,$str_field = '*')
    {
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_where = $this->_except_empty_data($arr_params_where);
        $arr_params_where = $this->_check_query_params($this->table_define, $arr_params_where);
        if($arr_params_where['ret'] !=0)
        {
            return $arr_params_where;
        }
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        return $this->make_query_only_sql($arr_params_where,$this->str_base_table,$str_field);
    }
}