<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/13
 * Time: 10:14
 */
class order_clothing_parts_info extends order_clothing_parts_info_base
{
    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function add()
    {
        $insert_params = array(
            'cms_name' => $this->arr_params['cms_name'],
            'cms_parts_type_id' => $this->arr_params['cms_parts_type_id'],
            'cms_image' => $this->arr_params['cms_image'],
            'cms_create_time' => date("Y-m-d H:i:s",time()),
            'cms_modify_time' => date("Y-m-d H:i:s",time()),
        );
        return $this->make_insert_sql($this->except_useless_params($insert_params,$this->table_define));
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function del()
    {
        return $this->make_delete_sql($this->except_useless_params($this->arr_params,$this->table_define));
    }

    /**
     * 修改
     * @param $arr_params
     * @return array|multitype|NULL|string
     */
    public function edit()
    {
        $arr_params_set = array(
            'cms_parts_type_id' => $this->arr_params['cms_parts_type_id'],
            'cms_image' => $this->arr_params['cms_image'],
            'cms_name' => $this->arr_params['cms_name'],
            'cms_modify_time' => date('Y-m-d H:i:s',time()),
        );
        $arr_params_where = array(
            'cms_id' => $this->arr_params['cms_id'],
        );
        return $this->make_update_sql($arr_params_set,$arr_params_where);
    }

    /**
     * LOGIC 查询
     * @param $arr_params
     * @return array|multitype|NULL
     */
    public function query()
    {
        $arr_params_where = array();
        if(isset($this->arr_params['cms_name']) && !empty($this->arr_params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $this->arr_params['cms_name']);
        }
        if(isset($this->arr_params['cms_parts_type_id']) && !empty($this->arr_params['cms_parts_type_id']))
        {
            $arr_params_where['cms_parts_type_id'] = $this->arr_params['cms_parts_type_id'];
        }

        return $this->make_query_sql($arr_params_where);
    }


    public function query_by_condition($params)
    {
        $arr_params_where = array();
        if(isset($params['cms_name']) && !empty($params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $params['cms_name']);
        }
        if(isset($this->arr_params['cms_parts_type_id']) && !empty($this->arr_params['cms_parts_type_id']))
        {
            $arr_params_where['cms_parts_type_id'] = $this->arr_params['cms_parts_type_id'];
        }

        $wh = array();
        if(!empty($arr_params_where))
        {
            foreach ($arr_params_where as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "m.$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "m.$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $mix_limit = $this->make_page_limit();
        if(strlen($mix_limit['sql'])>0 && strpos($mix_limit['sql'], ',') !== FALSE)
        {
            $sql_count = "select count(1) as count from {$this->str_base_table} as m $where ";
            $data_count = $this->_make_query_sql($sql_count);
            if($data_count['ret'] != 0)
            {
                return $data_count;
            }
            $mix_limit['cms_data_count'] = isset($data_count['data_info'][0]['count']) ? $data_count['data_info'][0]['count'] : 0;
        }
        $sql = "select m.*,f.cms_name as cms_clothing_type_name from {$this->str_base_table} as m LEFT JOIN order_clothing_parts_type as f on m.cms_parts_type_id=f.cms_id {$where} {$this->str_order} {$mix_limit['sql']}";
        unset($mix_limit['sql']);
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null,$mix_limit);
    }

    public function query_all($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select * from {$this->str_base_table} $where";
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }
}