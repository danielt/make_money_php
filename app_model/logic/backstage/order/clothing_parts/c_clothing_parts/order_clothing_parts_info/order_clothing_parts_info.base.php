<?php
class order_clothing_parts_info_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'cms_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => 'UUID',
        ),
        'cms_name' => array(
            'type' => 'varchar',
            'isempty' => '',
            'length' => '0-64',
            'desc' => '名称',
        ),
        'cms_parts_type_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '11',
            'desc' => 'UUID',
        ),
        'cms_image' => array(
            'type' => 'varchar',
            'isempty' => '',
            'length' => '0-255',
            'desc' => '海报',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
    );
}