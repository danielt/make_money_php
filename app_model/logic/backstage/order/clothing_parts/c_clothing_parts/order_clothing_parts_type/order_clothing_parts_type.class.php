<?php
/**
 * Created by IntelliJ IDEA.
 * User: LZ
 * Date: 2018/12/13
 * Time: 10:14
 */
class order_clothing_parts_type extends order_clothing_parts_type_base
{
    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function add($arr_params)
    {
        $insert_params = array(
            'cms_name' => $arr_params['cms_name'],
            'cms_create_time' => date("Y-m-d H:i:s",time()),
            'cms_modify_time' => date("Y-m-d H:i:s",time()),
        );
        return $this->make_insert_sql($this->except_useless_params($insert_params,$this->table_define));
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function del()
    {
        return $this->make_delete_sql($this->except_useless_params($this->arr_params,$this->table_define));
    }

    /**
     * 修改
     * @return array|multitype|NULL|string
     */
    public function edit()
    {
        $arr_params_set = array(
            'cms_name' => $this->arr_params['cms_name'],
            'cms_modify_time' => date('Y-m-d H:i:s',time()),
        );
        $arr_params_where = array(
            'cms_id' => $this->arr_params['cms_id'],
        );
        return $this->make_update_sql($arr_params_set,$arr_params_where);
    }

    /**
     * LOGIC 查询
     * @param $arr_params
     * @return array|multitype|NULL
     */
    public function query()
    {
        $arr_params_where = array();
        if(isset($this->arr_params['cms_name']) && !empty($this->arr_params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $this->arr_params['cms_name']);
        }
        return $this->make_query_sql($arr_params_where);
    }
    public function query_all($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select * from {$this->str_base_table} $where";
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }

    public function query_all_by_clothing_type($params)
    {
        $wh = array();
        if(is_array($params))
        {
            foreach ($params as $key=>$value)
            {
                if(is_array($value))
                {
                    if(!empty($value))
                    {
                        $wh[] = "ptbc.$key in ('" . implode("','", $value) . "') ";
                    }
                }
                else
                {
                    $wh[] = "ptbc.$key='$value'";
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where)
        {
            $where = "where $where";
        }
        $sql = "select cpt.* from {$this->str_base_table} as cpt LEFT JOIN order_parts_type_bind_clothing as ptbc on cpt.cms_id=ptbc.cms_parts_type_id $where";

        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok',isset($data['data_info']) ? $data['data_info'] : null);
    }
}