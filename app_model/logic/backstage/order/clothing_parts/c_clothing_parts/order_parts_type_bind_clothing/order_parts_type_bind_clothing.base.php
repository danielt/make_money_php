<?php
class order_parts_type_bind_clothing_base extends em_logic
{
    /**
     * 基本表定义参数用于排除非法字段，验证字段
     * @var array
     */
    public $table_define = array(
        'cms_parts_type_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '1-11',
            'desc' => 'UUID',
        ),
        'cms_clothing_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '1-11',
            'desc' => '服装分类',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
    );
}