<?php
include_once dirname(__FILE__).'/order_supplier_order.base.php';
class order_supplier_order extends order_supplier_order_base
{
    
     /**
      * 添加信息
      * @param array $arr_params参数
      * @return array('ret'=>'状态码','reason'=>'原因')|string
      */
    public function add($arr_params=null)
    {
        $insert_params = array(
            'cms_order_id' => $this->arr_params['cms_order_id'],
            'cms_manager_id' => $this->arr_params['cms_manager_id'],
            'cms_parent_order_id' => $this->arr_params['cms_parent_order_id'],
            'cms_content' => $this->arr_params['cms_content'],
            'cms_state' => 0,
            'cms_create_date' => date("Y-m-d H:i:s",time()),
            'cms_modify_date' => date("Y-m-d H:i:s",time()),
        );
        return $this->make_insert_sql($insert_params);
    }
    
    /**
     * 修改信息
     * @param array $arr_params参数
     * @return array('ret'=>'状态码','reason'=>'原因')|string
     */
    public function edit($arr_params=null)
    {
        $arr_params_set = $this->make_em_pre(isset($arr_params['set']) ? $arr_params['set'] : null);
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_set = $this->_check_query_params($this->table_define, $arr_params_set);
        $arr_params_where = $this->_check_query_params($this->table_define, $arr_params_where);
        if($arr_params_set['ret'] !=0)
        {
            return $arr_params_set;
        }
        if($arr_params_where['ret'] !=0)
        {
            return $arr_params_where;
        }
        $arr_params_set = (isset($arr_params_set['data_info']['info']['out']) && is_array($arr_params_set['data_info']['info']['out']) && !empty($arr_params_set['data_info']['info']['out'])) ? $arr_params_set['data_info']['info']['out'] : null;
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        return $this->make_update_sql($arr_params_set,$arr_params_where);
    }
    
    
    /**
     * 删除信息
     * @param array $arr_params参数
     * @return array('ret'=>'状态码','reason'=>'原因')|string
     */
    public function delete($arr_params=null)
    {
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_where = $this->_check_query_params($this->table_define, $arr_params_where);
        if($arr_params_where['ret'] !=0)
        {
            return $arr_params_where;
        }
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        return $this->make_delete_sql($arr_params_where);
    }
    
    
    /**
     * 查询信息
     * @param array $arr_params参数
     * @return array('ret'=>'状态码','reason'=>'原因')|string
     */
    public function query($arr_params=null)
    {
        $arr_params_where = array();
        if(isset($this->arr_params['cms_order_id']) && !empty($this->arr_params['cms_order_id']))
        {
            $arr_params_where['cms_order_id'] = $this->arr_params['cms_order_id'];
        }
        if(isset($this->arr_params['cms_name']) && !empty($this->arr_params['cms_name']))
        {
            $arr_params_where['like'] = array('cms_name' => $this->arr_params['cms_name']);
        }
        return $this->make_query_sql($arr_params_where);
    }
    
    /**
     * 添加+修改信息（如果没有数据则添加，如果有则修改）
     * @param array $arr_params参数
     * @return array('ret'=>'状态码','reason'=>'原因')|string
     */
    public function add_edit($arr_params=null)
    {
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_where = $this->_check_edit_del_params($this->table_define, $arr_params_where);
        if($arr_params_where['ret'] !=0)
        {
            return $arr_params_where;
        }
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        if(is_array($arr_params_where) && !empty($arr_params_where))
        {
            $result_query = $this->make_query_sql($arr_params_where);
            if($result_query['ret'] !=0)
            {
                return $result_query;
            }
            if(isset($result_query['data_info']) && is_array($result_query['data_info']) && !empty($result_query['data_info']))
            {
                $result = $this->edit($arr_params);
                $result['data_info'] = $result_query['data_info'];
                return $result;
            }
        }
        return $this->add($arr_params);
    }
    
    /**
     * 添加+修改信息（如果没有数据则添加，如果有则修改，仅仅只有一条数据，如果多条数据直接报错）
     * @param array $arr_params参数
     * @return array('ret'=>'状态码','reason'=>'原因')|string
     */
    public function add_edit_one($arr_params=null)
    {
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_where = $this->_check_edit_del_params($this->table_define, $arr_params_where);
        if($arr_params_where['ret'] !=0)
        {
            return $arr_params_where;
        }
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        if(is_array($arr_params_where) && !empty($arr_params_where))
        {
            $result_query = $this->make_query_sql($arr_params_where);
            if($result_query['ret'] !=0)
            {
                return $result_query;
            }
            if(isset($result_query['data_info']) && is_array($result_query['data_info']) && !empty($result_query['data_info']))
            {
                if(count($result_query['data_info']) >1)
                {
                    return em_return::return_data(1,'查询出两条数据');
                }
                $result = $this->edit($arr_params);
                $result['data_info'] = $result_query['data_info'][0];
                return $result;
            }
        }
        return $this->add($arr_params);
    }
}