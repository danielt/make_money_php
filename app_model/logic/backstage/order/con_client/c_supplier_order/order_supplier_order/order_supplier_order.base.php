<?php
class order_supplier_order_base extends em_logic
{
    public $table_define = array(
        'base_info'     =>      array(
            'cms_order_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '订单ID',
            ),
            'cms_parent_order_id'                        =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '父级订单ID',
            ),
            'cms_content'                      =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => '订单内容',
            ),
            'cms_manager_id'                      =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '',
                'desc'      => '订单所属的用户id',
            ),
            'cms_acceptor_id'        =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '',
                'desc'      => '接单的用户id',
            ),
            'cms_state'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '',
                'desc'      => '状态',
            ),
            'cms_create_date'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'cms_modify_date'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '修改时间',
            ),
        ),
        'index_info'                        =>      array(
            'PRIMARY'       =>      array (
                'rule'      => 'PRIMARY',
                'fields'    => array('cms_id'),
                'desc'      => '主键索引',
            ),
        ),
    );
}