<?php
/**
 * Created by PhpStorm.
 * User: fingal
 * Date: 2018/12/23
 * Time: 14:43
 */
include_once 'order_client_order.base.php';
class order_client_order extends order_client_order_base
{

    /**
     * LOGIC 添加 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function add($arr_params)
    {
        $insert_params = array(
            'cms_order_id' => $this->arr_params['cms_order_id'],
            'cms_manager_id' => $this->arr_params['cms_manager_id'],
            'cms_order_parent_type_id' => $this->arr_params['cms_order_parent_type_id'],
            'cms_order_type_id' => $this->arr_params['cms_order_type_id'],
            'cms_process_type' => $this->arr_params['cms_process_type'],
            'cms_material_list' => $this->arr_params['cms_material_list'],
            'cms_style' => $this->arr_params['cms_style'],
            'cms_state' => $this->arr_params['cms_state'],
            'cms_price' => $this->arr_params['cms_price'],
            'cms_create_time' => date("Y-m-d H:i:s", time()),
            'cms_modify_time' => date("Y-m-d H:i:s", time()),
        );
        return $this->make_insert_sql($insert_params);
    }

    /**
     * LOGIC 虚拟删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function del()
    {
        return $this->make_delete_sql($this->except_useless_params($this->arr_params, $this->table_define));
    }

    /**
     * LOGIC 真实删除 操作
     * @return array array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     * @author pan.liang
     * @date 2016-12-30 13:51:33
     */
    public function rel_del()
    {
        return $this->make_rel_del_sql($this->except_useless_params($this->arr_params, $this->table_define, true), __LINE__);
    }

    /**
     * 修改
     * @param $arr_params
     * @return array|multitype|NULL|string
     */
    public function edit($arr_params)
    {
        $arr_params_set = $arr_params['set'];
        $arr_params_where = $arr_params['where'];
        return $this->make_update_sql($arr_params_set, $arr_params_where);
    }

    /**
     * LOGIC 查询
     * @param $arr_params
     * @return array|multitype|NULL
     */
    public function query($arr_params, $str_field = '*')
    {
        $arr_params_where = array();
        if (isset($this->arr_params['cms_order_id']) && !empty($this->arr_params['cms_order_id'])) {
            $arr_params_where['cms_order_id'] = $this->arr_params['cms_order_id'];
        }
        if (isset($this->arr_params['cms_name']) && !empty($this->arr_params['cms_name'])) {
            $arr_params_where['like'] = array('cms_name' => $this->arr_params['cms_name']);
        }

        return $this->make_query_sql($arr_params_where);
    }

    /**
     * LOGIC 查询唯一 操作
     * @param $arr_params
     * @return array|null array('ret'=>'状态码','reason'=>'原因','data_info'=>'数据','page_info'=>'分页信息','other_info'=>'扩展信息')
     */
    public function query_only($arr_params, $str_field = '*')
    {
        $arr_params_where = $this->make_em_pre(isset($arr_params['where']) ? $arr_params['where'] : null);
        $arr_params_where = $this->_except_empty_data($arr_params_where);
        $arr_params_where = $this->_check_query_params($this->table_define, $arr_params_where);
        if ($arr_params_where['ret'] != 0) {
            return $arr_params_where;
        }
        $arr_params_where = (isset($arr_params_where['data_info']['info']['out']) && is_array($arr_params_where['data_info']['info']['out']) && !empty($arr_params_where['data_info']['info']['out'])) ? $arr_params_where['data_info']['info']['out'] : null;
        return $this->make_query_only_sql($arr_params_where, $this->str_base_table, $str_field);
    }


    public function query_client_order($params)
    {
        $arr_params_where = array();
        if (isset($params['cms_fabirc_id']) && !empty($params['cms_fabirc_id'])) {
            $arr_params_where['fm.cms_fabirc_id'] = $params['cms_fabirc_id'];
        }
        if (isset($params['cms_attribute_value_id']) && !empty($params['cms_attribute_value_id'])) {
            $arr_params_where['fmba.cms_attribute_value_id'] = $params['cms_attribute_value_id'];
        }
        $this->make_group("fm.cms_id");
        $this->make_order("fm.cms_create_time desc");
        $wh = array();
        if (!empty($arr_params_where)) {
            foreach ($arr_params_where as $key => $value) {
                if (is_array($value)) {
                    if (!empty($value)) {
                        $wh[] = "$key in ('" . implode("','", $value) . "') ";
                    }
                } else {
                    if (!empty($value)) {
                        $wh[] = "$key='$value'";
                    }
                }
            }
        }

        !empty($wh) && $where = implode(' and ', $wh);
        if (is_string($where) && $where) {
            $where = "where $where";
        } else {
            $where = '';
        }
        $mix_limit = $this->make_page_limit();
        if (strlen($mix_limit['sql']) > 0 && strpos($mix_limit['sql'], ',') !== FALSE) {
            $sql_count = "select count(DISTINCT cms_id) as count from {$this->str_base_table} as fm LEFT JOIN order_fabirc_material_bind_attribute as fmba on fm.cms_id=fmba.cms_material_id {$where}";
            $data_count = $this->_make_query_sql($sql_count);
            if ($data_count['ret'] != 0) {
                return $data_count;
            }
            $mix_limit['cms_data_count'] = isset($data_count['data_info'][0]['count']) ? $data_count['data_info'][0]['count'] : 0;
        }
        $sql = "select fm.* from {$this->str_base_table} as fm LEFT JOIN order_fabirc_material_bind_attribute as fmba on fm.cms_id=fmba.cms_material_id {$where} {$this->str_group} {$this->str_order} {$mix_limit['sql']}";

        unset($mix_limit['sql']);
        $data = $this->_make_query_sql($sql);
        return em_return::_return_right_data('ok', isset($data['data_info']) ? $data['data_info'] : null, $mix_limit);
    }

    /**
     * 获取主订单的价格
     *  ****request****
     * cms_order_id       订单id
     * *****return data*****
     * cms_process_type   加工类型  1包工包料2半包料3清加工
     * cms_basic_price    基本加工费 报价算法中的X
     * cms_material_price 辅料加工费  报价算法中的Z
     * cms_fabric_price   辅料加工费  报价算法中的Y
     */
    public function get_order_price()
    {
        //查询主订单
        if (strlen($this->arr_params['cms_order_id']) < 1) {
            return em_return::_return_error_data($this->arr_params['cms_order_id']);
        }
        $str_order_id = $this->arr_params['cms_order_id'];
        $sql = "select `cms_process_type`,`cms_material_list`,`cms_order_type_id` from order_client_order where `cms_order_id`='{$str_order_id}'";
        $data = $this->_make_query_sql($sql);
        if ($data['ret'] != 0) {
            return em_return::_return_error_data($data['reason']);
        }
        $arr_order = array_shift($data['data_info']);
        $arr_process_type = json_decode($arr_order['cms_process_type'], true);
        $process_type = array_shift($arr_process_type);
        //基本款式价格
        $int_basic_type_id = $arr_order['cms_order_type_id'];
        $sql = "select `cms_price` from order_order_type where `cms_type_id`='{$int_basic_type_id}'";
        $arr_basic_type = $this->_make_query_sql($sql);
        if ($arr_basic_type['ret'] != 0) {
            return em_return::_return_error_data($arr_basic_type['reason']);
        }
        $basic_price = $arr_basic_type['data_info'][0]['cms_price'];
        //辅料价格
        $json_material_id = $arr_order['cms_material_list'];
        $arr_material_id = json_decode($json_material_id, true);
        $str_material_ids = implode("','", $arr_material_id);

        $sql = "select `cms_price` from order_fabirc_material where `cms_id`in ('{$str_material_ids}')";

        $arr_material = $this->_make_query_sql($sql);
        if ($arr_material['ret'] != 0) {
            return em_return::_return_error_data($data['reason']);
        }
        $arr_material = $arr_material['data_info'];
        $material_price = '0.00';
        foreach ($arr_material as $arr_material_item) {
            $material_price += $arr_material_item['cms_price'];
        }
        //面料价格
        $fabric_price = '0.00';


        $re_data = array(
            'cms_process_type' => $process_type,
            'cms_basic_price' => $basic_price,
            'cms_material_price' => $material_price,
            'cms_fabric_price' => $fabric_price,
        );

        return em_return::_return_right_data('ok', $re_data);
    }

    /**
     * 支付成功之后，更新订单信息
     * 【必填】
     * cms_order_id     订单id
     * 【选填】
     * cms_state 0未支付1已支付
     * cms_price 价钱
     * cms_amount 数量
     *
     */
    public function update_client_oder()
    {
        if (strlen($this->arr_params['cms_order_id']) < 1) {
            return em_return::_return_error_data($this->arr_params['cms_order_id']);
        }
        //if (strlen($this->arr_params['cms_price']) < 1) {
        //    return em_return::_return_error_data($this->arr_params['cms_price']);
        //}
        //if (strlen($this->arr_params['cms_amount']) < 1) {
        //    return em_return::_return_error_data($this->arr_params['cms_amount']);
        //}
        //if (strlen($this->arr_params['cms_state']) < 1) {
        //    return em_return::_return_error_data($this->arr_params['cms_state']);
        //}
        $str_order_id = $this->arr_params['cms_order_id'];

        $set_params = array();
        if (isset($this->arr_params['cms_price'])) {
            $set_params['cms_price'] = $this->arr_params['cms_price'];
        }
        if (isset($this->arr_params['cms_amount'])) {
            $set_params['cms_amount'] = $this->arr_params['cms_amount'];
        }
        if (isset($this->arr_params['cms_state'])) {
            $set_params['cms_state'] = $this->arr_params['cms_state'];
        }
        $arr_where = array('cms_order_id' => $str_order_id);
        $update_result = $this->make_update_sql($set_params, $arr_where);
        if ($update_result['ret'] != 0) {
            return em_return::_return_error_data($update_result['data_info']);
        }
        return em_return::_return_right_data('ok');

    }


}