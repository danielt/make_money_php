<?php
/**
 * Created by PhpStorm.
 * Use : 用户积分基础类
 * User: kan.yang@starcor.com
 * Date: 18-12-28
 * Time: 上午1:20
 */

class logic_user_integral_base extends em_logic
{

    //数据表名称
    public $str_base_table = 'system_user_integral';

    //基本表定义参数用于排除非法字段，验证字段
    public $table_define = array(
        'cms_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-11',
            'desc' => '主键ID',
        ),
        'cms_user_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-11',
            'desc' => '用户ID',
        ),
        'cms_integral' => array(
            'type' => 'int',
            'default' => 0,
            'isempty' => '',
            'length' => '0-8',
            'desc' => '当前积分',
        ),
        'cms_integral_history' => array(
            'type' => 'int',
            'default' => 0,
            'isempty' => '',
            'length' => '0-11',
            'desc' => '历史积分（只增不减）',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'default' => '0000-00-00 00:00:00',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'default' => '0000-00-00 00:00:00',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
        'cms_uuid' => array(
            'type' => 'char',
            'default' => '',
            'isempty' => '',
            'length' => '32',
            'desc' => 'GUUID，外部标识',
        ),
        'cms_user_name'=>array(//仅用于联合查询
            'type' => 'varchar',
            'default' => '',
            'isempty' => '',
            'length' => '',
            'desc' => '用户名称',
        ),
        'cms_start_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '页面传过来的创建时间开始',
        ),
        'cms_end_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '页面传过来的创建时间结束',
        ),
    );

    /**
     * 默认构造函数
     */
    public function __construct($obj_controller,$arr_params = null)
    {
        //初始化父级构造函数
        parent::__construct($obj_controller,$this->str_base_table,$arr_params);
    }

} 