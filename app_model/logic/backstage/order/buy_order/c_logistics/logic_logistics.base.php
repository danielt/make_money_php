<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/10 10:33
 */

include_once dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))) . '/pub_class/helpers/string_helper.php';
class logic_logistics_base extends em_logic
{

    //数据表名称
    public $str_base_table = 'order_logistics';

    //基本表定义参数用于排除非法字段，验证字段
    public $table_define = array(
        'cms_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-11',
            'desc' => '订单id',
        ),
        'cms_buy_order_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-10',
            'desc' => '订单id',
        ),
        'cms_desc' => array(
            'type' => 'varchar',
            'isempty' => '',
            'length' => '0-255',
            'desc' => '物流描述',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
    );

    /**
     * 默认构造函数
     */
    public function __construct($obj_controller,$arr_params = null)
    {
        //初始化父级构造函数
        parent::__construct($obj_controller,$this->str_base_table,$arr_params);
    }

}