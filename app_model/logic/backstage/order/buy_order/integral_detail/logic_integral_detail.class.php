<?php
/**
 * Created by PhpStorm.
 * Use : 积分明细业务类
 * User: kan.yang@starcor.com
 * Date: 18-12-28
 * Time: 上午1:40
 */

include_once 'logic_integral_detail.base.php';
class integral_detail_logic extends logic_integral_detail_base
{

    /**
     * 默认构造函数
     */
    public function __construct($obj_controller,$table_name = '',$arr_params = null)
    {
        if(!empty($table_name))
        {
            $this->str_base_table = $table_name;
        }
        //初始化父级构造函数
        parent::__construct($obj_controller,$arr_params);
    }

    /**
     * 查询用户积分明细列表
     * @param array $arr_query_params array(
            'cms_id'            => '主键ID',
            'cms_user_id'       => '用户ID',
            'cms_start_time'    => '开始时间',
            'cms_end_time'      => '结束时间',
            'cms_integral_type' => '积分类型。0充值积分；1订购积分；2注册积分',
            'cms_uuid'          => '外部标识',
     * )
     * @param string $str_field     查询字段
     * @param array  $arr_limit     分页array('start','end')
     * @param string $str_join      联合查询（需要在外层拼装完成，这里只负责透传）
     * @param string $str_group     分组
     * @param string $str_order     排序
     * @return array array('ret' => 0/1,'reason' => '描述信息','data_info' => array(),'page_info' =>array())
     */
    public function get_list($arr_query_params = array(), $str_field = "*", $arr_limit = array(), $str_join = "", $str_group = '',$str_order = '')
    {
        //标准化入参
        $this->_init_logic($arr_query_params);
        //组装过滤条件
        $str_where_sql = '1=1';
        $this->_batch_comm_query_where($arr_query_params,array('cms_start_time','cms_end_time'),'in',$str_where_sql);
        //微信公众号名称
        if(isset($arr_query_params['cms_account_name']))
        {
            $str_where_sql .= ' and cms_account_name like \'%' . $arr_query_params['cms_account_name'] . '%\'';
        }
        //开始时间
        if(isset($arr_query_params['cms_start_time']))
        {
            $str_where_sql .= ' and cms_create_time >= \'' . $arr_query_params['cms_start_time'] . '\'';
        }
        //结束时间
        if(isset($arr_query_params['cms_end_time']))
        {
            $str_where_sql .= ' and cms_create_time <= \'' . $arr_query_params['cms_end_time'] . '\'';
        }
        //分页
        if(!empty($arr_limit))
        {
            $this->obj_controller->arr_page_params['cms_page_num'];
            $this->obj_controller->arr_page_params['cms_page_size'];
        }
        //分组
        if(!empty($str_group))
        {
            $str_where_sql .= ' group by ' . $str_group;
        }
        //排序
        if(!empty($str_order))
        {
            $str_where_sql .= ' order by ' . $str_order;
        }
        //联合查询
        if(!empty($str_join))
        {
            $this->str_base_table = $str_join;
        }
        return $this->make_query_sql($str_where_sql,$this->str_base_table,$str_field);
    }

    /**
     * 查询用户积分明细详情
     * @param array $arr_query_params array(
            'cms_id'            => '主键ID',
            'cms_user_id'       => '用户ID',
            'cms_integral_type' => '积分类型。0充值积分；1订购积分；2注册积分',
            'cms_uuid'          => '外部标识',
     * )
     * @param string $str_field     查询字段
     * @param string $str_join      联合查询（需要在外层拼装完成，这里只负责透传）
     * @param string $str_group     分组
     * @param string $str_order     排序
     * @return array array('ret' => 0/1,'reason' => '描述信息','data_info' => array())
     */
    public function get_one($arr_query_params = array(), $str_field = "*", $str_join = "", $str_group = '',$str_order = '')
    {
        //标准化入参
        $this->_init_logic($arr_query_params);
        //组装过滤条件
        $str_where_sql = '1=1';
        $this->_batch_comm_query_where($arr_query_params,array(),'=',$str_where_sql);
        //分组
        if(!empty($str_group))
        {
            $str_where_sql .= ' group by ' . $str_group;
        }
        //排序
        if(!empty($str_order))
        {
            $str_where_sql .= ' order by ' . $str_order;
        }
        //联合查询
        if(!empty($str_join))
        {
            $this->str_base_table = $str_join;
        }
        return $this->make_query_only_sql($str_where_sql,$this->str_base_table,$str_field);
    }

    /**
     * 添加用户积分明细
     * @param array$arr_add_params array(
            'cms_integral_type'   => '积分类型',
            'cms_user_id'         => '用户ID',
            'cms_integral'        => '积分值',
            'cms_incr_decr'       => '增减积分，默认0。0正；1负',
            'cms_extend_id'       => '支出积分关联数据',
            'cms_desc'            => '描述信息',
            'cms_uuid'            => 'GUUID，外部标识',
     * )
     * @return array array('ret' => 0/1,'reason' => '描述信息','data_info' => array('cms_id' => '唯一标识','cms_uuid' => '外部标识'))
     */
    public function add($arr_add_params)
    {
        //参数验证
        if(empty($arr_add_params['cms_user_id']) || empty($arr_add_params['cms_integral']))
        {
            return array('ret' => 1,'reason' => '参数校验失败：用户ID和积分值必传');
        }
        //缺省参数
        if(!isset($arr_add_params['cms_uuid']) || empty($arr_add_params['cms_uuid']))
        {
            $arr_add_params['cms_uuid'] = system_guid_rand();
        }
        //创建/修改时间
        if(empty($arr_add_params['cms_create_time']) || empty($arr_add_params['cms_modify_time']))
        {
            $arr_add_params['cms_create_time'] = $arr_add_params['cms_modify_time'] = date('Y-m-d H:i:s');
        }
        //标准化入参
        $this->_init_logic($arr_add_params);
        //添加
        $arr_add_ret = $this->make_insert_sql($arr_add_params,__LINE__);
        if($arr_add_ret['ret'] == 0)
        {
            $arr_add_ret['data_info']['cms_uuid'] = $arr_add_params['cms_uuid'];
            //同步更新用户积分
            include_once dirname(__DIR__) . '/user_integral/logic_user_integral.class.php';
            $obj_user_integral_logic = new user_integral_logic($this->obj_controller);
            //查询用户现有积分
            $arr_user_integral = $obj_user_integral_logic->get_one(array('cms_user_id' => $arr_add_params['cms_user_id']),'cms_id,cms_integral,cms_integral_history');
            if($arr_user_integral['ret'] == 0)
            {
                $arr_user_integral = $arr_user_integral['data_info'];
                if($arr_add_params['cms_incr_decr'] == 0)
                {//增加用户积分

                    if(empty($arr_user_integral['data_info']))
                    {//添加用户积分

                        $obj_user_integral_logic->add(array(
                            'cms_user_id'     => $arr_add_params['cms_user_id'],
                            'cms_integral'    => $arr_add_params['cms_integral'],
                            'cms_uuid'        => $arr_add_params['cms_uuid'],
                            'cms_create_time' => $arr_add_params['cms_create_time'],
                            'cms_modify_time' => $arr_add_params['cms_modify_time'],
                            'cms_integral_history' => $arr_add_params['cms_integral'],
                        ));
                    }
                    else
                    {//更新用户积分

                        $obj_user_integral_logic->edit(array('cms_id' => $arr_user_integral['cms_id']),array(
                            'cms_integral'    => $arr_user_integral['cms_integral'] + $arr_add_params['cms_integral'],
                            'cms_modify_time' => $arr_add_params['cms_modify_time'],
                            'cms_integral_history' => $arr_user_integral['cms_integral_history'] + $arr_add_params['cms_integral'],
                        ));
                    }
                }
                else
                {//减少用户积分

                    if(!empty($arr_user_integral['data_info']) && $arr_user_integral['cms_integral'] >= $arr_add_params['cms_integral'])
                    {//更新用户积分

                        $obj_user_integral_logic->edit(array('cms_id' => $arr_user_integral['cms_id']),array(
                            'cms_integral'    => $arr_user_integral['cms_integral'] - $arr_add_params['cms_integral'],
                            'cms_modify_time' => $arr_add_params['cms_modify_time'],
                        ));
                    }
                }
            }
            unset($obj_user_integral_logic,$arr_user_integral);
        }
        return $arr_add_ret;
    }

    /**
     * 删除用户积分明细
     * @param array $arr_ids 主键ID集合
     * @return array array('ret' => 0/1,'reason' => '描述信息')
     */
    public function del($arr_ids)
    {
        $this->_handle_array_string_params($arr_ids,false);
        $arr_edit_ret = $this->make_delete_sql(array(
            'cms_id' => $arr_ids
        ));
        $arr_edit_ret['reason'] = $arr_edit_ret['ret'] == 0 ? '删除用户积分明细成功' : '删除用户积分明细失败';

        return $arr_edit_ret;
    }
} 