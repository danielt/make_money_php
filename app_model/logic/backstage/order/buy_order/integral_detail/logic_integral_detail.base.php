<?php
/**
 * Created by PhpStorm.
 * Use : 积分明细基础类
 * User: kan.yang@starcor.com
 * Date: 18-12-28
 * Time: 上午1:40
 */

class logic_integral_detail_base extends em_logic
{

    //数据表名称
    public $str_base_table = 'system_user_integral_detail';

    //基本表定义参数用于排除非法字段，验证字段
    public $table_define = array(
        'cms_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-11',
            'desc' => '主键ID',
        ),
        'cms_integral_type' => array(
            'type' => 'tinyint',
            'isempty' => '',
            'length' => '0-1',
            'desc' => '积分类型。0充值积分；1订购积分；2注册积分',
        ),
        'cms_user_id' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-11',
            'desc' => '用户ID',
        ),
        'cms_integral' => array(
            'type' => 'int',
            'isempty' => '',
            'length' => '0-4',
            'desc' => '积分值',
        ),
        'cms_incr_decr' => array(
            'type' => 'tinyint',
            'isempty' => '',
            'length' => '0-1',
            'desc' => '增减积分，默认0。0正；1负',
        ),
        'cms_extend_id' => array(
            'type' => 'varchar',
            'default' => '',
            'isempty' => '',
            'length' => '0-32',
            'desc' => '支出积分关联数据',
        ),
        'cms_create_time' => array(
            'type' => 'datetime',
            'default' => '0000-00-00 00:00:00',
            'isempty' => '',
            'length' => '',
            'desc' => '创建时间',
        ),
        'cms_modify_time' => array(
            'type' => 'datetime',
            'default' => '0000-00-00 00:00:00',
            'isempty' => '',
            'length' => '',
            'desc' => '修改时间',
        ),
        'cms_uuid' => array(
            'type' => 'char',
            'isempty' => '',
            'length' => '32',
            'desc' => 'GUUID，外部标识',
        ),
        'cms_desc' => array(
            'type' => 'varchar',
            'default' => '',
            'isempty' => '',
            'length' => '0-128',
            'desc' => '描述信息',
        ),
        'cms_start_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '页面传过来的创建时间开始',
        ),
        'cms_end_time' => array(
            'type' => 'datetime',
            'isempty' => '',
            'length' => '',
            'desc' => '页面传过来的创建时间结束',
        ),
    );

    /**
     * 默认构造函数
     */
    public function __construct($obj_controller,$arr_params = null)
    {
        //初始化父级构造函数
        parent::__construct($obj_controller,$this->str_base_table,$arr_params);
    }

} 