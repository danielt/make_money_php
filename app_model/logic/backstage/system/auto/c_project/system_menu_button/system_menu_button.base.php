<?php
class system_menu_button_base extends em_logic
{
    public $table_define = array(
        'base_info'     =>      array(
            'cms_id'                        =>      array (
                'rule'      => 'auto_increment',
                'default'   => 'auto_increment',
                'length'    => '',
                'desc'      => '项目ID',
            ),
            'cms_menu_id'                      =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '',
                'desc'      => '目录ID',
            ),
            'cms_mark'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '项目栏目标示',
            ),
            'cms_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-128',
                'desc'      => '注入来源项目英文标示',
            ),
            'cms_function'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-32',
                'desc'      => '方法',
            ),
            'cms_url'        =>      array (
                'rule'      => '',
                'default'   => '',
                'length'     => '0-255',
                'desc'      => 'url链接地址',
            ),
            'cms_order'                     =>      array (
                'rule'      => 'float',
                'default'   => '0.000',
                'length'    => '',
                'desc'      => '排序权重',
            ),
            'cms_state'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '0',
                'length'    => '',
                'desc'      => '按钮状态  0 启用  1 禁用',
            ),
            'cms_is_default'                     =>      array (
                'rule'      => 'tinyint',
                'default'   => '1',
                'length'    => '',
                'desc'      => '0 默认入口 | 1 非默认入口',
            ),
            'cms_create_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'cms_modify_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '修改时间',
            ),
        ),
        'index_info'                        =>      array(
            'PRIMARY'       =>      array (
                'rule'      => 'PRIMARY',
                'fields'    => array('cms_id'),
                'desc'      => '主键索引',
            ),
            'idx_unique'                          =>      array (
                'rule'      => 'UNIQUE',
                'fields'    => array('cms_menu_id','cms_mark'),
                'desc'      => '唯一索引',
            ),
        ),
    );
}