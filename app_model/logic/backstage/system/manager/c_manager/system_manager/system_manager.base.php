<?php
class system_manager_base extends em_logic
{
    public $table_define = array(
        'base_info'     =>      array(
            'cms_id'                        =>      array (
                'rule'      => 'auto_increment',
                'default'   => 'auto_increment',
                'length'    => '',
                'desc'      => '管理员ID',
            ),
            'cms_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-255',
                'desc'      => '管理员名称',
            ),
            'cms_parent_manager'        =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '',
                'desc'      => 'cms_parent_manager',
            ),
            'cms_login_account'         =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '',
                'desc'      => '管理员登录账号',
            ),
            'cms_login_pass'         =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'    => '32',
                'desc'      => '登录密码',
            ),
            'cms_role_id'                    =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '',
                'desc'      => '角色ID',
            ),
            'cms_login_count'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '登录次数',
            ),
            'cms_login_time'                     =>      array (
                'rule'      => 'datetime',
                'default'   => '',
                'length'    => '',
                'desc'      => '最后登录时间',
            ),
            'cms_state'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '菜单状态  0 启用  1 禁用',
            ),
            'cms_create_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'cms_modify_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '修改时间',
            ),
        ),
        'index_info'                        =>      array(
            'PRIMARY'       =>      array (
                'rule'      => 'PRIMARY',
                'fields'    => array('cms_id'),
                'desc'      => '主键索引',
            ),
        ),
    );
}