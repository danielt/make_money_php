<?php
class system_role_base extends em_logic
{
    public $table_define = array(
        'base_info'     =>      array(
            'cms_id'                        =>      array (
                'rule'      => 'auto_increment',
                'default'   => 'auto_increment',
                'length'    => '',
                'desc'      => '角色ID',
            ),
            'cms_name'                      =>      array (
                'rule'      => 'noempty',
                'default'   => '',
                'length'     => '1-255',
                'desc'      => '角色名称',
            ),
            'cms_state'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '菜单状态  0 启用  1 禁用',
            ),
            'cms_desc'                     =>      array (
                'rule'      => '',
                'default'   => '',
                'length'    => '0-512',
                'desc'      => '角色描述',
            ),
            'cms_create_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'cms_modify_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '修改时间',
            ),
        ),
        'index_info'                        =>      array(
            'PRIMARY'       =>      array (
                'rule'      => 'PRIMARY',
                'fields'    => array('cms_id'),
                'desc'      => '主键索引',
            ),
        ),
    );
}