<?php
class system_role_bind_base extends em_logic
{
    public $table_define = array(
        'base_info'     =>      array(
            'cms_id'                        =>      array (
                'rule'      => 'auto_increment',
                'default'   => 'auto_increment',
                'length'    => '',
                'desc'      => '角色ID',
            ),
            'cms_role_id'                      =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'     => '',
                'desc'      => '角色ID',
            ),
            'cms_state'                     =>      array (
                'rule'      => 'int',
                'default'   => '0',
                'length'    => '',
                'desc'      => '状态  0 启用  1 禁用',
            ),
            'cms_button_id'                     =>      array (
                'rule'      => 'int',
                'default'   => '',
                'length'    => '',
                'desc'      => '按钮ID',
            ),
            'cms_create_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '创建时间',
            ),
            'cms_modify_time'               =>      array (
                'rule'      => 'datetime',
                'default'   => 'datetime',
                'length'    => '',
                'desc'      => '修改时间',
            ),
        ),
        'index_info'                        =>      array(
            'PRIMARY'       =>      array (
                'rule'      => 'PRIMARY',
                'fields'    => array('cms_id'),
                'desc'      => '主键索引',
            ),
        ),
    );
}