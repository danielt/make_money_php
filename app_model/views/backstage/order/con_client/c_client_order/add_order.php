<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_top_web_file.php'; ?>
    <script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-2.1.1.min.js" charset="UTF-8"></script>
    <!-- <link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/index.css"/>

    <style type="text/css">
        .btn{ margin: 0; }
        *{ box-sizing: border-box!important; }
    </style>
</head>
<body>
<?php //var_dump($parent_type);exit;?>
<!-- 引入head -->
<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_head_modal.php'; ?>

<div class="outter-wp zoe-wrap">
    <div class="cl mb-20">
        <div class="order-next"><a href="../../../order/fabirc/c_fabirc/fabirc_view"><i class="fa fa-hand-o-right fa-fw"></i>&nbsp;下一步</a></div>
    </div>
    <!-- 筛选条件 S -->
    <div class="filter-box">
        <div class="filter-item">
            <div class="filter-item-label">分类：</div>
            <div class="filter-item-list">
                <ul>
                    <li class="active"><span order_type="0" class="fist-type">全部</span></li>
                    <?php foreach ($parent_type as $item => $first_type): ?>
                        <?php if ($first_type['cms_category_parent_id'] == 1): ?>
                            <li><span order_type="<?php echo $first_type['cms_category_id']; ?>"
                                      class="first-type"><?php echo $first_type['cms_category_name']; ?></span></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div id="first-type" class="filter-item">
            <div class="filter-item-label">分类：</div>
            <div class="filter-item-list">
                <ul>
                    <li class="active"><span order_type="0" class="second-type">全部</span></li>
                    <?php foreach ($parent_type as $item => $first_type): ?>
                        <?php if ($first_type['cms_category_parent_id'] == 2): ?>
                            <li><span order_type="<?php echo $first_type['cms_category_id']; ?>"
                                      class="second-type"><?php echo $first_type['cms_category_name']; ?></span></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div id="second-type" class="filter-item">
            <div class="filter-item-label">分类：</div>
            <div class="filter-item-list">
                <ul>
                    <li class="active"><span order_type="0" class="third-type">全部</span></li>
                    <?php foreach ($parent_type as $item => $first_type): ?>
                        <?php if ($first_type['cms_category_parent_id'] == 3): ?>
                            <li><span order_type="<?php echo $first_type['cms_category_id']; ?>"
                                      class="third-type"><?php echo $first_type['cms_category_name']; ?></span></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <!-- 筛选条件 E -->
    </div>
    <!-- 产品展示 S -->
    <div class="product-box order-pro sys_item_spec">
        <ul class="product-list sys_spec_img">
        </ul>
    </div>
    <!-- 产品展示 E -->

    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_index_page.php';?>

</div>

<!-- 引入footer -->
<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_footer_modal.php'; ?>

<script type="text/javascript">
    $(document).ready(function () {
        create_item('',second_type,third_type);

        var first_type = 0;
        var second_type = 0;
        var third_type = 0;
        var basic_id = 0;

        $('.filter-item-list').on('click', 'li > span', function () {
            var _this = $(this);
            _this.parent('li').addClass('active').siblings().removeClass('active');
            if (_this.hasClass('first-type')) {
                first_type = _this.attr('order_type');
            }
            if (_this.hasClass('second-type')) {
                second_type = _this.attr('order_type');
            }
            if (_this.hasClass('third-type')) {
                third_type = _this.attr('order_type');
            }
            create_item(first_type, second_type, third_type);
        });

        $('.product-list').on('click','li',function () {
            if(!!$(this).hasClass('selected')){
                $(this).removeClass('selected');
                basic_id = $(this).find('input:hidden').val();
            }else {
                $(this).addClass("selected").siblings("li").removeClass("selected");
                basic_id = $(this).find('input:hidden').val();
            }
        });


        function create_item(first_type, second_type, third_type) {
            $.post(
                "../../../order/con_client/c_client_order/add_order?flag_ajax_reurn=1",
                {
                    first_type: first_type,
                    second_type: second_type,
                    third_type: third_type,
                    flag_ajax_reurn: 1
                },
                function (result) {
                    var data = eval("(" + result + ")");
                    var li_html = '';
                    $.each(data.data_info, function (index, item) {
                        li_html += "<li><span class='product-img'><img src='/data_model/upload/" + item.cms_src + "' alt='" + item.cms_type_name + "'></span><p class='product-title'>" + item.cms_type_name + "</p><input type='hidden' value='" + item.cms_type_id +"'></li>";
                    });
                    push_page_info(data.page_info.cms_page_num,data.page_info.cms_page_size,data.page_info.cms_data_count);
                    $('.product-list').html(li_html);
                }
            )
        }


        function list_refresh() {
            create_item('',second_type,third_type);
        }

        $('.order-next').on('click',function () {
            var url = '../../../order/fabirc/c_fabirc/fabirc_view?cms_first_type_id='+first_type+'&cms_second_type_id='+second_type+'&cms_third_type_id='+third_type+'&cms_basic_id='+basic_id;
            $(this).find("a").attr('href',url);
            if(first_type === 0){
                alert('请选择分类');
                return  false;
            }
            if(second_type === 0){
                alert('请选择分类');
                return  false;
            }
            if(third_type === 0){
                alert('请选择分类');
                return  false;
            }
            if(basic_id === 0){
                alert('请选择基本款式');
                return  false;
            }

        });

    });

</script>

</body>
</html>