<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_top_web_file.php'; ?>
</head>
<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li class="active">生产商订单列表</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal" method="post" action="<?php echo $arr_page_url['list_url']; ?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label">名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control1" name="cms_name" id="focusedinput"
                               value="<?php echo isset($arr_params['cms_name']) ? $arr_params['cms_name'] : ''; ?>">
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list">
                            <i class="fa fa-search">查询</i>
                        </button>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list">
                            <i class="fa">新建生产订单</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll"/></th>
                    <th>订单名称</th>
                    <th>类型</th>
                    <th>加工类型</th>
                    <th>面辅料</th>
                    <th>款式</th>
                    <th>状态</th>
                    <th>创建时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($data_info) && is_array($data_info) && !empty($data_info)): ?>
                    <?php foreach ($data_info as $val): ?>
                        <tr class='odd selected'>
                            <td><input type="checkbox" name="checkItem" attr-key="cms_id"
                                       value="<?php echo $val['cms_id']; ?>"/></td>
                            <?php if ($val['cms_name'] == $val['cms_mark']) { ?>
                                <td><font color='red'><?php echo $val['cms_name']; ?></font></td>
                            <?php } else { ?>
                                <td><?php echo $val['cms_name']; ?></td>
                            <?php } ?>
                            <td><?php echo $val['cms_order_type_id']; ?></td>
                            <td><?php echo $val['cms_process_type']; ?></td>
                            <td><?php echo $val['cms_material_list']; ?></td>
                            <td><?php echo $val['cms_style']; ?></td>
                            <td>
                                <?php echo $val['cms_status'] != '0' ? "<font color='red'>未完成</font>" : "<font color='green'>进行中</font>"; ?>
                            </td>
                            <td><?php echo $val['cms_create_time']; ?></td>
                            <td><?php echo $val['cms_modify_time']; ?></td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown"
                                       aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!--*********** 初始化必须加载 ***************** （下拉操作信息） *********** 初始化必须加载 ***************** -->
                                        <?php echo make_right_button($system_file_list, $val); ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_page.php'; ?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/public_bottom_button.php'; ?>
    </div>
</body>
</html>