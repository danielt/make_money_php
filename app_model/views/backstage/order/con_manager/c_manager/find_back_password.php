<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>云裳供应链-找回密码</title>
    </head>
    <body>
         <!-- 引入head -->
         <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_head_modal.php'; ?>
        <!-- 找回密码 -->
        <div class="find-back clearfix">
            <p class="pwd-title">找回密码</p>
            <div class="pwd-box">
                <form id="findBackPwdForm" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="findTelephone" class="col-sm-2 control-label">手机号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="findTelephone" name="findTelephone" placeholder="请输入手机号">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="findNum" class="col-sm-2 control-label">验证码</label>
                        <div class="col-sm-10 find-password cl">
                            <div class="cl">
                                <input type="text" class="form-control fl" id="findNum" name="findNum" placeholder="请输入验证码" style="width: 355px;">
                                <button type="button" id="verificationCode" class="btn btn-primary btn-gain-num fr">发送验证码</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="findPassword" class="col-sm-2 control-label">密 码</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="findPassword"  name="findPassword" placeholder="请输入密码">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmFindPassword" class="col-sm-2 control-label">确认密码</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="confirmFindPassword" name="confirmFindPassword" placeholder="再次输入密码">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" id="nextFindBtn" class="btn btn-primary">确认找回密码</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
         <!-- 引入footer -->
         <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_footer_modal.php'; ?>

        <script type="text/javascript">
            $('#verificationCode').on('click',function(){
                var telephone = $('#findTelephone').val();
                $(this).prop('disabled',true);
                findRoof();
                var key = '04997110aa2db7e27991ece0749064f4';
                var timestamp=new Date().getTime();
                var sign = hex_md5(telephone+timestamp+key);
                var submitData = "cms_mobile_code=" + telephone + "&sign=" + sign + "&cms_time=" + timestamp;
                $.ajax({
                    url:'../../../../backstage/system/auto/c_smsg/send_msg',
                    type:"POST",
                    data:submitData,
                    cache:false,//false是不缓存，true为缓存
                    async:true,//true为异步，false为同步
                    success:function(result){
                        var dataObj=eval("("+result+")");
                        if(dataObj.ret != 0)
                        {
                            
                            swal(
                                {
                                    title:'发送失败',
                                    text:dataObj.reason,
                                    type:"error",
                                    showCancelButton:false,
                                    showConfirmButton:false,
                                    timer:1500
                                },
                            );
                        }
                        else
                        {
                            swal(
                                {
                                    title:'发送成功',
                                    text:'',
                                    type:"success",
                                    showCancelButton:false,
                                    showConfirmButton:false,
                                    timer:1500
                                }
                            );
                        }
                    }
                });
            });
            // 发送验证码倒计时
            var times = 60;
            function findRoof(){
                if(times == 0){
                    $('.btn-gain-num').text('发送验证码('+times+'s)');
                    $('.btn-gain-num').prop('disabled',false);
                    $('.btn-gain-num').text('发送验证码');
                    times = 60;
                    return
                }
                $('.btn-gain-num').text('重新发送('+times+'s)');
                times--;
                setTimeout(findRoof,1000);
            }
           // 验证找回密码
           $('#findBackPwdForm').bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {/*input状态样式图片*/
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'

                    },
                    fields: {/*验证：规则*/
                        findPassword: {
                            message:'密码无效',
                            validators: {
                                notEmpty: {
                                    message: '密码不能为空'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: '用户名长度必须在6到30之间'
                                },
                                identical: {//相同
                                    field: 'password', //需要进行比较的input name值
                                    message: '两次密码不一致'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z0-9_\.]+$/,
                                    message: '用户名只能由字母、数字、点和下划线组成'
                                }
                            }
                        },
                        confirmFindPassword: {
                            message:'密码无效',
                            validators: {
                                notEmpty: {
                                    message: '密码不能为空'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 30,
                                    message: '用户名长度必须在6到30之间'
                                },
                                identical: {//相同
                                    field: 'password', //需要进行比较的input name值
                                    message: '两次密码不一致'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z0-9_\.]+$/,
                                    message: '用户名只能由字母、数字、点和下划线组成'
                                }
                            }
                        },
                        findTelephone: {
                            message: 'The phone is not valid',
                            validators: {
                                notEmpty: {
                                    message: '手机号码不能为空'
                                },
                                regexp: {
                                    min: 11,
                                    max: 11,
                                    regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                                    message: '请输入正确的手机号码'
                                }
                            }
                        },
                        findNum: {
                            message:'密码无效',
                            validators: {
                                notEmpty: {
                                    message: '验证码不能为空'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 6,
                                    message: '验证码填写错误'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z0-9_\.]+$/,
                                    message: '用户名只能由字母、数字、点和下划线组成'
                                }
                            }
                        }
                    }
                }).on('success.form.bv',function(e){
                    e.preventDefault();
                    var url = 're_password';
                    var telephone =  $('#findTelephone').val();
                    var num =  $('#findNum').val();
                    var md5_password =  hex_md5($('#findPassword').val());
                    var md5_confirmPassword =  hex_md5($('#confirmFindPassword').val());
                    $('#findPassword').val(md5_password);
                    $('#confirmFindPassword').val(md5_confirmPassword);
                    var submitData = "telephone="+ telephone +"&num="+ num +"&password="+ md5_password +"&confirmPassword="+ md5_confirmPassword +"&flag_ajax_reurn=1";
                    //submitData是解码后的表单数据，结果同上
                    $.post(url, submitData, function(result){
                        var dataObj=eval("("+result+")");
                        if(dataObj.ret != 0)
                        {
                            swal(
                                {
                                    title:'重置密码失败',
                                    text:dataObj.reason,
                                    type:"error",
                                    showCancelButton:false,
                                    confirmButtonText:"确定",
                                    closeOnConfirm:false
                                },
                                function()
                                {
                                    $('#findPassword').val("");
                                    $('#confirmFindPassword').val("");
                                    location.reload();
                                }
                            );
                        }
                        else
                        {
                            swal(
                                {
                                    title:'重置密码成功',
                                    text:'',
                                    type:"success",
                                    showCancelButton:false,
                                    confirmButtonText:"确定",
                                    closeOnConfirm:false
                                },function(){
                                    window.location.href='../../../order/con_manager/c_manager/home';
                                }
                            );
                        }
                    });        
                });
        </script>

    </body>
</html>