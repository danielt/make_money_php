<?php
/**
 * Created by <stacrcor.com>.
 * Author: xinxin.deng
 * Date: 2018/12/16 15:27
 */
if(!defined('VIEW_MODEL_BACKGROUD'))
{
    define('VIEW_MODEL_BACKGROUD', '/CodeIgniter/view_model/backstage/');
}

?>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrapValidator.min.js"></script>
    <script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/md5.js"></script>
    <!-- Sweet Alert -->
    <!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">-->
    <!--<script src="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/js/plugins/sweetalert/sweetalert.min.js"></script>-->
    <style type="text/css">
        .htmleaf-header{margin-bottom: 15px;font-family: "Segoe UI", "Lucida Grande", Helvetica, Arial, "Microsoft YaHei", FreeSans, Arimo, "Droid Sans", "wenquanyi micro hei", "Hiragino Sans GB", "Hiragino Sans GB W3", "FontAwesome", sans-serif;}
        .htmleaf-icon{color: #fff;}

        .profile-user-info-striped {
            border: 1px solid #dcebf7;
        }
        .profile-user-info {
            margin: 0 12px;
        }
        .profile-info-row {
            position: relative;
        }
        .profile-info-name {
            position: absolute;
            width: 21%;
            text-align: right;
            padding: 6px 10px 6px 0;
            left: 0;
            top: 0;
            bottom: 0;
            font-weight: normal;
            color: #667e99;
            background-color: transparent;
            border-top: 1px dotted #d5e4f1;
        }
        .profile-user-info-striped .profile-info-name {
            color: #336199;
            background-color: #edf3f4;
            border-top: 1px solid #f7fbff;
        }
        .profile-info-row:first-child .profile-info-name {
            border-top: 0;
        }
        .profile-info-value {
            padding: 6px 4px 6px 6px;
            margin-left: 21%;
            border-top: 1px dotted #d5e4f1;
            margin-bottom: 0!important;
        }
        .editable-click {
            border-bottom: 1px dashed #BBB;
            cursor: pointer;
            font-weight: normal;
        }
        .profile-picture {
            border: 1px solid #CCC;
            background-color: #FFF;
            padding: 4px;
            display: inline-block;
            max-width: 100%;
            -moz-box-sizing: border-box;
            box-shadow: 1px 1px 1px rgba(0,0,0,0.15);
        }
        .editable-click {
            border-bottom: 1px dashed #BBB;
            cursor: pointer;
            font-weight: normal;
        }
        img.editable-click {
            border: 1px dotted #BBB;
        }
        .space-4 {
            max-height: 1px;
            min-height: 1px;
            overflow: hidden;
            margin: 4px 0 3px;
        }
        .label {
            border-radius: 0;
            text-shadow: none;
            font-weight: normal;
            display: inline-block;
            background-color: #abbac3!important;
        }
        .label.arrowed:before, .label.arrowed-in:before {
            display: inline-block;
            content: "";
            position: absolute;
            top: 0;
            z-index: -1;
            border: 1px solid transparent;
            border-right-color: #abbac3;
        }
        .label.arrowed-in:before {
            left: -5px;
            border-width: 10px 5px;
            border-color: #abbac3;
            border-left-color: transparent!important;
        }
        .label.arrowed-in-right {
            position: relative;
            z-index: 1;
        }

        .width-80 {
            width: 80%!important;
        }
        .label-info{
            background-color: #3a87ad!important;
        }
        .label-info.arrowed-in-right:after {
            border-color: #3a87ad;
        }
        .label.arrowed-right:after, .label.arrowed-in-right:after {
            display: inline-block;
            content: "";
            position: absolute;
            top: 0;
            z-index: -1;
            border: 1px solid transparent;
            border-left-color: #abbac3;
        }
        .label.arrowed-in-right:after {
            border-color: #abbac3;
            border-right-color: transparent!important;
        }
        .label.arrowed-in-right:after {
            right: -5px;
            border-width: 10px 5px;
        }
        .label-xlg {
            padding: .3em .7em .4em;
            font-size: 14px;
            line-height: 1.3;
            height: 28px;
        }
        .label-xlg.arrowed-in {
            margin-left: 7px;
        }
        .label-xlg.arrowed-in:before {
            left: -7px;
            border-width: 14px 7px;
        }
        .label-xlg.arrowed-in-right:after {
            right: -7px;
            border-width: 14px 7px;
        }
        .label-info.arrowed-in:before {
            border-color: #3a87ad;
        }
        .label-info.arrowed-in-right:after {
            border-color: #3a87ad;
        }

        .label-xlg.arrowed-in-right {
            margin-right: 7px;
        }

        .hr {
            display: block;
            height: 0;
            overflow: hidden;
            font-size: 0;
            border-top: 1px solid #e3e3e3;
            margin: 12px 0;
        }
        .hr.dotted, .hr-dotted {
            border-top-style: dotted;
        }
        .row {
            margin-right: -12px;
            margin-left: -12px;
        }
        .inline {
            display: inline-block!important;
        }
        .position-relative {
            position: relative;
        }
        .white {
            color: #fff!important;
        }
        .center{
            text-align: center!important;
        }

        .nav-tabs {
            border-color: #c5d0dc;
            margin-bottom: 0;
            margin-left: 0;
            position: relative;
            top: 1px;
        }
        .nav-tabs.padding-12 {
            padding-left: 12px;
        }
        .nav-tabs.background-blue {
            padding-top: 6px;
            background-color: #eff3f8;
            border: 1px solid #c5d0dc;
        }
        .form-control-feedback{
            line-height: 45px!important;
        }
        .radio-inline input[type=radio] {
            margin-top: 4px!important;
        }
    </style>
    <script type="text/javascript">
        $(function(){/* 文档加载，执行一个函数*/
            init_edit_profile_form();
            //多选 start
            <?php if(!empty($user['cms_good_at_style']) && isset($user['cms_good_at_style'])){?>
                var select_cms_good_at_style = "<?php echo $user['cms_good_at_style'];?>";
                var select_cms_good_at_style_arr = select_cms_good_at_style.split(',');
                $('#select_cms_good_at_style').selectpicker('val', select_cms_good_at_style_arr);
            <?php }?>

            <?php if(!empty($user['cms_use_software']) && isset($user['cms_use_software'])){?>
                var select_cms_use_software = "<?php echo $user['cms_use_software'];?>";
                var select_cms_use_software_arr = select_cms_use_software.split(',');
                $('#select_cms_use_software').selectpicker('val', select_cms_use_software_arr);
            <?php }?>

            <?php if(!empty($user['cms_good_at_material']) && isset($user['cms_good_at_material'])){?>
                var select_cms_good_at_material = "<?php echo $user['cms_good_at_material'];?>";
                var select_cms_good_at_material_arr = select_cms_good_at_material.split(',');
                $('#select_cms_use_software').selectpicker('val', select_cms_good_at_material_arr);
            <?php }?>

            <?php if(!empty($user['cms_own_equipment']) && isset($user['cms_own_equipment'])){?>
                var select_cms_own_equipment = "<?php echo $user['cms_own_equipment'];?>";
                var select_cms_own_equipment_arr = select_cms_own_equipment.split(',');
                $('#select_cms_own_equipment').selectpicker('val', select_cms_own_equipment_arr);
            <?php }?>

            <?php if(!empty($user['cms_good_at_model']) && isset($user['cms_good_at_model'])){?>
                var select_cms_good_at_model = "<?php echo $user['cms_good_at_model'];?>";
                var select_cms_good_at_model_arr = select_cms_good_at_model.split(',');
                $('#select_cms_good_at_model').selectpicker('val', select_cms_good_at_model_arr);
            <?php }?>
            <?php if(!empty($user['cms_product_nature']) && isset($user['cms_product_nature'])){?>
                var select_cms_product_nature = "<?php echo $user['cms_product_nature'];?>";
                var select_cms_product_nature_arr = select_cms_product_nature.split(',');
                $('#select_cms_product_nature').selectpicker('val', select_cms_product_nature_arr);
            <?php }?>
            <?php if(!empty($user['cms_production_chain']) && isset($user['cms_production_chain'])){?>
                var select_cms_production_chain = "<?php echo $user['cms_production_chain'];?>";
                var select_cms_production_chain_arr = select_cms_production_chain.split(',');
                $('#select_cms_production_chain').selectpicker('val', select_cms_production_chain_arr);
            <?php }?>
            <?php if(!empty($user['cms_good_at_goods']) && isset($user['cms_good_at_goods'])){?>
                var select_cms_good_at_goods = "<?php echo $user['cms_good_at_goods'];?>";
                var select_cms_good_at_goods_arr = select_cms_good_at_goods.split(',');
                $('#select_cms_good_at_goods').selectpicker('val', select_cms_good_at_goods_arr);
            <?php }?>
            <?php if(!empty($user['cms_equipment_state']) && isset($user['cms_equipment_state'])){?>
                var select_cms_equipment_state = "<?php echo $user['cms_equipment_state'];?>";
                var select_cms_equipment_state_arr = select_cms_equipment_state.split(',');
                $('#select_cms_equipment_state').selectpicker('val', select_cms_equipment_state_arr);
            <?php }?>
            <?php if(!empty($user['cms_people_num']) && isset($user['cms_people_num'])){?>
                var select_cms_people_num = "<?php echo $user['cms_people_num'];?>";
                var select_cms_people_num_arr = select_cms_people_num.split(',');
                $('#select_cms_people_num').selectpicker('val', select_cms_people_num_arr);
            <?php }?>
            <?php if(!empty($user['cms_capacity']) && isset($user['cms_capacity'])){?>
                var select_cms_capacity = "<?php echo $user['cms_capacity'];?>";
                var select_cms_capacity_arr = select_cms_capacity.split(',');
                $('#select_cms_capacity').selectpicker('val', select_cms_capacity_arr);
            <?php }?>
            <?php if(!empty($user['cms_retriever']) && isset($user['cms_retriever'])){?>
                var select_cms_retriever = "<?php echo $user['cms_retriever'];?>";
                var select_cms_retriever_arr = select_cms_retriever.split(',');
                $('#select_cms_retriever').selectpicker('val', select_cms_retriever_arr);
            <?php }?>
            <?php if(!empty($user['cms_taxation']) && isset($user['cms_taxation'])){?>
                var select_cms_taxation = "<?php echo $user['cms_taxation'];?>";
                var select_cms_taxation_arr = select_cms_taxation.split(',');
                $('#select_cms_taxation').selectpicker('val', select_cms_taxation_arr);
            <?php }?>
            <?php if(!empty($user['cms_nature']) && isset($user['cms_nature'])){?>
                var select_cms_nature = "<?php echo $user['cms_nature'];?>";
                var select_cms_nature_arr = select_cms_nature.split(',');
                $('#select_cms_nature').selectpicker('val', select_cms_nature_arr);
            <?php }?>
            <?php if(!empty($user['cms_company_type']) && isset($user['cms_company_type'])){?>
                var select_cms_company_type = "<?php echo $user['cms_company_type'];?>";
                var select_cms_company_type_arr = select_cms_company_type.split(',');
                $('#select_cms_company_type').selectpicker('val', select_cms_company_type_arr);
            <?php }?>
            <?php if(!empty($user['cms_main_product']) && isset($user['cms_main_product'])){?>
                var select_main_product = "<?php echo $user['cms_main_product'];?>";
                var select_main_product_arr = select_main_product.split(',');
                $('#select_main_product').selectpicker('val', select_main_product_arr);
            <?php }?>
            <?php if(!empty($user['cms_sale_channels']) && isset($user['cms_sale_channels'])){?>
                var select_sale_channels = "<?php echo $user['cms_sale_channels'];?>";
                var select_sale_channels_arr = select_sale_channels.split(',');
                $('#select_sale_channels').selectpicker('val', select_sale_channels_arr);
            <?php }?>
            //面料织法
            <?php if(!empty($user['cms_fabric_weaving']) && isset($user['cms_fabric_weaving'])){?>
                var select_cms_fabric_weaving = "<?php echo $user['cms_fabric_weaving'];?>";
                var select_cms_fabric_weaving_arr = select_cms_fabric_weaving.split(',');
                $('#select_cms_fabric_weaving').selectpicker('val', select_cms_fabric_weaving_arr);
            <?php }?>
            //面料花型
            <?php if(!empty($user['cms_fabric_pattern']) && isset($user['cms_fabric_pattern'])){?>
                var select_cms_fabric_pattern = "<?php echo $user['cms_fabric_pattern'];?>";
                var select_cms_fabric_pattern_arr = select_cms_fabric_pattern.split(',');
                $('#select_cms_fabric_pattern').selectpicker('val', select_cms_fabric_pattern_arr);
            <?php }?>
            //面料成分
            <?php if(!empty($user['cms_fabric_composition']) && isset($user['cms_fabric_composition'])){?>
                var select_cms_fabric_composition = "<?php echo $user['cms_fabric_composition'];?>";
                var select_cms_fabric_composition_arr = select_cms_fabric_composition.split(',');
                $('#select_cms_fabric_composition').selectpicker('val', select_cms_fabric_composition_arr);
            <?php }?>
            //多选 end

            //修改密码
            $('#edit-password-form').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {/*input状态样式图片*/
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {/*验证：规则*/
                    old_password: {
                        message:'密码无效',
                        validators: {
                            notEmpty: {
                                message: '密码不能为空'
                            },
                            stringLength: {
                                min: 6,
                                max: 30,
                                message: '密码长度必须在6到30之间'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: '用户名只能由字母、数字、点和下划线组成'
                            }
                        }
                    },
                    new_password: {
                        message:'密码无效',
                        validators: {
                            notEmpty: {
                                message: '密码不能为空'
                            },
                            stringLength: {
                                min: 6,
                                max: 30,
                                message: '密码长度必须在6到30之间'
                            },
                            different: {//不能和用户名相同
                                field: 'old_password',//需要进行比较的input name值
                                message: '不能和旧密码相同'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: '用户名只能由字母、数字、点和下划线组成'
                            }
                        }
                    },
                    confirmPassword: {
                        message:'密码无效',
                        validators: {
                            notEmpty: {
                                message: '密码不能为空'
                            },
                            stringLength: {
                                min: 6,
                                max: 30,
                                message: '密码长度必须在6到30之间'
                            },
                            identical: {//相同
                                field: 'new_password', //需要进行比较的input name值
                                message: '两次密码不一致'
                            },
                            different: {//不能和用户名相同
                                field: 'old_password',//需要进行比较的input name值
                                message: '不能和旧密码相同'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: '用户名只能由字母、数字、点和下划线组成'
                            }
                        }
                    }
                }
            }).on('success.form.bv',function(e){
                e.preventDefault();
                var url = 'edit_password';
                var md5_old_password =  hex_md5($('#old_password').val());
                var md5_new_password =  hex_md5($('#new_password').val());
                var confirmPassword =  hex_md5($('#confirmPassword').val());
                $('#old_password').val(md5_old_password);
                $('#new_password').val(md5_new_password);
                $('#confirmPassword').val(confirmPassword);
                var submitData = $('#edit-password-form').serialize() + "&flag_ajax_reurn=1";
                //submitData是解码后的表单数据，结果同上
                $.post(url, submitData, function(result){
                    var dataObj=eval("("+result+")");
                    if(dataObj.ret != 0)
                    {
                        swal(
                            {
                                title:'修改密码失败',
                                text:dataObj.reason,
                                type:"error",
                                showCancelButton:false,
                                showConfirmButton:false,
                                timer:1500
                            },function()
                            {
                                $('#old_password').val("");
                                $('#new_password').val("");
                                $('#confirmPassword').val("");
                            }
                        );
                    }
                    else
                    {
                        swal(
                            {
                                title:'修改密码成功',
                                text:'',
                                type:"success",
                                showCancelButton:false,
                                showConfirmButton:false,
                                timer:1500
                            },function()
                            {
                                location.reload();
                            }
                        );
                    }
                });
            });

            //完善资料
            $('#edit_profile_form').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {/*input状态样式图片*/
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {/*验证：规则*/
                    email: {
                        message:'邮箱无效',
                        emailAddress: {
                            regexp: {
                                message: '邮箱地址不正确'
                            }
                        }
                    },
                    wx_num: {
                        message:'微信号无效',
                        validators: {
                            notEmpty: {
                                message: '微信号不能为空'
                            }
                        }
                    },
                <?php if ($user['cms_role_id'] == 5){ ?>
                    qq: {
                        message:'qq号无效',
                        validators: {
                            regexp: {
                                regexp: /^[1-9][0-9]{4,14}$/,
                                message: 'qq号只能由数字组成,5到15位，不能以0开头'
                            },
                            notEmpty: {
                                message: 'qq号不能为空'
                            }
                        }
                    },
                <?php }?>
                    select_cms_use_software: {
                        message:'请选择使用软件',
                        validators: {
                            notEmpty: {
                                message: '使用软件不能为空'
                            }
                        }
                    },
                    select_cms_good_at_style: {
                        message:'请选择擅长风格',
                        validators: {
                            notEmpty: {
                                message: '请选择擅长风格'
                            }
                        }
                    },
                    cms_plateing_price: {
                        message:'请选择制板费',
                        validators: {
                            notEmpty: {
                                message: '请选择制板费'
                            }
                        }
                    },
                    cms_pushing_plate_price: {
                        message:'请选择放板推板费',
                        validators: {
                            notEmpty: {
                                message: '请选择放板推板费'
                            }
                        }
                    },
                    select_cms_good_at_material: {
                        message:'请选择擅长布料',
                        validators: {
                            notEmpty: {
                                message: '请选择擅长布料'
                            }
                        }
                    },
                    select_cms_own_equipment: {
                        message:'请选择自有设备',
                        validators: {
                            notEmpty: {
                                message: '请选择自有设备'
                            }
                        }
                    },
                    cms_is_full_time: {
                        message:'请选择是否专职',
                        validators: {
                            notEmpty: {
                                message: '请选择是否专职'
                            }
                        }
                    },
                    cms_have_workroom: {
                        message:'请选择有无工作室',
                        validators: {
                            notEmpty: {
                                message: '请选择有无工作室'
                            }
                        }
                    },
                    select_cms_good_at_model: {
                        message:'请选择擅长款式',
                        validators: {
                            notEmpty: {
                                message: '请选择擅长款式'
                            }
                        }
                    },
                    select_cms_production_chain: {
                        message:'请选择生产链',
                        validators: {
                            notEmpty: {
                                message: '请选择生产链'
                            }
                        }
                    },
                    cms_people_num: {
                        message:'请选择车工人数',
                        validators: {
                            notEmpty: {
                                message: '请选择车工人数'
                            }
                        }
                    },
                    select_cms_product_nature: {
                        message:'请选择生产性质',
                        validators: {
                            notEmpty: {
                                message: '请选择生产性质'
                            }
                        }
                    },
                    cms_factor_name: {
                        message:'请填写厂名',
                        validators: {
                            notEmpty: {
                                message: '请填写厂名'
                            }
                        }
                    },
                    cms_company_name: {
                        message:'请填写企业名称',
                        validators: {
                            notEmpty: {
                                message: '请填写企业名称'
                            }
                        }
                    },
                    cms_capacity: {
                        message:'请选择产能',
                        validators: {
                            notEmpty: {
                                message: '请选择产能'
                            }
                        }
                    },
                    select_cms_retriever: {
                        message:'请选择品种',
                        validators: {
                            notEmpty: {
                                message: '请选择品种'
                            }
                        }
                    },
                    select_cms_taxation: {
                        message:'请选择税务情况',
                        validators: {
                            notEmpty: {
                                message: '请选择税务情况'
                            }
                        }
                    },
                    select_cms_nature: {
                        message:'请选供应商性质',
                        validators: {
                            notEmpty: {
                                message: '请选供应商性质'
                            }
                        }
                    },
                    select_sale_channels: {
                        message:'请选择销售渠道',
                        validators: {
                            notEmpty: {
                                message: '请选择销售渠道'
                            }
                        }
                    },
                    select_main_product: {
                        message:'请选择主营产品',
                        validators: {
                            notEmpty: {
                                message: '请选择主营产品'
                            }
                        }
                    },
                    select_cms_company_type: {
                        message:'请选择企业类型',
                        validators: {
                            notEmpty: {
                                message: '请选择企业类型'
                            }
                        }
                    }
                }
            }).on('success.form.bv',function(e){
                e.preventDefault();
                data_link_field();
                //$('#cms_good_at_style').val($('#select_good_at_style').val());//擅长
                var url = 'edit_profile';
                var submitData = $('#edit_profile_form').serialize() + "&flag_ajax_reurn=1";
                //submitData是解码后的表单数据，结果同上
                $.post(url, submitData, function(result){
                    var dataObj=eval("("+result+")");
                    if(dataObj.ret != 0)
                    {
                        swal(
                            {
                                title:'完善资料失败',
                                text:dataObj.reason,
                                type:"error",
                                showCancelButton:false,
                                showConfirmButton:false,
                                timer:1500
                            },function()
                            {
                                location.reload();
                            }
                        );
                    }
                    else
                    {
                        swal(
                            {
                                title:'完善资料成功',
                                text:'',
                                type:"success",
                                showCancelButton:false,
                                showConfirmButton:false,
                                timer:1500
                            },function()
                            {
                                location.reload();
                            }
                        );
                    }
                });
            });

            //修改手机号
            $('#edit-telephone-form').bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {/*input状态样式图片*/
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {/*验证：规则*/
                    old_telephone: {
                        message: 'The phone is not valid',
                        validators: {
                            notEmpty: {
                                message: '手机号码不能为空'
                            },
                            regexp: {
                                min: 11,
                                max: 11,
                                regexp: /^1\d{10}$/,
                                message: '请输入正确的手机号码'
                            }
                        }
                    },
                    num: {
                        message:'验证码无效',
                        validators: {
                            notEmpty: {
                                message: '验证码不能为空'
                            },
                            stringLength: {
                                min: 6,
                                max: 6,
                                message: '验证码填写错误'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: '用户名只能由字母、数字、点和下划线组成'
                            }
                        }
                    },
                    new_telephone: {
                        message: 'The phone is not valid',
                        validators: {
                            notEmpty: {
                                message: '手机号码不能为空'
                            },
                            regexp: {
                                min: 11,
                                max: 11,
                                regexp: /^1\d{10}$/,
                                message: '请输入正确的手机号码'
                            }
                        }
                    },
                    re_num: {
                        message:'验证码无效',
                        validators: {
                            notEmpty: {
                                message: '验证码不能为空'
                            },
                            stringLength: {
                                min: 6,
                                max: 6,
                                message: '验证码填写错误'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9_\.]+$/,
                                message: '用户名只能由字母、数字、点和下划线组成'
                            }
                        }
                    }
                }
            }).on('success.form.bv',function(e){
                e.preventDefault();
                var url = 'edit_telephone';
                var submitData = $('#edit-telephone-form').serialize() + "&flag_ajax_reurn=1";
                //submitData是解码后的表单数据，结果同上
                $.post(url, submitData, function(result){
                    var dataObj=eval("("+result+")");
                    if(dataObj.ret != 0)
                    {
                        swal(
                            {
                                title:'修改手机号失败',
                                text:dataObj.reason,
                                type:"error",
                                showCancelButton:false,
                                showConfirmButton:false,
                                timer:1500
                            }
                        );
                    }
                    else
                    {
                        swal(
                            {
                                title:'修改手机号成功',
                                text:'',
                                type:"success",
                                showCancelButton:false,
                                showConfirmButton:false,
                                timer:1500
                            },function()
                            {
                                location.reload();
                            }
                        );
                    }
                });
            });
        });

        //初始化文件上传
        function init_edit_profile_form()
        {
            var params = <?php if(isset($user)){echo "'".base64_encode(json_encode($user))."'";}else{echo '';}?>;
            //0.初始化fileinput
            var oFileInput = new FileInput();
            oFileInput.Init('edit',"image_data_add_1",'image_data_add_value_1',params);
            oFileInput.Init('edit',"image_data_add_2",'image_data_add_value_2',params);
            oFileInput.Init('edit',"image_data_add_3",'image_data_add_value_3',params);
            oFileInput.Init('edit',"image_data_add_4",'image_data_add_value_4',params);
            oFileInput.Init('edit',"image_data_add_5",'image_data_add_value_5',params);
            oFileInput.Init('edit',"image_data_add_6",'image_data_add_value_6',params);
            oFileInput.Init('edit',"image_data_add_7",'image_data_add_value_7',params);
        }
    </script>
</head>
<body>
<div class="col-xs-12">
    <div class="tabbale">
        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab4">

            <li class="active">
                <a data-toggle="tab" href="#profile">基本资料</a>
            </li>

            <li>
                <a data-toggle="tab" href="#edit_profile">完善资料</a>
            </li>

            <li>
                <a data-toggle="tab" href="#edit_password">修改密码</a>
            </li>

            <li>
                <a data-toggle="tab" href="#edit_telephone">修改手机号</a>
            </li>
        </ul>
        <div class="tab-content">
            <!--基本资料-->
            <div class="user-profile row tab-pane active" id="profile">
                <div class="col-xs-12 col-sm-3 center">
                    <div>
                    <span class="profile-picture">
                        <img id="avatar" class="editable img-resposive editable-click editable-empty" style="width: 180px!important;height: 200px!important;" src="<?php
                        if (isset($user['cms_head_img'])&&!empty($user['cms_head_img']))
                        {
                            echo VIEW_MODEL_BASE . '/data_model/upload/' . $user['cms_head_img'];
                        }else{
                            echo VIEW_MODEL_BACKGROUD . 'images/profile-pic.jpg';
                        } ?>">
                    </span>
                        <div class="space-4"></div>
                        <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                            <div class="inline position-relative">
                                <span class="white"><?PHP echo $user['cms_name']?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="profile-user-info profile-user-info-striped">
                        <div class="profile-info-row">
                            <div class="profile-info-name">用户id</div>

                            <div class="profile-info-value">
                                <span class="editable"><?PHP echo $user['cms_id']?></span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">用户名</div>

                            <div class="profile-info-value" style="height: 33px">
                                <span class="editable" style="height: 20px"><?PHP echo $user['cms_name']?></span>
                            </div>
                        </div>
                        <div class="profile-info-row">
                            <div class="profile-info-name">用户积分</div>

                            <div class="profile-info-value" style="height: 33px">
                                <span class="editable" style="height: 20px"><?PHP echo $user['cms_integral']?></span>
                            </div>
                        </div>

                        <div class="profile-info-row">
                            <div class="profile-info-name">手机号</div>

                            <div class="profile-info-value">
                                <span class="editable"><?PHP echo $user['cms_telephone']?></span>
                            </div>
                        </div>

                        <div class="profile-info-row" style="height: 33px">
                            <div class="profile-info-name">地址</div>

                            <div class="profile-info-value">
                                <span class="editable" style="height: 20px"><?PHP echo $user['cms_country']?></span>
                                <span class="editable" style="height: 20px"><?PHP echo $user['cms_address']?></span>
                            </div>
                        </div>

                        <div class="profile-info-row" style="height: 33px">
                            <div class="profile-info-name">邮箱</div>

                            <div class="profile-info-value">
                                <span class="editable" style="height: 20px"><?PHP echo $user['cms_email']?></span>
                            </div>
                        </div>

                        <div class="profile-info-row">
                            <div class="profile-info-name">用户角色</div>

                            <div class="profile-info-value">
                                <?php switch ($user['cms_role_id']){
                                    case 1:
                                        echo '<span class="editable" id="login">订货客户</span>';
                                        break;
                                    case 2:
                                        echo '<span class="editable" id="login">平台管理员</span>';
                                        break;
                                    case 3:
                                        echo '<span class="editable" id="login">生产厂商</span>';
                                        break;
                                    case 4:
                                        echo '<span class="editable" id="login">供应商</span>';
                                        break;
                                    case 5:
                                        echo '<span class="editable" id="login">样板师</span>';
                                        break;
                                    case 6:
                                        echo '<span class="editable" id="login">样衣师</span>';
                                        break;
                                } ?>
                            </div>
                        </div>

                        <div class="profile-info-row" style="height: 33px">
                            <div class="profile-info-name">简介</div>

                            <div class="profile-info-value">
                                <span class="editable"><?PHP echo $user['cms_desc']?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--完善资料-->
            <div class="user-profile row tab-pane" id="edit_profile" >
                <div class="col-xs-12 col-sm-12">
                    <form id="edit_profile_form">
                        <div class="profile-user-info profile-user-info-striped">
                            <input hidden name="user_id" value="<?PHP echo $user['cms_id']?>">
                            <input hidden name="telephone" value="<?PHP echo $user['cms_telephone']?>">
                            <input hidden name="role_id" value="<?PHP echo $user['cms_role_id']?>">
                            <div class="profile-info-row">
                                <div class="profile-info-name">头像</div>
                                <div class="profile-info-value form-group">
                                    <input type="file" id="image_data_add_7" name="image_data" accept="image/*">
                                </div>
                                <div id="image_data_add_value_7" post_name="cms_head_img"></div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">用户名称<font color="red">*</font></div>
                                <div class="profile-info-value">
                                    <input type="text" class="editable" name="username" value="<?PHP echo $user['cms_name']?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">微信号<font color="red">*</font></div>
                                <div class="profile-info-value form-group">
                                    <input type="text" class="editable" name="wx_num" value="<?PHP echo $user['cms_wx_num']?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">QQ号
                                    <?php if ($user['cms_role_id'] == 5){ ?>
                                        <font color="red">*</font>
                                    <?php }?>
                                </div>
                                <div class="profile-info-value form-group">
                                    <input type="text" class="editable" id="qq" name="qq" value="<?PHP echo $user['cms_qq']?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">邮箱</div>
                                <div class="profile-info-value">
                                    <input type="email" class="editable" id="email" name="email" value="<?PHP echo $user['cms_email']?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">地址</div>

                                <div class="profile-info-value">
                                    <input type="text" style="width: 50%" class="editable" id="address" name="address" value="<?PHP if (isset($user['cms_address'])){echo $user['cms_address'];}?>">
                                </div>
                            </div>
                            <?php if ($user['cms_role_id'] == 5 || $user['cms_role_id'] == 6){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">是否专职<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <label class="radio-inline"><input name="cms_is_full_time" type="radio" value="0" <?php if (isset($user['cms_is_full_time']) && $user['cms_is_full_time'] == '0') echo 'checked'; ?>>否</label>
                                        <label class="radio-inline"><input name="cms_is_full_time" type="radio" value="1" <?php if ($user['cms_is_full_time'] == '1') echo 'checked'; ?>>是</label>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">有无工作室<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <label class="radio-inline"><input name="cms_have_workroom" type="radio" value="0" <?php if (isset($user['cms_have_workroom']) && $user['cms_have_workroom'] == '0') echo 'checked'; ?>>无</label>
                                        <label class="radio-inline"><input name="cms_have_workroom" type="radio" value="1" <?php if ($user['cms_have_workroom'] == '1') echo 'checked'; ?>>有</label>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">擅长风格<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_good_at_style" id="select_cms_good_at_style" name="select_cms_good_at_style">
                                            <option value="厚型">厚型</option>
                                            <option value="薄型">薄型</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_good_at_style" name="cms_good_at_style">
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 5){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">使用软件<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_use_software" id="select_cms_use_software" name="select_cms_use_software">
                                            <option value="ET">ET</option>
                                            <option value="BKR">BKR</option>
                                            <option value="至尊宝">至尊宝</option>
                                            <option value="其他">其他</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_use_software" name="cms_use_software">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">制版费<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <label class="radio-inline"><input name="cms_plateing_price" type="radio" value="0" <?php if ($user['cms_plateing_price'] == '0') echo 'checked'; ?>>100元</label>
                                        <label class="radio-inline"><input name="cms_plateing_price" type="radio" value="1" <?php if ($user['cms_plateing_price'] == '1') echo 'checked'; ?>>120元</label>
                                        <label class="radio-inline"><input name="cms_plateing_price" type="radio" value="2" <?php if ($user['cms_plateing_price'] == '2') echo 'checked'; ?>>150元</label>
                                        <label class="radio-inline"><input name="cms_plateing_price" type="radio" value="3" <?php if ($user['cms_plateing_price'] == '3') echo 'checked'; ?>>175元</label>
                                        <label class="radio-inline"><input name="cms_plateing_price" type="radio" value="4" <?php if ($user['cms_plateing_price'] == '4') echo 'checked'; ?>>200元</label>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">放版推板费<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <label class="radio-inline"><input name="cms_pushing_plate_price" type="radio" value="0" <?php if ($user['cms_pushing_plate_price'] == '0') echo 'checked'; ?>>15元</label>
                                        <label class="radio-inline"><input name="cms_pushing_plate_price" type="radio" value="1" <?php if ($user['cms_pushing_plate_price'] == '1') echo 'checked'; ?>>20元</label>
                                        <label class="radio-inline"><input name="cms_pushing_plate_price" type="radio" value="2" <?php if ($user['cms_pushing_plate_price'] == '2') echo 'checked'; ?>>25元</label>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 6){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">擅长布料<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_good_at_material" id="select_cms_good_at_material" name="select_cms_good_at_material">
                                            <option value="针织">针织</option>
                                            <option value="梭织">梭织</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_good_at_material" name="cms_good_at_material">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">自有设备<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_own_equipment" id="select_cms_own_equipment" name="select_cms_own_equipment">
                                            <option value="平缝机">平缝机</option>
                                            <option value="锁边机">锁边机</option>
                                            <option value="平头锁眼机">平头锁眼机</option>
                                            <option value="圆头锁眼机">圆头锁眼机</option>
                                            <option value="钉扣机">钉扣机</option>
                                            <option value="套结机">套结机</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_own_equipment" name="cms_own_equipment">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">擅长款式<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_good_at_model" id="select_cms_good_at_model" name="select_cms_good_at_model">
                                            <option value="童装">童装</option>
                                            <option value="外套">外套</option>
                                            <option value="女时装">女时装</option>
                                            <option value="裤子">裤子</option>
                                            <option value="棉衣">棉衣</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_good_at_model" name="cms_good_at_model">
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 1 || $user['cms_role_id'] == 4){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">企业名称<font color="red">*</font></div>
                                    <div class="profile-info-value">
                                        <input type="text" class="editable" id="cms_company_name" name="cms_company_name" value="<?PHP echo $user['cms_company_name']?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 3){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">厂名<font color="red">*</font></div>
                                    <div class="profile-info-value">
                                        <input type="text" class="editable" id="cms_factor_name" name="cms_factor_name" value="<?PHP echo $user['cms_factor_name']?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">生产性质<font color="red">*</font></div>
                                    <div class="profile-info-value">
                                        <select class="selectpicker" data-live-search="false" data-max-options="1" multiple data-link-field="cms_product_nature" id="select_cms_product_nature" name="select_cms_product_nature">
                                            <option value="梭织服装">梭织服装</option>
                                            <option value="针织服装">针织服装</option>
                                            <option value="绣花厂">绣花厂</option>
                                            <option value="绣花厂">绣花厂</option>
                                            <option value="水洗厂">水洗厂</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_product_nature" name="cms_product_nature">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">生产链<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-max-options="1" data-link-field="cms_production_chain" id="select_cms_production_chain" name="select_cms_production_chain">
                                            <option value="裁床">裁床</option>
                                            <option value="车缝">车缝</option>
                                            <option value="后整">后整</option>
                                            <option value="技术科">技术科</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_production_chain" name="cms_production_chain">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">擅长产品</div>
                                    <div class="profile-info-value">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_good_at_goods" id="select_cms_good_at_goods">
                                            <option value="内衣/外套">内衣/外套</option>
                                            <option value="童装/大人">童装/大人</option>
                                            <option value="后整">薄料/厚料</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_good_at_goods" name="cms_good_at_goods">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">设备状况</div>
                                    <div class="profile-info-value">
                                        <select class="selectpicker" data-live-search="true" multiple data-link-field="cms_equipment_state" id="select_cms_equipment_state">
                                            <option value="平车(型号)">平车(型号)</option>
                                            <option value="拷边(型号)">拷边(型号)</option>
                                            <option value="平头锁眼机">平头锁眼机</option>
                                            <option value="圆头锁眼机">圆头锁眼机</option>
                                            <option value="套结机">套结机</option>
                                            <option value="钉扣机">钉扣机</option>
                                            <option value="四合扣机">四合扣机</option>
                                            <option value="橡筋机">橡筋机</option>
                                            <option value="烫台">烫台</option>
                                            <option value="裁床">裁床</option>
                                            <option value="裁床">裁床</option>
                                            <option value="其他专业设备(衬衫全套设备/西服全套设备/封胶设备/充绒机)">其他专业设备(衬衫全套设备/西服全套设备/封胶设备/充绒机)</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_equipment_state" name="cms_equipment_state">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">车工人数<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-max-options="1" name="cms_people_num" id="select_cms_people_num">
                                            <option value="~10">~10</option>
                                            <option value="11~30">11~30</option>
                                            <option value="30~50">30~50</option>
                                            <option value="50~100">50~100</option>
                                            <option value="100~200">100~200</option>
                                            <option value="200~">200~</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">产能<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-max-options="1" name="cms_capacity" id="select_cms_capacity">
                                            <option value="~10~20~烘箱数">~10~20~烘箱数</option>
                                            <option value="~2000~5000~件每天">~2000~5000~件每天</option>
                                            <option value="~1000~2000~4000~件每天">~1000~2000~4000~件每天</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">工厂图片</div>
                                    <div class="profile-info-value form-group">
                                        <input type="file" id="image_data_add_1" name="image_data" accept="image/*">
                                    </div>
                                    <div id="image_data_add_value_1" post_name="cms_factor_img"></div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">ISO证书</div>
                                    <div class="profile-info-value form-group">
                                        <input type="file" id="image_data_add_2" name="image_data" accept="image/*">
                                    </div>
                                    <div id="image_data_add_value_2" post_name="cms_iso_certificate"></div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">人权证书</div>
                                    <div class="profile-info-value form-group">
                                        <input type="file" id="image_data_add_3" name="image_data" accept="image/*">
                                    </div>
                                    <div id="image_data_add_value_3" post_name="cms_human_certificate"></div>
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 1 || $user['cms_role_id'] == 3|| $user['cms_role_id'] == 4){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">营业执照</div>
                                    <div class="profile-info-value form-group">
                                        <input type="file" id="image_data_add_4" name="image_data" accept="image/*">
                                    </div>
                                    <div id="image_data_add_value_4" post_name="cms_business_license_img"></div>
                                </div>
                            <?php } ?>

                            <?php if ($user['cms_role_id'] == 3 || $user['cms_role_id'] == 4){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">税务登记证</div>
                                    <div class="profile-info-value form-group">
                                        <input type="file" id="image_data_add_5" name="image_data" accept="image/*">
                                    </div>
                                    <div id="image_data_add_value_5" post_name="cms_taxation_certificate"></div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">对公银行账户数据信息</div>
                                    <div class="profile-info-value">
                                        <input type="text" style="width: 100%" class="editable" id="bank_info" name="bank_info" value="<?PHP if (isset($user['cms_bank_info'])){echo $user['cms_bank_info'];}?>" placeholder="如开户名、开户地、开户行、银行卡号">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 4){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">产品图片</div>
                                    <div class="profile-info-value form-group">
                                        <input type="file" id="image_data_add_6" name="image_data" accept="image/*">
                                    </div>
                                    <div id="image_data_add_value_6" post_name="cms_goods_img"></div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">(辅料供应商)经营品种<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="true" multiple data-link-field="cms_retriever" id="select_cms_retriever" name="select_cms_retriever">
                                            <option value="纽扣">纽扣</option>
                                            <option value="拉链">拉链</option>
                                            <option value="罗口">罗口</option>
                                            <option value="皮标">皮标</option>
                                            <option value="花边">花边</option>
                                            <option value="绳带">绳带</option>
                                            <option value="唛头">唛头</option>
                                            <option value="缝纫线">缝纫线</option>
                                            <option value="粘合衬">粘合衬</option>
                                            <option value="金属牌">金属牌</option>
                                            <option value="喷咬棉">喷咬棉</option>
                                            <option value="羽绒/填充物">羽绒/填充物</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_retriever" name="cms_retriever">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">(面料供应商)面料织法<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" multiple data-link-field="cms_fabric_weaving" id="select_cms_fabric_weaving" name="select_cms_fabric_weaving">
                                            <option value="针织">针织</option>
                                            <option value="梭织">梭织</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_fabric_weaving" name="cms_fabric_weaving">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">(面料供应商)面料花型<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" multiple data-link-field="cms_fabric_pattern" id="select_cms_fabric_pattern" name="select_cms_fabric_pattern">
                                            <option value="素色">素色</option>
                                            <option value="提花">提花</option>
                                            <option value="印花">印花</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_fabric_pattern" name="cms_fabric_pattern">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">(面料供应商)面料成分<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" multiple data-link-field="cms_fabric_composition" id="select_cms_fabric_composition" name="select_cms_fabric_composition">
                                            <option value="棉">棉</option>
                                            <option value="麻">麻</option>
                                            <option value="毛">毛</option>
                                            <option value="丝">丝</option>
                                            <option value="皮革">皮革</option>
                                            <option value="混纺类">混纺类</option>
                                            <option value="化纤类">化纤类</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_fabric_composition" name="cms_fabric_composition">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">供应商性质<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_nature" id="select_cms_nature" name="select_cms_nature">
                                            <option value="贸易商">贸易商</option>
                                            <option value="生产厂">生产厂</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_nature" name="cms_nature">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">税务情况<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-link-field="cms_taxation" id="select_cms_taxation" name="select_cms_taxation">
                                            <option value="梭织">无发票</option>
                                            <option value="增值税发票">普通发票</option>
                                            <option value="增值税发票">增值税发票</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_taxation" name="cms_taxation">
                                </div>
                            <?php } ?>
                            <?php if ($user['cms_role_id'] == 1){?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">成立时间<font color="red">*</font></div>
                                    <div class="profile-info-value">
                                        <div class="input-group date form_date" style="padding-bottom: 0!important;width: 33%" data-date="" data-date-format="yyyy-mm-dd" data-link-field="establish_date" data-link-format="yyyy-mm-dd">
                                            <input class="form-control" type="text" value="<?PHP if (isset($user['cms_establish_date']) && $user['cms_establish_date'] !== '0000-00-00'){echo $user['cms_establish_date'];}?>" readonly>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>
                                        <input type="text" hidden id="establish_date" name="establish_date" value="" />
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">企业类型<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-max-options="1" data-link-field="cms_company_type" id="select_cms_company_type" name="select_cms_company_type">
                                            <option value="服装品牌商">服装品牌商</option>
                                            <option value="服装贸易商">服装贸易商</option>
                                            <option value="批发档口">批发档口</option>
                                            <option value="个人设计师">个人设计师</option>
                                            <option value="淘宝店主">淘宝店主</option>
                                            <option value="微商">微商</option>
                                            <option value="网红及团队">网红及团队</option>
                                            <option value="其他">其他</option>
                                        </select>
                                    </div>
                                    <input hidden id="cms_company_type" name="cms_company_type">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">主营产品<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-max-options="1" data-link-field="main_product" id="select_main_product" name="select_main_product">
                                            <option value="梭织服装">梭织服装</option>
                                            <option value="针织服装">针织服装</option>
                                            <option value="毛衫">毛衫</option>
                                            <option value="裘皮/防裘皮">裘皮/防裘皮</option>
                                            <option value="皮革/仿皮">皮革/仿皮</option>
                                            <option value="家纺">家纺</option>
                                            <option value="服饰">服饰</option>
                                            <option value="其他">其他</option>
                                        </select>
                                    </div>
                                    <input hidden id="main_product" name="main_product">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">销售渠道<font color="red">*</font></div>
                                    <div class="profile-info-value form-group">
                                        <select class="selectpicker" data-live-search="false" multiple data-max-options="1" data-link-field="sale_channels" id="select_sale_channels" name="select_sale_channels">
                                            <option value="批发">批发</option>
                                            <option value="零售">零售</option>
                                            <option value="实体店">实体店</option>
                                            <option value="网销">网销</option>
                                            <option value="其他">其他</option>
                                        </select>
                                    </div>
                                    <input hidden id="sale_channels" name="sale_channels">
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">纳税人类型</div>
                                    <div class="profile-info-value">
                                        <label class="radio-inline"><input name="cms_taxpayer_type" type="radio" value="1" <?php if ($user['cms_taxpayer_type'] == '1') echo 'checked'; ?>>一般纳税人</label>
                                        <label class="radio-inline"><input name="cms_taxpayer_type" type="radio" value="2" <?php if ($user['cms_taxpayer_type'] == '2') echo 'checked'; ?>>小规模纳税人</label>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">是否需要发票</div>
                                    <div class="profile-info-value">
                                        <label class="radio-inline"><input name="cms_need_invoice" type="radio" value="1" <?php if ($user['cms_need_invoice'] == '1') echo 'checked'; ?>>需要发票</label>
                                        <label class="radio-inline"><input name="cms_need_invoice" type="radio" value="2" <?php if ($user['cms_need_invoice'] == '2') echo 'checked'; ?>>不要发票</label>
                                        <label class="radio-inline"><input name="cms_need_invoice" type="radio" value="3" <?php if ($user['cms_need_invoice'] == '3') echo 'checked'; ?>>待定</label>
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">税号</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_duty_num" name="cms_duty_num" value="<?PHP if (isset($user['cms_duty_num'])){echo $user['cms_duty_num'];}?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">开户银行</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_base_bank_name" name="cms_base_bank_name" value="<?PHP if (isset($user['cms_base_bank_name'])){echo $user['cms_base_bank_name'];}?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">基本户账号</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_base_bank_num" name="cms_base_bank_num" value="<?PHP if (isset($user['cms_base_bank_num'])){echo $user['cms_base_bank_num'];}?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">往来账号开户行</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_contact_bank_name" name="cms_contact_bank_name" value="<?PHP if (isset($user['cms_contact_bank_name'])){echo $user['cms_contact_bank_name'];}?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">往来账号</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_contact_bank_num" name="cms_contact_bank_num" value="<?PHP if (isset($user['cms_contact_bank_num'])){echo $user['cms_contact_bank_num'];}?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="profile-info-row">
                                <div class="profile-info-name">快递发货地址</div>
                                <div class="profile-info-value form-group">
                                    <input type="text" style="width: 100%" class="editable" id="courier_info" name="courier_info" value="<?PHP if (isset($user['cms_courier_info'])){echo $user['cms_courier_info'];}?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">快递收件人</div>
                                <div class="profile-info-value form-group">
                                    <input type="text" style="width: 100%" class="editable" id="cms_courier_contact" name="cms_courier_contact" value="<?PHP if (isset($user['cms_courier_contact'])){echo $user['cms_courier_contact'];}?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">快递收件人电话</div>
                                <div class="profile-info-value form-group">
                                    <input type="text" style="width: 100%" class="editable" id="cms_courier_telephone" name="cms_courier_telephone" value="<?PHP if (isset($user['cms_courier_telephone'])){echo $user['cms_courier_telephone'];}?>">
                                </div>
                            </div>
                            <?php if ($user['cms_role_id'] == 1 || $user['cms_role_id'] == 3 || $user['cms_role_id'] == 4){ ?>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">大件发货地址</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="courier_big_info" name="courier_big_info" value="<?PHP if (isset($user['cms_courier_big_info'])){echo $user['cms_courier_big_info'];}?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">大件快递收件人</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_courier_big_contact" name="cms_courier_big_contact" value="<?PHP if (isset($user['cms_courier_big_contact'])){echo $user['cms_courier_big_contact'];}?>">
                                    </div>
                                </div>
                                <div class="profile-info-row">
                                    <div class="profile-info-name">大件快递收件人电话</div>
                                    <div class="profile-info-value form-group">
                                        <input type="text" style="width: 100%" class="editable" id="cms_courier_big_telephone" name="cms_courier_big_telephone" value="<?PHP if (isset($user['cms_courier_big_telephone'])){echo $user['cms_courier_big_telephone'];}?>">
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="profile-info-row">
                                <div class="profile-info-name">简介</div>
                                <div class="profile-info-value">
                                    <input type="text" style="width: 100%" class="editable" id="desc" name="desc" value="<?PHP echo $user['cms_desc']?>">
                                </div>
                            </div>
                        </div>
                        <div class="profile-info-row center">
                            <button class="btn btn-primary">确认修改</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--修改密码-->
            <div class="user-profile row tab-pane" id="edit_password">
                <div class="col-xs-12 col-sm-12">
                    <form id="edit-password-form">
                        <div class="profile-user-info profile-user-info-striped">

                            <div class="profile-info-row">
                                <div class="profile-info-name">用户id</div>

                                <div class="profile-info-value">
                                    <span class="editable"><?PHP echo $user['cms_id']?></span>
                                    <input type="text"  hidden name="user_id" id="user_id" value="<?PHP echo $user['cms_id']?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">手机号</div>

                                <div class="profile-info-value">
                                    <span class="editable"><?PHP echo $user['cms_telephone']?></span>
                                    <input type="tel" hidden name="telephone" id="telephone" value="<?PHP echo $user['cms_telephone']?>">
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">旧密码</div>

                                <div class="profile-info-value">
                                    <input type="password" class="editable" id="old_password" name="old_password">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">新密码</div>

                                <div class="profile-info-value">
                                    <input type="password" class="editable" id="new_password" name="new_password">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">再次输入新密码</div>

                                <div class="profile-info-value">
                                    <input type="password" class="editable" id="confirmPassword" name="confirmPassword">
                                </div>
                            </div>
                        </div>
                        <div class="profile-info-row center">
                            <button type="submit" class="btn btn-primary">确认修改</button>
                        </div>
                    </form>
                </div>
            </div>
            <!--修改手机号-->
            <div class="user-profile row tab-pane" id="edit_telephone">
                <div class="col-xs-12 col-sm-12">
                    <form id="edit-telephone-form">
                        <div class="profile-user-info profile-user-info-striped">

                            <div class="profile-info-row">
                                <div class="profile-info-name">用户id</div>
                                <div class="profile-info-value">
                                    <span class="editable"><?PHP echo $user['cms_id']?></span>
                                    <input type="text"  hidden name="user_id" id="user_id" value="<?PHP echo $user['cms_id']?>">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">旧手机号</div>
                                <div class="profile-info-value">
                                    <span class="editable"><?PHP echo $user['cms_telephone']?></span>
                                    <input type="tel" hidden name="old_telephone" id="old_telephone" value="<?PHP echo $user['cms_telephone']?>">
                                </div>
                            </div>

                            <div class="profile-info-row">
                                <div class="profile-info-name">输入验证码</div>

                                <div class="profile-info-value">
                                    <input type="text" class="editable" id="num" name="num">
                                    <button type="button" onclick="send_smsg('num1')" id="num1" class="btn btn-info" style="padding: 1px!important;margin: 0px!important;">获取验证码</button>
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">新手机号</div>

                                <div class="profile-info-value">
                                    <input type="tel" name="new_telephone" id="new_telephone" value="">
                                </div>
                            </div>
                            <div class="profile-info-row">
                                <div class="profile-info-name">输入验证码</div>

                                <div class="profile-info-value">
                                    <input type="text" class="editable" id="re_num" name="re_num">
                                    <button type="button" onclick="send_smsg('num2')" id="num2" class="btn btn-info" style="padding: 1px!important;margin: 0px!important;" >获取验证码</button>
                                </div>
                            </div>
                        </div>
                        <div class="profile-info-row center">
                            <button type="submit" class="btn btn-primary">确认修改</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div>
</div>

<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/common.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.form_date').datetimepicker({
        language:  'zh-CN',
        weekStart: 0, //一周从哪一天开始
        todayBtn:  1, //
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0,
        showMeridian: 1
    });


    var times = 60;
    function roof_one(){
        if(times == 0){
            $('#num1').text('发送验证码('+times+'s)');
            $('#num1').prop('disabled',false);
            $('#num1').text('发送验证码');
            times = 60;
            return
        }
        $('#num1').text('发送验证码('+times+'s)');
        times--;
        setTimeout(roof_one,1000);
    }
    function roof_two(){
        if(times == 0){
            $('#num2').text('发送验证码('+times+'s)');
            $('#num2').prop('disabled',false);
            $('#num2').text('发送验证码');
            times = 60;
            return
        }
        $('#num2').text('发送验证码('+times+'s)');
        times--;
        setTimeout(roof_two,1000);
    }

    //发送短信
    function send_smsg(btn_id)
    {
        $('#'+btn_id).prop('disabled',true);

        if (btn_id == 'num1')
        {
            var telephone = $('#old_telephone').val();
            roof_one();
        }
        else
        {
            var telephone = $('#new_telephone').val();
            roof_two();
        }
        var key = '04997110aa2db7e27991ece0749064f4';
        var timestamp=new Date().getTime();
        var sign = hex_md5(telephone+timestamp+key);
        var submitData = "cms_mobile_code=" + telephone + "&sign=" + sign + "&cms_time=" + timestamp;
        $.ajax({
            url:'../../../../backstage/system/auto/c_smsg/send_msg',
            type:"POST",
            data:submitData,
            cache:false,//false是不缓存，true为缓存
            async:true,//true为异步，false为同步
            success:function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0)
                {
                    swal(
                        {
                            title:'发送失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            showConfirmButton:false,
                            timer:1500
                        },
                    );
                }
                else
                {
                    swal(
                        {
                            title:'发送成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            showConfirmButton:false,
                            timer:1500
                        }
                    );
                }
            }
        });
    }

    function data_link_field()
    {
        $('select[class="selectpicker"]').each(function(){
            var arr1 = $(this).attr('data-link-field');
            if(typeof(arr1) !== 'undefined')
            {
                $('#' + arr1).val($('#select_' + arr1).val());
            }
        });
    }
</script>
</body>
</html>