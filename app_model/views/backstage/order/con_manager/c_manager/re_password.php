<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/15 13:40
 */
if(!defined('VIEW_MODEL_BACKGROUD'))
{
    define('VIEW_MODEL_BACKGROUD', '/CodeIgniter/view_model/backstage/');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/sys_login.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>
<style>
    input{
        padding-right: 2px!important;
    }
</style>
<body>
<div class="container-fluid" id="login-main">
    <div class="container">
        <div id="mtem">
            <div class="col-sm-9 col-xs-6">
                <div class="logoDiv">
                    <div class="logoImg logo">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/logo_v2.png" alt="">
                    </div>
                    <div class="logoImg img1">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit1.png" alt="">
                    </div>
                    <div class="logoImg img2">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit2.png" alt="">
                    </div>
                    <div class="logoImg img3">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12 " id="form_re">
                <form id="defaultForm">
                    <div style="padding:20px 20px;">
                        <P class="form-title">找回密码</P>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                            <input type="tel" name="telephone" class="form-control " placeholder="手机号">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <input type="text" id="num" name="num" class="form-control shoujihao" placeholder="验证码" style="width:38%;">
                            <button type="button" id="verification_code" class="btn btn-primary btn-gain-num fr" >发送验证码</button>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="password" id="password" name="password" class="form-control sheng" placeholder="新密码">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="password" id="confirmPassword" name="confirmPassword" class="form-control sheng" placeholder="再次输入密码">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <button class="login-btn" id="system-button-submit-edit-ajax" type="button" style="padding:5px;width: 50%!important;">确认找回密码</button>
                        </div>
                        <span class="noid">没账号? <a href="register" class="agree">  点击注册</a></span>
                        <span class="noid">密码没忘记? <a href="login" class="registerCli"> 直接登录</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- //lined-icons -->
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/md5.js"></script>
<!-- Sweet Alert -->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function(){/* 文档加载，执行一个函数*/
        $('#defaultForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {/*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'

            },
            fields: {/*验证：规则*/
                password: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '用户名长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                confirmPassword: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '用户名长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                telephone: {
                    message: 'The phone is not valid',
                    validators: {
                        notEmpty: {
                            message: '手机号码不能为空'
                        },
                        regexp: {
                            min: 11,
                            max: 11,
                            regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                            message: '请输入正确的手机号码'
                        }
                    }
                },
                num: {
                    message:'验证码无效',
                    validators: {
                        notEmpty: {
                            message: '验证码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 6,
                            message: '验证码填写错误'
                        },
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: '用户名只能由数字组成'
                        }
                    }
                },
            }
        }).on('success.form.bv',function(e){
            e.preventDefault();
            var url = 're_password';
            var md5_password =  hex_md5($('#password').val());
            var md5_confirmPassword =  hex_md5($('#confirmPassword').val());
            $('#password').val(md5_password);
            $('#confirmPassword').val(md5_confirmPassword);
            var submitData = $('#defaultForm').serialize() + "&flag_ajax_reurn=1";
            //submitData是解码后的表单数据，结果同上
            $.post(url, submitData, function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0)
                {
                    swal(
                        {
                            title:'重置密码失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },
                        function()
                        {
                            $('#password').val("");
                            $('#confirmPassword').val("");
                            location.reload();
                        }
                    );
                }
                else
                {
                    swal(
                        {
                            title:'重置密码成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },function(){
                            window.location.href='login';
                        }
                    );
                }
            });
        });
    });

    var times = 60;
    function roof(){
        if(times == 0){
            $('.btn-gain-num').text('发送验证码('+times+'s)');
            $('.btn-gain-num').prop('disabled',false);
            $('.btn-gain-num').text('发送验证码');
            times = 10;
            return
        }
        $('.btn-gain-num').text('发送验证码('+times+'s)');
        times--;
        setTimeout(roof,1000);
    }
    $('#verification_code').on('click',function(){
        var telephone = $('#telephone').val();
        $(this).prop('disabled',true);
        roof();
        var key = '04997110aa2db7e27991ece0749064f4';
        var timestamp=new Date().getTime();
        var sign = hex_md5(telephone+timestamp+key);
        var submitData = "cms_mobile_code=" + telephone + "&sign=" + sign + "&cms_time=" + timestamp;
        $.ajax({
            url:'../../../../backstage/system/auto/c_smsg/send_msg',
            type:"POST",
            data:submitData,
            cache:false,//false是不缓存，true为缓存
            async:true,//true为异步，false为同步
            success:function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0)
                {
                    swal(
                        {
                            title:'发送失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            showConfirmButton:false,
                            timer:1500
                        },
                    );
                }
                else
                {
                    swal(
                        {
                            title:'发送成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            showConfirmButton:false,
                            timer:1500
                        }
                    );
                }
            }
        });
    });
</script>

</body>

</html>