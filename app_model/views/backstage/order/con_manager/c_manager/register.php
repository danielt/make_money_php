<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/15 13:40
 */
if(!defined('VIEW_MODEL_BACKGROUD'))
{
    define('VIEW_MODEL_BACKGROUD', '/CodeIgniter/view_model/backstage/');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>注册</title>
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/sys_login.css" rel="stylesheet">
    <!-- Sweet 弹窗 -->
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!--地区选择样式-->
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/city-picker.css" rel="stylesheet">
</head>
<style>
    #step1{
        display: block;
    }
    #step2{
        display: none;
    }
    .label-select{
        width: 25%;
        float: right;
        font-size: 13px;
        right: 10%;
        color: #858586;
        background-color: #f0f0f0;
        border: 1px solid #ccc;
        border-radius: 0 4px 4px 0;
        box-shadow: none;
        padding: 6.7px 12px;
        text-transform: capitalize;
        transition: all 0.5s ease 0s;
    }
    input{
        padding-right: 2px!important;
    }
</style>
<body>
<div class="container-fluid" id="login-main">
    <div class="container">
        <div id="mtem">
            <div class="col-sm-9 col-xs-6">
                <div class="logoDiv">
                    <div class="logoImg logo">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/logo_v2.png" alt="">
                    </div>
                    <div class="logoImg img1">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit1.png" alt="">
                    </div>
                    <div class="logoImg img2">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit2.png" alt="">
                    </div>
                    <div class="logoImg img3">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12 " id="form_re" style="top:8%;">
                <form id="defaultForm" class="form-signin wow bounceInLeft er">
                    <div id="step1" style="padding:0 20px;">
                        <P class="form-title">用户注册</P>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="text" id="username" name="username" class="form-control name"
                                       placeholder="姓名">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                            <div class="radio">
                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="1" checked> 未知
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="2"> 男
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="sex" value="3"> 女
                                </label>
                            </div>

                            </p>
                        </div>

                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="tel" id="telephone" name="telephone" class="form-control shoujihao"
                                       placeholder="手机号">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="text" id="num" name="num" class="form-control shoujihao" placeholder="验证码"
                                       style="width:38%;">
                                <button type="button" id="verification_code" class="btn btn-primary btn-gain-num fr">发送验证码</button>
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="password" id="password" name="password" class="form-control sheng"
                                       placeholder="密码">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="password" id="confirmPassword" name="confirmPassword" class="form-control sheng" placeholder="再次输入密码">
                            </p>
                        </div>
                        <div class="login-wrap form-group">
                            <button class="login-btn nextStep" id="nextBtn"  type="button">下一步</button>
                            <span class="noid">有账号？<a href="login" class="registerCli">立即登录</a></span>
                        </div>
                    </div>

                    <div id="step2" style="padding:0 20px;">
                        <P class="form-title">用户注册</P>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input type="text" id="company_name" name="company_name" class="form-control company"
                                       placeholder="企业名称（允许个人）">
                            </p>
                        </div>

                        <div class="login-wrap form-group">
                            <select class="form-control" name="role_id" id="role_id">
                                <optgroup>
                                    <option value='' disabled selected style='display:none;'>请选择角色</option>
                                    <option value="1">订货商</option>
                                    <option value="3">生产商</option>
                                    <option value="4">供应商</option>
                                    <option value="5">样板师</option>
                                    <option value="6">样衣师</option>
                                </optgroup>

                            </select>
                        </div>
                        <div class="login-wrap form-group">
                            <select class="form-control" name="country" id="country">
                                <optgroup>
                                    <option value='' disabled selected style='display:none;'>国家</option>
                                    <option value="中国">中国</option>
                                </optgroup>
                            </select>
                        </div>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                                <input id="city-picker3" class="form-control form-control-select" readonly type="text" value="" name="city-picker3" data-toggle="city-picker">
                                <button type="button" class="label-select" id="area-reset">重置</button>
                            </p>
                        </div>
                        <label class="checkbox" style="margin-left: 20px;margin-top: 60px!important;">
                            <input type="checkbox" class="yue" name="yue" value="1"> 我已阅读
                            <a style="color: #0a6ea7;font-size: 0.8em;" class="xy-model" data-toggle="modal"
                               data-target="#adminModal"> 《用户协议和隐私政策》
                            </a>
                        </label>
                        <div class="login-wrap form-group" style="text-align: center;">
                            <button class="login-btn previousStep" type="button " style="margin-right:50px;">上一步</button>
                            <button class="login-btn " type="submit" id="system-button-submit-edit-ajax" style="width: 85px;">注册</button>
                        </div>
                        <span class="forget">有账号? <a href="login" class="agree">立即登录</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<div class="modal fade modalIndex" id="adminModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">用户协议</h4>
            </div>
            <div class="modal-body">
                有关使用者的个人化信息 一般情况下，我们将收集使用者的个人基本信息（姓名、性别、出生年月日、手机号码、个人邮箱、地区等)。在获得使用者的同意后，unioncloth.com（包含我们及我们的关联方或委托的第三方，以下简称“我们”）可能利用这些资料向使用者发送相关职位信息或提供我们认为使用者可能感兴趣的相关产品和服务信息、进行风险提示、unioncloth.com网站发生改变时通知使用者或和使用者联系。
                在某些情况下，如果使用者参加unioncloth.com上举办的专题活动、参加unioncloth.com的互动专区或要购买某一种在unioncloth.com上公开的产品和服务，可能需要使用者提供个性信息及照片等，上述资料将用于引导客户提供更高质量的内容以及更好的参与上述活动或享受相关服务。
                我们也可能向使用者发起问卷调研，例如使用者感兴趣的资料、喜欢的网站或不喜欢的网站、感兴趣的职业等等，其目的均是向使用者提供更好的服务和网络使用环境。 在使用zhaopin.com提供的服务时，请使用者不要主动向unioncloth.com提供除接受服务外需要提供的其他个人敏感信息（如种族、宗教、政治立场等），该等信息有被unioncloth.com意外收集的可能性。
                在收集您的个人化信息后，我们将通过技术手段对数据进行去标识化处理，去标识化处理的信息将无法识别主体。我们有权使用已经去标识化的信息；并在不透露您个人信息的前提下，对用户数据库进行分析并予以商业化的利用。用户在使用我们的产品与/或服务时所提供的所有个人信息，除非您删除或通过系统设置拒绝我们收集，否则将在您使用我们的产品与/或服务期间持续授权我们使用。在您注销账号后，我们将按照法律、法规的要求处理您的个人信息。
                公开一般情况的资料 除"有关使用者的非个人化信息"所述情况外，我们不会公开使用者个人浏览unioncloth.com的情况，以及使用者向我们提供的个人资料，如姓名、地址、电子邮件地址、电话号码、信用卡号码、照片等等个人信息资料，除非（1）事先获得使用者的同意或授权；（2）依照法律、法规、法院命令、监管机构命令的要求，或根据政府行为、监管要求或请求，或因Zhaopin.com其认为系为遵守监管义务所需时；（3）为免除使用者在生命、身体或财产方面的紧急危险；（4）只有共享使用者的信息，才能提供使用者需要的服务（包括但不限于关联方及业务合作伙伴提供的产品），或处理使用者与他人的纠纷或争议；（5）符合与用户签署的相关协议（包括在线签署的电子协议以及相应的平台规则）或其他的法律文件约定所提供；（6）为完成合并、分立、收购或资产转让；（7）维护智联招聘、智联招聘的关联方或合作伙伴的合法权益；（8）基于符合法律法规的社会公共利益而使用。
                如果使用者把个人资料输入我们和我们的业务合作伙伴们共同策划的网页上，例如unioncloth.com和伙伴公司共同策划的活动页面，那么使用者的资料将属于zhaopin.com和业务合作伙伴公司。zhaopin.com对业务合作伙伴公司获得该个人信息及之后个人信息的使用不承担任何法律责任。
                智联招聘可能会将您的个人信息与我们的关联方共享。但我们只会共享必要的个人信息，且受本隐私政策中所声明目的的约束。 使用者请谨慎考虑通过我们的服务上传、发布和交流的信息内容。在一些情况下，使用者可通过我们某些服务的隐私设定来控制有权浏览共享信息的用户范围。
                简历 由于unioncloth.com是一个招聘网站，您可以进行网站注册并将您的简历放入我们的数据库。向unioncloth.com购买了相应服务的招聘企业或人员可以通过zhaopin.com的简历数据库找到您的简历。当您向zhaopin.com递交您的简历，并选择公开您的简历选项时，即视为您已经同意向购买了unioncloth.com相应服务的招聘企业或人员提供上述简历并允许其浏览及为招聘目的合理使用您简历的权利。对于因此而引起的任何法律纠纷（包括但不限于招聘企业或人员错误或非法使用前述简历信息），zhaopin.com不承担任何法律责任。
                unioncloth.com已尽商业合理努力设置了安全防范措施，以下情况仍然有可能发生，包括但不限于 （1）某一第三方躲过了我们的安全措施并进入我们的数据库，查找到您的简历；（2）因不可抗力导致网络系统的瘫痪导致服务中断；（3）因网站和服务器收并额度或其他第三方侵害导致您个人信息泄露、损毁和丢失等。
                unioncloth.com认为在您把您的简历放入我们的数据库时，您已经意识到了这种风险的存在，并同意承担这样的风险。对于因此而引起的任何法律纠纷，zhaopin.com不承担任何法律责任。
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">确认</button>
            </div>
        </div>
    </div>
</div>
</body>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/md5.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
<!--地区选择-->
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/city-picker.data.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/city-picker.js"></script>
<script>
    $(document).ready(function () {
        $('.nextStep').click(function (e) {
            $('#step2').show();
            $('#step1').hide();
        })
        $('.previousStep').click(function (e) {
            $('#step1').show();
            $('#step2').hide();
        })
    });

    $(function(){/* 文档加载，执行一个函数*/
        $('#defaultForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {/*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'

            },
            fields: {/*验证：规则*/
                username: {
                    message:'名称无效',
                    validators: {
                        notEmpty: {
                            message: '名称不能为空'
                        },
                        stringLength: {
                            min: 1,
                            max: 30,
                            message: '密码长度必须在1到30之间'
                        }
                    }
                },
                password: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '密码长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                confirmPassword: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '密码长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                telephone: {
                    message: 'The phone is not valid',
                    validators: {
                        notEmpty: {
                            message: '手机号码不能为空'
                        },
                        regexp: {
                            min: 11,
                            max: 11,
                            regexp: /^1\d{10}$/,
                            message: '请输入正确的手机号码'
                        }
                    }
                },
                num: {
                    message:'验证码无效',
                    validators: {
                        notEmpty: {
                            message: '验证码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 6,
                            message: '验证码填写错误'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
            }
        }).on('success.form.bv',function(e){
            e.preventDefault();
            $('#city-picker3').citypicker('destroy');
            var url = 'registry';
            var md5_password =  hex_md5($('#password').val());
            var md5_confirmPassword =  hex_md5($('#confirmPassword').val());
            $('#password').val(md5_password);
            $('#confirmPassword').val(md5_confirmPassword);
            var submitData = $('#defaultForm').serialize() + "&flag_ajax_reurn=1";
            //submitData是解码后的表单数据，结果同上
            $.post(url, submitData, function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0)
                {
                    swal(
                        {
                            title:'注册失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },
                        function()
                        {
                            $('#password').val("");
                            $('#confirmPassword').val("");
                            location.reload();
                        }
                    );
                }
                else
                {
                    swal(
                        {
                            title:'注册成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },function(){
                            window.location.href='login';
                        }
                    );
                }
            });
        });
    });

    var times = 60;
    function roof(){
        if(times == 0){
            $('#verification_code').text('发送验证码('+times+'s)');
            $('#verification_code').prop('disabled',false);
            $('#verification_code').text('发送验证码');
            times = 60;
            return
        }
        $('#verification_code').text('发送验证码('+times+'s)');
        times--;
        setTimeout(roof,1000);
    }
    //发送短信
    $('#verification_code').on('click',function(){
        var telephone = $('#telephone').val();
        $(this).prop('disabled',true);
        roof();
        var key = '04997110aa2db7e27991ece0749064f4';
        var timestamp=new Date().getTime();
        var sign = hex_md5(telephone+timestamp+key);
        var submitData = "cms_mobile_code=" + telephone + "&sign=" + sign + "&cms_time=" + timestamp;
        $.ajax({
            url:'../../../../backstage/system/auto/c_smsg/send_msg',
            type:"POST",
            data:submitData,
            cache:false,//false是不缓存，true为缓存
            async:true,//true为异步，false为同步
            success:function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0)
                {
                    swal(
                        {
                            title:'发送失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            showConfirmButton:false,
                            timer:2000
                        },
                    );
                }
                else
                {
                    swal(
                        {
                            title:'发送成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            showConfirmButton:false,
                            timer:2000
                        }
                    );
                }

            }
        });
    });

    //重置地区选择
    $('#area-reset').click(function () {
        $('#city-picker3').citypicker('reset');
    });

</script>

</html>
