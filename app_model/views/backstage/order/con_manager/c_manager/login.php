<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/15 13:40
 */
if(!defined('VIEW_MODEL_BACKGROUD'))
{
    define('VIEW_MODEL_BACKGROUD', '/CodeIgniter/view_model/backstage/');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/sys_login.css" rel="stylesheet">
    <!-- Sweet Alert -->
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid" id="login-main">
    <div class="container">
        <div id="mtem">
            <div class="col-sm-9 col-xs-6">
                <div class="logoDiv">
                    <div class="logoImg logo">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/logo_v2.png" alt="">
                    </div>
                    <div class="logoImg img1">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit1.png" alt="">
                    </div>
                    <div class="logoImg img2">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit2.png" alt="">
                    </div>
                    <div class="logoImg img3">
                        <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>images/tit3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-3 col-xs-12 " id="form_re">
                <form id="defaultForm">
                    <div style="padding:20px 20px;">
                        <P class="form-title">云裳供应链平台</P>
                        <div class="login-wrap form-group">
                            <p style="text-align: left">
                            <input type="tel" class="form-control" id="telephone" name="telephone" placeholder="手机号">
                            </p>
                        </div>
                        <div class="login-wrap  form-group">
                            <p style="text-align: left">
                                <input type="password" class="form-control" id="password" name="password" placeholder="密码">
                            </p>
                        </div>
                        <div class="login-wrap"> <label class="checkbox">
                                <button class="login-btn" type="submit">登 录</button>
                        </div>
                        <span class="noid">忘记密码？<a href="re_password" class="agree">找回密码</a></span>
                        <span class="noid">没账号？<a href="register" class="registerCli">点击注册</a></span>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- //lined-icons -->
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-1.10.2.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/md5.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $(function(){/* 文档加载，执行一个函数*/
        $('#defaultForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {/*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {/*验证：规则*/
                password: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '用户名长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        different: {//不能和用户名相同
                            field: 'username',//需要进行比较的input name值
                            message: '不能和用户名相同'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                telephone: {
                    message: 'The phone is not valid',
                    validators: {
                        notEmpty: {
                            message: '手机号码不能为空'
                        },
                        regexp: {
                            min: 11,
                            max: 11,
                            regexp: /^1\d{10}$/,
                            message: '请输入正确的手机号码'
                        }
                    }
                }
            }
        }).on('success.form.bv',function(e){
            e.preventDefault();
            var url = 'sigin';
            var md5_password =  hex_md5($('#password').val());
            $('#password').val(md5_password);
            var submitData = $('#defaultForm').serialize() + "&flag_ajax_reurn=1";
            //submitData是解码后的表单数据，结果同上
            $.post(url, submitData, function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0)
                {
                    swal(
                        {
                            title:'登录失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },
                    );
                    $('#password').val("");
                }
                else
                {
                    swal(
                        {
                            title:'登录成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            showConfirmButton:false,
                            closeOnConfirm:false,
                            timer:1500
                        },function(){
                            window.location.href='../../../../backstage/order/index/c_index/index';
                        }
                    );
                }
            });
        });
    });

</script>

</body>

</html>
