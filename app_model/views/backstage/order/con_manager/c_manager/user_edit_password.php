<script type="text/javascript">
    $(document).ready(function() {
        $('#<?php echo $system_file_list_value['class'];?>-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                re_password: {
                    message:'密码无效',
                    validators: {
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '密码长度必须在6到30之间'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '密码只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                re_confirmPassword: {
                    message:'密码无效',
                    validators: {
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '密码长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 're_password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '密码名只能由字母、数字、点和下划线组成'
                        }
                    }
                }
            }
        });
    });
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){if(!isset($button_data_value['button_display']) || !$button_data_value['button_display']){?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }}?>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">修改密码</h2>
            </div>
            <div class="modal-body">
                <form id="<?php echo $system_file_list_value['class'];?>-form" method="post" action="" >
                    <input id="cms_id" name="cms_id" class="form-control1 icon" type="hidden" value="">
                    <div class="form-group">
                        <label class="col-md-2 control-label">新密码</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-lock"></i>
                				</span> <input id="re_password" name="re_password" class="form-control1 icon" type="password" placeholder="密码">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">确认密码</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-lock"></i>
                				</span> <input id="re_confirmPassword" name="re_confirmPassword" class="form-control1 icon" type="password" placeholder="请再次输入密码">
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        function md5_password_third()
                        {
                            var md5_password = hex_md5($('#re_password').val());
                            $('#re_password').val(md5_password);
                            var md5_confirmPassword = hex_md5($('#re_confirmPassword').val());
                            $('#re_confirmPassword').val(md5_confirmPassword);
                            sys_sweetalert('submit','<?php echo $system_file_list_value['class'];?>','您确定要修改这条信息吗','提交数据后系统将可能会添加此条数据，请谨慎操作！','<?php echo $system_file_list_value['ajax'];?>','',true);
                        }
                    </script>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="md5_password_third()">
                        	   <i class="fa fa-book"> 提交 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>