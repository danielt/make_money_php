<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>云裳供应链</title>
    </head>
    <body>

        <!-- 引入head -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_head_modal.php'; ?>

        <!-- banner滚动 S -->
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/s1.jpg" alt="" />
                </li>
                <li>
                    <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/s2.jpg" alt="" />
                </li>
                <li>
                    <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/s3.jpg" alt="" />
                </li>
                <li>
                    <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/s4.jpg" alt="" />
                </li>
            </ul>
        </div>
        <!-- banner滚动 E -->
        <div class="top_banner cl"> 
            <div class="top_left ddzc_side pr fl"> 
                <ul id="labelUL"> 
                </ul>
            </div> 
            <div class="top_center fr"> 
                <p class="home-product-title">产品推荐<a class="fr" data-url="../../../order/con_client/c_client_order/add_order" onclick="handleLogin(this)" style="display: none;">查看更多&gt;</a></p>
                <ul id="productUL" class="home-product-list cl">
                    
                </ul>
            </div>

        </div> 
        
        <!-- 产品分类模板 -->
        <script type="text/template" id="labelTemp">
            {{each list}}
            <li class="pr"> 
                <a href="javascript:;" target="_blank" class="side1 li_a" data-id="${category_id}">${category_name}</a> 
                <!-- <div class="ddzc_sub1 pa"> 
                    <dl class="cl pr"> 
                        <dt>
                            梭织服装
                        </dt> 
                        <dd>
                            <a href="" target="_blank">普通梭织薄料服装</a>
                            <em>|</em>
                        </dd> 
                        <dd>
                            <a href="" target="_blank">普通梭织厚料服装</a>
                            <em>|</em>
                        </dd>
                    </dl>
                </div>  -->
            </li>
            {{/each}}
        </script>
        <!-- 产品模板 -->
        <script type="text/template" id="productTemp">
            {{each list}}
                <li data-id="${cms_type_name}">
                    <img src="/data_model/upload/${cms_src}" alt="${cms_type_name}" />
                    <p class="product-name">${cms_type_name}</p>
                </li>
            {{/each}}
        </script>

        <!-- 引入footer -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_footer_modal.php'; ?>

        <script type="text/javascript">
            $(function() {
                
                // banner
                $(".flexslider").flexslider({
                    slideshowSpeed: 4000, //展示时间间隔ms
                    animationSpeed: 400, //滚动时间ms
                    directionNav: false //是否支持触屏滑动
                });
                console.log()
                getLabel();
                getProduct();

                // 获取分类
                function getLabel() {
                    var url = '../../../order/con_client/c_client_order/client_basic_category?flag_ajax_reurn=1';
                    var params = {};
                    $.get(url, params, function(result){
                        var dataObj=eval("("+result+")");
                        $('#labelUL').html($('#labelTemp').tmpl({list: dataObj.data_info.first}))
                    });
                }
                // 获取产品
                function getProduct() {
                    var url = '../../../order/con_client/c_client_order/client_basic_type?flag_ajax_reurn=1';
                    var params = {};
                    $.get(url, params, function(result){
                        var dataObj=eval("("+result+")");
                        $('#productUL').html($('#productTemp').tmpl({list: dataObj.data_info}))
                    });
                }


            });	
        </script>

<!--        --><?php //include_once dirname(dirname(dirname(__DIR__))) . '/pay/pay_order/c_pay_order/c_shopping_cart.php'; ?>
<!--        <button onclick="obj_shopping_cart.init(1,0,0)">支付</button>-->
    </body>
</html>