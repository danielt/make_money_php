<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>创建订单</title>
    <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/font-awesome.css" />
    <link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/index.css"/>
    <script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/pictureScroll.js"></script>
    <script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/fabric.js"></script>
    <style type="text/css">
        .btn{ margin: 0; }
        *{ box-sizing: border-box!important; }
        .order-next-two{float: right;width: 120px;}
        .order-next-two a {
            display: inline-block;
            width: 100%;
            height: 32px;
            line-height: 32px;
            color: #fff;
            text-align: center;
            background-color: #337ab7;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
</head>
<body>

<!-- 引入head -->
<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_head_modal.php'; ?>  

<div class="outter-wp zoe-wrap">
<!--    <div class="order-item">-->
<!--        <label>所选类目</label>-->
<!--        <div class="order-item-all" id="orderAll">请选择</div>-->
<!--    </div>-->
    <div class="filter-tabs">
        <ul class="filter-tabs-title">
        </ul>
        <div class="filter-tabs-content order-picture">
            <!-- 图片滚动 S -->
            <div id="wrapper">
                <div id="carousel">
                    <ul>

                    </ul>
                    <div class="clearfix"></div>
                    <a id="prev" class="prev" href="#">&lt;</a>
                    <a id="next" class="next" href="#">&gt;</a>
                    <div id="pager" class="pager"></div>
                </div>
            </div>
            <!-- 图片滚动 E -->
        </div>
    </div>
    <!-- 图片拼接 S -->
    <div class="clearfix" style="position: relative;">
        <p class="canvas-title">请选择图片点击加入画布开启拼图</p>
        <div class="canvas">
            <canvas id="canvas"></canvas>
        </div>
        <a class="create" id="create">生成</a>
        <p class="canvas-title">生成完整拼图</p>
        <div class="show-img">

        </div>
    </div>

    <!-- 图片拼接 E -->
    <div class="cl mt-20">
        <div class="order-next"><a>确认购买</a></div>
    </div>

</div>
<!-- 订单支付弹窗 -->
<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/backstage/pay/pay_order/c_pay_order/c_shopping_cart.php'; ?>


<!-- 引入footer -->
<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_footer_modal.php'; ?>

<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/pictureScroll.js"></script>

<script>
        
    var pic = '';
    $(function () {
        //获取服装部件类型数据
        $.post(
            "../../../order/clothing_parts/c_clothing/parts_type?flag_ajax_reurn=1",
            {
                cms_clothing_id : "<?php echo (isset($cms_clothing_id) && strlen($cms_clothing_id) > 0) ? $cms_clothing_id : '';?>",
            },
            function (result) {
                var data = eval("("+result+")");
                var li_html = '';
                if(data.ret == 0)
                {
                    $.each(data.data_info,function(index,item){
                        if(index == 0)
                        {
                            li_html += "<li class='active' data-id='"+item.cms_id+"'>"+item.cms_name+"</li>";
                            clothing_parts_list_by_type(item.cms_id);
                        }
                        else
                        {
                            li_html += "<li data-id='"+item.cms_id+"'>"+item.cms_name+"</li>";
                        }
                    })
                    $('.filter-tabs-title').html(li_html);

                    // tab切换事件
                    $('.filter-tabs-title li').on('click', function() {
                        var _this = $(this);
                        var index = _this.index();
                        _this.addClass('active').siblings().removeClass('active');
                        clothing_parts_list_by_type(_this.data('id'));
                    })
                }
            }
        )



        // 图片拼接
        var canvas = new fabric.Canvas('canvas', {
            width: 1086,
            height: 300,
            selectionColor: 'blue',
            selectionLineWidth: 2
        });
        $('#create').on('click', function () {
            var dataUrl = canvas.toDataURL("image/png");
            $.post(
                "<?php echo VIEW_MODEL_BASE;?>/app_model/libraries/em_upload.class.php",
                {
                    system_func: 'file_base64',
                    input_image_key: 'data_img',
                    data_img: dataUrl
                },
                function (result) {
                    var data = eval("("+result+")").data_info.base_pth;
                    pic = data;
                 $('<img src="<?php echo VIEW_MODEL_BASE;?>/data_model/upload/'+ data +'" />').appendTo('.show-img');
            })
        })
        canvas.on('mouse:up', function(options){

        });
        // 双击删除
        canvas.on('mouse:dblclick', (e) => {
            canvas.remove(
                canvas.item(
                    canvas.getObjects().indexOf(e.target)
            )
          );
          $('.show-img').html('');
        })

        
        $('.filter-tabs-content').on('click', '#carousel li', function () {
            var url = $(this).find('img').attr('src');
            var img = fabric.Image.fromURL(url, function(oImg) {
                canvas.add(oImg).setActiveObject(oImg);
            }, {crossOrigin: 'anonymous'});
        })
    })
    // 确认购买
    $('.order-next').on('click', function () {
        var checkId = sessionStorage.getItem('checkFabirc') || [];
        var processType = sessionStorage.getItem('processType');
        var processVal = sessionStorage.getItem('processVal') || [];
        var firstId = GetQueryString('cms_first_type_id');
        var basicId = GetQueryString('cms_type_id');
        var newArr = checkId ? checkId.split(',') : [];
        var processValArr = processVal ? processVal.split(',') : [];
        var processArr = [processType,processValArr];
        $.post(
            "../../../order/con_client/c_client_order/generate_order?flag_ajax_reurn=1",
            {
                cms_style: pic,
                cms_first_type_id: firstId,
                cms_basic_type_id: basicId,
                cms_process: processArr,
                cms_material_list: newArr
            },
            function (result) {
                var data = eval("("+result+")");
                $('.order-next > a').html('前往个人中心');
                $('.order-next > a').attr('href','../../../order/index/c_index/index');
                $('.order-next').unbind('click');
                obj_shopping_cart.init(data.data_info.cms_order_id,0);
            }
        )
    })
    //获取部件
    function clothing_parts_list_by_type(parts_type_id)
    {
        $('#carousel ul').html('');
        $.post(
            "../../../order/clothing_parts/c_clothing/parts_list?flag_ajax_reurn=1",
            {
                cms_parts_type_id : parts_type_id,
            },
            function (result) {
                var data = eval("("+result+")");
                var li_html = '';
                if(data.ret == 0)
                {
                    $.each(data.data_info,function(index,item){
                        li_html += '<li><img src="<?php echo VIEW_MODEL_BASE;?>/data_model/upload/'+item.cms_image+'" alt="" /></li>';
                    })
                    $('#carousel ul').html(li_html);
                    // 图片滚动
                    $('#carousel ul').carouFredSel({
                        prev: '#prev',
                        next: '#next',
                        pagination: "#pager",
                        scroll: 1000
                    });
                }
            }
        )
    }
    
    // 获取参数蓝参数
    function GetQueryString(name)
    {
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if(r!=null)return  unescape(r[2]); return null;
    }
</script>
</body>
</html>