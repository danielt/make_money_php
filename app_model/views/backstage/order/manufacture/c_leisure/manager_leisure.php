<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_top_web_file.php'; ?>
</head>
<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li class="active">空期列表</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="<?php echo $arr_page_url['list_url']; ?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">名称:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_name" id="cms_name" value="<?php echo isset($arr_params['cms_name']) ? $arr_params['cms_name'] : '';?>">
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">人数:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_person_amount" id="cms_person_amount" value="<?php echo isset($arr_params['cms_person_amount']) ? $arr_params['cms_person_amount'] : '';?>">
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">空期</label>
                    <div class="col-sm-3">
                        <div class="input-group date form_datetime" style="width: 100%!important;padding-bottom: 0px!important;" data-date="" data-date-format="yyyy-mm-dd hh:ii:ss" data-link-field="cms_free_time" data-link-format="yyyy-mm-dd hh:ii:ss">
                            <input class="form-control" style="background-color: white!important;" type="text" value="<?php echo isset($arr_params['cms_free_time']) ? $arr_params['cms_free_time'] : '';?>" readonly/>
                            <span class="input-group-addon user-list-span"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon user-list-span"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <input type="text" hidden class="form-control1" name="cms_free_time" id="cms_free_time" value="<?php echo isset($arr_params['cms_free_time']) ? $arr_params['cms_free_time'] : '';?>">
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list_search">
                            <i class="fa fa-search" >查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll"/></th>
                    <th>空期名称</th>
                    <th>空期时间</th>
                    <th>工厂人数</th>
                    <th>状态</th>
                    <th>创建时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php if (isset($data_info) && is_array($data_info) && !empty($data_info)): ?>
                    <?php foreach ($data_info as $val): ?>
                        <tr class='odd selected'>
                            <td><input type="checkbox" name="checkItem" attr-key="cms_id"
                                       value="<?php echo $val['cms_id']; ?>"/></td>
                            <?php if ($val['cms_name'] == $val['cms_mark']) { ?>
                                <td><font color='red'><?php echo $val['cms_name']; ?></font></td>
                            <?php } else { ?>
                                <td><?php echo $val['cms_name']; ?></td>
                            <?php } ?>
                            <td><?php echo date("Y-m-d",$val['cms_free_time']); ?></td>
                            <td><?php echo $val['cms_person_amount']; ?></td>
                            <td>
                                <?php echo $val['cms_status'] != '0' ? "<font color='red'>未完成</font>" : "<font color='green'>进行中</font>"; ?>
                            </td>
                            <td><?php echo $val['cms_create_time']; ?></td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown"
                                       aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!--*********** 初始化必须加载 ***************** （下拉操作信息） *********** 初始化必须加载 ***************** -->
                                        <?php echo make_right_button($system_file_list, $val); ?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_page.php'; ?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/public_bottom_button.php'; ?>
    </div>
</body>
</html>