<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>创建订单</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/font-awesome.css" />
        <link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/index.css"/>
        <script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-2.1.1.min.js"></script>
        <script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.tmpl.min.js"></script>
        <style>
            body{ background-color: #f0f3ef!important; }
            .btn{ margin: 0; }
            *{ box-sizing: border-box!important; }
        </style>
    </head>
    <body>
        <!-- 引入head -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_head_modal.php'; ?>  
        <div class="outter-wp zoe-wrap">
            <div class="order-item clearfix">
                <label>所选类目</label>
                <div class="order-item-all" id="orderAll">请选择</div>
                <div class="order-next" id="confirmDesign"><a href="../../../order/clothing_parts/c_clothing/clothing_parts?cms_first_type_id=<?php echo $arr_params['cms_first_type_id'];?>&cms_type_id=<?php echo $arr_params['cms_basic_id'];?>"><i class="fa fa-hand-o-right fa-fw"></i>&nbsp;款式确认</a></div>
            </div>
            <div class="filter-tabs">
                <ul class="filter-tabs-title">
                    <?php foreach ($fabirc_type as $i=>$type){?>
                    <li <?php if($i === 0){?>class="active"<?php }?> data-value="<?php echo $type['cms_id'];?>"><?php echo $type['cms_name']; ?></li>
                    <?php }?>
                </ul>
                <div class="filter-tabs-content">
                    <!-- 筛选条件 S -->
                    <div class="filter-box">
                        <div class="filter-item">
                            <div class="filter-item-label">分类：</div>
                            <div class="filter-item-list">
                                <ul id="filterUl">
<!--                                    <li><span>大身料</span></li>-->
<!--                                    <li><span>里料</span></li>-->
<!--                                    <li><span>拼接料</span></li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="filter-append"></div>
                    </div>
                    <!-- 筛选条件 E -->
                </div>
            </div>
            
            <!-- 产品展示 S -->
            <div class="product-box">
                <ul class="product-list">

                </ul>
            </div>
            <!-- 产品展示 E -->
            <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
            <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_index_page.php';?>

        </div>
        <!-- 引入footer -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_footer_modal.php'; ?>

        <script type="text/template" id="filterTemp">
            <div class="filter-item">
                <div class="filter-item-label">分类：</div>
                <div class="filter-item-more"></div>
                <div class="filter-item-list">
                    <ul>
                        {{each(i, data) list}}
                        <li>
                            <span>
                                ${data.value}
                                {{if data.childrend.length > 0}}
                                    <i class="glyphicon glyphicon-menu-down" aria-hidden="true"></i>
                                {{/if}}
                            </span>
                            {{if childrend.length > 0}}
                            <div class="filter-iten-son">
                                <i class="filter-iten-arrow"><i></i></i>
                                {{each data.childrend}}
                                    <span data-value="${id}" data-parent="${data.id}">${value}</span>
                                {{/each}}
                            </div>
                            {{/if}}
                        </li>
                        {{/each}}
                    </ul>
                </div>
            </div>
        </script>
        <!-- 产品 -->
        <script type="text/template" id="productTemp">
            {{each list}}
            <li data-id="${cms_id}">
                <span class="product-img">
                    <img src="<?php echo VIEW_MODEL_BASE;?>/data_model/upload/${cms_image}" alt="${cms_name}" />\
                    {{if cms_is_scarce != 0 }} 
                        <label class="product-scare">奇缺</label>
                    {{/if}}
                </span>
                <div class="product-title tx-l">${cms_name}<font color="red">${cms_price}</font>元/${cms_unit}</div>
                <p class="product-intro" title="材料描述">${cms_summary}</p>
            </li>
            {{/each}}
        </script>
        <script>
            var pageSize = 9, pageNum = 1,  idArr = [], processVal = [] , processType = 1;
            $(document).ready(function () {
                var type_id = $('.filter-tabs-title li.active').attr('data-value');
                attribute_by_type(type_id);

                processType = type_id;
                sessionStorage.setItem('processType', processType);
            })

            // tab切换事件
            $('.filter-tabs-title li').on('click', function() {
                var _this = $(this);
//                var index = _this.index();
                var type_id = _this.attr('data-value');
                _this.addClass('active').siblings().removeClass('active');
                $('.filter-append').html('');
                $('#orderAll').html('请选择');
                attribute_by_type(type_id);

                processType = type_id;
                sessionStorage.setItem('processType', processType);
            })

            // 动态追加分类
            $('.filter-item-list').on('click', 'li > span', function () {  
                var _this = $(this);
                var text = _this.text(),
//                value = _this.data('value');
                fabirc_id = _this.data('id');
                _this.parents('li').addClass('active').siblings().removeClass('active');
                $('.filter-append').html('');
                $.post(
                    "../../../order/fabirc/c_fabirc/fabirc_attribute_value?flag_ajax_reurn=1",
                    {
                        cms_fabirc_attribute : _this.attr('data-value')
                    },
                    function (result) {
                        var data = eval("("+result+")");
                        if(data.ret == 0)
                        {
                            _this.parents('.filter-item').siblings('.filter-append').html($('#filterTemp').tmpl({list: data.data_info}))
                        }
                    }
                );
                addChecked(fabirc_id, text, 'parent', '');
            });
            $('.filter-box').on('click', ' .filter-iten-son > span', function () { 
                var _this = $(this);
                var text = _this.text(),
                    value = _this.data('value'),
                    parentId = _this.data('parent');
                addChecked(value, text, 'son', parentId);
            })
            // 增加选中标签
            function addChecked(value, text, mark, parentId) {
                var _all = $('#orderAll');
                var itemLen = _all.find('.order-item-span'),
                    arr = [];
                if (itemLen.length > 0) {
                    _all.find('.order-item-span').each(function(index) {
                        var _this = $(this),
                            id = _this.data('id'),
                            parent = _this.data('parent');     
                        if (parentId && (parent == parentId) ) {
                            _this.remove();
                        } else {
                            arr.push(id);
                        }
                    });
                    if ($.inArray(value, arr) < 0) { // 判断当前点击的标签是否已经选择过
                        _all.append('<span class="order-item-span" data-id="' + value + '" data-mark="'+ mark +'" data-parent="'+ parentId +'" >'+ text +'<i class="fa fa-times" aria-hidden="true"></i></span>');
                        getSource('')
                    }
                } else { // 还未选择任何标签时
                    _all.html('<span class="order-item-span" data-id="' + value + '" data-mark="'+ mark +'" data-parent="'+ parentId +'"  >'+ text +'<i class="fa fa-times" aria-hidden="true"></i></span>');
                    getSource('')
                }
            }
            // 每增加一个标签获取一次数据: id若有值表示删除已选标签，否则为''
            function getSource(checkId) {
                //初始化processVal数据
                processVal = [];

                var fabirc_id = '';//面辅料分类ID，多个以逗号分隔，如大身料，里料等
                var fabirc_arr = [];//面辅料数组存储ID
                var attribute_value_id = '';//面辅料属性ID，多个以逗号分隔，如红色等
                var currentIndex = null;
                $('#orderAll > span').each(function (index) {
                    var _this = $(this);
                    var mark = _this.data('mark'),
                        id = _this.data('id');
                    if (mark == 'parent' && checkId != id) {
                        fabirc_id += id + ',';
                        fabirc_arr.push(id);
                    } else if(mark == 'son' && checkId != id) {
                        attribute_value_id += id + ',';
                    } else {
                        currentIndex = index;
                    }
                })
                // 请求数据
                $.post(
                    "../../../order/fabirc/c_fabirc/fabirc_product_list?flag_ajax_reurn=1",
                    {
                        cms_fabirc_id : fabirc_id,
                        cms_attribute_value_id : attribute_value_id,
                        cms_page_num: pageNum,
                        cms_page_size: pageSize
                    },
                    function (result) {
                            var data = eval("("+result+")");
                        if (data.ret == 0) {
                            var li_html = '';
                            // $.each(data.data_info,function(index,item){
                            //     li_html += '';
                            // })
                            push_page_info(data.page_info.cms_page_num,data.page_info.cms_page_size,data.page_info.cms_data_count)
                            $('.product-box').find(".product-list").html($('#productTemp').tmpl({list: data.data_info}));
                            if(fabirc_arr.length > 0)
                            {
                                processVal.push(fabirc_arr);
                            }
                            sessionStorage.setItem('processVal', processVal);
                            if (checkId != '' && currentIndex) { // 删除已选标签的请求成功后移除改标签
                                $('#orderAll > span').eq(currentIndex).remove(); 
                            }
                        } else {
                            alert(data.reason);
                        }
                    }
                )
            }
            // 关闭事件
            $('#orderAll').on('click', '.fa-times', function () {
                var _this = $(this);
                var id = _this.parent('span').data('id');
                _this.parent('span').remove();
                getSource(id);
                if($('#orderAll').find('span').length < 1) {
                    $('#orderAll').html('请选择');
                }
            })

            function attribute_by_type(type_id)
            {
                $.post(
                    "../../../order/fabirc/c_fabirc/fabirc_attribute?flag_ajax_reurn=1",
                    {
                        cms_fabirc_type_id : type_id
                    },
                    function (result) {
                        var data = eval("("+result+")");
                        var li_html = '';
                        $.each(data.data_info,function(index,item){
                            li_html += "<li><span data-id='"+item.cms_id+"' data-value='"+item.cms_fabirc_attribute+"'>"+item.cms_name+"</span></li>";
                        })
                        $('.filter-tabs-content').find("#filterUl").html(li_html);
                        getSource('');
                    }
                )
            }
            function list_refresh() {
                getSource('');
            }
            // 选中产品存储
            $(".product-list").on("click", 'li', function() {
                var now =  $(this).data('id');
                if (idArr.length > 0) { 
                    if (idArr.indexOf(now) < 0) {
                        idArr.push(now);
                    }
                } else {
                    idArr.push(now);
                }
                sessionStorage.setItem('checkFabirc', idArr);
            })

        </script>
    </body>
</html>