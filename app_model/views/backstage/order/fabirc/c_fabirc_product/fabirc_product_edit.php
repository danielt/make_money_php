<script type="text/javascript">
    $(document).ready(function() {
        $('#<?php echo $system_file_list_value['class'];?>-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cms_mark: {
                    group: '.col-lg-4',
                    validators: {
                        notEmpty: {
                            message: '不能为空'
                        },
                        stringLength: {
                            min: 1,
                            max: 64,
                            message: '输入字符长度需要在1-64之间'
                        },
                    }
                },
                cms_name: {
                    group: '.col-lg-4',
                    validators: {}
                },
                captcha: {
                    validators: {
                        callback: {
                            message: 'Wrong answer',
                            callback: function(value, validator) {
                                var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        });
    });
    //初始化入口函数
    function init_<?php echo $system_file_list_value['class'];?>(str_class,paramas)
    {
        var oFileInput = new FileInput();
        oFileInput.Init('edit',"image_data_edit_1",'image_data_edit_value_1',paramas);
    }
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){if(!isset($button_data_value['button_display']) || !$button_data_value['button_display']){?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }}?>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">修改数据</h2>
            </div>
            <div class="modal-body">
                <form id="<?php echo $system_file_list_value['class'];?>-form" method="post" action="" >
                    <input id="cms_id" name="cms_id" class="form-control1 icon" type="hidden" value="" placeholder="">
                    <div class="form-group">
                        <label class="col-md-2 control-label">名称</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>
                                <input id="cms_name" name="cms_name" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">价格</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>
                                <input id="cms_price" name="cms_price" class="form-control1 icon" type="text" value="" placeholder="最小单位至分">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">单位</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>
                                <input id="cms_unit" name="cms_unit" class="form-control1 icon" type="text" value="" placeholder="产品单位，如米、件等">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">珍稀程度</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right" style="margin-top: 10px!important;">
                                <input type="radio" name="cms_is_scarce" value="0" />普通
                                <input type="radio" name="cms_is_scarce" value="1" />奇缺
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">面辅料分类</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right" style="margin-top: 10px!important;">
                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>
                                <select name="cms_fabirc_id" style="width: 200px;height: 30px;">
                                    <option>请选择</option>
                                    <?php foreach ($fabirc as $value){?>
                                        <option value="<?php echo $value['cms_id'];?>"><?php echo $value['cms_name'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">属性选择</label>
                        <div class="col-sm-8" style="width:72%;">
                            <div class="input-group input-icon right">
                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>
                                <input type="hidden" name="cms_attribute_value_id" id="select_checkbox_button_hidden_value" />
                                <select class="select_checkbox_button" multiple="multiple">
                                    <?php foreach ($attribute_list as $attribute){?>
                                        <?php if (isset($attribute['cms_value']) && !empty($attribute['cms_value'])){?>
                                            <optgroup label="<?php echo $attribute['cms_name'];?>">
                                                <?php foreach ($attribute['cms_value'] as $a_value){?>
                                                    <option value="<?php echo $a_value['cms_id'];?>"><?php echo $a_value['cms_name'];?></option>
                                                <?php }?>
                                            </optgroup>
                                        <?php }}?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">产品描述</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                                <!--                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>-->
                                <textarea name="cms_summary" cols="60" rows="10"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">图片上传</label>
                        <div class="col-md-10">
                            <input type="file" id="image_data_edit_1" name="image_data" accept="*/*">
                        </div>
                        <div id="image_data_edit_value_1" post_name="cms_image" ></div>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="sys_sweetalert('submit','<?php echo $system_file_list_value['class'];?>','您确定要添加这条信息吗','提交数据后系统将可能会添加此条数据，请谨慎操作！','<?php echo $system_file_list_value['ajax'];?>','',true);">
                        	   <i class="fa fa-book"> 提交 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>