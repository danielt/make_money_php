<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_top_web_file.php';?>
</head>
<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li><a href="#">产品管理</a></li>
            <li class="active">面辅料产品管理</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="<?php echo $arr_page_url['list_url'];?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label">名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_name" id="focusedinput" value="<?php echo isset($arr_params['cms_name']) ? $arr_params['cms_name'] : '';?>">
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label">面辅料分类</label>
                    <div class="col-sm-2">
                        <select name="cms_fabirc_id" style="width:120px;" class="form-control-static">
                            <option value="">请选择</option>
                            <?php foreach ($fabirc as $value){?>
                                <option value="<?php echo $value['cms_id'];?>" <?php if($arr_params['cms_fabirc_id'] == $value['cms_id']) echo "selected='selected'";?>><?php echo $value['cms_name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label">珍稀程度</label>
                    <div class="col-sm-2">
                        <select name="cms_is_scarce" style="width:120px;" class="form-control-static">
                            <option value="">请选择</option>
                            <option value="0" <?php if(isset($arr_params['cms_is_scarce']) && $arr_params['cms_is_scarce'] !== '' && $arr_params['cms_is_scarce'] == 0) echo "selected='selected'";?>>普通</option>
                            <option value="1" <?php if($arr_params['cms_is_scarce'] == 1) echo "selected='selected'";?>>奇缺</option>
                        </select>
                    </div>

                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list_search">
                            <i class="fa fa-search">查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
                    <th>名称</th>
                    <th>面辅料分类</th>
                    <th>珍稀程度</th>
                    <th>属性</th>
                    <th>图片</th>
                    <th>创建时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php  if(isset($data_info) && is_array($data_info) && !empty($data_info))
                {
                    foreach ($data_info as $val)
                    {
                        ?>
                        <tr class='odd selected'>
                            <td><input type="checkbox" name="checkItem" attr-key="cms_id" value="<?php echo $val['cms_id'];?>"/></td>
                            <td><?php echo $val['cms_name'];?></td>
                            <td><?php echo $val['cms_fabirc_name'];?></td>
                            <td><?php echo ($val['cms_is_scarce'] == '1') ? "<span class='ur'>奇缺</span>" : "<span class='work'>普通</span>" ?></td>
                            <td><?php echo $val['cms_attribute_value_name'];?></td>
                            <td>
                                <alt alt="<img src='<?php echo VIEW_MODEL_BASE;?>/data_model/upload/<?php echo $val['cms_image'];?>' width='100px' height='100px'>">
                                    <a <?php if(!empty($val['cms_image'])){?> href="<?php echo VIEW_MODEL_BASE;?>/data_model/upload/<?php echo $val['cms_image'];?>"<?php }?> target="_blank">查看</a>
                                </alt>
                            </td>
                            <td><?php echo $val['cms_create_time'];?></td>
                            <td><?php echo $val['cms_modify_time'];?></td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!--*********** 初始化必须加载 ***************** （下拉操作信息） *********** 初始化必须加载 ***************** -->
                                        <?php echo make_right_button($system_file_list, $val);?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php     }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/pub_page.php';?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/model/backstage/public_bottom_button.php';?>
    </div>
</body>
</html>