<script type="text/javascript">
    $(document).ready(function() {
    	var defaultData = <?php echo (isset($menu_data_list)&&is_array($menu_data_list)) ? json_encode($menu_data_list) : json_encode(array());?>;
        $('#<?php echo $system_file_list_value['class'];?>-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            	cms_name: {
                    group: '.col-lg-4',
                    validators: {
                        notEmpty: {
                            message: '不能为空'
                        },
                        stringLength: {
                            min: 1,
                            max: 255,
                            message: '输入字符长度需要在1-255之间'
                        },
                    }
                },
                captcha: {
                    validators: {
                        callback: {
                            message: 'Wrong answer',
                            callback: function(value, validator) {
                                var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        });
        init_checkableTree(defaultData);
    });
    
    function init_<?php echo $system_file_list_value['class'];?>(str_class,paramas)
    {
        init_checkableTree(defaultData);
    };
</script>
<script type="text/javascript">
function init_checkableTree(defaultData)
{
	var $checkableTree = $('#treeview-checkable').treeview({
    data: defaultData,
    showIcon: false,
    showCheckbox: true,
    selectedBackColor: '#ffffcc',
    selectedColor: '#000000',
    showTags: true,
	levels: 1,
    onNodeChecked: function (event, node) {
      if(node.levels == 3)
      {
    	  $('#checkable-output').prepend('<input name="cms_button_id[]" type="hidden" value="'+node.cms_id+'" >');
      }
      checkAllParent(node);
    },
    onNodeUnchecked: function (event, node) {
      if(node.levels == 3)
      {
    	  $('#checkable-output').find("input[name='cms_button_id[]']").each(function(){
			if($(this).val() == node.cms_id)
			{
				$(this).remove();
			}
          });
      }
      uncheckAllParent(node);
      uncheckAllSon(node);
    }
  });
  $('#treeview-checkable').on('nodeSelected', function (event, data) {
    console.log(data.id);
  });
}
var nodeCheckedSilent = false;
function nodeChecked(event, node) {
  if (nodeCheckedSilent) {
    return;
  }
  nodeCheckedSilent = true;
  checkAllParent(node);
  checkAllSon(node);
  nodeCheckedSilent = false;
}

var nodeUncheckedSilent = false;
function nodeUnchecked(event, node) {
  if (nodeUncheckedSilent)
    return;
  nodeUncheckedSilent = true;
  uncheckAllParent(node);
  uncheckAllSon(node);
  nodeUncheckedSilent = false;
}
//选中全部父节点  
function checkAllParent(node) {
    $('#treeview-checkable').treeview('checkNode', node.nodeId, { silent: true });
    var parentNode = $('#treeview-checkable').treeview('getParent', node.nodeId);
    if (!("nodeId" in parentNode)) {
      return;
    } else {
      checkAllParent(parentNode);
    }
}
//取消全部父节点  
function uncheckAllParent(node) {
    $('#treeview-checkable').treeview('uncheckNode', node.nodeId, { silent: true });
    var siblings = $('#treeview-checkable').treeview('getSiblings', node.nodeId);
    var parentNode = $('#treeview-checkable').treeview('getParent', node.nodeId);
    if (!("nodeId" in parentNode)) {
      return;
    }
    var isAllUnchecked = true;  //是否全部没选中  
    for (var i in siblings) {
      if (siblings[i].state.checked) {
        isAllUnchecked = false;
        break;
      }
    }
    if (isAllUnchecked) {
      uncheckAllParent(parentNode);
    }
}

//级联选中所有子节点  
function checkAllSon(node) {
    $('#treeview-checkable').treeview('checkNode', node.nodeId, { silent: true });
    if (node.nodes != null && node.nodes.length > 0) {
      for (var i in node.nodes) {
        checkAllSon(node.nodes[i]);
      }
    }
}
//级联取消所有子节点  
function uncheckAllSon(node) {
    $('#treeview-checkable').treeview('uncheckNode', node.nodeId, { silent: true });
    if (node.nodes != null && node.nodes.length > 0) {
      for (var i in node.nodes) {
        uncheckAllSon(node.nodes[i]);
      }
    }
}  
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){?>
    <button class="btn purple btn-lg" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content animated bounceInTop">
			<div class="modal-header">
				<button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
				<h2 class="modal-title">添加数据</h2>
			</div>
			<div class="modal-body">
                <form id="<?php echo $system_file_list_value['class'];?>-form" method="post" action="" >
                    <div class="form-group" style="display:none;">
<!--                 		<input name="aa" type="text" value="" > -->
                	</div>
                	<div class="form-group">
                		<label class="col-md-3 control-label">角色名称</label>
                		<div class="col-md-9">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_name" name="cms_name" class="form-control1 icon" type="text" value="" placeholder="">
                			</div>
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="col-md-3 control-label">描述</label>
                		<div class="col-md-9">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-book"></i>
                				</span>
                				<textarea name="cms_desc" id="txtarea1" cols="50" rows="4" class="form-control"></textarea>
                			</div>
                		</div>
                	</div>
					<div class="form-group">
						<label class="col-md-3 control-label">权限列表</label>
						<div class="col-md-9">
							<div id="treeview-checkable" class=""></div>
							<div id="checkable-output" style="display: none;"></div>
						</div>
					</div>
                </form>
			</div>
			<div class="clearfix"></div>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="sys_sweetalert('submit','<?php echo $system_file_list_value['class'];?>','您确定要添加这条信息吗','提交数据后系统将可能会添加此条数据，请谨慎操作！','<?php echo $system_file_list_value['ajax'];?>','',true);">
                        	   <i class="fa fa-book"> 提交 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-reset">
                        	   <i class="fa fa-book"> 重置数据 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
        		</div>
            </div>
		</div>
	</div>
</div>