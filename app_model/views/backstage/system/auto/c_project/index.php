<html>
<head>
<!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
</head> 
<body >
	<div class="outter-wp">
		<div class="but_list">
			<ol class="breadcrumb">
				<li><a href="#">系统配置</a></li>
				<li><a href="#">自动化管理</a></li>
				<li class="active">项目列表</li>
			</ol>
		</div>
		<div class="graph">
			<div class="form-body">
				<form class="form-horizontal" method="post" action="<?php echo $arr_page_url['list_url'];?>">
    				<div class="form-group">
    				    
    					<label class="col-sm-1 control-label">多选</label>
						<div class="col-sm-2">
							<select data-placeholder="选择省份" class="chosen-select col-sm-2" multiple style="width:100%;" tabindex="4">
                                    <option value="">请选择省份</option>
                                    <option value="110000" hassubinfo="true">北京</option>
                                    <option value="120000" hassubinfo="true">天津</option>
                                    <option value="130000" hassubinfo="true">河北省</option>
                                    <option value="140000" hassubinfo="true">山西省</option>
                                    <option value="150000" hassubinfo="true">内蒙古自治区</option>
                                    <option value="210000" hassubinfo="true">辽宁省</option>
                                    <option value="220000" hassubinfo="true">吉林省</option>
                                    <option value="230000" hassubinfo="true">黑龙江省</option>
                                    <option value="310000" hassubinfo="true">上海</option>
                                    <option value="320000" hassubinfo="true">江苏省</option>
                                    <option value="330000" hassubinfo="true">浙江省</option>
                                    <option value="340000" hassubinfo="true">安徽省</option>
                                    <option value="350000" hassubinfo="true">福建省</option>
                                    <option value="360000" hassubinfo="true">江西省</option>
                                    <option value="370000" hassubinfo="true">山东省</option>
                                    <option value="410000" hassubinfo="true">河南省</option>
                                    <option value="420000" hassubinfo="true">湖北省</option>
                                    <option value="430000" hassubinfo="true">湖南省</option>
                                    <option value="440000" hassubinfo="true">广东省</option>
                                    <option value="450000" hassubinfo="true">广西壮族自治区</option>
                                    <option value="460000" hassubinfo="true">海南省</option>
                                    <option value="500000" hassubinfo="true">重庆</option>
                                    <option value="510000" hassubinfo="true">四川省</option>
                                    <option value="520000" hassubinfo="true">贵州省</option>
                                    <option value="530000" hassubinfo="true">云南省</option>
                                    <option value="540000" hassubinfo="true">西藏自治区</option>
                                    <option value="610000" hassubinfo="true">陕西省</option>
                                    <option value="620000" hassubinfo="true">甘肃省</option>
                                    <option value="630000" hassubinfo="true">青海省</option>
                                    <option value="640000" hassubinfo="true">宁夏回族自治区</option>
                                    <option value="650000" hassubinfo="true">新疆维吾尔自治区</option>
                                    <option value="710000" hassubinfo="true">台湾省</option>
                                    <option value="810000" hassubinfo="true">香港特别行政区</option>
                                    <option value="820000" hassubinfo="true">澳门特别行政区</option>
                                    <option value="990000" hassubinfo="true">海外</option>
                                </select>
						</div>
    					<label for="disabledinput" class="col-sm-1 control-label">名称</label>
    					<div class="col-sm-2">
    						<input type="text" class="form-control1" name="cms_name" id="focusedinput" value="<?php echo isset($arr_params['cms_name']) ? $arr_params['cms_name'] : '';?>">
    					</div>
    					<div class="col-sm-1">
    						<button class="btn btn-primary" type="button" id="button_query_list">
    							<i class="fa fa-search">查询</i>
    						</button>
    					</div>
    				</div>
			     </form>
		    </div>
    		<div class="view_tables">
    			<table class="table table-hover" id="index_list">
    				<thead>
    					<tr>
    					    <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
    						<th>项目名称</th>
    						<th>联系人手机号码</th>
    						<th>联系人座机号码</th>
    						<th>备注</th>
    						<th>排序权重</th>
    						<th>状态</th>
    						<th>创建时间</th>
    						<th>修改时间</th>
    						<th>操作</th>
    					</tr>
    				</thead>
    				<tbody>
    				    <?php  if(isset($data_info) && is_array($data_info) && !empty($data_info))
    						   {
    						       foreach ($data_info as $val)
    						       {
    				    ?>
    								<tr class='odd selected'>
    								    <td><input type="checkbox" name="checkItem" attr-key="cms_id" value="<?php echo $val['cms_id'];?>"/></td>
    								    <?php if($val['cms_name'] == $val['cms_mark']){?>
    									<td><font color='red'><?php echo $val['cms_name'];?></font></td>
    								    <?php }else{?>
    									<td><?php echo $val['cms_name'];?></td>
    									<?php }?>
    									<td><?php echo $val['cms_mobilephone_number'];?></td>
    									<td><?php echo $val['cms_telphone_number'];?></td>
    									<td><?php echo $val['cms_remark'];?></td>
    									<td><?php echo $val['cms_order'];?></td>
    									<td>
    									<?php echo $val['cms_state'] != '0' ? "<font color='red'>禁用</font>" : "<font color='green'>启用</font>";?>
    									</td>
    									<td><?php echo $val['cms_create_time'];?></td>
    									<td><?php echo $val['cms_modify_time'];?></td>
    									<td>
                                            <div class="dropdown">
    											<a href="#" title="" class="btn btn-info" data-toggle="dropdown" aria-expanded="false"> 
    											    <i class="fa fa-cog icon_8"></i>
    											       
    											    <i class="fa fa-chevron-down icon_8"></i>
    										    </a>
    											<ul class="dropdown-menu float-right">
                                            		<!--*********** 初始化必须加载 ***************** （下拉操作信息） *********** 初始化必须加载 ***************** -->
    												<?php echo make_right_button($system_file_list, $val);?>
    											</ul>
    										</div>
    									</td>
    								</tr>
    					<?php     }
    						   }
    				     ?>
				</tbody>
			</table>
		</div>
		<!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_page.php';?>
		<!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/public_bottom_button.php';?>
	</div>
</body>
</html>