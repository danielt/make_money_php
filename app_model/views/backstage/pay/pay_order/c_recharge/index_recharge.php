<?php
/**
 * Use：充值
 * Author：kan.yang@starcor.cn
 * DateTime：18-12-27 下午10:08
 * Description：
 */

//POST请求地址
$str_query_url = !empty($arr_page_url['list_url']) ? $arr_page_url['list_url'] : rtrim($this->config->config['base_url'],'/') . '/backstage/pay/pay_order/c_channel/list_partner';
//获取Controller层数据
$arr_partner_list = array();
if(isset($ret) && $ret == 0 && !empty($data_info))
{
    $arr_partner_list = $data_info;
}


?>

<script type="text/javascript">
    //初始化入口函数
    function init_<?php echo $system_file_list_value['class'];?>(str_class,paramas,str_order_data)
    {
        if(str_order_data.length < 1)
        {
            $('div.pay-order-fail').show().find('h2').html('错误：参数异常，请重试');
            return false;
        }
        //加载中……
        $('div.pay-order-ing').show();
        //支付
        var b = new Base64();
        str_order_data = b.decode(str_order_data);
        str_order_data =  eval("("+str_order_data+")");
        //异步请求支付
        var str_url = paramas['url'];
        var data = {
            'cms_user_id':str_order_data.cms_user_id,
            'cms_order_price':str_order_data.cms_order_price,
            'cms_order_type':str_order_data.cms_order_type,
            'cms_pay_order_code':str_order_data.cms_pay_order_code
        };
        ajax(str_url,'post',data,function(response){
            $('div.pay-order-ing').hide();
            if(response.ret == 0)
            {
                $('div.pay-order-fail').show();
            }
            else
            {
                $('div.pay-order-fail').show();
                var recharge = $('button.pay-recharge');
                var url = recharge.find('a').attr('href');
                url += '?cms_user_id=' + str_order_data.cms_user_id + '&cms_oder_id=' + response.data_info.cms_uuid + '&cms_order_price=' + str_order_data.cms_order_price;
                recharge.show().find('a').attr('href',url);
            }
        },function(a,b,c){
            $('div.pay-order-ing').hide();
            $('div.pay-order-fail').find('h2').html('错误：服务器异常，请联系管理员');
        });
        return true;
    }
    //异步请求
    function ajax(str_url,type,data,success,error)
    {
        $.ajax({
            'url':str_url,
            'type':type,
            'data':data,
            'dataType':'json',
            'success':success(response),
            'error':error(a,b,c)
        });
    }
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){
    if($button_data_value['bottom']){ continue; }
    ?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<style type="text/css">
    .pay-channel-platform-type
    {
        margin-right: 20px;
    }
    .pay-channel-platform-type input
    {
        margin-right: 5px;
        cursor: pointer;
    }
    .pay-order-ing,.pay-order-success,.pay-order-fail
    {
        text-align: center;
        display: none;
    }
</style>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">充值</h2>
            </div>
            <div class="modal-body">
                <!-- 支付金额 开始 -->
                <div class="pay-order-ing">
                    <p>充值金额：</p>
                </div>
                <!-- 支付金额 结束 -->

            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <span>
                    <button type="button" class="btn btn-default pay-recharge" data-dismiss="modal">
                        <i class="fa fa-pencil-square-o">
                            <a href="./c_recharge/recharge">充值</a>
                        </i>
                    </button>
                </span>
                <span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-book"> 关闭 </i>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>