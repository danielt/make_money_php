<?php if(isset($system_file_list_value['button_data']) && is_array($system_file_list_value['button_data'])) {
    foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){
    if($button_data_value['bottom']){ continue; }
    ?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<?php }?>

<script type="text/javascript">
    //初始参数
    function init_<?php echo $system_file_list_value['class'];?>(str_class,obj_params)
    {
        if(obj_params)
        {
            //解析数据
            var b = new Base64();
            temp_paramas = b.decode(obj_params);
            temp_paramas = eval("("+temp_paramas+")");
            $('#'+str_class).find("label.pay-order-detail-value").each(function(a,b)
            {
                var str_column = $(this).attr('data-name');
                var str_value  = temp_paramas[str_column];
                switch (str_column)
                {
                    case 'cms_order_type':
                        if(str_value == '0')
                        {
                            str_value = '批量订单';
                        }
                        else if(str_value == '1')
                        {
                            str_value = '面料小样订单';
                        }
                        else if(str_value == '2')
                        {
                            str_value = '样板订单';
                        }
                        else if(str_value == '3')
                        {
                            str_value = '样衣订单';
                        }
                        else if(str_value == '4')
                        {
                            str_value = '稀缺面料定金订单';
                        }
                        else if(str_value == '5')
                        {
                            str_value = '充值订单';
                        }
                        else
                        {
                            str_value = '未知订单';
                        }
                        break;
                    case 'cms_pay_channel_id':
                        if(str_value == '1')
                        {
                            str_value = '微信支付';
                        }
                        else if(str_value == '1')
                        {
                            str_value = '支付宝支付';
                        }
                        else
                        {
                            str_value = '未知支付渠道';
                        }
                        break;
                    case 'cms_pay_channel_mode':
                        if(str_value == '0')
                        {
                            str_value = '扫码支付';
                        }
                        else if(str_value == '1')
                        {
                            str_value = 'APP支付';
                        }
                        else if(str_value == '2')
                        {
                            str_value = '网页支付';
                        }
                        else
                        {
                            str_value = '未知支付渠道';
                        }
                        break;
                    case 'cms_pay_mode_type':
                        if(str_value == '0')
                        {
                            str_value = '余额支付';
                        }
                        else if(str_value == '1')
                        {
                            str_value = '线上支付';
                        }
                        else if(str_value == '2')
                        {
                            str_value = '积分支付';
                        }
                        else
                        {
                            str_value = '未知支付渠道';
                        }
                        break;
                    case 'cms_order_price':
                        str_value = '¥' + str_value;
                        break;
                    case 'cms_order_state':
                        if(str_value == '0')
                        {
                            str_value = '未支付';
                        }
                        else if(str_value == '1')
                        {
                            str_value = '已支付';
                        }
                        else if(str_value == '2')
                        {
                            str_value = '已取消';
                        }
                        else if(str_value == '3')
                        {
                            str_value = '已过期';
                        }
                        else if(str_value == '4')
                        {
                            str_value = '退款中';
                        }
                        else if(str_value == '5')
                        {
                            str_value = '已退订';
                        }
                        else if(str_value == '6')
                        {
                            str_value = '异常订单';
                        }
                        else
                        {
                            str_value = '未知状态';
                        }
                        break;
                    case 'cms_business_state':
                        if(str_value == '0')
                        {
                            str_value = '未完成';
                        }
                        else if(str_value == '1')
                        {
                            str_value = '已完成';
                        }
                        else
                        {
                            str_value = '未知状态';
                        }
                        break;
                    default :
                        str_value = str_value ? str_value : '';
                        break;
                }
                $(this).html(str_value);
            });
        }
    }
</script>

<style type="text/css">
    .pay-order-detail-name
    {
        text-align: right;
    }
    .clear
    {
        clear: both;
    }
    .pay-order-group-item
    {
        margin-bottom: 0 !important;
    }
</style>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content animated bounceInTop">
			<div class="modal-header">
				<button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
				<h2 class="modal-title">订单详情</h2>
			</div>
			<div class="modal-body">
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单标识:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_uuid" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单名称:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_order_name" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单类型:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_order_type" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单金额:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_order_price" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">支付订单号:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_pay_order_id" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">支付商户ID:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_pay_partner_id" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">支付渠道:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_pay_channel_id" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">支付方式:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_pay_channel_mode" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">支付类型:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_pay_mode_type" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">父级订单标识:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_order_parent" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">购买订单标识:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_pay_order_code" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单创建时间:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_create_time" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单修改时间:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_modify_time" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单状态:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_order_state" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">业务状态:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_business_state" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group pay-order-group-item">
                    <label class="col-md-2 control-label pay-order-detail-name">订单描述:</label>
                    <div class="col-md-8 control-label">
                        <label data-name="cms_order_desc" class="col-md-10 control-label pay-order-detail-value"></label>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
			<div class="clearfix"></div>
            <div class="modal-footer"></div>
		</div>
	</div>
</div>