
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){
    if($button_data_value['bottom']){ continue; }
    ?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<script type="text/javascript">

    //初始参数
    var obj_pay_child_order_logic = window.obj_pay_child_order_logic || {};
    obj_pay_child_order_logic.arr_pay_order_type = {
        0:'批量订单',
        1:'面料小样',
        2:'样板订单',
        3:'样衣订单',
        4:'稀缺面料定金订单'
    };
    obj_pay_child_order_logic.arr_pay_order_state = {
        0:'未支付',
        1:'已支付',
        2:'已取消',
        3:'已过期',
        4:'退款中',
        5:'已退订',
        6:'订单异常'
    };

    //入口函数
    function init_<?php echo $system_file_list_value['class'];?>(str_class,obj_params)
    {
        $('h2.modal-title').html('子支付订单列表');
        if(obj_params)
        {
            //解析数据
            var b = new Base64();
            temp_paramas = b.decode(obj_params);
            temp_paramas = eval("("+temp_paramas+")");

            var url = './list_order?flag_ajax_reurn=1&cms_order_parent=' + temp_paramas.cms_id;
            obj_pay_child_order_logic.ajax(url,'get',null,function(response){
                var data = [];
                if(response.ret == 0 && response.data_info.length > 0)
                {
                    data = response.data_info;
                }
                obj_pay_child_order_logic.init_table_content(data);
            },function(a,b,c){
                obj_pay_child_order_logic.init_table_content([]);
            });
        }
    }

    //构建Table内容
    obj_pay_child_order_logic.init_table_content = function(arr_order_list)
    {
        var str_td_content = '',
            int_len = arr_order_list.length;
        if(int_len <= 0)
        {
            str_td_content = '<tr><td colspan="9" sytle="text-align: center !important;">暂无任何数据</td></tr>'
        }
        else
        {
            var b = new Base64();
            order_data = JSON.stringify({});
            for(var i=0; i < int_len; i++)
            {
                var data = JSON.stringify(arr_order_list[i]);
                data = b.encode(data);
                str_td_content +=
                '<tr class="odd selected">' +
                    '<td><input type="checkbox" name="checkItem" attr-key="cms_id" value="'+arr_order_list[i].cms_id+'"/></td>'+
                    '<td>'+arr_order_list[i].cms_order_name+'</td>'+
                    '<td>'+arr_order_list[i].cms_order_type_name+'</td>'+
                    '<td>'+arr_order_list[i].cms_pay_channel_name+'</td>'+
                    '<td>'+arr_order_list[i].cms_pay_channel_mode_name+'</td>'+
                    '<td>'+arr_order_list[i].cms_order_price+'/元</td>'+
                    '<td>'+arr_order_list[i].cms_order_state_name+'</td>'+
                    '<td>'+arr_order_list[i].cms_modify_time+'</td>'+
                    '<td>——</td>' +
                '</tr>';
            }
        }
        var tbody = $('.pay-order-child-content');
        tbody.find('tr').remove();
        tbody.html(str_td_content);
    };

    //支付订单
    obj_pay_child_order_logic.pay_order = function()
    {

    };

    //异步请求
    obj_pay_child_order_logic.ajax = function(str_url,type,data,success,error)
    {
        $.ajax({
            'url':str_url,
            'type':type,
            'data':data,
            'dataType':'json',
            'success':function(response) { success(response); },
            'error':function(a,b,c) { error(a,b,c); }
        });
    };
</script>
<style type="text/css">
    .pay-channel-platform-type
    {
        margin-right: 20px;
    }
    .pay-channel-platform-type input
    {
        margin-right: 5px;

    }
</style>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">子支付订单列表</h2>
            </div>
            <div class="modal-body">
                <div class="view_tables">
                    <table class="table table-hover" id="index_list">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
                            <th>订单名称</th>
                            <th>订单类型</th>
                            <th>支付渠道</th>
                            <th>支付方式</th>
                            <th>订单金额</th>
                            <th>订单状态</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody class="pay-order-child-content"> </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>