<?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_pay_file.php';?>
<script type="text/javascript">
    //初始化参数
    var obj_shopping_cart = window.obj_shopping_cart || {};
    obj_shopping_cart.base_url = '<?= $this->config->config['base_url']; ?>';
    obj_shopping_cart.obj_pay_msg={
        para_ord_err:'错误：请选择有效的物理订单',
        que_ord_err :'错误：校验可用的物理信息失败'
    };
    obj_shopping_cart.order_type_items = {
        '0':'批量订单',
        '1':'面料小样',
        '2':'样板订单',
        '3':'样衣订单',
        '4':'稀缺面料定金订单'
    };
    obj_shopping_cart.bool_quoted_price = true;

    /**
     * 支付入口函数
     * @param str_physical_orders 物理订单ID
     * @param int_order_type      物理订单类型
     * @param int_pay_parent      支付订单-主订单ID（子订单时，必传）
     * @returns {boolean}
     */
    obj_shopping_cart.init = function(str_physical_orders,int_order_type,int_pay_parent)
    {
        //参数验证
        if(!str_physical_orders)
        {
            obj_shopping_cart.error(obj_shopping_cart.obj_pay_msg.para_ord_err);
            return false;
        }
        obj_shopping_cart.pay_order_parent= int_pay_parent;
        obj_shopping_cart.physical_orders = str_physical_orders;
        obj_shopping_cart.order_type      = int_order_type;
        $('button.button-shopping-cart').click();
        return true;
    };

    //入口函数
    obj_shopping_cart.init_pay = function()
    {
        //初始化
        obj_shopping_cart.fail_mode  = $('div.pay-shopping-cart-fail').empty().hide();
        obj_shopping_cart.table_mode = $('div.pay-shopping-cart-table').empty().hide();
        obj_shopping_cart.settle_mode= $('div.pay-shopping-settle-accounts').hide();
        obj_shopping_cart.total_price= $('span.pay-shopping-total-price').html('¥0.00');
        obj_shopping_cart.pay_button = $('.pay-shopping-cart').show();
        obj_shopping_cart.earnest_price= $('span.pay-shopping-earnest-price').html('¥0.00');
        //物理订单号
        var orders = obj_shopping_cart.physical_orders;
        //查询物理订单信息
        var url = obj_shopping_cart.base_url + '/backstage/order/con_client/c_client_order/query_order_info?order_id='+obj_shopping_cart.physical_orders+'&order_type='+obj_shopping_cart.order_type+'&flag_ajax_reurn=1';
        obj_shopping_cart.ajax(url,'get',null,function(reason){
            if(reason.ret != 0 || reason.data_info.length < 1)
            {
                obj_shopping_cart.error(obj_shopping_cart.obj_pay_msg.que_ord_err);
                return false;
            }
            //组建前端界面
            var len   = reason.data_info.length,
                total = 0.00;
                t_body=
                    '<ul class="title">'+
                        '<li class="name">订单名称</li>'+
                        '<li class="price">单价</li>'+
                        '<li class="number">数量</li>'+
                        '<li class="ord_tol">小计</li>'+
                    '</ul>';
            for(var i=0; i< len; i++)
            {
                var type = obj_shopping_cart.order_type;
                var order_name = reason.data_info[i].cms_name ? reason.data_info[i].cms_name : obj_shopping_cart.order_type_items[type];
                t_body+=
                    '<ul class="'+(i+1 == len ? 'foot' : '')+'">'+
                        '<li class="name" data-value="'+reason.data_info[i].cms_id+'">'+order_name+'</li>'+
                        '<li class="price" data-value="'+reason.data_info[i].cms_price+'">¥'+reason.data_info[i].cms_price+'</li>'+
                        '<li class="number">' +
                            '<input type="button" value="—" onclick="obj_shopping_cart.shopping_number_action(this,1)"/>' +
                            '<input type="text" class="shopping-number" value="1"/>' +
                            '<input type="button" value="+" onclick="obj_shopping_cart.shopping_number_action(this,0)"/>' +
                        '</li>'+
                        '<li class="ord_tol" data-value="'+reason.data_info[i].cms_price+'">¥'+reason.data_info[i].cms_price+'</li>'+
                        '<li class="ord_type" data-value="'+obj_shopping_cart.order_type+'"></li>'+
                    '</ul>';
                total = total + Number(reason.data_info[i].cms_price);
            }
            t_body += '<div class="clear"></div>';
            obj_shopping_cart.table_mode.empty().html(t_body).show();
            //订单整体结算
            obj_shopping_cart.shopping_number($('li.number')[1],1);
            obj_shopping_cart.settle_mode.show();
            //自定义购买数量
            $('div.pay-shopping-cart-table input.shopping-number').focus(function(){

            }).blur(function(){
                obj_shopping_cart.shopping_number($(this).parent('li.number'),$(this).val());
            });
            return true;
        },function(a,b,c){
            obj_shopping_cart.error(obj_shopping_cart.obj_pay_msg.que_ord_err);
        });
        return true;
    };

    //支付
    obj_shopping_cart.recharge = function()
    {
        var data = [];
        var li_action = function(that)
        {
            var id = $(that).find('li.name').attr('data-value');
            if(!id)
            {
                return false;
            }
            data.push({
                'cms_order_price':$(that).find('li.ord_tol').attr('data-value'),
                'cms_order_price_payed':$(that).find('li.ord_tol').attr('data-earnest'),
                'cms_order_type':$(that).find('li.ord_type').attr('data-value'),
                'cms_pay_order_code':id,
                'cms_order_parent':obj_shopping_cart.pay_order_parent
            });
            return true;
        };
        $('div.pay-shopping-cart-table ul').each(function(){
            li_action(this);
        });
        //调用支付模块
        $('button.button-shopping-cart-close').click();
        $('button.button-pay-mode').click();
        obj_pay_order_logic.pay_order(data[0]);
    };

    //关闭模态框
    obj_shopping_cart.close_shopping_cart_mode = function()
    {
        $('#pay_order_shopping_cart').modal('hide');
    };

    //更新订单数量
    obj_shopping_cart.shopping_number_action = function(that,int_type)
    {
        var obj_num = $(that).parent('li.number');
        var obj_num_inp = $(that).parent('li.number').find('input.shopping-number');
        var int_num = obj_num_inp.val();
        int_num = obj_shopping_cart.check_number(int_num);
        //校验类型
        if(int_type == 1)
        {//递减

            int_num = int_num < 2 ? 1 : -- int_num;
        }
        else
        {//递加

            ++ int_num;
        }
        obj_shopping_cart.shopping_number(obj_num,int_num);
        return true;
    };

    //计算金额
    obj_shopping_cart.shopping_number = function(that,int_num)
    {
        //当前订单价格
        int_num = obj_shopping_cart.check_number(int_num);
        $(that).find('input.shopping-number').val(int_num);

        //异步获取订单报价
        var url = obj_shopping_cart.base_url + '/backstage/pay/pay_order/c_pay_order/order_quoted_price',
            data= {
                'cms_order_id' : $(that).parent('ul').find('li.name').attr('data-value'),
                'cms_order_type':obj_shopping_cart.order_type,
                'cms_order_num':int_num,
                'cms_unit_price':$(that).prev('li.price').attr('data-value')
            };
        obj_shopping_cart.active_convert(0);
        obj_shopping_cart.ajax(url,'post',data,function(reason)
        {
            if(reason.ret != 0 || !reason.data_info.price)
            {
                alert('请求订单报价失败：' + reason.reason);
            }
            else
            {
                var price   = reason.data_info.price;
                var earnest = reason.data_info.earnest;
                $(that).next('li.ord_tol').attr({'data-value':price,'data-earnest':earnest}).html('¥'+price);
                //计算总价
                var total = earnest = 0.00;
                $('div.pay-shopping-cart-table li.ord_tol').each(function(){
                    var price = $(this).attr('data-value');
                    if(price)
                    {
                        total = total + Number($(this).attr('data-value'));
                    }
                    price = $(this).attr('data-earnest');
                    if(price)
                    {
                        earnest = earnest + Number($(this).attr('data-earnest'));
                    }
                });
                obj_shopping_cart.total_price.attr('data-total-price',total.toFixed(2)).html('¥' + total.toFixed(2));
                obj_shopping_cart.earnest_price.attr('data-earnest-price',earnest.toFixed(2)).html('¥' + earnest.toFixed(2));
            }
            obj_shopping_cart.active_convert(1);
        },function(a,b,c)
        {
            obj_shopping_cart.active_convert(1);
            alert('请求订单报价失败：服务器请求异常');
        });
    };

    //校验正整数
    obj_shopping_cart.check_number = function(int_num)
    {
        int_num     = parseInt(int_num);
        var pre     = /^[1-9]\d*$/;
        if(!int_num || !pre.test(int_num))
        {
            int_num = 1;
        }
        return int_num;
    };

    //异常场景
    obj_shopping_cart.error = function(str_msg)
    {
        obj_shopping_cart.fail_mode.show().find('h2').html(str_msg);
        obj_shopping_cart.pay_button.hide();
    };

    //解封、加封
    obj_shopping_cart.active_convert = function($int_convert_type)
    {
        if($int_convert_type == 1)
        {
            $('div.pay-shopping-cart-table').find('input').removeAttr('disabled');
            $('button.pay-shopping-cart').removeAttr('disabled');

        }
        else
        {
            $('div.pay-shopping-cart-table').find('input').attr('disabled','disabled');
            $('button.pay-shopping-cart').attr('disabled','disabled');
        }
    };

    //异步请求
    obj_shopping_cart.ajax = function(str_url,type,data,success,error)
    {
        $.ajax({
            'url':str_url,
            'type':type,
            'data':data,
            'dataType':'json',
            'success':function(response) { success(response); },
            'error':function(a,b,c) { error(a,b,c); }
        });
    };

</script>
<style type="text/css">
    .button-shopping-cart,.button-pay-mode
    {
        display: none;
    }
    #pay_pay_order_c_pay_order_c_pay button.btn.btn-default,#pay_order_shopping_cart button.btn.btn-default
    {
        background: #00C6D7;
        color: #fff;
        font-size: 1em;
        outline: none;
        border: none;
        width: 92px;
        height: 42px;
        margin-right: 20px;
    }
    #pay_pay_order_c_pay_order_c_pay i,#pay_order_shopping_cart i
    {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        font: normal normal normal 16px/1 FontAwesome !IMPORTANT;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    .pay-shopping-cart-table ul
    {
        margin-left: 2.5%;
        list-style: none;
    }
    .pay-shopping-cart-table ul li
    {
        float: left;
        text-align: center;
        font-size: 16px;
        height: 40px;
        line-height: 40px;
        border-bottom: 1px dashed #e5e5e5;
    }
    .pay-shopping-cart-table ul li.name
    {
        width: 35%;
        text-align: left;
    }
    .pay-shopping-cart-table ul li.price
    {
        width: 15%;
    }
    .pay-shopping-cart-table ul li.number
    {
        width: 25%;
    }
    .pay-shopping-cart-table ul li.ord_tol
    {
        width: 20%;
        text-align: right;
    }
    .pay-shopping-cart-table ul.title li
    {
        font-weight: bold;
        border-bottom: 1px solid #000;
    }
    .pay-shopping-cart-table ul.foot li
    {
        border-bottom: 1px solid #000;
    }
    .pay-shopping-cart-table ul li.number input
    {
        display: inline-block;
    }
    .pay-shopping-cart-table ul li.number input[type="text"]
    {
        /*float: left;*/
        width: 60px;
        height: 30px;
        padding: 5px;
        border: 1px solid #eee;
        font-size: 14px;
    }
    .pay-shopping-cart-table ul li.number input[type="button"]
    {
        /*float: left;*/
        width: 30px;
        height: 30px;
        line-height: 28px;
        border: 1px solid #eee;
    }
    .pay-shopping-settle-accounts
    {
        text-align: right;
        height: 60px;
        /*line-height: 50px;*/
        margin-top: 20px;
        background-color: #eee;
        width: 92.5%;
        margin-left: 2.5%;
    }
    .pay-shopping-settle-accounts div
    {
        height: 30px;
        line-height: 30px;
    }
    .pay-shopping-settle-accounts .span-pay-shopping-price
    {
        font-weight: bold;
        font-size: 16px;
    }

    .clear
    {
        clear: both;
    }
    .ord_type
    {
        display: none;
    }
</style>
<div class="modal fade" id="pay_order_shopping_cart" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">购物车</h2>
            </div>
            <div class="modal-body">
                <!-- 支付失败模板 开始 -->
                <div class="pay-shopping-cart-fail">
                    <h2></h2>
                </div>
                <!-- 支付失败模板 结束 -->
                <!-- 订单列表 开始 -->
                <div class="pay-shopping-cart-table">
                    <ul class="title">
                        <li class="name">订单名称</li>
                        <li class="price">单价</li>
                        <li class="number">数量</li>
                        <li class="ord_tol">小计</li>
                    </ul>
                    <ul>
                        <li class="name">订单名称</li>
                        <li class="price" data-price="120">单价</li>
                        <li class="number">
                            <input type="button" value="—" onclick="">
                            <input type="text" class="shopping-number">
                            <input type="button" value="+" onclick="">
                            <div class="clear"></div>
                        </li>
                        <li class="ord_tol">小计</li>
                    </ul>
                    <div class="clear"></div>
                </div>
                <!-- 订单列表 结束 -->
                <!-- 结算模块 开始 -->
                <div class="pay-shopping-settle-accounts">
                    <div>
                        <span>定金：</span><span class="span-pay-shopping-price pay-shopping-earnest-price"></span>
                    </div>
                    <div>
                        <span>总价：</span><span class="span-pay-shopping-price pay-shopping-total-price"></span>
                    </div>
                </div>
                <!-- 结算模块 结束 -->
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <span>
                    <button type="button" class="btn btn-default pay-shopping-cart" onclick="obj_shopping_cart.recharge(this)">
                        <i class="fa fa-pencil-square-o"> 支付 </i>
                    </button>
                </span>
                <span>
                    <button type="button" class="btn btn-default button-shopping-cart-close" data-dismiss="modal" onclick="obj_shopping_cart.close_shopping_cart_mode()">
                        <i class="fa fa-book"> 关闭 </i>
                    </button>
                </span>
            </div>
        </div>
    </div>
</div>
<button type="button" class="button-shopping-cart" onclick="obj_shopping_cart.init_pay()" data-toggle="modal" data-target="#pay_order_shopping_cart"></button>
<button type="button" class="button-pay-mode" onclick="" data-toggle="modal" data-target="#pay_pay_order_c_pay_order_c_pay"></button>

<?php include_once dirname(__FILE__) . '/c_pay.php'; ?>
