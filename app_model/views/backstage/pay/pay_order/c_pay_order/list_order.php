<?php
/**
  * Use：充值列表
  * Author：kan.yang@starcor.cn
  * DateTime：18-12-27 下午10:08
  * Description：
*/

//POST请求地址
$str_query_url = !empty($arr_page_url['list_url']) ? $arr_page_url['list_url'] : rtrim($this->config->config['base_url'],'/') . '/backstage/pay/pay_order/c_channel/list_partner';
//获取Controller层数据
$arr_partner_list = array();
if(isset($ret) && $ret == 0 && !empty($data_info))
{
    $arr_partner_list = $data_info;
}


?>
<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
    <!--    自定义CSS-->
    <style type="text/css">
        .pay-order-label
        {
            padding-top: 0 !important;
            padding-right: 0 !important;
            font-size: 14px;!important;
            /*text-align: right!important;*/
        }
        .pay-order-select
        {
            padding: 0!important;
            border-radius: 4px!important;
        }
        .pay-order-excision
        {
            padding: 0 !important;
            text-align: center;
            width: auto !important;
        }
        .form_datetime
        {
            float: left !important;
            padding: 0 15px !important;
        }
    </style>

</head>

<script type="text/javascript">

</script>
<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li><a href="#">支付管理</a></li>
            <li class="active">支付订单列表</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="<?php echo $str_query_url;?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">订单名称:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_order_name" id="focusedinput" value="<?php
                            echo empty($arr_params['cms_order_name']) ? '' : $arr_params['cms_order_name'];
                        ?>"/>
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">支付方式:</label>
                    <div class="col-sm-3">
                        <select class="form-control pay-order-select" name="cms_pay_channel_mode">
                            <option value="">全部</option>
                            <?php
                                foreach ($this->config->config['system_pay_channel_mode_type'] as $key => $value)
                                {
                                    $str = isset($arr_params['cms_pay_channel_mode']) && strlen($arr_params['cms_pay_channel_mode']) > 0 && $arr_params['cms_pay_channel_mode'] == $key ? 'selected' : '';
                                    echo '<option value="' . $key . '" ' . $str . '>' . $value . '</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">订单类型:</label>
                    <div class="col-sm-3">
                        <select class="form-control pay-order-select" name="cms_order_type">
                            <option value="" <?php if (!isset($arr_params['cms_order_type']) || empty($arr_params['cms_order_type'])) echo 'selected';?>>全部</option>
                            <option value="0" <?php if (isset($arr_params['cms_order_type']) && strlen($arr_params['cms_order_type']) > 0 && $arr_params['cms_order_type'] == '0') echo 'selected';?>>批量订单</option>
                            <option value="1" <?php if ($arr_params['cms_order_type'] == '1') echo 'selected';?>>面料小样订单</option>
                            <option value="2" <?php if ($arr_params['cms_order_type'] == '2') echo 'selected';?>>样板订单</option>
                            <option value="3" <?php if ($arr_params['cms_order_type'] == '3') echo 'selected';?>>样衣订单</option>
                            <option value="4" <?php if ($arr_params['cms_order_type'] == '4') echo 'selected';?>>稀缺面料定金订单</option>
<!--                            <option value="5" --><?php //if ($arr_params['cms_order_type'] == '5') echo 'selected';?><!-->充值订单</option>-->
                        </select>
                    </div>
                    <div class="col-sm-12"><br/></div>
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">订单金额(元):</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_order_price" id="focusedinput" value="<?php
                        echo empty($arr_params['cms_order_price']) ? '' : $arr_params['cms_order_price'];?>"/>
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">时间范围:</label>
                    <div class="input-group date form_datetime col-sm-3" data-date="" data-link-field="cms_start_time">
                        <input class="form-control" style="background-color: white!important;" type="text" value="<?php
                        echo empty($arr_params['cms_start_time']) ? '' : $arr_params['cms_start_time'];
                        ?>" readonly/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="text" hidden id="cms_start_time" name="cms_start_time" value="">
                    <label class="col-sm-1 control-label pay-order-excision">-</label>
                    <div class="input-group date form_datetime col-sm-3" data-date="" data-link-field="cms_end_time">
                        <input class="form-control" style="background-color: white!important;" type="text" value="<?php
                        echo empty($arr_params['cms_end_time']) ? '' : $arr_params['cms_end_time'];
                        ?>" readonly/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="text" hidden id="cms_end_time" name="cms_end_time" value="" />
                    <!--<div class="col-sm-4"><br/></div>-->
                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list_search">
                            <i class="fa fa-search">查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
                    <th>订单名称</th>
                    <th>订单类型</th>
                    <!--<th>支付渠道</th>-->
                    <th>支付方式</th>
                    <th>订单金额</th>
                    <th>已支付金额</th>
                    <th>订单状态</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arr_partner_list as $k => $val) { ?>
                    <tr class='odd selected'>
                        <td><input type="checkbox" name="checkItem" attr-key="cms_id" value="<?php echo $val['cms_id'];?>"/></td>
                        <td><?php echo $val['cms_order_name'];?></td>
                        <td><?php
                            if($val['cms_order_type'] == '0')
                            {
                                echo '批量订单';
                            }
                            elseif($val['cms_order_type'] == '1')
                            {
                                echo '面料小样订单';
                            }
                            elseif($val['cms_order_type'] == '2')
                            {
                                echo '样板订单';
                            }
                            elseif($val['cms_order_type'] == '3')
                            {
                                echo '样衣订单';
                            }
                            elseif($val['cms_order_type'] == '4')
                            {
                                echo '稀缺面料定金订单';
                            }
                            elseif($val['cms_order_type'] == '5')
                            {
                                echo '充值订单';
                            }
                            else
                            {
                                echo '未知订单类型';
                            }
                        ?></td>
                        <!--<td>--><?php //echo $this->config->config['system_pay_channel'][$val['cms_pay_channel_id']]; ?><!--</td>-->
                        <td><?php echo $this->config->config['system_pay_channel_mode_type'][$val['cms_pay_channel_mode']]; ?></td>
                        <td><?php echo $val['cms_order_price'] . '/元'; ?></td>
                        <td><?php echo $val['cms_order_price_payed'] . '/元'; ?></td>
                        <td>
                            <?php if($val['cms_order_state'] == 1)
                            {
                                echo "<font color='green'>已支付</font>";
                            }
                            elseif($val['cms_order_state'] == 2)
                            {
                                echo "<font color='yellow'>已取消</font>";
                            }
                            elseif($val['cms_order_state'] == 3)
                            {
                                echo "<font color='yellow'>已过期</font>";
                            }
                            elseif($val['cms_order_state'] == 4)
                            {
                                echo "<font color='blue'>退款中</font>";
                            }
                            elseif($val['cms_order_state'] == 5)
                            {
                                echo "<font color='blue'>已退订</font>";
                            }
                            elseif($val['cms_order_state'] == 6)
                            {
                                echo "<font color='red'>异常订单</font>";
                            }
                            else
                            {
                                echo "<font color='red'>未支付</font>";
                            }
                            ?>
                        </td>
                        <td><?php echo $val['cms_modify_time'];?></td>
                        <td>
                            <?php if(isset($system_file_list) && !empty($system_file_list)) {?>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!-- *********** 下拉操作信息 *********** -->
                                        <?php
                                            if($val['cms_business_state'] != 1)
                                            {
                                                echo '<li>'.
                                                        '<a onclick="obj_pay_order_logic.pay_master_order(\''.base64_encode(json_encode($val)).'\')" href="#" data-toggle="modal" data-target="#pay_pay_order_c_pay_order_c_pay"><i class="fa fa-pencil-square-o icon_9" ></i>支付</a>'.
                                                    '</li>';
                                            }
                                        ?>
                                        <?php echo make_right_button($system_file_list, $val);?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_page.php';?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/public_bottom_button.php';?>
    </div>
</body>
</html>


 