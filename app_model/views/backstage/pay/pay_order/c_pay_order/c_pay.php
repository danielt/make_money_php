<!-- 二维码 -->
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.qrcode.min.js"></script>

<script type="text/javascript">
    //初始化参数
    var public_order_data = window.public_order_data || {};
    var obj_pay_order_logic = window.obj_pay_order_logic || {};
    obj_pay_order_logic.base_url = '<?= $this->config->config['base_url']; ?>';
    obj_pay_order_logic.obj_pay_msg = {
        pam_err:'错误：参数异常，请重试',
        ord_suc:'恭喜您，订单支付成功',
        ord_rec:'恭喜您，充值成功',
        mon_def:'警告：您的余额不足，请充值',
        sys_err:'错误：服务器异常，请联系管理员',
        sys_ing:'正在加载，请求稍后……',
        ord_hel_err:'错误：系统未配置任何支付渠道',
        ord_hel_mo_err:'错误：系统未配置任何支付方式',
        ord_qr_err:'错误：获取支付二维码失败'
    };
    obj_pay_order_logic.b = new Base64();

    //初始化入口函数
    function init_pay_pay_order_c_pay_order_c_pay(str_class,paramas,str_order_data)
    {
        //隐藏所有模块数据
        $('button.pay-order-again').hide();
        $('button.pay-recharge').hide();
        $('div.pay-order-success').hide();
        $('div.pay-order-fail').hide();
        $('div.pay-recharge-mode').hide();
        $('div.pay-order-ing').hide();
        $('h2.modal-title').html('订单支付');
        if(str_order_data.length < 1)
        {
            $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.pam_err);
            return false;
        }
        //加载中……
        $('div.pay-order-ing').html(obj_pay_order_logic.obj_pay_msg.sys_ing).show();
        //支付

        str_order_data = obj_pay_order_logic.b.decode(str_order_data);
        str_order_data = eval("("+str_order_data+")");
        public_order_data.str_class = str_class;
        public_order_data.paramas   = paramas;
        public_order_data.order_data= str_order_data;
        //异步请求支付

        if(public_order_data.order_data.cms_order_type == 5)
        {
            $('h2.modal-title').html('充值');
            var recharge = $('button.pay-recharge');
            recharge.click();
        }
        else
        {
            var str_url = obj_pay_order_logic.base_url + '/backstage/pay/pay_order/c_pay_order/c_pay',
                data = {
                'cms_user_id':public_order_data.order_data.cms_user_id,
                'cms_order_price':public_order_data.order_data.cms_order_price,
                'cms_order_type':public_order_data.order_data.cms_order_type,
                'cms_pay_order_code':public_order_data.order_data.cms_pay_order_code,
                'cms_order_id':public_order_data.order_data.cms_order_id,
                'cms_order_price_payed':public_order_data.order_data.cms_order_price_payed,
                'cms_order_parent':public_order_data.order_data.cms_order_parent
            };
            obj_pay_order_logic.ajax(str_url,'post',data,function(response){
                $('div.pay-order-ing').hide();
                if(response.ret == 0)
                {
                    $('div.pay-order-success').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.ord_suc);
                }
                else
                {
                    public_order_data.order_data.cms_order_id = response.data_info.cms_uuid;
                    $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.mon_def);
                    $('button.pay-recharge').show();
                }
            },function(a,b,c){
                $('div.pay-order-ing').hide();
                $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.sys_err);
            });
        }
        return true;
    }

    //充值
    obj_pay_order_logic.recharge = function(that)
    {
        $(that).hide();
        $('div.pay-recharge-channel').show();
        $('div.pay-recharge-qrcode').hide();
        $('h2.modal-title').html('充值');
        $('div.pay-order-fail').hide();
        $('div.pay-order-ing').show();
        //查询支付商户
        var url = obj_pay_order_logic.base_url + '/backstage/pay/pay_order/c_channel/item_partner?cms_status=1&cms_user_id=' + public_order_data.order_data.cms_user_id + '&cms_system_partner=1&flag_ajax_reurn=1';
        obj_pay_order_logic.ajax(url,'get',null,function(response){

            if(response.ret != 0 || response.data_info.cms_id.length < 1)
            {
                $('div.pay-order-ing').hide();
                $('div.pay-order-fail').html(obj_pay_order_logic.obj_pay_msg.ord_hel_err);
            }
            else
            {
                var partner_id = response.data_info.cms_id;
                url = obj_pay_order_logic.base_url + '/backstage/pay/pay_order/c_channel/list_channel?cms_system_partner=1&flag_ajax_reurn=1&cms_status=1&cms_partner_id=' + partner_id;
                obj_pay_order_logic.ajax(url,'get',null,function(response){
                    $('div.pay-order-ing').hide();
                    if(response.ret != 0 || response.data_info.length < 1)
                    {
                        $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.ord_hel_err);
                    }
                    else
                    {
                        $('div.pay-recharge-mode').show();
                        $('input#cms_order_price').val(public_order_data.order_data.cms_order_price_payed);
                        var pay_html = '';
                        for(var i=0;i<response.data_info.length;i++)
                        {
                            var platform_name = platform_img = platform_class = channel_name ='',
                                platform = response.data_info[i];
                            if(platform.cms_platform_id == 1)
                            {
                                channel_name = '微信';
                                platform_class = 'pay-recharge-wx';
                                platform_name= platform.cms_platform_name ? platform.cms_platform_name : '微信支付';
                                platform_img = platform.cms_platform_img ? platform.cms_platform_img : '../../../../../view_model/backstage/images/wechat.jpg';
                            }
                            else if(platform.cms_platform_id == 2)
                            {
                                channel_name = '支付宝';
                                platform_class = 'pay-recharge-alipay';
                                platform_name= platform.cms_platform_name ? platform.cms_platform_name : '支付宝支付';
                                platform_img = platform.cms_platform_img ? platform.cms_platform_img : '../../../../../view_model/backstage/images/alipay.png';
                            }
                            pay_html +=
                            '<div class="pay-recharge-channel-item ' + platform_class + ' pay-order-display-inline">'+
                                '<a href="javascript:;" onclick="obj_pay_order_logic.recharge_pay(' + platform.cms_id + ','+ partner_id +',\''+channel_name+'\')">'+
                                    '<img src="' + platform_img + '" title="' + platform_name + '"/>'+
                                '</a>'+
                            '</div>';
                        }
                        $('div.pay-recharge-channel-item').remove();
                        $('div.pay-recharge-channel').html(pay_html);
                    }
                },function(a,b,c){
                    $('div.pay-order-ing').hide();
                    $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.sys_err);
                });
            }
        },function(a,b,c){
            $('div.pay-order-ing').hide();
            $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.sys_err);
        });
    };

    //支付
    obj_pay_order_logic.recharge_pay = function(int_pay_type,partner_id,channel_name)
    {
        var p = obj_pay_order_logic.pay_price($('input#cms_order_price'));
        if(!p)
        {
            return false;
        }
        var timeout = $('div.pay-recharge-qrcode-timeout').hide();
        //加载支付模式
        url = obj_pay_order_logic.base_url + '/backstage/pay/pay_order/c_channel/item_channel_mode?cms_system_partner=1&flag_ajax_reurn=1&cms_status=1&cms_channel_id=' + int_pay_type + '&cms_channel_mode_flag=1';
        obj_pay_order_logic.ajax(url,'get',null,function(response){
            if(response.ret != 0 || !response.data_info)
            {
                $('div.pay-recharge-mode').hide();
                $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.ord_hel_mo_err).show();
            }
            else
            {
                //获取支付二维码
                url = obj_pay_order_logic.base_url + '/backstage/pay/pay_order/c_recharge/recharge';
                var data = {
                    'cms_user_id': public_order_data.order_data.cms_user_id,
                    'cms_order_price': p,
                    'cms_order_type': 5,
                    'cms_pay_partner_id': partner_id,
                    'cms_pay_channel_id': int_pay_type,
                    'cms_pay_channel_mode_id': response.data_info['cms_id'],
                    'cms_order_id': public_order_data.order_data.cms_recharge_order_id,
                    'cms_order_price_payed': p
                };
                obj_pay_order_logic.ajax(url,'post',data,function(response){
                    if(response.ret != 0 || !response.qr_url)
                    {
                        $('div.pay-recharge-mode').hide();
                        $('div.pay-order-fail').show().find('h2').html(response.reason ? response.reason : obj_pay_order_logic.obj_pay_msg.ord_qr_err);
                    }
                    else
                    {
                        $('div.pay-recharge-channel').hide();
                        $('div.pay-recharge-qrcode').show();
                        var qrcode = $('div#pay-recharge-qrcode-content');
                        qrcode.find('canvas').remove();
                        qrcode.qrcode(response.qr_url);
                        //重置订单ID
                        public_order_data.order_data.cms_recharge_order_id = response.cms_order_id;
                        //开启定时器
                        var int_max_time = response.cms_expire_time;
                        $('span.pay-recharge-qrcode-channel-name').html(channel_name);
                        $('span.pay-recharge-qrcode-time-interval').html(obj_pay_order_logic.format_seconds(int_max_time));
                        var timeout_func = function(int_max_time)
                        {
                            if(int_max_time <= 0)
                            {
                                timeout.show().find('input').attr('onclick','obj_pay_order_logic.recharge_pay('+int_pay_type+','+partner_id+',"'+channel_name+'")');
                                obj_pay_order_logic.close();
                            }
                        };
                        obj_pay_order_logic.interval = setInterval(function(){
                            -- int_max_time;
                            $('span.pay-recharge-qrcode-time-interval').html(obj_pay_order_logic.format_seconds(int_max_time));
                            //间隔3秒请求一次订单状态
                            if(int_max_time % 3 == 0)
                            {
                                var url = obj_pay_order_logic.base_url + '/backstage/pay/pay_order/c_recharge/poll?cms_order_id=' + response.cms_order_id;
                                obj_pay_order_logic.ajax(url,'get',null,function(data)
                                {
                                    if(data.ret == 310001)
                                    {
                                        int_max_time = 0;
                                        //支付成功后逻辑处理
                                        $('div.pay-recharge-mode').hide();
                                        $('div.pay-order-success').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.ord_rec);
                                        public_order_data.order_data.cms_recharge_order_id = '';
                                        if(public_order_data.order_data.cms_order_id)
                                        {
                                            var order_data = obj_pay_order_logic.b.encode(JSON.stringify(public_order_data.order_data));
                                            $('button.pay-order-again').attr('onclick','init_pay_pay_order_c_pay_order_c_pay("'+public_order_data.str_class+'","'+public_order_data.paramas+'","'+order_data+'")').show();
                                        }
                                    }
                                    //清除定时器
                                    //timeout_func(int_max_time);
                                },function(a, b, c){});
                            }
                            //清除定时器
                            timeout_func(int_max_time);
                        },1000);
                    }
                },function(a,b,c){
                    $('div.pay-recharge-mode').hide();
                    $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.sys_err).show();
                });
            }
        },function(a,b,c){
            $('div.pay-recharge-mode').hide();
            $('div.pay-order-fail').show().find('h2').html(obj_pay_order_logic.obj_pay_msg.sys_err).show();
        });

        return true;
    };

    //支付订单
    obj_pay_order_logic.pay_order = function(data)
    {
        var order_data = JSON.stringify({
            'cms_user_id':data.cms_user_id,//用户ID
            'cms_order_price':data.cms_order_price,//订单价格
            'cms_order_price_payed':data.cms_order_price_payed,//支付金额
            'cms_order_type':data.cms_order_type,//订单类型。0批量订单；1面料小样；2样板订单；3样衣订单；4稀缺面料定金订单；5充值订单
            'cms_pay_order_code':data.cms_pay_order_code,//购买订单ID
            'cms_order_parent':data.cms_order_parent,//父级订单ID
            'cms_order_id':data.cms_uuid//订单ID，支付未完成订单上报
        });
        $('div.pay-order-data').attr('data-order',order_data);
        system_auto_load('pay_pay_order_c_pay_order_c_pay','');
    };

    //支付主订单尾款
    obj_pay_order_logic.pay_master_order = function(data)
    {
        var b = new Base64();
        data = b.decode(data);
        data =  eval("("+data+")");
        var order_data = JSON.stringify({
            'cms_user_id':data.cms_user_id,//用户ID
            'cms_order_price':data.cms_order_price,//订单价格
            'cms_order_price_payed':data.cms_pay_order_unpaid_price,//支付金额
            'cms_order_type':data.cms_order_type,//订单类型。0批量订单；1面料小样；2样板订单；3样衣订单；4稀缺面料定金订单；5充值订单
            'cms_pay_order_code':data.cms_pay_order_code,//购买订单ID
            'cms_order_parent':data.cms_order_parent,//父级订单ID
            'cms_order_id':data.cms_uuid//订单ID，支付未完成订单上报
        });
        $('div.pay-order-data').attr('data-order',order_data);
        system_auto_load('pay_pay_order_c_pay_order_c_pay','');
    };

    //充值订单
    obj_pay_order_logic.recharge_order = function()
    {
        var order_data = JSON.stringify({
            'cms_order_type':5//订单类型。0批量订单；1面料小样；2样板订单；3样衣订单；4稀缺面料定金订单；5充值订单
        });
        $('div.pay-order-data').attr('data-order',order_data);
        system_auto_load('pay_pay_order_c_pay_order_c_pay','');
    };

    //金额处理
    obj_pay_order_logic.pay_price = function(that,int_focus)
    {
        var p    = $(that).val(),
            pre  = /^[0-9]+.?[0-9]*$/;
        if(!int_focus && (!p || !pre.test(p)))
        {
            $(that).css('border','1px solid red');
            return false;
        }
        else
        {
            $(that).css('border','1px solid #ddd');
        }
        return p;
    };

    //格式化时分秒
    obj_pay_order_logic.format_seconds = function(value)
    {
        var secondTime = parseInt(value);
        var minuteTime = 0;
        var hourTime = 0;
        if(secondTime > 60)
        {
            minuteTime = parseInt(secondTime / 60);
            secondTime = parseInt(secondTime % 60);
            if(minuteTime > 60)
            {
                hourTime = parseInt(minuteTime / 60);
                minuteTime = parseInt(minuteTime % 60);
            }
        }
        var result = "" + parseInt(secondTime) + "秒";
        if(minuteTime > 0)
        {
            result = "" + parseInt(minuteTime) + "分" + result;
        }
        if(hourTime > 0)
        {
            result = "" + parseInt(hourTime) + "小时" + result;
        }
        return result;
    };

    //关闭定时器
    obj_pay_order_logic.close = function()
    {
        clearInterval(obj_pay_order_logic.interval);
        obj_pay_order_logic.close_shopping_cart_mode();
    };

    //关闭模态框
    obj_pay_order_logic.close_shopping_cart_mode = function()
    {
        $('#pay_pay_order_c_pay_order_c_pay').modal('hide');
        window.location.reload();
    };

    //异步请求
    obj_pay_order_logic.ajax = function(str_url,type,data,success,error)
    {
        $.ajax({
            'url':str_url,
            'type':type,
            'data':data,
            'dataType':'json',
            'success':function(response) { success(response); },
            'error':function(a,b,c) { error(a,b,c); }
        });
    };

    //加载完成后触发
    $(function(){

        //充值金额
        $('input#cms_order_price').focus(function(){
            obj_pay_order_logic.pay_price(this,1);
        }).blur(function(){
            obj_pay_order_logic.pay_price(this);
        });

    });
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){
    if($button_data_value['bottom']){ continue; }
    $str_action = "system_auto_load('".$system_file_list_value['class']."','".$button_data_value['params']."')";
    if($button_data_value['self_action'])
    {
        $str_action = $button_data_value['self_action'];
    }
    ?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="<?= $str_action; ?>">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<style type="text/css">
    .pay-channel-platform-type input
    {
        margin-right: 5px;
        cursor: pointer;
    }
    .pay-order-ing,.pay-order-success,.pay-order-fail,.pay-recharge-mode,.pay-recharge-qrcode
    {
        text-align: center;
        display: none;
    }
    .pay-order-line
    {
        border-top: 1px solid #e5e5e5;
    }
    .pay-order-float-left
    {
        float: left;
    }
    .pay-order-float-right
    {
        float: right
    }
    .pay-order-float-clear
    {
        clear: both;
    }
    .pay-order-display-inline
    {
        display: inline-block;
    }
    .pay-recharge-mode
    {
        margin-right: 20px;
    }
    .pay-recharge-mode .input-group
    {
        width: 100%;
    }
    .pay-recharge-mode label
    {
        padding-right: 0;
    }
    .pay-recharge-mode .input-group input
    {
        padding: 7px 5px;
    }
    .pay-recharge-channel img
    {
        margin-right: 50px;
    }
    .pay-recharge-channel .pay-recharge-alipay img
    {
        width: 105px;
    }
    .pay-order-tip
    {
        display: none;
        color: red;
    }
    .pay-recharge-qrcode-time
    {
        padding: 10px 0;
    }
    .pay-recharge-qrcode-time a
    {
        margin-left: 10px;
    }
    .pay-recharge-qrcode-time-interval
    {
        font-weight: bold;
        color: red;
    }
    .pay-recharge-qrcode-canvas
    {
        padding: 20px 0;
        background-color: #f6f8f9;
        position: relative;
    }
    #pay-recharge-qrcode-content canvas
    {
        background-color: #fff;
        padding: 20px;
        width: 256px;
    }
    .pay-recharge-qrcode-timeout
    {
        display: none;
        position: absolute;
        background-color: #ddd;
        padding: 20px;
        width: 256px;
        height: 256px;
        left: 35%;
        top: 6.6%;
        opacity: 0.95;
        filter: alpha(opacity=95);
    }
    .pay-recharge-qrcode-timeout p
    {
        margin-top: 35%;
        font-weight: bold;
    }
    .pay-recharge-qrcode-timeout input
    {
        color: #fff;
        border: 1px solid #ffffff;
        width: 50%;
        background-color: red;
        border-radius: 4px;
        outline:none;
    }
    .pay-recharge-qrcode-channel-name
    {
        margin: 0 5px;
        color: red;
    }
    .pay-recharge-price-text
    {
        margin-top: 10px;
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 400;
        font-size: 16px;
        color: #777;
        font-family: 'Roboto', sans-serif;
    }
    .pay-recharge-price-group
    {
        padding-bottom: 1em;
        font-family: 'Roboto', sans-serif;
        font-size: 100%;
    }
    .pay-recharge-price-group input
    {
        float: left;
        border: 1px solid #ddd;
        padding: 5px 8px;
        color: rgb(119, 119, 119);
        background: rgb(255, 255, 255);
        width: 100%;
        font-size: 14px;
        font-weight: 300;
        height: 40px;
        border-radius: 0;
        font-family: Roboto, sans-serif;
        -webkit-appearance: none;
        resize: none;
        outline: none;
        margin-bottom: 10px;
        box-shadow: none !important;
        line-height: inherit;
    }
</style>
<div class="modal fade" id="pay_pay_order_c_pay_order_c_pay" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" onclick="obj_pay_order_logic.close_shopping_cart_mode()">×</button>
                <h2 class="modal-title">订单支付</h2>
            </div>
            <div class="modal-body">
                <!-- 支付中模板 开始 -->
                <div class="pay-order-ing">
                    <p>正在加载，请求稍后……</p>
                </div>
                <!-- 支付中模板 结束 -->
                <!-- 支付成功模板 开始 -->
                    <div class="pay-order-success">
                        <h2>恭喜您，订单支付成功</h2>
                    </div>
                <!-- 支付成功模板 结束 -->

                <!-- 支付失败模板 开始 -->
                <div class="pay-order-fail">
                    <h2>支付失败，您的余额不足，请求充值</h2>
                </div>
                <!-- 支付失败模板 结束 -->
                <!-- 充值模板 开始 -->
                <div class="pay-recharge-mode">
                    <div class="">
                        <label class="col-md-2 control-label pay-recharge-price-text">充值金额</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right pay-recharge-price-group">
                                <input id="cms_order_price" name="cms_order_price" class="form-control1 icon" type="text" value="" placeholder=""/>
                            </div>
                        </div>
                        <div class="pay-order-float-clear"></div>
                    </div>
                    <div class="pay-order-line"></div>
                    <div class="pay-recharge-channel"></div>
                    <div class="pay-recharge-qrcode">
                        <div class="pay-recharge-qrcode-time">
                            请使用<span class="pay-recharge-qrcode-channel-name"></span>扫描下面二维码完成支付，剩余时间  <span class="pay-recharge-qrcode-time-interval"></span>
                        </div>
                        <div class="pay-recharge-qrcode-canvas">
                            <div id="pay-recharge-qrcode-content"></div>
                            <div class="pay-recharge-qrcode-timeout">
                                <p>二维码已失效</p>
                                <input type="button" value="点击刷新"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 充值模板 结束 -->
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <span>
                    <button type="button" class="btn btn-default pay-order-again">
                        <i class="fa fa-pencil-square-o"> 再次支付 </i>
                    </button>
                </span>
                <span>
                    <button type="button" class="btn btn-default pay-recharge" onclick="obj_pay_order_logic.recharge(this)">
                        <i class="fa fa-pencil-square-o"> 充值 </i>
                    </button>
                </span>
                <span>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="obj_pay_order_logic.close()">
                        <i class="fa fa-book"> 关闭 </i>
                    </button>
                </span>
            </div>
            <div class="pay-order-data" data-order=""></div>
        </div>
    </div>
</div>