<script type="text/javascript">
    function init_<?php echo $system_file_list_value['class'];?>(str_class,obj_params)
    {
        //console.info(12321);
    }
    $(document).ready(function() {
        $('#<?php echo $system_file_list_value['class'];?>-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

            }
        });
    });
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){
    if($button_data_value['bottom']){ continue; }
    ?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<style type="text/css">
    .pay-channel-platform-type
    {
        margin-right: 20px;
    }
    .pay-channel-platform-type input
    {
        margin-right: 5px;

    }
</style>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content animated bounceInTop">
			<div class="modal-header">
				<button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
				<h2 class="modal-title">添加支付渠道</h2>
			</div>
			<div class="modal-body">
                <form id="<?php echo $system_file_list_value['class'];?>-form" method="post" action="" >
                    <input id="cms_user_id" name="cms_user_id" type="hidden"/>
                    <input id="cms_partner_id" name="cms_partner_id" type="hidden"/>
                	<div class="form-group">
                		<label class="col-md-2 control-label">支付渠道名称</label>
                		<div class="col-md-10">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_name" name="cms_name" class="form-control1 icon" type="text" value="" placeholder="">
                			</div>
                		</div>
                	</div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">支付平台</label>
                        <div class="col-md-10">
                            <div class="input-group input-icon right">
                                <?php foreach($this->config->config['system_pay_channel'] as $k => $v) { ?>
                                    <label class="pay-channel-platform-type"><input type="radio" value="<?= $k; ?>" name="cms_platform_id" /><?= $v ;?></label>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                	<div class="form-group">
                		<label class="col-md-2 control-label">支付平台名称</label>
                		<div class="col-md-10">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-phone"></i>
                				</span> <input id="cms_platform_name" name="cms_platform_name" class="form-control1 icon" type="text" value="" placeholder="">
                			</div>
                		</div>
                	</div>
                	<div class="form-group">
                		<label class="col-md-2 control-label">描述</label>
                		<div class="col-md-10">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-book"></i>
                				</span>
                				<textarea name="cms_desc" name="cms_desc" id="txtarea1" cols="50" rows="4" class="form-control"></textarea>
                			</div>
                		</div>
                	</div>
                </form>
			</div>
			<div class="clearfix"></div>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="sys_sweetalert('submit','<?php echo $system_file_list_value['class'];?>','您确定要添加这条信息吗','提交数据后系统将可能会添加此条数据，请谨慎操作！','<?php echo $system_file_list_value['ajax'];?>','',true);">
                        	   <i class="fa fa-book"> 提交 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
        		</div>
            </div>
		</div>
	</div>
</div>