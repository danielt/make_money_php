<script type="text/javascript">
    $(document).ready(function() {
        $('#<?php echo $system_file_list_value['class'];?>-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

            }
        });
    });
    //初始化入口函数
    function init_<?php echo $system_file_list_value['class'];?>(str_class,paramas)
    {
        //0.初始化fileinput
        var oFileInput = new FileInput();
        oFileInput.Init('add',"image_data_add_1",'image_data_add_value_1',paramas);
        oFileInput.Init('add',"image_data_add_2",'image_data_add_value_2',paramas);
    }
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){
    if($button_data_value['bottom']){ continue; }
    ?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<style type="text/css">
    .pay-channel-platform-type
    {
        margin-right: 20px;
    }
    .pay-channel-platform-type input
    {
        margin-right: 5px;
        cursor: pointer;
    }
    .pay-channel-mode-credential
    {
        border: none;
    }
    .clear
    {
        clear: both;
    }
</style>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" >
		<div class="modal-content animated bounceInTop">
			<div class="modal-header">
				<button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
				<h2 class="modal-title">添加支付方式</h2>
			</div>
			<div class="modal-body">
                <form id="<?php echo $system_file_list_value['class'];?>-form" method="post" action="" >
                    <input id="cms_user_id" name="cms_user_id" type="hidden"/>
                    <input id="cms_channel_id" name="cms_channel_id" type="hidden"/>
                	<div class="form-group">
                		<label class="col-md-2 control-label">支付方式名称</label>
                		<div class="col-md-9">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_pay_mode_name" name="cms_pay_mode_name" class="form-control1 icon" type="text" value="" placeholder="">
                			</div>
                		</div>
                        <div class="clear"></div>
                	</div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">支付方式类型</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                                <?php foreach($this->config->config['system_pay_channel_mode_flag'] as $k => $v) { ?>
                                    <label class="pay-channel-platform-flag"><input type="radio" value="<?= $k ;?>" name="cms_channel_mode_flag" /><?= $v ;?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                	<div class="form-group">
                		<label class="col-md-2 control-label">支付平台APP ID</label>
                		<div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_pay_appid" name="cms_pay_appid" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                		</div>
                        <div class="clear"></div>
                	</div>
                	<div class="form-group">
                		<label class="col-md-2 control-label">商户ID</label>
                		<div class="col-md-9">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_pay_partner_id" name="cms_pay_partner_id" class="form-control1 icon" type="text" value="" placeholder="">
                			</div>
                		</div>
                        <div class="clear"></div>
                	</div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">商户KEY</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_pay_partner_key" name="cms_pay_partner_key" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">商户名称</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_pay_partner" name="cms_pay_partner" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">密钥证书(公钥)</label>
                        <div class="col-md-9">
                            <input type="file" id="image_data_add_1" name="image_data" accept="">
                        </div>
                        <div id="image_data_add_value_1" post_name="cms_public_key" style="display: none;"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">密钥证书(私钥)</label>
                        <div class="col-md-9">
                            <input type="file" id="image_data_add_2" name="image_data" accept="">
                        </div>
                        <div id="image_data_add_value_2" post_name="cms_private_key" style="display: none;"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">二维码过期时间</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_qr_expire_time" name="cms_qr_expire_time" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">签名方式</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                                <?php foreach($this->config->config['system_pay_alipay_sign_type'] as $k => $v) { ?>
                                    <label class="pay-channel-platform-type"><input type="radio" value="<?= $k ;?>" name="cms_sign_type" /><?= $v ;?></label>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">异步通知地址</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_notify_url" name="cms_notify_url" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">输入字符集</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_input_charset" name="cms_input_charset" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">传输协议</label>
                        <div class="col-md-9">
                            <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-file-text-o"></i>
                				</span> <input id="cms_transport" name="cms_transport" class="form-control1 icon" type="text" value="" placeholder="">
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                	<div class="form-group">
                		<label class="col-md-2 control-label">描述</label>
                		<div class="col-md-9">
                			<div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-book"></i>
                				</span>
                				<textarea name="cms_desc" name="cms_desc" id="txtarea1" cols="50" rows="4" class="form-control"></textarea>
                			</div>
                		</div>
                        <div class="clear"></div>
                	</div>
                </form>
			</div>
			<div class="clearfix"></div>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="sys_sweetalert('submit','<?php echo $system_file_list_value['class'];?>','您确定要添加这条信息吗','提交数据后系统将可能会添加此条数据，请谨慎操作！','<?php echo $system_file_list_value['ajax'];?>','',true);">
                        	   <i class="fa fa-book"> 提交 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
        		</div>
            </div>
		</div>
	</div>
</div>