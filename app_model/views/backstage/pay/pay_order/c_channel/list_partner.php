<?php
/**
  * Use：商户列表
  * Author：kan.yang@starcor.cn
  * DateTime：18-12-25 下午9:08
  * Description：
*/

//POST请求地址
$str_query_url = !empty($arr_page_url['list_url']) ? $arr_page_url['list_url'] : rtrim($this->config->config['base_url'],'/') . '/backstage/pay/pay_order/c_channel/list_partner';
//获取Controller层数据
$arr_partner_list = array();
if(isset($ret) && $ret == 0 && !empty($data_info))
{
    $arr_partner_list = $data_info;
}


?>
<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
    <!--    自定义CSS-->
    <style type="text/css">
        .pay-order-label
        {
            padding-top: 0 !important;
            padding-right: 0 !important;
            font-size: 14px;!important;
        }
        .pay-order-button
        {
            margin: 0 !important;
            padding: 7px 20px !important;
        }
        .pay-order-excision
        {
            padding: 0 !important;
            text-align: center;
            width: auto !important;
        }
        .form_datetime
        {
            float: left !important;
            padding: 0 15px !important;
        }
    </style>

</head>


<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li><a href="#">支付管理</a></li>
            <li class="active">商户管理</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="<?php echo $str_query_url;?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">商户名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_name" id="focusedinput" value="<?php
                            echo empty($arr_params['cms_name']) ? '' : $arr_params['cms_name'];
                        ?>"/>
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">时间范围</label>
                    <div class="input-group date form_datetime col-sm-3" data-date="" data-link-field="cms_start_time">
                        <input class="form-control" type="text" value="<?php
                        echo empty($arr_params['cms_start_time']) ? '' : $arr_params['cms_start_time'];
                        ?>" readonly/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="text" hidden id="cms_start_time" name="cms_start_time" value="<?php
                    echo empty($arr_params['cms_start_time']) ? '' : $arr_params['cms_start_time'];
                    ?>" />
                    <label class="col-sm-1 control-label pay-order-excision">-</label>
                    <div class="input-group date form_datetime col-sm-3" data-date="" data-link-field="cms_end_time">
                        <input class="form-control" type="text" value="<?php
                        echo empty($arr_params['cms_end_time']) ? '' : $arr_params['cms_end_time'];
                        ?>" readonly/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="text" hidden id="cms_end_time" name="cms_end_time" value="<?php
                    echo empty($arr_params['cms_end_time']) ? '' : $arr_params['cms_end_time'];
                    ?>" />
                    <div class="col-sm-1">
                        <button class="btn btn-success pay-order-button" type="button" id="button_query_list_search">
                            <i class="fa fa-search">查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
                    <th>商户名称</th>
                    <th>联系人</th>
                    <th>联系电话</th>
                    <th>邮箱</th>
                    <th>状态</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arr_partner_list as $k => $val) { ?>
                    <tr class='odd selected'>
                        <td><input type="checkbox" name="checkItem" attr-key="cms_id" value="<?php echo $val['cms_id'];?>"/></td>
                        <td><?php echo $val['cms_name'];?></td>
                        <td><?php echo $val['cms_contact'];?></td>
                        <td><?php echo $val['cms_phone'];?></td>
                        <td><?php echo $val['cms_email'];?></td>
                        <td>
                            <?php echo $val['cms_status'] == '0' ? "<font color='red'>禁用</font>" : "<font color='green'>启用</font>";?>
                        </td>
                        <td><?php echo $val['cms_modify_time'];?></td>
                        <td>
                            <?php if(isset($system_file_list) && !empty($system_file_list)) {
                                $val['cms_state'] = $val['cms_status'] == 1 ? 0 : 1;
                            ?>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!-- *********** 下拉操作信息 *********** -->
                                        <?php echo make_right_button($system_file_list, $val);?>
                                        <li>
                                            <a href="./list_channel?cms_user_id=<?php echo $val['cms_user_id']; ?>&cms_partner_id=<?php echo $val['cms_id']; ?>"><i class="fa fa-pencil-square-o icon_9"></i>查看支付渠道</a>
                                        </li>
                                    </ul>
                                </div>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_page.php';?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/public_bottom_button.php';?>
    </div>
</body>
</html>


 