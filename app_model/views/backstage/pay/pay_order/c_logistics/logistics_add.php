<script type="text/javascript">
    $(document).ready(function() {
        $('#<?php echo $system_file_list_value['class'];?>-form').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                cms_name: {
                    group: '.col-lg-4',
                    validators: {
                        notEmpty: {
                            message: '不能为空'
                        },
                        stringLength: {
                            min: 1,
                            max:64,
                            message: '输入字符长度需要在6-30之间'
                        },
                    }
                },
                cms_name: {
                    group: '.col-lg-4',
                    validators: {}
                },
                captcha: {
                    validators: {
                        callback: {
                            message: 'Wrong answer',
                            callback: function(value, validator) {
                                var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        });
    });
</script>
<?php foreach ($system_file_list_value['button_data'] as $key=>$button_data_value){?>
    <button class="btn purple" type="button" data-toggle="modal" data-target="#<?php echo $system_file_list_value['class'];?>" onclick="system_auto_load('<?php echo $system_file_list_value['class'];?>','<?php echo $button_data_value['params'];?>');">
        <i class="fa <?php echo $button_data_value['icon'];?>"> <?php echo $button_data_value['name'];?></i>
    </button>
<?php }?>
<div class="modal fade" id="<?php echo $system_file_list_value['class'];?>" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">添加数据</h2>
            </div>
            <div class="modal-body">
                <form id="<?php echo $system_file_list_value['class'];?>-form" method="post" action="" >
                    <div class="form-group">
                        <label class="col-md-2 control-label">选择订单号</label>
                        <div class="col-sm-10">
                            <div class="input-group input-icon right">
                                <span class="input-group-addon"> <i class="fa fa-file-text-o"></i></span>
                                <input type="text" name="cms_buy_order_id" id="select_checkbox_button_hidden_value" readonly />
                                <button id="buy_order_frame_btn" class="btn purple" type="button" data-toggle="modal" data-target="#pay_pay_order_c_logistics_buy_order" onclick="system_auto_load('pay_pay_order_c_logistics_buy_order','');">
                                    <i class="fa fa-gg">绑定订单号</i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">物流描述:</label>
                        <div class="col-sm-10" style="width:72%;">
                            <div class="input-group input-icon right" style="margin-top: 10px!important;">
                                <div class="input-group input-icon right">
                				<span class="input-group-addon"> <i class="fa fa-book"></i>
                				</span>
                                    <textarea name="cms_desc" id="cms_desc" cols="50" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="sys_sweetalert('submit','<?php echo $system_file_list_value['class'];?>','您确定要添加这条信息吗','提交数据后系统将可能会添加此条数据，请谨慎操作！','<?php echo $system_file_list_value['ajax'];?>','',true);">
                        	   <i class="fa fa-book"> 提交 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="<?php echo $system_file_list_value['class'];?>-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pay_pay_order_c_logistics_buy_order" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content animated bounceInTop">
            <div class="modal-header">
                <button type="button" class="close second" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">选择数据</h2>
            </div>
            <iframe style="width: 100%;height: 435px!important;" name="pay_pay_order_c_logistics_buy_order_frame" id="pay_pay_order_c_logistics_buy_order_frame">

            </iframe>
            <div class="modal-footer">
                <div class="form-body">
                    <span>
                		<button class="btn purple" type="button" onclick="select_order();">
                        	   <i class="fa fa-book"> 确定 </i>
                        </button>
                    </span>
                    <span>
                        <button class="btn purple" type="button" id="pay_pay_order_c_logistics_buy_order-cancel" data-dismiss="modal">
                        	   <i class="fa fa-book"> 取消 </i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#pay_pay_order_c_logistics_buy_order_frame").attr("src","buy_order_frame");
    });
    function check_radio_value()
    {
        var cms_id = '';
        $(window.frames["pay_pay_order_c_logistics_buy_order_frame"].document).find("input[name='checkItem']").each(function(){
            if ($(this).is(':checked') && $(this).val().length >0 && $(this).attr('attr-key').length >0)
            {
                cms_id = $(this).val();
            }
        });
        return cms_id;
    }
    function select_order()
    {
        var radio_value = check_radio_value();
        if(radio_value.length<1)
        {
            sys_sweetalert("error",'',"请选择一条数据",'请选择一条数据！','','','',false);
        }
        $('#pay_pay_order_c_logistics_logistics_add-form') . find("input[name='cms_buy_order_id']").val(radio_value);
        $('#pay_pay_order_c_logistics_buy_order').css({"display":"none"});
    }
</script>