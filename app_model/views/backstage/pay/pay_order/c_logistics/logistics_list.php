<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/10 12:03
 */

?>
<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
    <!--    自定义CSS-->
    <style type="text/css">
        .user-list-label
        {
            padding-top: 0 !important;
            padding-right: 0 !important;
            font-size: 14px;!important;
            /*text-align: right!important;*/
        }
        .user-list-span
        {
            border-radius: 4px!important;
        }
    </style>
</head>
<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li class="active">物流单列表</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="<?php echo $arr_page_url['list_url'];?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">订单号:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_buy_order_id" id="cms_buy_order_id" value="<?php echo isset($arr_params['cms_buy_order_id']) ? $arr_params['cms_buy_order_id'] : '';?>">
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">物流描述:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_desc" id="cms_desc" value="<?php echo isset($arr_params['cms_desc']) ? $arr_params['cms_desc'] : '';?>">
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">创建日期:</label>
                    <div class="col-sm-3">
                        <div class="input-group date form_datetime" style="width: 100%!important;padding-bottom: 0px!important;" data-date="" data-date-format="yyyy-mm-dd hh:ii:ss" data-link-field="cms_create_time" data-link-format="yyyy-mm-dd hh:ii:ss">
                            <input class="form-control" style="background-color: white!important;" type="text" value="<?php echo isset($arr_params['cms_create_time']) ? $arr_params['cms_create_time'] : '';?>" readonly/>
                            <span class="input-group-addon user-list-span"><span class="glyphicon glyphicon-remove"></span></span>
                            <span class="input-group-addon user-list-span"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                        <input type="text" hidden class="form-control1" name="cms_create_time" id="cms_create_time" value="<?php echo isset($arr_params['cms_create_time']) ? $arr_params['cms_create_time'] : '';?>">
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list_search">
                            <i class="fa fa-search" >查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
                    <th>订单号</th>
                    <th>物流信息</th>
                    <th>创建时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php  if(isset($data_info) && is_array($data_info) && !empty($data_info))
                {
                    foreach ($data_info as $val)
                    {
                        ?>
                        <tr class='odd selected'>
                            <td><input type="checkbox" name="checkItem" attr-key="cms_id" value="<?php echo $val['cms_id'];?>"/></td>
                            <td><?php echo $val['cms_buy_order_id'];?></td>
                            <td><?php echo $val['cms_desc'];?></td>

                            <td><?php echo $val['cms_create_time'];?></td>
                            <td><?php echo $val['cms_modify_time'];?></td>
                            <td>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!--*********** 初始化必须加载 ***************** （下拉操作信息） *********** 初始化必须加载 ***************** -->
                                        <?php echo make_right_button($system_file_list, $val);?>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php     }
                }
                ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_page.php';?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/public_bottom_button.php';?>
    </div>
</body>
</html>
