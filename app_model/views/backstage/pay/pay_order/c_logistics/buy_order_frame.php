<?php
/**
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2019/1/10 12:03
 */

?>
<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
    <!--    自定义CSS-->
    <style type="text/css">
        .user-list-label
        {
            padding-top: 0 !important;
            padding-right: 0 !important;
            font-size: 14px;!important;
            /*text-align: right!important;*/
        }
        .user-list-span
        {
            border-radius: 4px!important;
        }
    </style>
</head>
<body>
<div class="outter-wp">
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="buy_order_frame">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">订单号:</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_order_id" id="cms_order_id" value="<?php echo isset($arr_params['cms_order_id']) ? $arr_params['cms_order_id'] : '';?>">
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label user-list-label">订单名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_name" id="cms_name" value="<?php echo isset($arr_params['cms_name']) ? $arr_params['cms_name'] : '';?>">
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-success" type="button" id="button_query_list_search">
                            <i class="fa fa-search" >查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>订单号</th>
                    <th>订单名称</th>
                    <th>创建时间</th>
                </tr>
                </thead>
                <tbody>
                <?php  if(isset($data_info) && is_array($data_info) && !empty($data_info))
                {
                    foreach ($data_info as $val)
                    {
                        ?>
                        <tr class='odd selected'>
                            <td><input type="radio" name="checkItem" attr-key="cms_order_id" value="<?php echo $val['cms_order_id'];?>"/></td>
                            <td><?php echo $val['cms_order_id'];?></td>
                            <td><?php echo $val['cms_name'];?></td>
                            <td><?php echo $val['cms_create_time'];?></td>
                        </tr>
                    <?php     }
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_page.php';?>
    </div>
</body>
</html>