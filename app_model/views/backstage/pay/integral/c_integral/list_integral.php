<?php
/**
  * Use：用户积分列表
  * Author：kan.yang@starcor.cn
  * DateTime：19-01-12 上午02:15
  * Description：
*/

//POST请求地址
$str_query_url = !empty($arr_page_url['list_url']) ? $arr_page_url['list_url'] : rtrim($this->config->config['base_url'],'/') . '/backstage/pay/pay_order/c_channel/list_partner';
//获取Controller层数据
$arr_partner_list = array();
if(isset($ret) && $ret == 0 && !empty($data_info))
{
    $arr_partner_list = $data_info;
}


?>
<!DOCTYPE HTML>
<html>
<head>
    <!-- *********** 初始化必须加载 ***************** （顶部JS加载） *********** 初始化必须加载 ***************** -->
    <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_top_web_file.php';?>
    <!--    自定义CSS-->
    <style type="text/css">
        .pay-order-label
        {
            padding-top: 0 !important;
            padding-right: 0 !important;
            font-size: 14px;!important;
        }
        .pay-order-button
        {
            margin: 0 !important;
            padding: 7px 20px !important;
        }
        .pay-order-excision
        {
            padding: 0 !important;
            text-align: center;
            width: auto !important;
        }
        .form_datetime
        {
            float: left !important;
            padding: 0 15px !important;
        }
    </style>

</head>


<body>
<div class="outter-wp">
    <div class="but_list">
        <ol class="breadcrumb">
            <li><a href="#">积分管理</a></li>
            <li class="active">用户积分</li>
        </ol>
    </div>
    <div class="graph">
        <div class="form-body">
            <form class="form-horizontal_search" method="post" action="<?php echo $str_query_url;?>">
                <div class="form-group">
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">用户名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" name="cms_user_name" id="cms_user_name" value="<?php
                            echo empty($this->arr_params['cms_user_name']) ? '' : $this->arr_params['cms_user_name'];
                        ?>"/>
                    </div>
                    <label for="disabledinput" class="col-sm-1 control-label pay-order-label">时间范围</label>
                    <div class="input-group date form_datetime col-sm-3" data-date="" data-link-field="cms_start_time">
                        <input class="form-control" type="text" value="<?php
                        echo empty($this->arr_params['cms_start_time']) ? '' : $this->arr_params['cms_start_time'];
                        ?>" readonly/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="text" hidden id="cms_start_time" name="cms_start_time" value="<?php
                    echo empty($this->arr_params['cms_start_time']) ? '' : $this->arr_params['cms_start_time'];
                    ?>" />
                    <label class="col-sm-1 control-label pay-order-excision">-</label>
                    <div class="input-group date form_datetime col-sm-3" data-date="" data-link-field="cms_end_time">
                        <input class="form-control" type="text" value="<?php
                        echo empty($this->arr_params['cms_end_time']) ? '' : $this->arr_params['cms_end_time'];
                        ?>" readonly/>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                    <input type="text" hidden id="cms_end_time" name="cms_end_time" value="<?php
                    echo empty($this->arr_params['cms_end_time']) ? '' : $this->arr_params['cms_end_time'];
                    ?>" />
                    <div class="col-sm-1">
                        <button class="btn btn-success pay-order-button" type="button" id="button_query_list_search">
                            <i class="fa fa-search">查询</i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="view_tables">
            <table class="table table-hover" id="index_list">
                <thead>
                <tr>
                    <th><input type="checkbox" id="checkAll" name="checkAll" /></th>
                    <th>用户名称</th>
                    <th>积分值</th>
                    <th>历史积分</th>
                    <th>创建时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arr_partner_list as $k => $val) { ?>
                    <tr class='odd selected'>
                        <td><input type="checkbox" name="checkItem" attr-key="cms_id" value="<?php echo $val['cms_id'];?>"/></td>
                        <td><?php echo empty($val['cms_user_name']) ? $val['cms_user_id'] : $val['cms_user_name']; ?></td>
                        <td><?php echo empty($val['cms_integral']) ? '0' : $val['cms_integral']; ?></td>
                        <td><?php echo empty($val['cms_integral_history']) ? '0' : $val['cms_integral_history']; ?></td>
                        <td><?php echo $val['cms_create_time'];?></td>
                        <td><?php echo $val['cms_modify_time'];?></td>
                        <td><?php echo '——';?></td>
                        <td>
                            <?php if(isset($system_file_list) && !empty($system_file_list)) {?>
                                <div class="dropdown">
                                    <a href="#" title="" class="btn btn-default wh-btn" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog icon_8"></i>
                                        <i class="fa fa-chevron-down icon_8"></i>
                                    </a>
                                    <ul class="dropdown-menu float-right">
                                        <!-- *********** 下拉操作信息 *********** -->
                                        <?php echo make_right_button($system_file_list, $val);?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <!--*********** 初始化必须加载 ***************** （分页信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/pub_page.php';?>
        <!--*********** 初始化必须加载 ***************** （底部按钮信息） *********** 初始化必须加载 ***************** -->
        <?php include_once dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/model/backstage/public_bottom_button.php';?>
    </div>
</body>
</html>


 