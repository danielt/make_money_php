<!-- footer -->
<div id="footer" class="footer"> 
    <div class="copyright home-footer gray"> 
        <p>
            <a target="_blank" href="" style="display:inline-block;text-decoration:none;height:20px;line-height:20px;">
            <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/beian_ico.jpg" class="fl mr10" />&nbsp;
            <span class="pr10 gray">浙公网安备 33010302000359号</span></a>Copyright&copy;2019-2025 &nbsp;unioncloth.com 版权所有
        </p> 
        <!-- QQ客服 -->
        <div class="qq-customer" id="small-chat">
            <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=285474183&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:285474183:53" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>
        </div>
    </div> 
    <div class="footer_link mt20 mb10"> 
        <a class="gs" href="http://gsj.zj.gov.cn/zjaic/?spm=6146" rel="nofollow" title="网络工商" target="_blank"></a> 
        <a class="wj" href="http://www.pingpinganan.gov.cn/?spm=6146" rel="nofollow" title="网络警察" target="_blank"></a> 
        <a class="ba" href="http://www.ppaaol.com/verifySite.do?id=1168&amp;spm=6146" rel="nofollow" title="备案网站" target="_blank"></a> 
        <a class="ncc" href="http://net.china.com.cn/index.htm?spm=6146" rel="nofollow" title="不良信息举报中心" target="_blank"></a> 
        <a class="se_360" href="http://trust.360.cn/?spm=6146" rel="nofollow" title="360绿色网站" target="_blank"></a> 
    </div> 
</div>
<!-- 登录注册遮罩层 -->
<div class="popup_overlay" id="popup_overlay"></div>
<div class="popup_container" id="popup_container">
    <div id="popup_content" class="pop-up">
        <div class="login_reg_box"> 
            <div class="l_r_menu"> 
                <a class="cur login-tab">登录</a> 
                <a class="regist-tab">免费注册</a> 
            </div> 
            <div class="l_r_info pr"> 
                <div id="loginCont" >
                    <form id="loginForm" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="telephone" class="col-sm-2 control-label">手机号</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="telephone" name="telephone" placeholder="请输入手机号">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">密 码</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" id="password" name="password"  placeholder="请输入密码">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-10 cl">
                                <span class="fl">忘记密码？<a id="findPwd">找回密码</a></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">登录</button>
                                <button type="button" class="btn btn-primary cancel">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="registerContOne"  style="display: none;">
                    <form id="registerForm" class="form-horizontal" role="form">
                        <div id="registerSonOne" >
                            <div class="form-group">
                                <label for="username" class="col-sm-2 control-label">姓 名</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="请输入姓名">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="sex" class="col-sm-2 control-label">性 别</label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="sex" value="1" checked> 未知
                                        </label>
                                        <label>
                                            <input type="radio" name="sex" value="2"> 男
                                        </label>
                                        <label>
                                            <input type="radio" name="sex" value="3"> 女
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regTelephone" class="col-sm-2 control-label">手机号</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="regTelephone" name="regTelephone" placeholder="请输入手机号">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="num" class="col-sm-2 control-label">验证码</label>
                                <div class="col-sm-10 cl register-yzm">
                                    <div class="cl">
                                    <input type="text" class="form-control fl" id="num" name="num" placeholder="请输入验证码" style="width: 355px;">
                                    <button type="button" id="verification_code" class="btn btn-primary btn-gain-num fr">发送验证码</button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regPassword" class="col-sm-2 control-label">密 码</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="regPassword"  name="regPassword" placeholder="请输入密码">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirmPassword" class="col-sm-2 control-label">确认密码</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="再次输入密码">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" id="nextBtn" class="btn btn-primary">下一步</button>
                                    <button type="button" class="btn btn-primary cancel">取消</button>
                                </div>
                            </div>
                        </div>
                        <div id="registerSonTwo"  style="display: none;">
                            <div class="form-group">
                                <label for="company_name" class="col-sm-2 control-label">企业名称</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="请输入姓名">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="col-sm-2 control-label">选择角色</label>
                                <div class="col-sm-10">
                                    <select class="selectpicker" name="role_id">
                                        <option value="" selected>请选择角色</option>
                                        <option value="1">订货商</option>
                                        <option value="3">生产商</option>
                                        <option value="4">供应商</option>
                                        <option value="5">制板师</option>
                                        <option value="6">样衣师</option>                     
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="col-sm-2 control-label">国家</label>
                                <div class="col-sm-10">
                                    <select class="selectpicker" name="country">
                                        <option value="中国" selected>中国</option>            
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telephone" class="col-sm-2 control-label">省市区</label>
                                <div class="col-sm-10">
                                    <input id="city-picker3" class="form-control-select" readonly type="text" value="" name="city-picker3" data-toggle="city-picker">
                                    <i class="glyphicon glyphicon-map-marker"></i>
                                    <button type="button" class="label-select" id="area-reset">重置</button>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12">
                                    我已阅读并同意遵守《<a>订购用户服务协议</a>》
                                </div>
                            </div> -->
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12">
                                    <button type="button" id="prevBtn" class="btn btn-primary">上一步</button>
                                    <button type="submit" class="btn btn-primary" style="margin-left: 6px;">同意协议并注册</button>
                                    <button type="button" class="btn btn-primary cancel">取消</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div> 
        </div>
    </div>
</div>



<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery-2.1.1.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.tmpl.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.flexslider-min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrapValidator.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap-select.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/md5.js"></script>

<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/city-picker.data.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/city-picker.js"></script>
<!-- Sweet Alert -->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- 二维码 -->
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.qrcode.min.js"></script>

<script type="text/javascript">
    $(function() {
        //首先判断是否登录
        isLogin(function() {
            // 若登录获取个人信息
            var userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
                if (userInfo) {
                    $('#gotoLogin, #gotoRegister').hide();
                    var name = '';
                    userInfo.cms_name ? name = userInfo.cms_name : name = userInfo.cms_telephone;
                    $('.name_info').show().find('#userName').html(name);
                    if(userInfo.cms_role_id == 1){
                        $(".home-product-title a").show();
                    }
                }
        }, function () {
            // 否则显示登录，注册接口
            $('#gotoLogin, #gotoRegister').show();
            sessionStorage.removeItem('userInfo');
        });
        // 取消
        $('.cancel').on('click', function () {
            $('.l_r_menu a').eq(0).addClass('cur').siblings().removeClass('cur');
            $('#popup_overlay, #popup_container').hide();
            $('#loginCont').show();
            $('#registerContOne').hide();
            $('#registerSonOne').show();
            $('#registerSonTwo').hide();
        })
        // 登录
        $('#gotoLogin').on('click', function () {
            $('#popup_overlay, #popup_container').show();
            $('.l_r_menu a').eq(0).addClass('cur').siblings().removeClass('cur');
            $('#loginCont').show();
            $('#registerContOne').hide();
            $('#registerSonOne').show();
            $('#registerSonTwo').hide();
        })
        // 退出登录
        $('#logout').on('click', function () {
            sessionStorage.removeItem('userInfo');
            var url = '../../../order/con_manager/c_manager/outline';
            $.post(url, {}, function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0){
                    swal({
                            title:'退出失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },function () {
                            window.location.reload();
                        }
                    );
                } else {
                    window.location.href="../../../order/con_manager/c_manager/home";
                    swal({
                            title:'退出成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            showConfirmButton:false,
                            closeOnConfirm:false,
                            timer:1500
                    });
                }
            });
        })
        // 注册
        $('#gotoRegister').on('click', function () {
            $('#popup_overlay, #popup_container').show();
            $('.l_r_menu a').eq(1).addClass('cur').siblings().removeClass('cur');
            $('#loginCont').hide();
            $('#registerContOne').show();
            $('#registerSonOne').show();
            $('#registerSonTwo').hide();
        })
        // 找回密码
        $('#findPwd').on('click', function () {
            window.location.href='../../../order/con_manager/c_manager/find_back_password';
            $('#popup_overlay, #popup_container').hide();
        })

        // 验证登录
        $('#loginForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                password: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '用户名长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        different: {//不能和用户名相同
                            field: 'username',//需要进行比较的input name值
                            message: '不能和用户名相同'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                telephone: {
                    message: 'The phone is not valid',
                    validators: {
                        notEmpty: {
                            message: '手机号码不能为空'
                        },
                        phone: {
                            country: 'CN',
                            message: '请输入正确的手机号码'
                        }
                    }
                }
            }
        }).on('success.form.bv',function(e){
            e.preventDefault();
            var url = 'sigin';
            var md5_password =  hex_md5($('#password').val());
            $('#password').val(md5_password);
            var submitData = $('#loginForm').serialize() + "&flag_ajax_reurn=1";
            //submitData是解码后的表单数据，结果同上
            $.post(url, submitData, function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0){
                    swal(
                        {
                            title:'登录失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },
                    );
                    $('#password').val("");
                } else {
                    swal({
                            title:'登录成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            showConfirmButton:false,
                            closeOnConfirm:false,
                            timer:1500
                        });
                    // 获取个人信息
                    sessionStorage.setItem('userInfo', JSON.stringify(dataObj.data_info));
                    window.location.reload();
                }
            });
        });
        
        // 切换注册
        $('.l_r_menu').on('click', 'a', function () {
            var index = $(this).index();
            $(this).addClass('cur').siblings().removeClass('cur');
            if(index == 1) {
                $('#loginCont').hide();
                $('#registerContOne').show();
                $('#registerSonOne').show();
                $('#registerSonTwo').hide();
                // 清空注册数据
                $('#registerForm')[0].reset();
            } else {
                $('#loginCont').show();
                $('#registerContOne').hide();
                $('#registerSonOne').hide();
                $('#registerSonTwo').hide();
            }
        })
        // 验证注册
        $('#registerForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {/*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'

            },
            fields: {/*验证：规则*/
                regPassword: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '密码长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                confirmPassword: {
                    message:'密码无效',
                    validators: {
                        notEmpty: {
                            message: '密码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: '密码长度必须在6到30之间'
                        },
                        identical: {//相同
                            field: 'password', //需要进行比较的input name值
                            message: '两次密码不一致'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                regTelephone: {
                    validators: {
                        notEmpty: {
                            message: '手机号码不能为空'
                        },
                        phone: {
                            country: 'CN',
                            message: '请输入正确的手机号码'
                        }
                    }
                },
                num: {
                    message:'验证码无效',
                    validators: {
                        notEmpty: {
                            message: '验证码不能为空'
                        },
                        stringLength: {
                            min: 6,
                            max: 6,
                            message: '验证码填写错误'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: '用户名只能由字母、数字、点和下划线组成'
                        }
                    }
                },
                role_id: {
                    message:'用户角色必选',
                    validators: {
                        notEmpty: {
                            message: '用户角色必选'
                        }
                    }
                },
            }
        }).on('success.form.bv',function(e){
            e.preventDefault();
            $('#city-picker3').citypicker('destroy');
            var url = 'registry';
            var md5_password =  hex_md5($('#regPassword').val());
            var regTelephone =  $('#regTelephone').val();
            var md5_confirmPassword =  hex_md5($('#confirmPassword').val());
            $('#regPassword').val(md5_password);
            $('#confirmPassword').val(md5_confirmPassword);
            var submitData = $('#registerForm').serialize() + "&flag_ajax_reurn=1&telephone=" + regTelephone + "&password=" + md5_password;
            //submitData是解码后的表单数据，结果同上
            $.post(url, submitData, function(result){
                var dataObj=eval("("+result+")");
                if(dataObj.ret != 0){
                    swal(
                        {
                            title:'注册失败',
                            text:dataObj.reason,
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },
                        function(){
                            $('#regPassword').val("");
                            $('#confirmPassword').val("");
                            location.reload();
                        }
                    );
                } else {
                    swal({
                            title:'注册成功',
                            text:'',
                            type:"success",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                    });
                    $('.l_r_menu a').eq(0).addClass('cur').siblings().removeClass('cur');
                    $('#loginCont').show();
                    $('#registerContOne').hide();
                    $('#registerSonTwo').hide();
                    // 清空注册数据
                    $('#registerForm')[0].reset();
                }
            });
        });
        //发送验证码
        $('#verification_code').on('click',function(){
            var telephone = $('#registerContOne #regTelephone').val();
            if (telephone) {
                $(this).prop('disabled',true);
                roof();
                var key = '04997110aa2db7e27991ece0749064f4';
                var timestamp=new Date().getTime();
                var sign = hex_md5(telephone+timestamp+key);
                var submitData = "cms_mobile_code=" + telephone + "&sign=" + sign + "&cms_time=" + timestamp;
                $.ajax({
                    url:'../../../../backstage/system/auto/c_smsg/send_msg',
                    type:"POST",
                    data:submitData,
                    cache:false,//false是不缓存，true为缓存
                    async:true,//true为异步，false为同步
                    success:function(result){
                        var dataObj=eval("("+result+")");
                        if(dataObj.ret != 0)
                        {
                            swal(
                                {
                                    title:'发送失败',
                                    text:dataObj.reason,
                                    type:"error",
                                    showCancelButton:false,
                                    showConfirmButton:false,
                                    timer:2000
                                },
                            );
                        }
                        else
                        {
                            swal(
                                {
                                    title:'发送成功',
                                    text:'',
                                    type:"success",
                                    showCancelButton:false,
                                    showConfirmButton:false,
                                    timer:2000
                                }
                            );
                        }

                    }
                });
            } else {
                swal(
                    {
                        title:'请填写手机号码',
                        text:'',
                        type:"error",
                        showCancelButton:false,
                        showConfirmButton:false,
                        timer:1000
                    }
                );
            }
            
        });
        // 发送验证码倒计时
        var times = 60;
        function roof(){
            if(times == 0){
                $('.btn-gain-num').text('发送验证码('+times+'s)');
                $('.btn-gain-num').prop('disabled',false);
                $('.btn-gain-num').text('发送验证码');
                times = 60;
                return
            }
            $('.btn-gain-num').text('重新发送('+times+'s)');
            times--;
            setTimeout(roof,1000);
        }
        //下一步进行表单检查
        $('#nextBtn').on('click',function(){
            var flag1 = $('#registerForm').data("bootstrapValidator").validateField('regTelephone');
            var flag2 = $('#registerForm').data("bootstrapValidator").validateField('num');
            var flag3 = $('#registerForm').data("bootstrapValidator").validateField('regPassword');
            var flag4 = $('#registerForm').data("bootstrapValidator").validateField('confirmPassword');
            if(flag1.isValid() && flag2.isValid() && flag3.isValid() && flag4.isValid())
            {
                $('#registerSonTwo').show();
                $('#registerSonOne').hide();
            }
        });
        //上一步
        $('#prevBtn').on('click',function(){
            $('#registerSonOne').show();
            $('#registerSonTwo').hide();
        });
        //重置地区选择
        $('#area-reset').click(function () {
            $('#city-picker3').citypicker('reset');
        });


    });	
</script>