<?php if(!defined('VIEW_MODEL_BACKGROUD')){define('VIEW_MODEL_BACKGROUD', '/CodeIgniter/view_model/backstage/');}?>
<title>业务后台管理系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!--基本 CSS-->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/bootstrap.min14ed.css" rel="stylesheet">
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/font-awesome.min93e3.css" rel="stylesheet">
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/animate.min.css" rel="stylesheet">
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/style.min862f.css" rel="stylesheet">
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/style.css" rel='stylesheet' type='text/css' />
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/icon-font.min.css" rel="stylesheet" />
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/fabochart.css" rel='stylesheet' type='text/css' />

<!--基本 JS-->
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/jquery.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/bootstrap.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/content.min.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/skycons.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/amcharts.js"></script>	
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/serial.js"></script>	
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/light.js"></script>	
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/radar.js"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/md5.js"></script>

<!-- 参数验证 -->
<link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrapvalidator-master/dist/css/bootstrapValidator.css" />
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrapvalidator-master/dist/js/bootstrapValidator.js"></script>

<!-- 树形图 -->
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-tree-view-demo/js/bootstrap-treeview.min.js"></script>

<!-- 弹窗提示 -->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>hplus/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- 文件上传 -->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" type="text/css" />
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-fileinput-master/js/locales/zh.js" type="text/javascript"></script>

<!-- 下拉复选框 -->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/Select.css" rel="stylesheet" type="text/css" />


<!-- 文件上传 -->
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-fileinput-master/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-fileinput-master/js/locales/zh.js" type="text/javascript"></script>
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>bootstrap-fileinput-master/css/fileinput.css" rel="stylesheet" type="text/css" />

<!-- 二维码 -->
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.qrcode.min.js"></script>

<style>
    .form-horizontal_search input
    {
        border-radius: 4px!important;
    }
    .form-horizontal_search button
    {
        margin-top: 0px!important;
        margin-bottom: 0px!important;
        padding: 5px 20px!important;
        border-radius: 4px!important;
    }
</style>
<script type="text/javascript">
    var toggle = true;
    $(".sidebar-icon").click(function() {                
        if (toggle)
        {
            $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
            $("#menu span").css({"position":"absolute"});
        }
        else
        {
        	$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
        	setTimeout(function() {
          		$("#menu span").css({"position":"relative"});
        	}, 400);
        }		
    	toggle = !toggle;
    });

    $(function(){
        function initTableCheckbox() {  
            var $thr = $('#index_list thead tr');  
            var $checkAll = $thr.find('input');  
            $checkAll.click(function(event){  
                /*将所有行的选中状态设成全选框的选中状态*/  
                $tbr.find("input[name='checkItem']").prop('checked',$(this).prop('checked'));  
                /*并调整所有选中行的CSS样式*/  
                if ($(this).prop('checked')) {  
                    $tbr.find("input[name='checkItem']").parent().parent().addClass('warning');  
                } else{  
                    $tbr.find("input[name='checkItem']").parent().parent().removeClass('warning');  
                }  
                /*阻止向上冒泡，以防再次触发点击操作*/  
                event.stopPropagation();  
            });
            var $tbr = $('#index_list tbody tr');  
            $tbr.find("input[name='checkItem']").click(function(event){  
                /*调整选中行的CSS样式*/  
                $(this).parent().parent().toggleClass('warning');  
                /*如果已经被选中行的行数等于表格的数据行数，将全选框设为选中状态，否则设为未选中状态*/  
                $checkAll.prop('checked',$tbr.find('input:checked').length == $tbr.length ? true : false);  
                /*阻止向上冒泡，以防再次触发点击操作*/  
                event.stopPropagation();  
            });  
            /*点击每一行时也触发该行的选中操作*/  
            $tbr.click(function(){  
                $(this).find("input[name='checkItem']").click();  
            });  
        }
        initTableCheckbox();
    });
    
    /**
    *  Base64 encode / decode
    */
    function Base64() {
     
        // private property
        _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
     
        // public method for encoding
        this.encode = function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = _utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
            }
            return output;
        }
     
        // public method for decoding
        this.decode = function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = _keyStr.indexOf(input.charAt(i++));
                enc2 = _keyStr.indexOf(input.charAt(i++));
                enc3 = _keyStr.indexOf(input.charAt(i++));
                enc4 = _keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = _utf8_decode(output);
            return output;
        }
     
        // private method for UTF-8 encoding
        _utf8_encode = function (string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
     
            }
            return utftext;
        }
     
        // private method for UTF-8 decoding
        _utf8_decode = function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while ( i < utftext.length ) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i+1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i+1);
                    c3 = utftext.charCodeAt(i+2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    }
    
    $('.main-search').hide();
	$('button').click(function (){
		$('.main-search').show();
		$('.main-search text').focus();
	});
	$('.close').click(function(){
		$('.main-search').hide();
    });

	
	//ajax 提交数据
	//submitData 需要提交参数 ：user_id=12&user_name=John&user_age=20
	//url  请求的地址 ../../../system/auto/c_project/add
	//need_refresh  true 界面需要刷新  | false 界面不刷新
    function system_submit_data(str_class,submitData,url,need_refresh)
    {
        var show_log=true;
    	//submitData是解码后的表单数据，结果同上
    	submitData+="&flag_ajax_reurn=1";
		var temp_data = $('#'+str_class+'-form').serialize();
		if(temp_data !== undefined && temp_data !== null){
			submitData+='&'+temp_data;
		}
        console.log(submitData);
    	$.ajax({
        	url:url,
			type:"POST",
        	data:submitData,
        	cache:false,//false是不缓存，true为缓存
        	async:true,//true为异步，false为同步
        	beforeSend:function(){
            	
        	},
        	success:function(result){
        		var result =  eval("("+result+")");
        		if(result.ret == 0)
				{
        			result.reason = result.reason === undefined ? "数据操作失败，请联系管理员，抱歉！" : result.reason;
					var log_text = show_log ? result.reason : "数据操作完毕，恭喜！";
					sys_sweetalert("success",str_class,"操作成功！",log_text,'','',need_refresh);
				}
				else
				{
        			result.reason = result.reason === undefined ? "数据操作失败，请联系管理员，抱歉！" : result.reason;
					var log_text = show_log ? result.reason : "数据操作失败，请联系管理员，抱歉！";
					sys_sweetalert("error",str_class,"操作失败！",log_text,'','',false);
				}
        	},
        	complete:function(){
        	    //请求结束时
        	},
        	error:function(){
        	    //请求失败时
        	    var log_text = show_log ? "请求接口地址失败，地址["+url+"]" : "数据操作失败，请联系管理员，抱歉！";
				sys_sweetalert("error",str_class,"请求接口地址失败！",log_text,'','','',false);
        	}
    	});
    }
    function check_checkbox_value()
    {
        var cms_id = ''; 
        $('#index_list tbody tr').find("input[name='checkItem']").each(function(){
        	if ($(this).is(':checked') && $(this).val().length >0 && $(this).attr('attr-key').length >0) 
            {
        		cms_id+=$(this).attr('attr-key')+'[]='+$(this).val()+'&';
    		} 
    	});
    	return cms_id;
    }

    //系统弹窗
    //action_type 提醒？还是提交数据
	//title 弹窗标题
	//text  弹窗内容
	//url 是否需要ajax请求  如果不需要为空值
	//submitData  url不为空的时候 请求的参数
	//str_id_class  获取#ID 或者 class  .xxxx 内容数据
	//need_refresh 是否需要刷新界面
    function sys_sweetalert(action_type,str_class,title,text,url,submitData,need_refresh)
    {
       	switch(action_type)
       	{
       		case "warning":
       		case "error":
       		case "success":
       			title = title.length <1 ? "您需要选择操作的数据" : title;
       			text = text.length <1 ? "再次操作后数据可能无法恢复，请谨慎！" : text;
       			if(need_refresh)
           		{
           			swal(
           					{
           						title:title,
           						text:text,
           						type:action_type,
           					},
           					function(){
               					if(action_type == 'success')
               					{
               						//url_refresh();
                                    window.location.reload();
                   				}
            				}
           				)
               	}
       			else
           		{
       				swal(
           					{
           						title:title,
           						text:text,
           						type:action_type,
           					}
           				)
           		}
       			break;
       		case "submit":
           		if(url.length<1)
                {
           			sys_sweetalert("error",str_class,"AJAX数据请求URL为空",'','','',false);
           			break;
                }
                url='../../../'+url;
           		var bootstrapValidator = $('#'+str_class+'-form').data('bootstrapValidator');
                bootstrapValidator.validate();
                if(!bootstrapValidator.isValid()){
					break;
                }
       			title = title.length <1 ? "您确定要提交这条信息吗" : title;
       			text = text.length <1 ? "提交数据后系统将可能会添加、修改、删除数据，请谨慎操作！" : text;
       			swal(
       					{
       						title:title,
       						text:text,
       						type:"warning",
       						showCancelButton:true,
        					confirmButtonColor:"#DD6B55",
       						confirmButtonText:"确定",
       						cancelButtonText:"取消",
       						closeOnConfirm:false,
       						closeOnCancel:false
   						},
   						function(isConfirm){
   							if(isConfirm){
   								system_submit_data(str_class,submitData,url,need_refresh);
       	 					}else{
           	 					sys_sweetalert("warning",str_class,"您取消了操作","取消了操作系统数据不会产生任何改变，请谨慎操作！",'','',false);
       						}
       					}
       				)
       			break;
       		case 'nopage_action_button':
       		case 'nopage_action_right':
       		case 'nopage_action_button_nocheck':
       			if(url.length<1)
                {
           			sys_sweetalert("error",str_class,"AJAX数据请求URL为空",'AJAX数据请求URL为空！','','','',false);
           			break;
                }
                url='../../../'+url;
                var checkbox_value='';
                if(action_type == 'nopage_action_button')
                {
                	checkbox_value = check_checkbox_value();
                	if(checkbox_value.length<1)
                    {
                		sys_sweetalert("error",str_class,"请至少选择一条数据",'请至少选择一条数据！','','','',false);
               			break;
                    }
                    submitData+="&"+checkbox_value;
                }
                title = title.length <1 ? "您确定要提交这条信息吗" : title;
       			text = text.length <1 ? "提交数据后系统将可能会添加、修改、删除数据，请谨慎操作！" : text;
       			swal(
       					{
       						title:title,
       						text:text,
       						type:"warning",
       						showCancelButton:true,
        					confirmButtonColor:"#DD6B55",
       						confirmButtonText:"确定",
       						cancelButtonText:"取消",
       						closeOnConfirm:false,
       						closeOnCancel:false
   						},
   						function(isConfirm){
   							if(isConfirm){
   								system_submit_data(str_class,submitData,url,need_refresh);
       	 					}else{
           	 					sys_sweetalert("warning",str_class,"您取消了操作","取消了操作系统数据不会产生任何改变，请谨慎操作！",'','',false);
       						}
       					}
       				)
           		break;
       		default :
       			sys_sweetalert("error",str_class,"未找到任何弹窗方法",'未找到任何弹窗方法,开发的锅~~~~','','',false);
   				break;
        }
    }
    
    function system_auto_load(str_class,paramas)
    {
        var b = new Base64();
		if(paramas.length > 0){
			temp_paramas = b.decode(paramas);
			temp_paramas =  eval("("+temp_paramas+")");

        	$('#'+str_class+'-form').find("input").each(function(){
        		var i_type = $(this).attr('type');
            	var i_name = $(this).attr('name');
            	if(i_type == 'checkbox')
                {
                    i_name = i_name.replace("[]","");
                }
                if(i_type !== undefined && i_name !== undefined)
                {
                    if(i_type.length > 0 && i_name.length > 0)
                    {
                        var temp_data = temp_paramas[i_name];
                        if(temp_data !== undefined && temp_data !== null)
                        {
                            if(i_type == 'text' || i_type=='password' || i_type == 'hidden')
                            {
                                $(this).val(temp_data);
                            }
                            else if(i_type == 'checkbox')
                            {
                                if(temp_data.indexOf($(this).val()) != -1)
                                {
                                    $(this).attr("checked", true);
                                }
                                else
                                {
                                    $(this).attr("checked", false);
                                }
                            }
                            else if(i_type == 'radio')
                            {
                                if(temp_data == $(this).val())
                                {
                                    $(this).attr("checked", true);
                                }
                                else
                                {
                                    $(this).attr("checked", false);
                                }
                            }
                            else
                            {
                                $(this).val('');
                            }
                        }
                        else
                        {
                            $(this).val('');
                        }
                    }
                    else
                    {
                        $(this).val('');
                    }
                }
                else
                {
                    $(this).val('');
                }
        	});

        	$('#'+str_class+'-form').find("textarea").each(function(){
        		var i_name = $(this).attr('name');
            	if(i_name.length>0)
                {
                    var temp_data = temp_paramas[i_name];
                    if(temp_data !== undefined && temp_data !== null)
                    {
                    	$(this).html(temp_data);
                    }
                }
            	else
                {
                    $(this).html('');
                }
        	});

        	//下拉框
            $('#'+str_class+'-form').find("select").each(function(){
                var i_name = $(this).attr('name');
                if(i_name !== undefined)
                {
                    if(i_name.length>0)
                    {
                        var temp_data = temp_paramas[i_name];
                        if(temp_data !== undefined && temp_data !== null)
                        {
                            $(this).find("option").each(function () {
                                if($(this).val() == temp_data)
                                {
                                    $(this).attr("selected", true);
                                }
                                else
                                {
                                    $(this).attr("selected", false);
                                }
                            })
                        }
                    }
                    else
                    {
                        $(this).html('');
                    }
                }

            });
        	//下来复选框,定制处理---begin
            var labelText_edit = [];//初始化名称
            $('#'+str_class+'-form').find(".fs-option").each(function(){
                var i_name =  $('#'+str_class+'-form').find("#select_checkbox_button_hidden_value").attr("name");
                var temp_data = temp_paramas[i_name];//得到当前下拉框的值，多个以逗号分隔
                if(temp_data !== undefined)
                {
                    if(temp_data.indexOf($(this).attr("data-value")) != -1)
                    {
                        $(this).addClass("selected");
                        labelText_edit.push($(this).find(".fs-option-label").text());
                    }
                    else
                    {
                        $(this).removeClass("selected");
                    }
                }
            });
            if (labelText_edit.length < 1)
            {
                labelText_edit = '请选择';
            }
            else
            {
                labelText_edit = labelText_edit.join(',');
            }
            $('#'+str_class+'-form').find('.fs-label').html(labelText_edit);

            //---------下拉框定制化end
		}
		var str_function = "init_"+str_class;
        //定向参数：支付订单定制
        var order_data = $('div.pay-order-data').attr('data-order');
//        order_data = JSON.stringify({
//            'cms_user_id':1,//用户ID
//            'cms_order_price':0.01,//订单价格
//            'cms_order_type':1,//订单类型。0批量订单；1面料小样；2样板订单；3样衣订单；4稀缺面料定金订单；5充值订单
//            'cms_pay_order_code':1,//购买订单ID
//            'cms_order_parent':'1',//父级订单ID
//            //'cms_order_id':'1',//订单ID，支付未完成订单上报
//            'cms_order_price_payed':0.01//本次支付金额
//        });
        if(order_data)
        {
            order_data = b.encode(order_data);
        }

		eval(str_function+"('"+str_class+"','"+paramas+"','"+order_data+"')");
    }
    //初始化fileinput
    var FileInput = function () {
        var oFile = new Object();
        //初始化fileinput控件（第一次初始化）
        oFile.Init = function(type,ctrlName,need_modify_id,params) {
            var control = $('#' + ctrlName);
            control.fileinput('destroy'); 
            if(type == 'add')
            {
                //初始化上传控件的样式
                control.fileinput({
                    language: 'zh', //设置语言
                    uploadUrl: '<?php echo VIEW_MODEL_BASE; ?>/app_model/libraries/em_upload.class.php', //上传的地址
                    allowedFileExtensions: ['jpg', 'gif', 'png', 'pem', 'txt'],//接收的文件后缀
                    showUpload: false, //是否显示上传按钮
                    showCaption: false,//是否显示标题
                    showClose: false,
                    browseClass: "btn btn-primary", //按钮样式     
                    autoReplace:true,
                    overwriteInitial: true,
                    showUploadedThumbs: false,
                    showRemove:false,
                    initialPreviewShowDelete: false,
                    maxFileCount: 1, //表示允许同时上传的最大文件个数
                    validateInitialCount:true,
                    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
                    msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
                    uploadExtraData:function(){
        				var data = {
        							'input_image_key':'image_data'
        						};
                        return data; 
                   },
                }).on("fileuploaded", function (event, data, previewId, index) {
                    if(data.response.ret !=0)
                    {
                    	sys_sweetalert("error",'',"上传文件失败！",data.response.reason,'','',false);
                    }
                    else
                    {
                    	sys_sweetalert("success",'',"上传文件成功！",data.response.reason,'','',false);
                    	var html = '<input type="hidden" name="'+$('#'+need_modify_id).attr('post_name')+'" value="'+data.response.data_info.base_pth+'">';
                    	$('#'+need_modify_id).html(html);
                    }
                }).on('filesuccessremove', function (event, previewId, extra) {
              　　　　　　//在移除事件里取出所需数据，并执行相应的删除指令
                	$('#'+need_modify_id).html('');
                });
            }
            else if(type == 'edit')
            {            	
            	var temp_paramas = [];
            	if(params.length >0){
            		var b = new Base64();
            		temp_paramas = b.decode(params);
            		temp_paramas =  eval("("+temp_paramas+")");
            	}
            	var initialPreview_v =[];
            	var initialPreviewConfig_v = [];
            	if(temp_paramas[$('#'+need_modify_id).attr('post_name')] != undefined && temp_paramas[$('#'+need_modify_id).attr('post_name')] != null && temp_paramas[$('#'+need_modify_id).attr('post_name')].length>0)
                {
            		initialPreview_v = ["<?php echo VIEW_MODEL_BASE; ?>/data_model/upload/"+temp_paramas[$('#'+need_modify_id).attr('post_name')]];
            		initialPreviewConfig_v = [
                                                {
                                                	url: "<?php echo VIEW_MODEL_BASE; ?>/app_model/libraries/em_upload.class.php?system_func=delete"
                                                },
              		                  		 ];
                    var html = '<input type="hidden" name="'+$('#'+need_modify_id).attr('post_name')+'" value="'+temp_paramas[$('#'+need_modify_id).attr('post_name')]+'">';
                    $('#'+need_modify_id).html(html);
                }
                //初始化上传控件的样式
                control.fileinput({
                    language: 'zh', //设置语言
                    uploadUrl: '<?php echo VIEW_MODEL_BASE; ?>/app_model/libraries/em_upload.class.php', //上传的地址theme: 'fas',
                    showUpload: false,
                    showCaption: false,
                    showClose: false,
                    browseClass: "btn btn-primary", //按钮样式     
                    allowedFileExtensions: ['jpg', 'gif', 'png'],//接收的文件后缀
                    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>", 
                    autoReplace:true,
                    overwriteInitial: true,
                    initialPreviewAsData: true,
                    showRemove:false,
                    initialPreviewShowDelete: false,
                    maxFileCount: 1, //表示允许同时上传的最大文件个数
                    initialPreview: initialPreview_v,
                    initialPreviewConfig: initialPreviewConfig_v,
                    uploadExtraData:function(){
        				var data = {
        							'input_image_key':'image_data',
        						};
                        return data; 
                   }
                }).on("fileuploaded", function (event, data, previewId, index) {
                    if(data.response.ret !=0)
                    {
                    	sys_sweetalert("error",'',"上传文件失败！",data.response.reason,'','',false);
                    }
                    else
                    {
                    	sys_sweetalert("success",'',"上传文件成功！",data.response.reason,'','',false);
                    	var html = '<input type="hidden" name="'+$('#'+need_modify_id).attr('post_name')+'" value="'+data.response.data_info.base_pth+'">';
                    	$('#'+need_modify_id).html(html);
                    }
                }).on('filepredelete', function(event, key, jqXHR, data) { 
                	$('#'+need_modify_id).html('');
                }).on('filesuccessremove', function (event, previewId, extra) {
              　　　　　　//在移除事件里取出所需数据，并执行相应的删除指令
                	$('#'+need_modify_id).html('');
                });
            }
        };
        return oFile;
    };
</script>

<!-- 单选多选框 -->
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap-select.min.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/bootstrap-select.zh_CN.js"></script>
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/common.js"></script>
<?php
    function make_right_button($system_file_list,$val)
    {
        $str = '';
        if(isset($system_file_list) && is_array($system_file_list) && !empty($system_file_list))
        {
            foreach ($system_file_list as $system_file_list_key=>$system_file_list_value)
            {
                $arr_button_temp = null;
                if(!isset($system_file_list_value['button_data']) || !is_array($system_file_list_value['button_data']) || empty($system_file_list_value['button_data']))
                {
                    continue;
                }
                if(!isset($system_file_list_value['function']) || !_check_action($system_file_list_value['function']))
                {
                    continue;
                }
                //if($system_file_list_value['function'] == 'edit')
                if(_check_action($system_file_list_value['function'],'edit'))
                {
                    foreach ($system_file_list_value['button_data'] as $button_data_key=>$button_data_value)
                    {
                        if(!isset($val) || empty($val))
                        {
                            continue;
                        }
                        $arr_button_temp[] = array(
                            'a'=>'<a href="#" data-toggle="modal" data-target="#'.$system_file_list_value['class'].'" '.
                            ' onclick="system_auto_load('."'".$system_file_list_value['class']."','".base64_encode(json_encode($val))."'".');">',
                            'name'=>$button_data_value['name'],
                            'icon'=>$button_data_value['icon'],
                        ); 
                    }
                }
                //else if($system_file_list_value['function'] == 'state')
                else if(_check_action($system_file_list_value['function'],'state'))
                {
                    foreach ($system_file_list_value['button_data'] as $button_data_key=>$button_data_value)
                    {
                        $flag = true;
                        if(isset($button_data_value['where']) && is_array($button_data_value['where']) && !empty($button_data_value['where']))
                        {
                            foreach ($button_data_value['where'] as $button_data_where_key=>$button_data_where_value)
                            {
                                if(!isset($val[$button_data_where_key]) || $val[$button_data_where_key] == $button_data_where_value)
                                {
                                    $flag = false;
                                    break;
                                }
                            }
                        }
                        if(!$flag)
                        {
                            continue;
                        }
                        if(!isset($val['cms_state']) || $val['cms_state'] != '0')
                        {
                            $arr_button_temp[] = array(
                                'a'=>'<a href="#" onclick="sys_sweetalert('."'".'nopage_action_right'."','".$system_file_list_value['class']."','您确定要启用这条信息吗','".
                                    '提交数据后系统将可能会启用此条数据，请谨慎操作！'."','".$system_file_list_value['ajax']."','cms_id[]=".$val['cms_id'].'&cms_state=0'."'".',true);">',
                                'name'=>$button_data_value['name'],
                                'icon'=>$button_data_value['icon'],
                            );
                        }
                        else
                        {
                            $arr_button_temp[] = array(
                                'a'=>'<a href="#" onclick="sys_sweetalert('."'".'nopage_action_right'."','".$system_file_list_value['class']."','您确定要禁用这条信息吗','".
                                    '提交数据后系统将可能会禁用此条数据，请谨慎操作！'."','".$system_file_list_value['ajax']."','cms_id[]=".$val['cms_id'].'&cms_state=1'."'".',true);">',
                                'name'=>$button_data_value['name'],
                                'icon'=>$button_data_value['icon'],
                            );
                        }
                    }
                }
                //else if($system_file_list_value['function'] == 'delete')
                else if(_check_action($system_file_list_value['function'],'delete'))
                {
                    foreach ($system_file_list_value['button_data'] as $button_data_key=>$button_data_value)
                    {
                        $arr_button_temp[] = array(
                                'a'=>'<a href="#" onclick="sys_sweetalert('."'".'nopage_action_right'."','".$system_file_list_value['class']."','您确定要删除这条信息吗','".
                                    '提交数据后系统将可能会删除此条数据，请谨慎操作！'."','".$system_file_list_value['ajax']."','cms_id[]=".$val['cms_id']."'".',true);">',
                            'name'=>$button_data_value['name'],
                            'icon'=>$button_data_value['icon'],
                        );
                    }
                }
                if(empty($arr_button_temp) || !is_array($arr_button_temp))
                {
                    continue;
                }
                foreach ($arr_button_temp as $arr_button_temp_value)
                {
                    $str_button_temp_value_icon = isset($arr_button_temp_value['icon']) ? $arr_button_temp_value['icon'] : '';
                    $str_button_temp_value_name = (isset($arr_button_temp_value['name']) && strlen($arr_button_temp_value['name']) >0) ? $arr_button_temp_value['name'] : '<font color="red">未知按钮</font>';
                    $str.= '<li>';
                    $str.=      $arr_button_temp_value['a'];
                    $str.=          '<i class="fa '. $str_button_temp_value_icon .' icon_9"></i>'.$str_button_temp_value_name;
                    $str.=      '</a>';
                    $str.= '</li>';
                }
            }
        }
        return $str;
    }

    /**
     * 校验是否满足条件
     */
    function _check_action($str_action,$str_preg = '')
    {
        switch($str_preg)
        {
            case 'edit':
                $str_preg = '/edit/';
                break;
            case 'delete':
                $str_preg = '/delete/';
                break;
            case 'state':
                $str_preg = '/state/';
                break;
            default:
                $str_preg = '/delete|edit|state/';
                break;
        }
        return preg_match($str_preg,$str_action) ? true : false;
    }
?>
