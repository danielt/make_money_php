<link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/home.css"/>
<link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrapValidator.min.css"/>
<link rel="stylesheet" href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/bootstrap-select.min.css"/>
<link href="<?php echo VIEW_MODEL_BACKGROUD; ?>css/city-picker.css" rel="stylesheet">
<style>
    .header{ background: url(<?php echo VIEW_MODEL_BACKGROUD; ?>images/bg.jpg) no-repeat center -50px; }
    .bootstrap-select .btn{ height: 34px!important; line-height: 34px!important; padding: 0 12px!important; }
    .bootstrap-select>.dropdown-toggle{ padding-bottom: 0!important;}
    .bootstrap-select .glyphicon{ left: 186px; top: 0; }
    .city-picker-span{     width: 100%; height: 34px!important; padding: 0 25px; font-size: 14px; line-height: 34px!important;
color: #555; background-color: #fff; border: 1px solid #ccc; border-radius: 4px 0 0 4px; }
    #registerSonTwo .glyphicon{ position: absolute; top: 9px; left:20px; font-size: 17px; color: #c8c8c8; transition : all 0.5s ease 0s; }
    .label-select{
        width: 25%; float: right; font-size: 14px; font-weight: 500; right: 10%; height: 34px; line-height: 20px;
        background-color: #f0f0f0; border-radius: 0 4px 4px 0; box-shadow: none;
        padding: 6px 20px; text-transform: capitalize;
        color: #555; background-color: #fff; border: 1px solid #ccc; border-left: none;
        transition: all 0.5s ease 0s;
    }
    .register-yzm .form-control-feedback{
        right: 130px!important;
    }
    .find-password .form-control-feedback{
        right: 156px!important;
    }
</style>
<script>
        // 判断是否登录，若登录则跳转相应页面
        function handleLogin(obj) {
            var url = $(obj).attr('data-url');
            isLogin(function () {
                // 已登录
                window.top.location.href = url;
            }, function () {
                // 未登录
                $('#popup_overlay, #popup_container').show();
            })
        }
        // 判断是否登录接口调用
        function isLogin(sucFun, failFun) {
            // 判断是否都登录
            var loginUrl = "../../../../backstage/order/con_manager/c_manager/check_online";
            $.ajax(loginUrl, {
                success:function(data){
                    if(data.ret == 0) {
                        if (typeof(sucFun) == 'function') {
                            sucFun();
                        }
                    } else {
                        if (typeof(failFun) == 'function') {
                            failFun();
                        }
                    }
                },
                error : function(data){
                    swal(
                        {
                            title:'错误提示',
                            text:'请求出错，请稍后重试',
                            type:"error",
                            showCancelButton:false,
                            confirmButtonText:"确定",
                            closeOnConfirm:false
                        },
                    );
                },
                dataType:'json',
                type:'get'
            });
        }
</script>

<div class="site_nav">
    <div class="site_nav_bd">
        <ul class="fl" style="margin-left: 280px;">
            <li> <a class="nav_title nav_row" href="../../../order/con_manager/c_manager/home"> 首页</a> </li>
            <li> <a class="nav_title nav_row" data-url="../../../order/index/c_index/index" onclick="handleLogin(this)"> 个人中心</a> </li>
        </ul>
        <ul class="fr">
            <li id="gotoLogin"> <a class="nav_title nav_row"> 登录</a> </li>
            <li id="gotoRegister"> <a class="nav_title nav_row"> 免费注册</a> </li>
            <li class="name_info"> <a class="nav_title nav_row"> <span id="userName" title="酸酸"> 酸酸2 </span> <em class="row"></em> </a> 
                <div class="drop_down"> 
                    <ul class="pr">
                        <li class="last"> <a id="logout" class="a_blue" href="javascript:;"><b>退出登录</b></a> </li> 
                    </ul> 
                </div> 
            </li>
        </ul> 
    </div>
</div>

<div id="header" class="header clearfix"> 
    <div class="ddzc_wrap cl"> 
        <div class="logo"> 
            <h1> <a href="" title=" - 专业的纺织服装加工订单交易平台"> <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/logo.png" class="pngFix" alt="" /> </a> </h1> 
        </div> 
        <div class="fr topCon">
            <img src="<?php echo VIEW_MODEL_BACKGROUD; ?>/images/wm.jpg" alt="微信小程序" />
            <p>关注微信小程序</p>
        </div>
    </div> 
</div> 