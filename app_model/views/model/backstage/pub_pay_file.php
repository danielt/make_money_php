<?php if(!defined('VIEW_MODEL_BACKGROUD')){define('VIEW_MODEL_BACKGROUD', '/CodeIgniter/view_model/backstage/');}?>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!--基本 CSS-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/css/bootstrap.min14ed.css" rel="stylesheet">-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/css/font-awesome.min93e3.css" rel="stylesheet">-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/css/animate.min.css" rel="stylesheet">-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/css/style.min862f.css" rel="stylesheet">-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--css/style.css" rel='stylesheet' type='text/css' />-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--css/icon-font.min.css" rel="stylesheet" />-->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--css/fabochart.css" rel='stylesheet' type='text/css' />-->

<!--基本 JS-->
<!--<script src="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/js/jquery.min.js"></script>-->
<!--<script src="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/js/bootstrap.min.js"></script>-->

<!--<!-- 弹窗提示 -->
<!--<link href="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">-->
<!--<script src="--><?php //echo VIEW_MODEL_BACKGROUD; ?><!--hplus/js/plugins/sweetalert/sweetalert.min.js"></script>-->

<!-- 二维码 -->
<script type="text/javascript" src="<?php echo VIEW_MODEL_BACKGROUD; ?>js/jquery.qrcode.min.js"></script>

<script type="text/javascript">

    /**
    *  Base64 encode / decode
    */
    function Base64() {
     
        // private property
        _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
     
        // public method for encoding
        this.encode = function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = _utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
            }
            return output;
        }
     
        // public method for decoding
        this.decode = function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = _keyStr.indexOf(input.charAt(i++));
                enc2 = _keyStr.indexOf(input.charAt(i++));
                enc3 = _keyStr.indexOf(input.charAt(i++));
                enc4 = _keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = _utf8_decode(output);
            return output;
        }
     
        // private method for UTF-8 encoding
        _utf8_encode = function (string) {
            string = string.replace(/\r\n/g,"\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
     
            }
            return utftext;
        }
     
        // private method for UTF-8 decoding
        _utf8_decode = function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while ( i < utftext.length ) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i+1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i+1);
                    c3 = utftext.charCodeAt(i+2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    }
    
    $('.main-search').hide();
	$('button').click(function (){
		$('.main-search').show();
		$('.main-search text').focus();
	});
	$('.close').click(function(){
		$('.main-search').hide();
    });
    
    function system_auto_load(str_class,paramas)
    {
        var b = new Base64();
		if(paramas.length > 0){
			temp_paramas = b.decode(paramas);
			temp_paramas =  eval("("+temp_paramas+")");
        	$('#'+str_class+'-form').find("input").each(function(){
        		var i_type = $(this).attr('type');
            	var i_name = $(this).attr('name');
            	if(i_type == 'checkbox')
                {
                    i_name = i_name.replace("[]","");
                }
                if(i_type !== undefined && i_name !== undefined)
                {
                    if(i_type.length > 0 && i_name.length > 0)
                    {
                        var temp_data = temp_paramas[i_name];
                        if(temp_data !== undefined && temp_data !== null)
                        {
                            if(i_type == 'text' || i_type=='password' || i_type == 'hidden')
                            {
                                $(this).val(temp_data);
                            }
                            else if(i_type == 'checkbox')
                            {
                                if(temp_data.indexOf($(this).val()) != -1)
                                {
                                    $(this).attr("checked", true);
                                }
                                else
                                {
                                    $(this).attr("checked", false);
                                }
                            }
                            else if(i_type == 'radio')
                            {
                                if(temp_data == $(this).val())
                                {
                                    $(this).attr("checked", true);
                                }
                                else
                                {
                                    $(this).attr("checked", false);
                                }
                            }
                            else
                            {
                                $(this).val('');
                            }
                        }
                        else
                        {
                            $(this).val('');
                        }
                    }
                    else
                    {
                        $(this).val('');
                    }
                }
                else
                {
                    $(this).val('');
                }
        	});

        	$('#'+str_class+'-form').find("textarea").each(function(){
        		var i_name = $(this).attr('name');
            	if(i_name.length>0)
                {
                    var temp_data = temp_paramas[i_name];
                    if(temp_data !== undefined && temp_data !== null)
                    {
                    	$(this).html(temp_data);
                    }
                }
            	else
                {
                    $(this).html('');
                }
        	});

        	//下拉框
            $('#'+str_class+'-form').find("select").each(function(){
                var i_name = $(this).attr('name');
                if(i_name !== undefined)
                {
                    if(i_name.length>0)
                    {
                        var temp_data = temp_paramas[i_name];
                        if(temp_data !== undefined && temp_data !== null)
                        {
                            $(this).find("option").each(function () {
                                if($(this).val() == temp_data)
                                {
                                    $(this).attr("selected", true);
                                }
                                else
                                {
                                    $(this).attr("selected", false);
                                }
                            })
                        }
                    }
                    else
                    {
                        $(this).html('');
                    }
                }

            });
        	//下来复选框,定制处理---begin
            var labelText_edit = [];//初始化名称
            $('#'+str_class+'-form').find(".fs-option").each(function(){
                var i_name =  $('#'+str_class+'-form').find("#select_checkbox_button_hidden_value").attr("name");
                var temp_data = temp_paramas[i_name];//得到当前下拉框的值，多个以逗号分隔
                if(temp_data !== undefined)
                {
                    if(temp_data.indexOf($(this).attr("data-value")) != -1)
                    {
                        $(this).addClass("selected");
                        labelText_edit.push($(this).find(".fs-option-label").text());
                    }
                    else
                    {
                        $(this).removeClass("selected");
                    }
                }
            });
            if (labelText_edit.length < 1)
            {
                labelText_edit = '请选择';
            }
            else
            {
                labelText_edit = labelText_edit.join(',');
            }
            $('#'+str_class+'-form').find('.fs-label').html(labelText_edit);
            //---------下拉框定制化end
		}
		var str_function = "init_"+str_class;
        //定向参数：支付订单定制
        var order_data = $('div.pay-order-data').attr('data-order');
//        order_data = JSON.stringify({
//            'cms_user_id':1,//用户ID
//            'cms_order_price':0.01,//订单价格
//            'cms_order_type':1,//订单类型。0批量订单；1面料小样；2样板订单；3样衣订单；4稀缺面料定金订单；5充值订单
//            'cms_pay_order_code':1,//购买订单ID
//            'cms_order_parent':'1',//父级订单ID
//            //'cms_order_id':'1',//订单ID，支付未完成订单上报
//            'cms_order_price_payed':0.01//本次支付金额
//        });
        if(order_data)
        {
            order_data = b.encode(order_data);
        }
		eval(str_function+"('"+str_class+"','"+paramas+"','"+order_data+"')");
    }

</script>
