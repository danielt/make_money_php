<?php
/**
 * 前端AJAX分页界面
 */
$arr_page_index = array(
    '5',
    '10',
    '25',
    '50',
    '100',
);
?>
<div class="form-body">
    <input type="hidden" name="cms_page_num" id="cms_page_num" value="1">
    <input type="hidden" name="cms_page_size" id="cms_page_size" value="10">
    <div class="col-md-5 page_1">
        <ul class="pagination pagination-lg">
            <li class="disabled">分页条数:</li>
        </ul>
        <select name="cms_page_size" id="page_select">
            <?php foreach ($arr_page_index as $page_index){?>
                <option value="<?php echo $page_index;?>"><?php echo $page_index;?></option>
            <?php }?>
        </select>
        <ul class="pagination pagination-lg">
            <li class="disabled" id="page_info"></li>
        </ul>
    </div>
    <div class="col-md-5 page_1">
        <ul class="pagination pagination-lg" id="page_num_ul">
            <li id="last_all_page" class="disabled" style="cursor:pointer;"><a aria-label="Previous"><span aria-hidden="true">«</span></a></li>
            <li id="last_page" class="disabled" style="cursor:pointer;"><a><i class="fa fa-angle-left"></i></a></li>

            <li id="next_page" class="disabled" style="cursor:pointer;"><a><i class="fa fa-angle-right"></i></a></li>
            <li id="next_all_page" class="disabled" style="cursor:pointer;"><a aria-label="Previous"><span aria-hidden="true">»</span></a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    //往分页里面丢数据
    function push_page_info(page_num,page_size,data_count)
    {
        var li_html = '';
        //初始化
        $('#page_num_ul').find(".page_num_button").html('');
        //计算总共多少页
        page = Math.ceil(data_count/page_size);
        if(page_num !== 1)//当前页数不等于1
        {
            $('#page_num_ul > #last_all_page').removeClass("disabled");
            $('#page_num_ul > #last_page').removeClass("disabled");
        }
        else
        {
            $('#page_num_ul > #last_all_page').addClass("disabled");
            $('#page_num_ul > #last_page').addClass("disabled");
        }

        if(page_num < page)//当前页数小于总页数
        {
            $('#page_num_ul > #next_all_page').removeClass("disabled");
            $('#page_num_ul > #next_page').removeClass("disabled");
        }
        else
        {
            $('#page_num_ul > #next_all_page').addClass("disabled");
            $('#page_num_ul > #next_page').addClass("disabled");
        }
        //移除之前所有点击事件
        $("#page_num_ul > #last_all_page").prop("onclick",null).off("click");
        $("#page_num_ul > #next_all_page").prop("onclick",null).off("click");
        $("#page_num_ul > #last_page").prop("onclick",null).off("click");
        $("#page_num_ul > #next_page").prop("onclick",null).off("click");
        //增加点击事件,往前翻
        $('#page_num_ul > #last_all_page').click(function () {
            if($(this).attr("class") !== 'disabled')
            {
                p_select = $("#page_select").val();//获取大页翻页的页数
                //计算上一大页
                if((page_num - p_select) <= 0)//当前页减去最大翻页数小于等于0，则跳转第一页
                {
                    pageNum = 1;
                    list_refresh();
                }
                else//当前页减去最大翻页数大于0，则跳转减后的那一页
                {
                    pageNum = page_num - p_select;
                    list_refresh();
                }
            }
        })
        //增加点击事件,往后翻
        $('#page_num_ul > #next_all_page').click(function () {
            if($(this).attr("class") !== 'disabled')
            {
                p_select = $("#page_select").val();//获取大页翻页的页数
                //计算上一大页
                if((page_num + p_select) >= page)//当前页加上最大翻页数大于等于最大页数，则跳转最后一页
                {
                    pageNum = page;
                    list_refresh();
                }
                else//当前页加上最大翻页数小于最大页数，则跳转加后的那一页
                {
                    pageNum = page_num + p_select;
                    list_refresh();
                }
            }
        })
        //增加点击事件，上一页
        $('#page_num_ul > #last_page').click(function () {
            if($(this).attr("class") !== 'disabled')
            {
                //计算上一大页
                if((page_num - 1) <= 0)//当前页减去1小于等于0，则跳转第一页
                {
                    pageNum = 1;
                    list_refresh();
                }
                else//当前页减去1大于0，则跳转减后的那一页
                {
                    pageNum = page_num - 1;
                    list_refresh();
                }
            }
        })
        //增加点击事件，下一页
        $('#page_num_ul > #next_page').click(function () {
            if($(this).attr("class") !== 'disabled')
            {
                //计算上一大页
                if((page_num + 1) >= page)//当前页加上1大于等于最大页数，则跳转最后一页
                {
                    pageNum = page;
                    list_refresh();
                }
                else//当前页加上1小于最大页数，则跳转加后的那一页
                {
                    pageNum = page_num + 1;
                    list_refresh();
                }
            }
        })
        $("#page_info").html("记录条数:"+data_count+";当前第"+page_num+"页;共计"+page+"页");

        for (var i=1;i<=page;i++)
        {
            if(i === page_num)
            {
                li_html += '<li class="active page_num_button" style="cursor:pointer;"><a>'+i+'</a></li>';
            }
            else
            {
                li_html += '<li class="page_num_button" style="cursor:pointer;"><a>'+i+'</a></li>';
            }
        }
        $('#page_num_ul > #last_page').after(li_html);
    }
    $(document).ready(function () {
        //UL下的数据改变
        $('#page_num_ul').on('click', '.page_num_button', function () {
            var _this = $(this);
            _this.siblings().removeClass("active");
            _this.addClass("active");
            //刷新页数
            pageNum = _this.text();
            list_refresh();
        })
        //下拉框数据内的改变
        $("#page_select").change(function () {
            pageSize = $(this).val();
            list_refresh();
        })
    })
</script>
