<?php
/**
  * Use：自定义项目配置 + 全局配置
  * Author：kan.yang@starcor.cn
  * DateTime：18-12-16 下午2:32
  * Description：
  * ========================================================================================
*/

/*========================================== 全局变量 开始 ===================================*/

//交易成功
define('TRADE_SUCCESS', 310001);
//交易失败
define('TRADE_FAIL', 310002);
//处理中
define('TRADE_PROCESS', 310003);
//退款中
define('TRADE_REFUND', 310004);
//订单状态缓存时间
define('ORDER_STATUS_BUFF_TIME',1800);
//成功状态码
define('NF_RETURN_SUCCESS_CODE',0);
//失败状态码
define('NF_RETURN_ERROR_CODE',1);



/*========================================== 全局变量 结束 ===================================*/

//253短信配置项
$arr_config_app['short_message_config'] = array(
    'api_send_url' => 'dysmsapi.aliyuncs.com',                 //请求地址
    'api_account'  => 'LTAIzDNqjq2w6NdI',                      //Access Id
    'api_password' => 'ZqFBhPCfFcHU7pRqB1bpSMSZ5p5w95',        //Access Secret
    'api_sign_name'=> '云裳供应链',                             //短信签名
    'api_template_code' => 'SMS_152853072',                    //短信模板ID
);

//支付渠道
$arr_config_app['system_pay_channel'] = array(
    '1' => '微信支付',
    '2' => '支付宝支付',
);

//支付方式类型
$arr_config_app['system_pay_channel_mode_flag'] = array(
    '1' => '扫码支付',
    '2' => 'APP支付',
    '3' => '网页支付',
);

//支付宝签名类型
$arr_config_app['system_pay_alipay_sign_type'] = array(
    '1' => 'RSA1',
    '2' => 'RSA2',
);

//支付类型
$arr_config_app['system_pay_channel_mode_type'] = array(
    '0' => '余额支付',
    '1' => '线上支付',
    '2' => '积分支付',
);

//积分类型
$arr_config_app['system_integral_type'] = array(
    '0' => '充值积分',
    '1' => '订购积分',
    '2' => '注册积分',
);

//允许积分支付类订单 【需要和 system_integral_type 一一对应】
$arr_config_app['pay_order_to_integral'] = array(
    '0' => array(//充值积分
        //'0-100'   => '100',//充值金额范围 => 积分
        //'101-500' => '200',
    ),
    '1' => array(//订购积分
        //'0' => '500',//订单类型 => 积分
        '1' => '500',//订单类型 => 积分
    ),
    '2' => '100',//注册积分
);

//平台支付系统管理员
$arr_config_app['system_pay_manager'] = 1;

//支付订单类型
$arr_config_app['system_pay_order_type'] = array(
    '0' => '批量订单',
    '1' => '面料小样',
    '2' => '样板订单',
    '3' => '样衣订单',
    '4' => '稀缺面料定金订单'
);

//订单状态
$arr_config_app['system_pay_order_state'] = array(
    '0' => '未支付',
    '1' => '已支付',
    '2' => '已取消',
    '3' => '已过期',
    '4' => '退款中',
    '5' => '已退订',
    '6' => '订单异常'
);

//业务状态
$arr_config_app['system_pay_business_state'] = array(
    '0' => '未完成',
    '1' => '已完成'
);

//配置多少分钟无操作自动下线,
$config['no_operation_auto_unline_according_to_minute'] = 120;

//系统角色
$arr_config_app['system_role_list'] = array(
    1 => '系统管理员',
    2 => '供应商',
    3 => '订购商',
    4 => '生产商'
);

//自动报价
$arr_config_app['pay_order_quoted_price'] = array(
    //包工包料
    1 => array(
        'range' => array(
            '0-200'    => 1.5,
            '201-500'  => 1.4,
            '501-1000' => 1.3,
            '1000'     => 1.2,
        ),
        'earnest' => '0.5'
    ),
    //半包料
    2 => array(
        'range' => array(
            '0-200'    => 3,
            '201-500'  => 2.5,
            '501-1000' => 2,
            '1000'     => 1.5,
        ),
        'earnest' => '1'
    ),
    //清加工
    3 => array(
        'range' => array(
            '0-200'    => 3,
            '201-500'  => 2.5,
            '501-1000' => 2,
            '1000'     => 1.5,
        ),
        'earnest' => '0'
    ),
);

/*========================================== 系统配置 开始 ===================================*/

//添加实例：$arr_config_app[''] = '';

/*========================================== 系统配置 结束 ===================================*/

